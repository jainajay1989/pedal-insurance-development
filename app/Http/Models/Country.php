<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request;

class Country extends Model
{
    use Notifiable;
    protected $table        = 'tbl_countries';
    protected $primaryKey   = 'id';
//    const CREATED_AT        = 'CT_createdAt';
//    const UPDATED_AT        = 'CT_updatedAt';
    public $field;

    /**
     * Get city detail.
     *
     * @param $city_id
     * @return $return
     */
    public static function getAllCountries()
    {
        $countries = self::select('id', 'name')->get()->toArray();
        return $countries;
    }
    
    /**
     * Get city detail.
     *
     * @param $city_id
     * @return $return
     */
    public static function getCountryDetail($countryId = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($countryId == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Country id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($countryId);
        }

        return $return;
    }
}
