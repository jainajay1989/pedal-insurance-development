<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request, \Carbon\Carbon;

class Vehicle extends Model
{
    use Notifiable;
    protected $table        = 'tbl_vehicle';
    protected $primaryKey   = 'V_id';
    const CREATED_AT        = 'V_createdAt';
    const UPDATED_AT        = 'V_updatedAt';
    public $field;

    /**
     * Get vehicle detail.
     *
     * @param $vehicle_id
     * @return $return
     */
    public static function getVehicleDetail($vehicle_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($vehicle_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Vehicle id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($vehicle_id);
        }

        return $return;
    }

    /**
     * Get vehicle list.
     *
     * @return $return
     */
    public static function getAllVehicles()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = self::all();
        }

        return $return;
    }

    /**
     * Add company.
     *
     * @return string
     */
    public function addVehicle()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Vehicle = new Vehicle;
            $Vehicle->V_code = $this->field['V_code'];
            $Vehicle->V_make = $this->field['V_make'];
            $Vehicle->V_model = $this->field['V_model'];
            $Vehicle->V_modelcode = $this->field['V_modelcode'];
            $Vehicle->V_price = $this->field['V_price'];
            $Vehicle->V_active = $this->field['V_active'];
            $Vehicle->V_createdAt = date('Y-m-d H:i:s');
            $Vehicle->V_updatedAt = date('Y-m-d H:i:s');
            $Vehicle->V_createdFrom = Request::ip();
            $Vehicle->V_updatedFrom = Request::ip();
            if($Vehicle->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Vehicle added successfully';
                $return['data'] = $Vehicle;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Vehicle, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update vehicle detail.
     *
     * @return string
     */
    public function updateVehicleDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['V_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Vehicle id is empty';
            }
        }

        if($fn_status == true)
        {
            $Vehicle = self::find($this->field['V_id']);
            $Vehicle->V_code = $this->field['V_code'];
            $Vehicle->V_make = $this->field['V_make'];
            $Vehicle->V_model = $this->field['V_model'];
            $Vehicle->V_price = $this->field['V_price'];
            $Vehicle->V_active = $this->field['V_active'];
            $Vehicle->V_updatedFrom = Request::ip();
            if($Vehicle->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Vehicle updated successfully';
                $return['data'] = $Vehicle;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update Vehicle, please try again!';
            }
        }

        return $return;
    }

    /**
     * Delete vehicle.
     *
     * @return string
     */
    public function deleteVehicle()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['V_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 501;
                $return['message'] = 'Vehicle id is empty';
            }
        }

        if($fn_status == true)
        {
            $Vehicle = self::find($this->field['V_id']);
            if($Vehicle->delete() == true)
            {
                $return['status'] = true;
                $return['code'] = 500;
                $return['message'] = 'Vehicle deleted successfully';
                $return['data'] = $Vehicle;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 502;
                $return['message'] = 'Unable to delete Vehicle, please try again!';
            }
        }

        return $return;
    }
    
    /* Added by Ajay Jain
     * Date : 16/08/2018
     * Used to get list of models based on make
     */
    public static function getMakeModels($makeId = 0) {
        $models = self::where('V_code', $makeId)->where('V_active', 1)->select(['V_id', 'V_model', 'V_modelcode'])->orderBy('V_model', 'ASC')->groupBy('V_model')->get();
        return $models;
    }
    
    /* Added by Ajay Jain
     * Date : 16/08/2018
     * Used to get list of make
     */
    public static function getMakes()
    {
        $makes = self::selectRaw('V_code, V_make')->orderBy('V_make', 'ASC')->groupBy(['V_code', 'V_make'])->get();
        return $makes;
    }
    
        
    public static function checkModelExist($modelCode = NULL) {
        return self::where('V_modelcode', $modelCode)->where('V_active', 1)->count();
    }
    
    public static function checkMakeExist($make = NULL, $makeCode = NULL) {
        $data = self::where('V_code', $makeCode)->where('V_active', 1)->first();
        if(empty($data)) {
            return false;
        } elseif($data->V_make != strtoupper($make)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function checkMakeCodeExist($make = NULL, $makeCode = NULL) {
        $data = self::where('V_make', strtoupper($make))->where('V_active', 1)->first();
        if(empty($data)) {
            return false;
        } elseif($data->V_code != $makeCode) {
            return true;
        } else {
            return false;
        }
    }
}
