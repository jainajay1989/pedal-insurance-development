<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request;


class Page extends Model
{
    use Notifiable;
    protected $table        = 'tbl_page';
    protected $primaryKey   = 'PG_id';
    const CREATED_AT        = 'PG_createdAt';
    const UPDATED_AT        = 'PG_updatedAt';
    public $field;


    /**
     * Get page list.
     *
     * @return $return
     */
    public static function getAllPages()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::all();
        }

        return $return;
    }

    /**
     * Get page detail.
     *
     * @param $page_id
     * @return $return
     */
    public static function getPageDetail($page_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($page_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Page id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($page_id);
        }

        return $return;
    }

    /**
     * Get page content.
     *
     * @param $page_key
     * @return $return
     */
    public static function getPageContent($page_key = null)
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            if($page_key == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Page key is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = $query->select('*')
                                    ->where('PG_key', $page_key)
                                    ->first();
        }

        return $return;
    }

    /**
     * Add page.
     *
     * @return string
     */
    public function addPage()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Page = new Page;
            $Page->PG_key = $this->field['PG_key'];
            $Page->PG_title = $this->field['PG_title'];
            $Page->PG_content = $this->field['PG_content'];
            $Page->PG_active = $this->field['PG_active'];
            $Page->PG_createdFrom = Request::ip();
            $Page->PG_updatedFrom = Request::ip();
            if($Page->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Page added successfully';
                $return['data'] = $Page;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Page, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update page.
     *
     * @return string
     */
    public function updatePageDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['PG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Page id is empty';
            }
        }

        if($fn_status == true)
        {
            $Page = self::find($this->field['PG_id']);
            $Page->PG_title = $this->field['PG_title'];
            $Page->PG_content = $this->field['PG_content'];
            $Page->PG_active = $this->field['PG_active'];
            $Page->PG_updatedFrom = Request::ip();
            if($Page->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Page updated successfully';
                $return['data'] = $Page;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update Page, please try again!';
            }
        }

        return $return;
    }
}
