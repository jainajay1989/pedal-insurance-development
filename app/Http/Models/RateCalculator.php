<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request, \Carbon\Carbon;

class RateCalculator extends Model
{
    use Notifiable;
    protected $table        = 'tbl_ratecalculator';
    protected $primaryKey   = 'RC_id';
    public $field;
    protected $casts = ['RC_minAmount'=>'float', 'RC_maxAmount'=>'float', 'RC_basicCover'=>'float', 'RC_lossDamage'=>'float', 'RC_burglury'=>'float', 'RC_paCover'=>'float', 'RC_paPremium'=>'float', 'RC_plCover'=>'float', 'RC_plPremium' => 'float', 'RC_longTermDiscount' => 'float'];
    
    /**
     * Get company detail.
     *
     * @param $company_id
     * @return $return
     */
    public static function getRateCalculator($tenure = null, $purchaseAmt = 0.00, $imdCode = NULL)
    {
        $query = self::query();
        $query = $query->where('RC_tenure', $tenure)->whereRaw("$purchaseAmt Between `RC_minAmount` AND `RC_maxAmount`");
        if(!empty($imdCode)) {
            $query->where('IMDCode', $imdCode);
        }
        $rates = $query->first();
        return $rates;
    }
}
