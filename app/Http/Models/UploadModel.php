<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request, \Carbon\Carbon;

class UploadModel extends Model
{
    use Notifiable;
    protected $table        = 'tbl_uploadedmodels';
    protected $primaryKey   = 'id';
    public $timestamps = false;
    public $field;

    /**
     * Get vehicle list.
     *
     * @return $return
     */
    public static function getList()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = UploadModel::orderBy('id', 'desc')->first();
        }

        return $return;
    }
    
    public function addRow() {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $obj = new UploadModel;
            $obj->file = $this->field['file'];
            $obj->created_at = $this->field['created_at'];
            $obj->total = $this->field['total'];
            $obj->success = $this->field['success'];
            $obj->error = $this->field['error'];
            $obj->repeated = $this->field['repeated'];
            $obj->error_report = $this->field['error_report'];
            if($obj->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Record added successfully';
                $return['data'] = $obj;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Vehicle, please try again!';
            }
        }

        return $return;
    }
}
