<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request;

class CityPincode extends Model
{
    use Notifiable;
    protected $table        = 'tbl_citypincodes';
    protected $primaryKey   = 'CP_id';
    public $timestamps = false;
//    const CREATED_AT        = 'CT_createdAt';
//    const UPDATED_AT        = 'CT_updatedAt';
    public $field;
    
    /*
     * Added by Ajay Jain
     * Date : 16-08-2018
     * Used to get pincode by city id
     */
    public function addCityPincodes() {
        $pincode = new CityPincode;
        $pincode->CP_city_id = $this->field['CP_city_id'];
        $pincode->CP_pincode = $this->field['CP_pincode'];
        $pincode->CP_city_name = $this->field['CP_city_name'];
        $pincode->save();
    }

    /*
     * Added by Ajay Jain
     * Date : 16-08-2018
     * Used to get pincode by city id
     */
    public static function getCityPincodes($cityId = NULL) {
        $return = array();
        $pincodes = self::where('CP_city_id', $cityId)->groupBy('CP_pincode')->get()->toArray();
        if(!empty($pincodes)) {
            $otherObj['CP_city_id'] = 0;
            $otherObj['CP_id'] = 0;
            $otherObj['CP_pincode'] = "Other";
            $otherObj['CT_id'] = 0;
            array_push($pincodes,$otherObj);
        }
        //print_r($pincodes);exit(" Pincode");
        $return['status'] = 1;
        $return['code'] = 200;
        $return['data'] = $pincodes ;
        return $return;
    }
    
    /*
     * Added by Pravinsingh Waghela
     * Date : 26-April-2019
     * Used to get pincode if available
     */
    public static function isPinCodeAvailable($pincode) {
        $return = array();
        $pincodes = self::where('CP_pincode', $pincode)->count();
        if(!empty($pincodes)) {
            return true;
        }
        return false;
    }
}
