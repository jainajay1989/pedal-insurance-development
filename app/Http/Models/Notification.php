<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB, Request;

class Notification extends Model
{
    use Notifiable;
    protected $table        = 'tbl_notification';
    protected $primaryKey   = 'N_id';
    const CREATED_AT        = 'N_createdAt';
    const UPDATED_AT        = 'N_updatedAt';
    public $field;

    /**
     * Get notification list.
     *
     * @param $where
     * @return $return
     */
    public static function getAllNotifications($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select('*')
                                    ->where($where)
                                    ->orderBy('N_createdAt', 'asc')
                                    ->get();
            $return['message'] = null;
        }

        return $return;
    }

    /**
     * Add notification.
     *
     * @return string
     */
    public function addNotification()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Notification = new Notification;
            $Notification->N_type = $this->field['L_type'];
            $Notification->N_Lid = $this->field['L_Lid'];
            $Notification->N_LGid = $this->field['L_LGid'];
            $Notification->N_message = $this->field['L_message'];
            $Notification->N_createdFrom = Request::ip();
            $Notification->N_updatedFrom = Request::ip();
            if($Notification->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Notification added successfully';
                $return['data'] = $Notification;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Notification, please try again!';
            }
        }

        return $return;
    }

    /**
     * Delete notification.
     *
     * @param $where
     * @return string
     */
    public static function deleteNotification($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true) {

            if($query->select('*')->where($where)->delete() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Notification deleted successfully';
                $return['data'] = null;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to delete Notification, please try again!';
                $return['data'] = null;
            }

        }

        return $return;
    }

}
