<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request, \Carbon\Carbon;

class InsuranceCompany extends Model
{
    use Notifiable;
    protected $table        = 'tbl_insurancecompany';
    protected $primaryKey   = 'IC_id';
    const CREATED_AT        = 'IC_createdAt';
    const UPDATED_AT        = 'IC_updatedAt';
    public $field;

    /**
     * Get company detail.
     *
     * @param $company_id
     * @return $return
     */
    public static function getCompanyDetail($company_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($company_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Company id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($company_id);
        }

        return $return;
    }

    /**
     * Get company list.
     *
     * @return $return
     */
    public static function getAllCompanies()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = self::all();
        }

        return $return;
    }

    /**
     * Add company.
     *
     * @return string
     */
    public function addCompany()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Company = new InsuranceCompany;
            $Company->IC_name = $this->field['IC_name'];
            $Company->IC_active = $this->field['IC_active'];
            $Company->IC_createdFrom = Request::ip();
            $Company->IC_updatedFrom = Request::ip();
            if($Company->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Company added successfully';
                $return['data'] = $Company;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Company, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update company detail.
     *
     * @return string
     */
    public function updateCompanyDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['IC_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Company id is empty';
            }
        }

        if($fn_status == true)
        {
            $Company = self::find($this->field['IC_id']);
            $Company->IC_name = $this->field['IC_name'];
            $Company->IC_active = $this->field['IC_active'];
            $Company->IC_updatedFrom = Request::ip();
            if($Company->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Company updated successfully';
                $return['data'] = $Company;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update Company, please try again!';
            }
        }

        return $return;
    }

    /**
     * Delete company.
     *
     * @return string
     */
    public function deleteCompany()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['IC_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 501;
                $return['message'] = 'Company id is empty';
            }
        }

        if($fn_status == true)
        {
            $Company = self::find($this->field['IC_id']);
            if($Company->delete() == true)
            {
                $return['status'] = true;
                $return['code'] = 500;
                $return['message'] = 'Company deleted successfully';
                $return['data'] = $Company;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 502;
                $return['message'] = 'Unable to delete Company, please try again!';
            }
        }

        return $return;
    }
}
