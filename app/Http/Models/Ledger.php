<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB, Request;

class Ledger extends Model
{
    use Notifiable;
    protected $table        = 'tbl_ledger';
    protected $primaryKey   = 'LG_id';
    const CREATED_AT        = 'LG_createdAt';
    const UPDATED_AT        = 'LG_updatedAt';
    public $field;

    /**
     * Get table name
     *
     * @return $return
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    /**
     * Get transaction detail.
     *
     * @param $transaction_id
     * @return $return
     */
    public static function getTransactionDetail($transaction_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($transaction_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Transaction id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($transaction_id);
        }

        return $return;
    }

    /**
     * Get lead transaction detail.
     *
     * @param $where
     * @return $return
     */
    public static function getLeadTransactionDetail($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();
        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = $query->select('*')
                                    ->where($where)
                                    ->first();
            $return['message'] = null;
        }

        return $return;
    }

    /**
     * Get transaction list.
     *
     * @param $where
     * @return $return
     */
    public static function getAllTransactions($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=LG_AGid) AS LG_agent'))
                                    ->where($where)
                                    ->orderBy('LG_createdAt', 'desc')
                                    ->orderBy('LG_id', 'desc')
                                    ->get();
        }

        return $return;
    }

    /**
     * Get agents transaction list.
     *
     * @param $agent_ids
     * @return $return
     */
    public static function getAgentsTransactions($agent_ids = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=LG_AGid) AS LG_agent'))
                                    ->whereIn('LG_AGid', $agent_ids)
                                    ->orderBy('LG_createdAt', 'desc')
                                    ->get();
        }

        return $return;
    }

    /**
     * Get agent transaction list.
     *
     * @param $agent_id
     * @param $where
     * @return $return
     */
    public static function getAgentTransactions($agent_id = null, $where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=LG_AGid) AS LG_agent'))
                                    ->where('LG_AGid', $agent_id)
                                    ->where($where)
                                    ->orderBy('LG_createdAt', 'desc')
                                    ->orderBy('LG_id', 'desc')
                                    ->get();
        }

        return $return;
    }

    /**
     * Add transaction.
     *
     * @return string
     */
    public function addTransaction()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Ledger = new Ledger;
            $Ledger->LG_Lid = $this->field['LG_Lid'];
            $Ledger->LG_AGid = $this->field['LG_AGid'];
            $Ledger->LG_type = $this->field['LG_type'];
            $Ledger->LG_message = $this->field['LG_message'];
            $Ledger->LG_amount = $this->field['LG_amount'];
            $Ledger->LG_note = $this->field['LG_note'];
            if(@$this->field['LG_attachment']) {$Ledger->LG_attachment = $this->field['LG_attachment'];}
            $Ledger->LG_status = $this->field['LG_status'];
            $Ledger->LG_createdFrom = Request::ip();
            $Ledger->LG_updatedFrom = Request::ip();
            if($Ledger->save() == true)
            {
                // Add Notification
                if($Ledger->LG_type == config('constant.transaction_type.RECHARGE.value'))
                {
                    $Notification = new Notification;
                    $Notification->field['L_type'] = 'LEDGER';
                    $Notification->field['L_Lid'] = null;
                    $Notification->field['L_LGid'] = $Ledger->LG_id;
                    $Notification->field['L_message'] = 'New recharge pending';
                    $Notification->addNotification();
                }
                //-----------------

                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Transaction added successfully';
                $return['data'] = $Ledger;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Transaction, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update transaction detail.
     *
     * @return string
     */
    public function updateTransactionDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['LG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Transaction id is empty';
            }
        }

        if($fn_status == true)
        {
            $Ledger= self::find($this->field['LG_id']);
            if(@$this->field['LG_Lid']) {$Ledger->LG_Lid = $this->field['LG_Lid'];}
            if(@$this->field['LG_AGid']) {$Ledger->LG_AGid = $this->field['LG_AGid'];}
            if(@$this->field['LG_type']) {$Ledger->LG_type = $this->field['LG_type'];}
            if(@$this->field['LG_message']) {$Ledger->LG_message = $this->field['LG_message'];}
            if(@$this->field['LG_amount']) {$Ledger->LG_amount = $this->field['LG_amount'];}
            if(@$this->field['LG_note']) {$Ledger->LG_note = $this->field['LG_note'];}
            if(@$this->field['LG_attachment']) {$Ledger->LG_attachment = $this->field['LG_attachment'];}
            $Ledger->LG_status = $this->field['LG_status'];
            $Ledger->LG_updatedFrom = Request::ip();
            if($Ledger->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Transaction updated successfully';
                $return['data'] = $Ledger;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update Transaction, please try again!';
            }
        }

        return $return;
    }

    public static function getAgentAccountBalance($agent_id = null)
    {

        $fn_status = true;
        $return = null;
        $query = self::query();

        //$query->select(DB::raw('((SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="RECHARGE" AND LG_status=21 AND LG_AGid='.$agent_id.')+(SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="COMMISSION" AND LG_status=21 AND LG_AGid='.$agent_id.'))-((SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="PAIDFORPOLICY" AND LG_status=21 AND LG_AGid='.$agent_id.')+(SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="WITHDRAW" AND LG_status=21 AND LG_AGid='.$agent_id.')) AS LG_balance'));
        //dd($query->toSql());

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 500;
            //$return['data'] = $query->select(DB::raw('SELECT ((SELECT IFNULL(SUM(LG_amount), 0) FROM ledger WHERE LG_type="RECHARGE" AND LG_status=21 AND LG_AGid='.$agent_id.')+(SELECT IFNULL(SUM(LG_amount), 0) FROM ledger WHERE LG_type="COMMISSION" AND LG_status=21 AND LG_AGid='.$agent_id.'))-((SELECT IFNULL(SUM(LG_amount), 0) FROM ledger WHERE LG_type="PAIDFORPOLICY" AND LG_status=21 AND LG_AGid='.$agent_id.')+(SELECT IFNULL(SUM(LG_amount), 0) FROM ledger WHERE LG_type="WITHDRAW" AND LG_status=21 AND LG_AGid='.$agent_id.')) AS LG_balance FROM ledger GROUP BY LG_balance'))
            $return['data'] = $query->select(DB::raw('((SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="RECHARGE" AND LG_status=21 AND LG_AGid='.$agent_id.')+(SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="COMMISSION" AND LG_status=21 AND LG_AGid='.$agent_id.'))-((SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="PAIDFORPOLICY" AND LG_status=21 AND LG_AGid='.$agent_id.')+(SELECT IFNULL(SUM(LG_amount), 0) FROM tbl_ledger WHERE LG_type="WITHDRAW" AND LG_status=21 AND LG_AGid='.$agent_id.')) AS LG_balance'))
                                    ->groupBy('LG_balance')
                                    ->first();

        }

        return $return;

    }
}
