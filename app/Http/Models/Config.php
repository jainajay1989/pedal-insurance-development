<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use Notifiable;
    protected $table        = 'tbl_config';
    protected $primaryKey   = 'CN_name';
    protected $keyType      = 'String';
    const CREATED_AT        = 'CN_createdAt';
    const UPDATED_AT        = 'CN_updatedAt';
    public $field;

    /**
     * Get config detail.
     *
     * @param $config_name
     * @return $return
     */
    public static function getConfigDetail($config_name = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($config_name == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Config name is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($config_name);
        }

        return $return;
    }

    /**
     * Get config list.
     *
     * @return $return
     */
    public static function getAllConfigs()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = self::all();
        }

        return $return;
    }


    /**
     * Update config detail.
     *
     * @return string
     */
    public function updateConfigDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['CN_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'Config id is empty';
            }
        }

        if($fn_status == true)
        {
            $Config = self::find($this->field['CN_id']);
            $Config->CN_value = $this->field['CN_value'];
            if($Config->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Config updated successfully';
                $return['data'] = $Config;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to update Config, please try again!';
            }
        }

        return $return;
    }
}
