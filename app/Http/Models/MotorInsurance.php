<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use App\Http\Models\Lead;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class MotorInsurance extends Authenticatable
{
    use Notifiable;
    protected $table        = 'tbl_motorinsurance';
    protected $primaryKey   = 'id';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'modified_at';
    public $field;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * Add user.
     *
     * @return string
     */
    public function add()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Lead = new Lead();
            $Lead->L_AGid = $this->field['L_AGid'];
            $Lead->L_policyType = $this->field['L_policyType'];
            $Lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $Lead->L_customerLastName = $this->field['L_customerLastName'];
            $Lead->L_customerEmail = $this->field['L_customerEmail'];
            $Lead->L_customerDOB = $this->field['L_customerDOB'];
            $Lead->L_customerMobile = $this->field['L_customerMobile'];
            $Lead->L_customerPhone = $this->field['L_customerPhone'];
            $Lead->L_customerAddress = $this->field['L_customerAddress'];
            $Lead->L_customerCity = $this->field['L_customerCity'];
            $Lead->L_customerState = $this->field['L_customerState'];
            $Lead->L_customerZip = $this->field['L_customerZip'];
            $Lead->save();

            $motor = new MotorInsurance();
            $motor->lead_id = $Lead->L_id;
            $motor->vehicle_type = $this->field['vehicle_type'];
            if(@$this->field['vehicle_make']) {$motor->vehicle_make = $this->field['vehicle_make'];}
            $motor->vehicle_model = $this->field['vehicle_model'];
            $motor->vehicle_year = $this->field['vehicle_year'];
            $motor->vehicle_engine_no = $this->field['vehicle_engine_no'];
            $motor->vehicle_chassis_no = $this->field['vehicle_chassis_no'];
            $motor->vehicle_reg_no = $this->field['vehicle_reg_no'];
            $motor->vehicle_net_price = $this->field['vehicle_net_price'];
            $motor->vehicle_cgst = $this->field['vehicle_cgst'];
            $motor->vehicle_sgst = $this->field['vehicle_sgst'];
            $motor->vehicle_gross_price = $this->field['vehicle_gross_price'];

//            if($this->field['dl_photo']) {$motor->L_DLPhoto = $this->field['dl_photo'];}
            if($this->field['rc_photo']) {$motor->rc_photo = $this->field['rc_photo'];}
            if($this->field['puc_photo']) {$motor->puc_photo = $this->field['puc_photo'];}
//            if($this->field['policy_photo']) {$motor->L_policyPhoto = $this->field['policy_photo'];}
//            if(@$this->field['vehicle_invoice_photo']) {$motor->vehicle_invoice_photo = $this->field['vehicle_invoice_photo'];}
//            if(@$this->field['vehicle_chassis_photo']) {$motor->vehicle_chassis_photo = $this->field['vehicle_chassis_photo'];}
            if(!empty($this->field['vehicle_photo1'])) {$motor->vehicle_photo1 = $this->field['vehicle_photo1'];}
            if(!empty($this->field['vehicle_photo2'])) {$motor->vehicle_photo2 = $this->field['vehicle_photo2'];}
            if(!empty($this->field['vehicle_photo3'])) {$motor->vehicle_photo3 = $this->field['vehicle_photo3'];}
            if(!empty($this->field['vehicle_photo4'])) {$motor->vehicle_photo4 = $this->field['vehicle_photo4'];}

            $motor->old_policy_number = $this->field['old_policy_number'];
            $motor->old_policy_company = $this->field['old_policy_company'];
            $motor->old_policy_start_date = $this->field['old_policy_start_date'];
            $motor->old_policy_end_date = $this->field['old_policy_end_date'];
            $motor->last_year_claim = $this->field['last_year_claim'];
            if($this->field['old_policy_attachment1']) {$motor->old_policy_attachment1 = $this->field['old_policy_attachment1'];}
            if($this->field['old_policy_attachment2']) {$motor->old_policy_attachment2 = $this->field['old_policy_attachment2'];}

            $motor->new_policy_company = $this->field['new_policy_company'];
            $motor->new_policy_product = $this->field['new_policy_product'];
            $motor->new_policy_estimated_premium = $this->field['new_policy_estimated_premium'];
//            if(@$this->field['new_policy_coverage']) {$motor->new_policy_coverage = $this->field['new_policy_coverage'];}
//            if(@$this->field['new_policy_rsa']) {$motor->new_policy_rsa = $this->field['new_policy_rsa'];}
//            if(@$this->field['new_policy_lopb']) {$motor->new_policy_lopb = $this->field['new_policy_lopb'];}
//            if(@$this->field['new_policy_od_premium']) {$motor->new_policy_od_premium = $this->field['new_policy_od_premium'];}
//            if(@$this->field['new_policy_tp_premium']) {$motor->new_policy_tp_premium = $this->field['new_policy_tp_premium'];}
//            if(@$this->field['new_policy_tax_premium']) {$motor->new_policy_tax_premium = $this->field['new_policy_tax_premium'];}
//            if(@$this->field['new_policy_total_premium']) {$motor->new_policy_total_premium = $this->field['new_policy_total_premium'];}
            $motor->status = $this->field['status'];
            $motor->remark = $this->field['remark'];
            if ($motor->save() == true) {
                $Notification = new Notification;
                $Notification->field['L_type'] = 'LEAD';
                $Notification->field['L_Lid'] = $Lead->L_id;
                $Notification->field['L_message'] = 'New lead added';
                $Notification->field['L_LGid'] = null;
                $Notification->addNotification();

                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Lead added successfully';
                $return['data'] = $motor;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Lead, please try again!';
            }
        }

        return $return;
    }

    public function updateDetails()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            
            $Lead = Lead::find($this->field['L_id']);
            $Lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $Lead->L_customerLastName = $this->field['L_customerLastName'];
            $Lead->L_customerEmail = $this->field['L_customerEmail'];
            $Lead->L_customerDOB = $this->field['L_customerDOB'];
            $Lead->L_customerMobile = $this->field['L_customerMobile'];
            $Lead->L_customerPhone = $this->field['L_customerPhone'];
            $Lead->L_customerAddress = $this->field['L_customerAddress'];
            $Lead->L_customerCity = $this->field['L_customerCity'];
            $Lead->L_customerState = $this->field['L_customerState'];
            $Lead->L_customerZip = $this->field['L_customerZip'];
            $Lead->save();
            
            $motor = Self::find($this->field['id']);
            
            $motor->lead_id = $Lead->L_id;
            $motor->vehicle_type = $this->field['vehicle_type'];
            if(@$this->field['vehicle_make']) {$motor->vehicle_make = $this->field['vehicle_make'];}
            $motor->vehicle_model = $this->field['vehicle_model'];
            $motor->vehicle_year = $this->field['vehicle_year'];
            $motor->vehicle_engine_no = $this->field['vehicle_engine_no'];
            $motor->vehicle_chassis_no = $this->field['vehicle_chassis_no'];
            $motor->vehicle_reg_no = $this->field['vehicle_reg_no'];
            $motor->vehicle_net_price = $this->field['vehicle_net_price'];
            $motor->vehicle_cgst = $this->field['vehicle_cgst'];
            $motor->vehicle_sgst = $this->field['vehicle_sgst'];
            $motor->vehicle_gross_price = $this->field['vehicle_gross_price'];
            if(!empty($this->field['rc_photo'])) {$motor->rc_photo = $this->field['rc_photo'];}
            if(!empty($this->field['puc_photo'])) {$motor->puc_photo = $this->field['puc_photo'];}
            if(!empty($this->field['vehicle_photo1'])) {$motor->vehicle_photo1 = $this->field['vehicle_photo1'];}
            if(!empty($this->field['vehicle_photo2'])) {$motor->vehicle_photo2 = $this->field['vehicle_photo2'];}
            if(!empty($this->field['vehicle_photo3'])) {$motor->vehicle_photo3 = $this->field['vehicle_photo3'];}
            if(!empty($this->field['vehicle_photo4'])) {$motor->vehicle_photo4 = $this->field['vehicle_photo4'];}
            $motor->old_policy_number = $this->field['old_policy_number'];
            $motor->old_policy_company = $this->field['old_policy_company'];
            $motor->old_policy_start_date = $this->field['old_policy_start_date'];
            $motor->old_policy_end_date = $this->field['old_policy_end_date'];
            $motor->last_year_claim = $this->field['last_year_claim'];
            if(!empty($this->field['old_policy_attachment1'])) {$motor->old_policy_attachment1 = $this->field['old_policy_attachment1'];}
            if(!empty($this->field['old_policy_attachment2'])) {$motor->old_policy_attachment2 = $this->field['old_policy_attachment2'];}
            
            $motor->new_policy_company = $this->field['new_policy_company'];
            $motor->new_policy_product = $this->field['new_policy_product'];
            $motor->new_policy_estimated_premium = $this->field['new_policy_estimated_premium'];
            if(@$this->field['new_policy_od_premium']) {$motor->new_policy_od_premium = $this->field['new_policy_od_premium'];}
            if(@$this->field['new_policy_tp_premium']) {$motor->new_policy_tp_premium = $this->field['new_policy_tp_premium'];}
            if(@$this->field['new_policy_tax_premium']) {$motor->new_policy_tax_premium = $this->field['new_policy_tax_premium'];}
            if(@$this->field['new_policy_total_premium']) {$motor->new_policy_total_premium = $this->field['new_policy_total_premium'];}
            if(@$this->field['new_policy_number']) {$motor->new_policy_number = $this->field['new_policy_number'];}
            if(@$this->field['new_policy_start_date']) {$motor->new_policy_start_date = $this->field['new_policy_start_date'];}
            if(@$this->field['new_policy_end_date']) {$motor->new_policy_end_date = $this->field['new_policy_end_date'];}
            if(@$this->field['quotations']) {
                $oldQuotes = (!empty($motor->quotations)?json_decode($motor->quotations, true):array());
                $newQuotes = array_merge($oldQuotes, $this->field['quotations']);
                $motor->quotations = (!empty($newQuotes)?json_encode($newQuotes):array());
            }
            if(@$this->field['new_policy_attachment']) {$motor->new_policy_attachment = $this->field['new_policy_attachment'];}
            $motor->paid_by_whom = $this->field['paid_by_whom'];
            $motor->status = $this->field['status'];
            if (!empty($this->field['customer_selected_quote'])) { $motor->customer_selected_quote = $this->field['customer_selected_quote']; }
            if (!empty($this->field['customer_bank_cheque'])) { $motor->customer_bank_cheque = $this->field['customer_bank_cheque']; }
            if (!empty($this->field['remark'])) { $motor->remark = $this->field['remark']; }
                
            if($motor->save() == true)
            {
                $Notification = new Notification;
                $Notification->field['L_type'] = 'LEAD';
                $Notification->field['L_Lid'] = $Lead->L_id;
                $Notification->field['L_message'] = 'New lead added';
                $Notification->field['L_LGid'] = null;
                $Notification->addNotification();

                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Lead added successfully';
                $return['data'] = $motor;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Lead, please try again!';
            }
        }

        return $return;
    }
}
