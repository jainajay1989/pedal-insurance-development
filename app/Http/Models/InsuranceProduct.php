<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request;

class InsuranceProduct extends Model
{
    use Notifiable;
    protected $table        = 'tbl_insuranceproduct';
    protected $primaryKey   = 'IP_id';
    const CREATED_AT        = 'IP_createdAt';
    const UPDATED_AT        = 'IP_updatedAt';
    public $field;

    /**
     * Get product detail.
     *
     * @param $product_id
     * @return $return
     */
    public static function getProductDetail($product_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($product_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Product id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($product_id);
        }

        return $return;
    }

    /**
     * Get product list.
     *
     * @param $where
     * @return $return
     */
    public static function getAllProducts($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select('*')
                                    ->where($where)
                                    ->get();
        }

        return $return;
    }
    
    public static function getInsuranceProducts($policyType = NULL)
    {
        $return = self::select('*')
                ->where('IP_policyType', $policyType)
                ->get()->toArray();
        return $return;
    }

    /**
     * Add product.
     *
     * @return string
     */
    public function addProduct()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Product = new InsuranceProduct;
            $Product->IP_policyType = $this->field['IP_policyType'];
            $Product->IP_name = $this->field['IP_name'];
            $Product->IP_active = $this->field['IP_active'];
            $Product->IP_createdFrom = Request::ip();
            $Product->IP_updatedFrom = Request::ip();
            if($Product->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Product added successfully';
                $return['data'] = $Product;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Product, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update product detail.
     *
     * @return string
     */
    public function updateProductDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['IP_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'Product id is empty';
            }
        }

        if($fn_status == true)
        {
            $Product = self::find($this->field['IP_id']);
            $Product->IP_policyType = $this->field['IP_policyType'];
            $Product->IP_name = $this->field['IP_name'];
            $Product->IP_active = $this->field['IP_active'];
            $Product->IP_updatedFrom = Request::ip();
            if($Product->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Product updated successfully';
                $return['data'] = $Product;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to update Company, please try again!';
            }
        }

        return $return;
    }

    /**
     * Delete product.
     *
     * @return string
     */
    public function deleteProduct()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['IP_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Product id is empty';
            }
        }

        if($fn_status == true)
        {
            $Product = self::find($this->field['IP_id']);
            if($Product->delete() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Product deleted successfully';
                $return['data'] = $Product;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to delete product, please try again!';
            }
        }

        return $return;
    }
}
