<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class OtherInsurance extends Authenticatable
{
    use Notifiable;
    protected $table        = 'tbl_otherinsurance';
    protected $primaryKey   = 'id';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'modified_at';
    public $field;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * Add user.
     *
     * @return string
     */
    public function add()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            // Used to save lead details
            $lead = new Lead;
            $lead->L_policyType = $this->field['L_policyType'];
            $lead->L_AGid = $this->field['L_AGid'];
            $lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $lead->L_customerLastName = $this->field['L_customerLastName'];
            $lead->L_customerEmail = $this->field['L_customerEmail'];
            $lead->L_customerDOB = $this->field['L_customerDOB'];
            $lead->L_customerMobile = $this->field['L_customerMobile'];
            $lead->L_customerPhone = $this->field['L_customerPhone'];
            $lead->L_customerAddress = $this->field['L_customerAddress'];
            $lead->L_customerCity = $this->field['L_customerCity'];
            $lead->L_customerState = $this->field['L_customerState'];
            $lead->L_customerZip = $this->field['L_customerZip'];
            $lead->save();
            
            // Used to save health insurance details
            $other = new OtherInsurance;
            $other->lead_id = $lead->L_id;
            $other->proposer_type = $this->field['proposer_type'];
            $other->new_policy_product = $this->field['new_policy_product'];
            $other->sum_insured = $this->field['sum_insured'];
            $other->new_policy_company = $this->field['new_policy_company'];
            $other->estimated_premium = $this->field['estimated_premium'];
            $other->status = $this->field['status'];
            if($other->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'Lead created successfully';
                $return['data'] = $other;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to create lead, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update user detail.
     *
     * @return string
     */
    public function updateDetails()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            // Used to save lead details
            $lead = Lead::find($this->field['L_id']);
            $lead->L_id = $this->field['L_id'];
            $lead->L_policyType = $this->field['L_policyType'];
            $lead->L_AGid = $this->field['L_AGid'];
            $lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $lead->L_customerLastName = $this->field['L_customerLastName'];
            $lead->L_customerEmail = $this->field['L_customerEmail'];
            $lead->L_customerDOB = $this->field['L_customerDOB'];
            $lead->L_customerMobile = $this->field['L_customerMobile'];
            $lead->L_customerPhone = $this->field['L_customerPhone'];
            $lead->L_customerAddress = $this->field['L_customerAddress'];
            $lead->L_customerCity = $this->field['L_customerCity'];
            $lead->L_customerState = $this->field['L_customerState'];
            $lead->L_customerZip = $this->field['L_customerZip'];
            $lead->save();
            
            // Used to save health insurance details
            $other = self::find($this->field['id']);
            $other->id = $this->field['id'];
            $other->lead_id = $lead->L_id;
            $other->proposer_type = $this->field['proposer_type'];
            $other->new_policy_product = $this->field['new_policy_product'];
            $other->sum_insured = $this->field['sum_insured'];
            $other->new_policy_company = $this->field['new_policy_company'];
            $other->estimated_premium = $this->field['estimated_premium'];
            $other->net_premium = $this->field['net_premium'];
            $other->gst = $this->field['gst'];
            $other->gross_premium = $this->field['gross_premium'];
            $other->paid_by_whom = $this->field['paid_by_whom'];
            if(@$this->field['new_policy_number']) {$other->new_policy_number = $this->field['new_policy_number'];}
            if(@$this->field['new_policy_start_date']) {$other->new_policy_start_date = $this->field['new_policy_start_date'];}
            if(@$this->field['new_policy_end_date']) {$other->new_policy_end_date = $this->field['new_policy_end_date'];}
            if(!empty($this->field['policy_attachment'])) {
                $other->policy_attachment = $this->field['policy_attachment'];
            }
            $other->status = $this->field['status'];
            if($other->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'Lead created successfully';
                $return['data'] = $other;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to create lead, please try again!';
            }
        }

        return $return;
    }
}
