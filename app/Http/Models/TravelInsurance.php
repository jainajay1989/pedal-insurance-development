<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class TravelInsurance extends Authenticatable
{
    use Notifiable;
    protected $table        = 'tbl_travelinsurance';
    protected $primaryKey   = 'id';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'modified_at';
    public $field;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * Add user.
     *
     * @return string
     */
    public function add()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            // Used to save lead details
            $lead = new Lead;
            $lead->L_policyType = $this->field['L_policyType'];
            $lead->L_AGid = $this->field['L_AGid'];
            $lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $lead->L_customerLastName = $this->field['L_customerLastName'];
            $lead->L_customerEmail = $this->field['L_customerEmail'];
            $lead->L_customerDOB = $this->field['L_customerDOB'];
            $lead->L_customerMobile = $this->field['L_customerMobile'];
            $lead->L_customerPhone = $this->field['L_customerPhone'];
            $lead->L_customerAddress = $this->field['L_customerAddress'];
            $lead->L_customerCity = $this->field['L_customerCity'];
            $lead->L_customerState = $this->field['L_customerState'];
            $lead->L_customerZip = $this->field['L_customerZip'];
            $lead->save();
            
            // Used to save health insurance details
            $travel = new TravelInsurance();
            $travel->lead_id = $lead->L_id;
            $travel->policy_for = $this->field['policy_for'];
            $travel->dest_country = $this->field['dest_country'];
            $travel->for_whom = $this->field['for_whom'];
            $travel->policy_holder_details = $this->field['policy_holder_details'];
            $travel->annual_trip = $this->field['annual_trip'];
            $travel->longest_trip_duration = $this->field['longest_trip_duration'];
            $travel->annual_trip_start = $this->field['annual_trip_start'];
            $travel->annual_trip_end = $this->field['annual_trip_end'];
            $travel->annual_trip_days = $this->field['annual_trip_days'];
            $travel->communication_state = $this->field['communication_state'];
            $travel->sum_insured = $this->field['sum_insured'];
            $travel->new_policy_company = $this->field['new_policy_company'];
            $travel->new_policy_product = $this->field['new_policy_product'];
            $travel->estimated_premium = $this->field['estimated_premium'];
            $travel->departed_from_india = $this->field['departed_from_india'];
            $travel->status = $this->field['status'];
            if($travel->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'Lead created successfully';
                $return['data'] = $travel;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to create lead, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update user detail.
     *
     * @return string
     */
    public function updateDetails()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            // Used to save lead details
            $lead = Lead::find($this->field['L_id']);
            $lead->L_policyType = $this->field['L_policyType'];
            $lead->L_AGid = $this->field['L_AGid'];
            $lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $lead->L_customerLastName = $this->field['L_customerLastName'];
            $lead->L_customerEmail = $this->field['L_customerEmail'];
            $lead->L_customerDOB = $this->field['L_customerDOB'];
            $lead->L_customerMobile = $this->field['L_customerMobile'];
            $lead->L_customerPhone = $this->field['L_customerPhone'];
            $lead->L_customerAddress = $this->field['L_customerAddress'];
            $lead->L_customerCity = $this->field['L_customerCity'];
            $lead->L_customerState = $this->field['L_customerState'];
            $lead->L_customerZip = $this->field['L_customerZip'];
            $lead->save();

            // Used to save health insurance details
            $travel = Self::find($this->field['id']);
            $travel->id = $this->field['id'];
            $travel->lead_id = $lead->L_id;
            $travel->policy_for = $this->field['policy_for'];
            $travel->dest_country = $this->field['dest_country'];
            $travel->for_whom = $this->field['for_whom'];
            $travel->policy_holder_details = $this->field['policy_holder_details'];
            $travel->annual_trip = $this->field['annual_trip'];
            $travel->longest_trip_duration = $this->field['longest_trip_duration'];
            $travel->annual_trip_start = $this->field['annual_trip_start'];
            $travel->annual_trip_end = $this->field['annual_trip_end'];
            $travel->annual_trip_days = $this->field['annual_trip_days'];
            $travel->communication_state = $this->field['communication_state'];
            $travel->sum_insured = $this->field['sum_insured'];
            $travel->new_policy_company = $this->field['new_policy_company'];
            $travel->new_policy_product = $this->field['new_policy_product'];
            $travel->estimated_premium = $this->field['estimated_premium'];
            $travel->net_premium = $this->field['net_premium'];
            $travel->gst = $this->field['gst'];
            $travel->gross_premium = $this->field['gross_premium'];
            $travel->paid_by_whom = $this->field['paid_by_whom'];
            if(@$this->field['new_policy_number']) {$travel->new_policy_number = $this->field['new_policy_number'];}
            if(@$this->field['new_policy_start_date']) {$travel->new_policy_start_date = $this->field['new_policy_start_date'];}
            if(@$this->field['new_policy_end_date']) {$travel->new_policy_end_date = $this->field['new_policy_end_date'];}
            if(!empty($this->field['policy_attachment'])) {
                $travel->policy_attachment = $this->field['policy_attachment'];
            }
            $travel->status = $this->field['status'];
            if($travel->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'Lead created successfully';
                $return['data'] = $travel;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to create lead, please try again!';
            }
        }

        return $return;
    }
}
