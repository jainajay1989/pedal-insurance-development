<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request;

class State extends Model
{
    use Notifiable;
    protected $table        = 'tbl_state';
    protected $primaryKey   = 'ST_id';
    const CREATED_AT        = 'ST_createdAt';
    const UPDATED_AT        = 'ST_updatedAt';
    public $field;

    /**
     * Get state detail.
     *
     * @param $state_id
     * @return $return
     */
    public static function getStateDetail($state_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($state_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'State id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($state_id);
        }

        return $return;
    }

    /**
     * Get state list.
     *
     * @return $return
     */
    public static function getAllStates()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = self::orderBy('ST_name', 'ASC')->get();
            //self::all()
        }

        return $return;
    }

    /**
     * Add state.
     *
     * @return string
     */
    public function addState()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $State = new State;
            $State->ST_code = $this->field['ST_code'];
            $State->ST_name = $this->field['ST_name'];
            $State->ST_createdFrom = Request::ip();
            $State->ST_updatedFrom = Request::ip();
            if($State->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'State added successfully';
                $return['data'] = $State;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add State, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update state detail.
     *
     * @return string
     */
    public function updateStateDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['ST_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'State id is empty';
            }
        }

        if($fn_status == true)
        {
            $State = self::find($this->field['ST_id']);
            $State->ST_code = $this->field['ST_code'];
            $State->ST_name = $this->field['ST_name'];
            $State->ST_updatedFrom = Request::ip();
            if($State->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'State updated successfully';
                $return['data'] = $State;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update State, please try again!';
            }
        }

        return $return;
    }
    
    public static function getStateByCode($state_code = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($state_code == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'State id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::where('ST_code', $state_code)->first();
        }

        return $return;
    }
    
    public static function getState($conditions = array(), $fields = '*') {
        return self::select($fields)->where($conditions)->first();
    }
    
    public static function getStateByCountry($countryId = 0) {
        return self::select('ST_id', 'ST_name', 'ST_code')->where('ST_countryid', $countryId)->get();
    }
}
