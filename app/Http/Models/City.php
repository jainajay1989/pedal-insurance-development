<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request;

class City extends Model
{
    use Notifiable;
    protected $table        = 'tbl_city';
    protected $primaryKey   = 'CT_id';
    const CREATED_AT        = 'CT_createdAt';
    const UPDATED_AT        = 'CT_updatedAt';
    public $field;

    /**
     * Get city detail.
     *
     * @param $city_id
     * @return $return
     */
    public static function getCityDetail($city_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($city_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'City id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($city_id);
        }

        return $return;
    }

    /**
     * Get city list.
     *
     * @return $return
     */
    public static function getAllCities()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = self::all();
        }

        return $return;
    }

    /**
     * Add city.
     *
     * @return string
     */
    public function addCity()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $City = new City;
            $City->CT_state = $this->field['CT_state'];
            $City->CT_name = $this->field['CT_name'];
            $City->CT_createdFrom = Request::ip();
            $City->CT_updatedFrom = Request::ip();
            if($City->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'City added successfully';
                $return['data'] = $City;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add City, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update city detail.
     *
     * @return string
     */
    public function updateCityDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['CT_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'City id is empty';
            }
        }

        if($fn_status == true)
        {
            $City = self::find($this->field['CT_id']);
            $City->CT_state = $this->field['CT_state'];
            $City->CT_name = $this->field['CT_name'];
            $City->CT_updatedFrom = Request::ip();
            if($City->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'City updated successfully';
                $return['data'] = $City;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to update City, please try again!';
            }
        }

        return $return;
    }
    
    /*
     * Added by Ajay Jain
     * Date : 16-08-2018
     * Used to get cities by state code
     */
    public static function getCityByState($stateCode = NULL) {
        $return = array();
        $cities = self::where('CT_state', $stateCode)->orderBy('CT_name', 'ASC')->get();
        $return['status'] = 1;
        $return['code'] = 200;
        $return['data'] = $cities ;
        return $return;
    }
}
