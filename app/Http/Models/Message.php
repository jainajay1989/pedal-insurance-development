<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB, Request;

class Message extends Model
{
    use Notifiable;
    protected $table        = 'tbl_message';
    protected $primaryKey   = 'MG_id';
    const CREATED_AT        = 'MG_createdAt';
    const UPDATED_AT        = 'MG_updatedAt';
    public $field;

    /**
     * Get message detail.
     *
     * @param $message_id
     * @return $return
     */
    public static function getMessageDetail($message_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($message_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Message id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($message_id);
        }

        return $return;
    }

    /**
     * Get message list.
     *
     * @param $where
     * @return $return
     */
    public static function getAllMessages($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=MG_from) AS MG_agent'))
                                    ->where($where)
                                    ->orderBy('MG_createdAt', 'asc')
                                    ->get();
            $return['message'] = null;
        }

        return $return;
    }

    /**
     * Get agent message list.
     *
     * @param $agent_id
     * @return $return
     */
    public static function getAgentMessages($agent_id = null)
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select('*')
                                    ->where('MG_to', $agent_id)
                                    ->orWhere('MG_from', $agent_id)
                                    ->orderBy('MG_createdAt', 'desc')
                                    ->get();
        }

        return $return;
    }

    /**
     * Get unread message list.
     *
     * @return $return
     */
    public static function getUnreadMessages()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = DB::table('tbl_message')
                                ->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=MG_from) AS MG_agent'))
                                ->where([['MG_read', '<>', 1], ['MG_type', '=', config('constant.message_type.ATB.value')]])
                                ->get();

        }

        return $return;
    }

    /**
     * Add message.
     *
     * @return string
     */
    public function addMessage()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Message = new Message;
            $Message->MG_Lid = $this->field['MG_Lid'];
            $Message->MG_type = $this->field['MG_type'];
            $Message->MG_from = $this->field['MG_from'];
            $Message->MG_to = $this->field['MG_to'];
            $Message->MG_subject = $this->field['MG_subject'];
            $Message->MG_message = $this->field['MG_message'];
            $Message->MG_read = $this->field['MG_read'];
            $Message->MG_createdFrom = Request::ip();
            $Message->MG_updatedFrom = Request::ip();
            if($Message->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Message sent successfully';
                $return['data'] = $Message;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to sent Message, please try again!';
            }
        }

        return $return;
    }

    /**
     * Read message.
     *
     * @return string
     */
    public function readMessage()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['MG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'Message id is empty';
            }
        }

        if($fn_status == true)
        {
            $Message = self::find($this->field['MG_id']);
            $Message->MG_read = 1;
            if($Message->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Message read successfully';
                $return['data'] = $Message;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to read Message, please try again!';
            }
        }

        return $return;
    }

    /**
     * Unread message.
     *
     * @return string
     */
    public function unreadMessage()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['MG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Message id is empty';
            }
        }

        if($fn_status == true)
        {
            $Message = self::find($this->field['MG_id']);
            $Message->MG_read = 0;
            if($Message->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Message unread successfully';
                $return['data'] = $Message;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to unread Message, please try again!';
            }
        }

        return $return;
    }


}
