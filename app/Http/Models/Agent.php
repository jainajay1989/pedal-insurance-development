<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Request, DB;

class Agent extends Authenticatable
{
    use Notifiable;
    protected $table        = 'tbl_agent';
    protected $primaryKey   = 'AG_id';
    const CREATED_AT        = 'AG_createdAt';
    const UPDATED_AT        = 'AG_updatedAt';
    public $field;
    
    public function rm() {
        return $this->belongsTo('App\Http\Models\Agent', 'AG_AGid', 'AG_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'AG_firstName', 'AG_username', 'AG_password'
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->attributes['AG_password'];
    }

    /**
     * Get agent detail.
     *
     * @param $agent_id
     * @return $return
     */
    public static function getAgentDetail($agent_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($agent_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Agent id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($agent_id);
        }

        return $return;
    }

    /**
     * Get agent list.
     *
     * @param $where
     * @return $return
     */
    public static function getAllAgents($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(A_firstName, " ", A_lastName) FROM tbl_admin WHERE A_id=AG_Aid) AS AG_manager'))
                                    ->where($where)
                                    ->get();
        }

        return $return;
    }


    /**
     * Add agent.
     *
     * @return string
     */
    public function addAgent()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Agent = new Agent;
            if(@$this->field['AG_Aid']) {$Agent->AG_Aid = $this->field['AG_Aid'];}
            if(@$this->field['AG_AGid']) {$Agent->AG_AGid = $this->field['AG_AGid'];}
            $Agent->AG_type = $this->field['AG_type'];
//            $Agent->AG_code = $this->field['AG_code'];
            if(@$this->field['AG_ImdCode']) {$Agent->AG_code = $this->field['AG_ImdCode'];}
            $Agent->AG_policyType = $this->field['AG_policyType'];//implode(',', $this->field['AG_policyType']);
            $Agent->AG_firstName = $this->field['AG_firstName'];
            $Agent->AG_lastName = $this->field['AG_lastName'];
            $Agent->AG_email = $this->field['AG_email'];
            $Agent->AG_username = $this->field['AG_username'];
            $Agent->AG_password = password_hash($this->field['AG_password'], PASSWORD_BCRYPT);
            $Agent->AG_mobile = $this->field['AG_mobile'];
            $Agent->AG_phone = $this->field['AG_phone'];
            $Agent->AG_address = $this->field['AG_address'];
            $Agent->AG_city = $this->field['AG_city'];
            $Agent->AG_state = $this->field['AG_state'];
            $Agent->AG_zip = $this->field['AG_zip'];
            if(@$this->field['AG_ImdCode']) {$Agent->AG_ImdCode = $this->field['AG_ImdCode'];}
            if(@$this->field['AG_ImdName']) {$Agent->AG_ImdName = $this->field['AG_ImdName'];}
            if(@$this->field['AG_companyName']) {$Agent->AG_companyName = $this->field['AG_companyName'];}
            if(@$this->field['AG_pan']) {$Agent->AG_pan = $this->field['AG_pan'];}
            if(@$this->field['AG_paymentModeType']) {$Agent->AG_paymentModeType = $this->field['AG_paymentModeType'];}
            if(@$this->field['AG_accountType']) {$Agent->AG_accountType = $this->field['AG_accountType'];}
            if(@$this->field['AG_bankName']) {$Agent->AG_bankName = $this->field['AG_bankName'];}
            if(@$this->field['AG_branchName']) {$Agent->AG_branchName = $this->field['AG_branchName'];}
            if(@$this->field['AG_bankAddress']) {$Agent->AG_bankAddress = $this->field['AG_bankAddress'];}
            if(@$this->field['AG_bankAccountNumber']) {$Agent->AG_bankAccountNumber = $this->field['AG_bankAccountNumber'];}
            if(@$this->field['AG_bankIfscCode']) {$Agent->AG_bankIfscCode = $this->field['AG_bankIfscCode'];}
            if(@$this->field['AG_MicrCode']) {$Agent->AG_MicrCode = $this->field['AG_MicrCode'];}
//            $Agent->AG_ImdCode = $this->field['AG_ImdCode'];
//            $Agent->AG_ImdName = $this->field['AG_ImdName'];
//            $Agent->AG_companyName = $this->field['AG_companyName'];
//            $Agent->AG_pan = $this->field['AG_pan'];
//            $Agent->AG_paymentModeType = $this->field['AG_paymentModeType'];
//            $Agent->AG_accountType = $this->field['AG_accountType'];
//            $Agent->AG_bankName = $this->field['AG_bankName'];
//            $Agent->AG_branchName = $this->field['AG_branchName'];
//            $Agent->AG_bankAddress = $this->field['AG_bankAddress'];
//            $Agent->AG_bankAccountNumber = $this->field['AG_bankAccountNumber'];
//            $Agent->AG_bankIfscCode = $this->field['AG_bankIfscCode'];
//            $Agent->AG_MicrCode = $this->field['AG_MicrCode'];
            $Agent->AG_active = $this->field['AG_active'];
            $Agent->AG_createdFrom = Request::ip();
            $Agent->AG_updatedFrom = Request::ip();
            if($Agent->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Agent added successfully';
                $return['data'] = $Agent;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Agent, please try again!';
            }
        }
        return $return;
    }

    /**
     * Update agent detail.
     *
     * @return string
     */
    public function updateAgentDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['AG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'Agent id is empty';
            }
        }

        if($fn_status == true)
        {
            $Agent = self::find($this->field['AG_id']);
            if(@$this->field['AG_Aid'])
            {
            	$Agent->AG_Aid = $this->field['AG_Aid'];
            }
            if(@$this->field['AG_AGid'])
            {
                $Agent->AG_AGid = $this->field['AG_AGid'];
            }
            $Agent->AG_policyType = $this->field['AG_policyType'];//implode(',', $this->field['AG_policyType']);
            $Agent->AG_firstName = $this->field['AG_firstName'];
            $Agent->AG_lastName = $this->field['AG_lastName'];
            $Agent->AG_email = $this->field['AG_email'];
            if(@$this->field['AG_username'])
            {
                $Agent->AG_username = $this->field['AG_username'];
            }
            $Agent->AG_mobile = $this->field['AG_mobile'];
            $Agent->AG_phone = $this->field['AG_phone'];
            $Agent->AG_address = $this->field['AG_address'];
            $Agent->AG_city = $this->field['AG_city'];
            $Agent->AG_state = $this->field['AG_state'];
            $Agent->AG_zip = $this->field['AG_zip'];
            
//            $Agent->AG_ImdCode = $this->field['AG_ImdCode'];
//            $Agent->AG_ImdName = $this->field['AG_ImdName'];
//            $Agent->AG_companyName = $this->field['AG_companyName'];
//            $Agent->AG_pan = $this->field['AG_pan'];
//            $Agent->AG_paymentModeType = $this->field['AG_paymentModeType'];
//            $Agent->AG_accountType = $this->field['AG_accountType'];
//            $Agent->AG_bankName = $this->field['AG_bankName'];
//            $Agent->AG_branchName = $this->field['AG_branchName'];
//            $Agent->AG_bankAddress = $this->field['AG_bankAddress'];
//            $Agent->AG_bankAccountNumber = $this->field['AG_bankAccountNumber'];
//            $Agent->AG_bankIfscCode = $this->field['AG_bankIfscCode'];
//            $Agent->AG_MicrCode = $this->field['AG_MicrCode'];
            if(@$this->field['AG_ImdCode']) {$Agent->AG_code = $this->field['AG_ImdCode'];}
            if(@$this->field['AG_ImdCode']) {$Agent->AG_ImdCode = $this->field['AG_ImdCode'];}
            if(@$this->field['AG_ImdName']) {$Agent->AG_ImdName = $this->field['AG_ImdName'];}
            if(@$this->field['AG_companyName']) {$Agent->AG_companyName = $this->field['AG_companyName'];}
            if(@$this->field['AG_pan']) {$Agent->AG_pan = $this->field['AG_pan'];}
            if(@$this->field['AG_paymentModeType']) {$Agent->AG_paymentModeType = $this->field['AG_paymentModeType'];}
            if(@$this->field['AG_accountType']) {$Agent->AG_accountType = $this->field['AG_accountType'];}
            if(@$this->field['AG_bankName']) {$Agent->AG_bankName = $this->field['AG_bankName'];}
            if(@$this->field['AG_branchName']) {$Agent->AG_branchName = $this->field['AG_branchName'];}
            if(@$this->field['AG_bankAddress']) {$Agent->AG_bankAddress = $this->field['AG_bankAddress'];}
            if(@$this->field['AG_bankAccountNumber']) {$Agent->AG_bankAccountNumber = $this->field['AG_bankAccountNumber'];}
            if(@$this->field['AG_bankIfscCode']) {$Agent->AG_bankIfscCode = $this->field['AG_bankIfscCode'];}
            if(@$this->field['AG_MicrCode']) {$Agent->AG_MicrCode = $this->field['AG_MicrCode'];}
            
            $Agent->AG_updatedFrom = Request::ip();
            if(@$this->field['AG_password'])
            {
                $Agent->AG_password = password_hash($this->field['AG_password'], PASSWORD_BCRYPT);
            }
            if(@$this->field['AG_active'])
            {
                $Agent->AG_active = $this->field['AG_active'];
            }
            if($Agent->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Agent updated successfully';
                $return['data'] = $Agent;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to update Agent, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update agent password.
     *
     * @return string
     */
    public function updateAgentPassword()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['AG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Agent id is empty';
            }
        }

        if($fn_status == true)
        {
            $Agent = self::find($this->field['AG_id']);
            $Agent->AG_password = password_hash($this->field['AG_password'], PASSWORD_BCRYPT);
            $Agent->AG_updatedFrom = Request::ip();
            if($Agent->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Password updated successfully';
                $return['data'] = $Agent;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update Password, please try again!';
            }
        }

        return $return;
    }

    /**
     * Delete agent.
     *
     * @return string
     */
    public function deleteAgent()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['AG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 501;
                $return['message'] = 'Agent id is empty';
            }
        }

        if($fn_status == true)
        {
            $Agent = self::find($this->field['AG_id']);
            if($Agent->delete() == true)
            {
                $return['status'] = true;
                $return['code'] = 500;
                $return['message'] = 'Agent deleted successfully';
                $return['data'] = $Agent;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 502;
                $return['message'] = 'Unable to delete Agent, please try again!';
            }
        }

        return $return;
    }


    /**
     * Update device id.
     *
     * @return string
     */
    public function updateDeviceId()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['AG_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 601;
                $return['message'] = 'Agent id is empty';
            }
        }

        if($fn_status == true)
        {
            $Agent = self::find($this->field['AG_id']);
            $Agent->AG_deviceId = $this->field['AG_deviceId'];
            $Agent->AG_updatedFrom = Request::ip();
            if($Agent->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 600;
                $return['message'] = 'Device Id updated successfully';
                $return['data'] = $Agent;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 602;
                $return['message'] = 'Unable to update Device Id, please try again!';
            }
        }

        return $return;
    }
    
    /* Added by Ajay Jain
    * Date : 17/08/2018
    * Used to Agent list by RM
    */
    public static function getRMAgentList($rmId = 0) {
        $agentList = self::select(['AG_id', 'AG_code', 'AG_type', 'AG_firstName', 'AG_lastName', 'AG_companyName'])->where('AG_AGid', $rmId)->where('AG_type', 'AGENT')->get()->toArray();
        return $agentList;
    }
    
    /* Added by Ajay Jain
    * Date : 18/08/2018
    * Used to Agent details with specified fields
    */
    public static function getAgentDetailByFields($agent_id = null, $fields = array())
    {
        $agentList = self::select($fields)->where('AG_id', $agent_id)->where('AG_type', 'AGENT')->first()->toArray();
        return $agentList;
    }
    
    public static function findAgentByImdCode($imdCode = null) {
        return self::where('AG_ImdCode', $imdCode)->count();
    }
    
    public static function findRMByRmcode($rmCode = null) {
        return self::select(['Ag_id'])->where('AG_code', $rmCode)->where('AG_type', 'DEALER')->first();
    }
}
