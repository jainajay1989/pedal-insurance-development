<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB, Request;

class Lead extends Model
{
    use Notifiable;
    protected $table        = 'tbl_lead';
    protected $primaryKey   = 'L_id';
    const CREATED_AT        = 'L_createdAt';
    const UPDATED_AT        = 'L_updatedAt';
    public $field;
    
    public function healthInsurance() {
        return $this->belongsTo('App\Http\Models\HealthInsurance', 'L_id', 'lead_id');
    }
    
    public function otherInsurance() {
        return $this->belongsTo('App\Http\Models\OtherInsurance', 'L_id', 'lead_id');
    }
    
    public function motorInsurance() {
        return $this->belongsTo('App\Http\Models\MotorInsurance', 'L_id', 'lead_id');
    }
    
    public function travelInsurance() {
        return $this->belongsTo('App\Http\Models\TravelInsurance', 'L_id', 'lead_id');
    }
//    
//    public function vehicleMake() {
//        return $this->belongsTo('App\Http\Models\Vehicle', 'L_vehicleMake', 'V_code');
//    }
//    
//    public function vehicleModel() {
//        return $this->belongsTo('App\Http\Models\Vehicle', 'L_vehicleModel', 'V_modelcode');
//    }
    
    public function agents() {
        return $this->belongsTo('App\Http\Models\Agent', 'L_AGid', 'AG_id');
    }
    
    /**
     * Get table name
     *
     * @return $return
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    /**
     * Get lead detail.
     *
     * @param $lead_id
     * @return $return
     */
    public static function getLeadDetail($lead_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($lead_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Lead id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::where('L_id', $lead_id)->with('healthInsurance', 'otherInsurance', 'motorInsurance', 'travelInsurance')->first();
        }
        return $return;
    }

    /**
     * Get lead list.
     *
     * @param $where
     * @return $return
     */
    public static function getAllLeads($where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('L_id, L_AGid, L_policyType, L_customerFirstName, L_customerLastName, L_customerMobile, L_customerAddress, AG_code, AG_ImdCode, L_status, L_createdAt, L_policyType, AG_companyName' ))
                                    ->join('tbl_agent', 'tbl_agent.AG_id', '=', 'tbl_lead.L_AGid')
                                    ->with('healthInsurance', 'otherInsurance', 'motorInsurance')
                                    ->where($where)
                                    ->orderBy('L_createdAt', 'desc')
                                    ->get();
        }
        return $return;
    }

    /**
     * Get manager lead list.
     *
     * @param $agent_ids
     * @return $return
     */
    public static function getAgentsLeads($agent_ids = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=L_AGid) AS L_agent'))
                                    ->with('healthInsurance', 'otherInsurance', 'motorInsurance')
                                    ->whereIn('L_AGid', $agent_ids)
                                    ->orderBy('L_createdAt', 'desc')
                                    ->get();
        }
        return $return;
    }

    /**
     * Get agent lead list.
     *
     * @param $agent_id
     * @return $return
     */
    public static function getAgentLeads($agent_id = null, $input = null)
    {
        $return = array();
        $query = self::query();
        $query->select('*')->with('healthInsurance', 'otherInsurance', 'motorInsurance')->join('tbl_agent', 'tbl_agent.AG_id', '=', 'tbl_lead.L_AGid')->whereIn('L_AGid', $agent_id);
        $search = array();
        if(!empty($agent_id) && count($agent_id) == 1) {
            $agentDetail = Agent::getAgentDetail($agent_id[0]);
            $policyType = $agentDetail['data']['AG_policyType'];
            $policyType = (!empty($policyType)?explode(',', $policyType):array());
            $query->whereIn('tbl_lead.L_policyType', $policyType);
        }
        if(!empty($input['dealer_id'])) {
            $query->where('tbl_agent.AG_id', 'like', "%{$input['dealer_id']}%");
        }
        if(!empty($input['dealer_code'])) {
            $query->where('tbl_agent.dealer_code', 'like', "%{$input['dealer_code']}%");
        }
        if(!empty($input['customer_name'])) {
            $query->where('tbl_lead.L_customerFirstName', 'like', "%{$input['customer_name']}%");
        }
        if(!empty($input['customer_mobile'])) {
            $query->where('tbl_lead.L_customerMobile', 'like', "%{$input['customer_mobile']}%");
        }
        if(!empty($input['customer_email'])) {
            $query->where('tbl_lead.L_customerEmail', 'like', "%{$input['customer_email']}%");
        }
        if(!empty($input['policy_no'])) {
            $query->where('tbl_lead.L_newPolicyNumber', 'like', "%{$input['policy_no']}%");
        }
        if(!empty($input['policy_status'])) {
            $query->where('tbl_lead.L_status', 'like', "%{$input['policy_status']}%");
        }
        if(!empty($input['policy_type'])) {
            $query->where('tbl_lead.L_policyType', 'like', "%{$input['policy_type']}%");
        }
        if(!empty($input['policy_from_date'])) {
            $fromDate = \Carbon\Carbon::createFromFormat('d/m/Y', $input['policy_from_date'])->format('Y-m-d');
            $query->whereRaw("DATE(tbl_lead.L_createdAt) >= '$fromDate'");
        }
        if(!empty($input['policy_to_date'])) {
            $toDate = \Carbon\Carbon::createFromFormat('d/m/Y', $input['policy_to_date'])->format('Y-m-d');
            $query->whereRaw("DATE(tbl_lead.L_createdAt) <= '$toDate'");
        }
        $return['status'] = true;
        $return['code'] = 200;
        $return['data'] = $query->orderBy('L_createdAt', 'desc')->get();
        return $return;
    }

    /**
     * Add lead.
     *
     * @return string
     */
    public function addLead()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Lead = new Lead;
            $Lead->L_AGid = $this->field['L_AGid'];
            $Lead->L_policyType = $this->field['L_policyType'];
            $Lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $Lead->L_customerLastName = $this->field['L_customerLastName'];
            $Lead->L_customerEmail = $this->field['L_customerEmail'];
            $Lead->L_customerDOB = $this->field['L_customerDOB'];
            $Lead->L_customerMobile = $this->field['L_customerMobile'];
            $Lead->L_customerPhone = $this->field['L_customerPhone'];
            $Lead->L_customerAddress = $this->field['L_customerAddress'];
            $Lead->L_customerCity = $this->field['L_customerCity'];
            $Lead->L_customerState = $this->field['L_customerState'];
            $Lead->L_customerZip = $this->field['L_customerZip'];
            if(@$this->field['L_vehicleMake']) {$Lead->L_vehicleMake = $this->field['L_vehicleMake'];}
            $Lead->L_vehicleModel = $this->field['L_vehicleModel'];
            $Lead->L_vehicleYear = $this->field['L_vehicleYear'];
            $Lead->L_vehicleChassisNo = $this->field['L_vehicleChassisNo'];
            if(@$this->field['L_vehicleAccessories']!==null) {$Lead->L_vehicleAccessories = $this->field['L_vehicleAccessories'];}
            if(@$this->field['L_vehicleAccessoryDetails']!==null) {$Lead->L_vehicleAccessoryDetails = $this->field['L_vehicleAccessoryDetails'];}
            $Lead->L_vehicleNetPrice = $this->field['L_vehicleNetPrice'];
            $Lead->L_vehicleCGST = $this->field['L_vehicleCGST'];
            $Lead->L_vehicleSGST = $this->field['L_vehicleSGST'];
            $Lead->L_vehicleIGST = $this->field['L_vehicleIGST'];
            $Lead->L_vehicleGrossPrice = $this->field['L_vehicleGrossPrice'];
            if(@$this->field['L_vehicleInvoicePhoto']) {$Lead->L_vehicleInvoicePhoto = $this->field['L_vehicleInvoicePhoto'];}
            if(@$this->field['L_vehicleChassisPhoto']) {$Lead->L_vehicleChassisPhoto = $this->field['L_vehicleChassisPhoto'];}
            if(@$this->field['L_newPolicyCoverage']) {$Lead->L_newPolicyCoverage = $this->field['L_newPolicyCoverage'];}
            if(@$this->field['L_newPolicyAD']!==null) {$Lead->L_newPolicyAD = $this->field['L_newPolicyAD'];}
            if(@$this->field['L_newPolicyADCover']!==null) {$Lead->L_newPolicyADCover = $this->field['L_newPolicyADCover'];}
            if(@$this->field['L_newPolicyADCoverPremium']!==null) {$Lead->L_newPolicyADCoverPremium = $this->field['L_newPolicyADCoverPremium'];}
            if(@$this->field['L_newPolicyPL']!==null) {$Lead->L_newPolicyPL = $this->field['L_newPolicyPL'];}
            if(@$this->field['L_newPolicyPLCover']!==null) {$Lead->L_newPolicyPLCover = $this->field['L_newPolicyPLCover'];}
            if(@$this->field['L_newPolicyPLPremium']!==null) {$Lead->L_newPolicyPLPremium = $this->field['L_newPolicyPLPremium'];}
            if(@$this->field['L_newPolicyBasicCover']!==null) {$Lead->L_newPolicyBasicCover = $this->field['L_newPolicyBasicCover'];}
            if(@$this->field['L_newPolicyRSACoverPremium']!==null) {$Lead->L_newPolicyRSACoverPremium = $this->field['L_newPolicyRSACoverPremium'];}
            if(@$this->field['L_newPolicyCGSTPremium']) {$Lead->L_newPolicyCGSTPremium = $this->field['L_newPolicyCGSTPremium'];}
            if(@$this->field['L_newPolicySGSTPremium']) {$Lead->L_newPolicySGSTPremium = $this->field['L_newPolicySGSTPremium'];}
            if(@$this->field['L_newPolicyIGSTPremium']) {$Lead->L_newPolicyIGSTPremium = $this->field['L_newPolicyIGSTPremium'];}

            $Lead->L_newPolicyNetPremium = $this->field['L_newPolicyNetPremium'];
            $Lead->L_newPolicyTotalPremium = $this->field['L_newPolicyTotalPremium'];
            if(@$this->field['L_newPolicyPaymentMode']) {$Lead->L_newPolicyPaymentMode = $this->field['L_newPolicyPaymentMode'];}
            $Lead->L_newPolicyNumber = $this->field['L_newPolicyNumber'];
            $Lead->L_newPolicyStartDate = $this->field['L_newPolicyStartDate'];
            $Lead->L_newPolicyEndDate = $this->field['L_newPolicyEndDate'];
            $Lead->L_invoiceNo = $this->field['L_invoiceNo'];
            $Lead->L_location = $this->field['L_location'];
            $Lead->L_status = $this->field['L_status'];
            $Lead->L_read = $this->field['L_read'];
            $Lead->L_appPostData = $this->field['L_appPostData'];
            $Lead->L_createdFrom = Request::ip();
            $Lead->L_updatedFrom = Request::ip();

            if($Lead->save() == true)
            {
                $Notification = new Notification;
                $Notification->field['L_type'] = 'LEAD';
                $Notification->field['L_Lid'] = $Lead->L_id;
                $Notification->field['L_message'] = 'New lead added';
                $Notification->field['L_LGid'] = null;
                $Notification->addNotification();

                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Lead added successfully';
                $return['data'] = $Lead;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Lead, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update lead detail.
     *
     * @return string
     */
    public function updateLeadDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['L_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'Lead id is empty';
            }
        }

        if($fn_status == true)
        {
            $Lead = self::find($this->field['L_id']);
            if(!empty($this->field['L_AGid'])) {$Lead->L_AGid = $this->field['L_AGid'];}
            if(!empty($this->field['L_customerFirstName'])) {$Lead->L_customerFirstName = $this->field['L_customerFirstName'];}
            if(!empty($this->field['L_customerLastName'])) {$Lead->L_customerLastName = $this->field['L_customerLastName'];}
            if(!empty($this->field['L_customerEmail'])) {$Lead->L_customerEmail = $this->field['L_customerEmail'];}
            if(!empty($this->field['L_customerDOB'])) {$Lead->L_customerDOB = $this->field['L_customerDOB'];}
            if(!empty($this->field['L_customerMobile'])) {$Lead->L_customerMobile = $this->field['L_customerMobile'];}
            if(!empty($this->field['L_customerPhone'])) {$Lead->L_customerPhone = $this->field['L_customerPhone'];}
            if(!empty($this->field['L_customerAddress'])) {$Lead->L_customerAddress = $this->field['L_customerAddress'];}
            if(!empty($this->field['L_customerCity'])) {$Lead->L_customerCity = $this->field['L_customerCity'];}
            if(!empty($this->field['L_customerState'])) {$Lead->L_customerState = $this->field['L_customerState'];}
            if(!empty($this->field['L_customerZip'])) {$Lead->L_customerZip = $this->field['L_customerZip'];}
            if(!empty($this->field['L_vehicleType'])) {$Lead->L_vehicleType = $this->field['L_vehicleType'];}
            if(!empty($this->field['L_vehicleMake'])) {$Lead->L_vehicleMake = $this->field['L_vehicleMake'];}
            if(!empty($Lead->field['L_vehicleModel'])) {$Lead->L_vehicleModel = $this->field['L_vehicleModel'];}
            if(!empty($this->field['L_vehicleYear'])) {$Lead->L_vehicleYear = $this->field['L_vehicleYear'];}
            if(!empty($this->field['L_vehicleChassisNo'])) {$Lead->L_vehicleChassisNo = $this->field['L_vehicleChassisNo'];}
            if(!empty($this->field['L_vehicleAccessories'])) {$Lead->L_vehicleAccessories = $this->field['L_vehicleAccessories'];}
            if(!empty($Lead->field['L_vehicleNetPrice'])) {$Lead->L_vehicleNetPrice = $this->field['L_vehicleNetPrice'];}
            if(!empty($Lead->field['L_vehicleCGST'])) {$Lead->L_vehicleCGST = $this->field['L_vehicleCGST'];}
            if(!empty($Lead->field['L_vehicleSGST'])) {$Lead->L_vehicleSGST = $this->field['L_vehicleSGST'];}
            if(!empty($Lead->field['L_vehicleIGST'])) {$Lead->L_vehicleIGST = $this->field['L_vehicleIGST'];}
            if(!empty($Lead->field['L_vehicleGrossPrice'])) {$Lead->L_vehicleGrossPrice = $this->field['L_vehicleGrossPrice'];}
            if(!empty($this->field['L_newPolicyCGSTPremium'])) {$Lead->L_newPolicyCGSTPremium = $this->field['L_newPolicyCGSTPremium'];}
            if(!empty($this->field['L_newPolicySGSTPremium'])) {$Lead->L_newPolicySGSTPremium = $this->field['L_newPolicySGSTPremium'];}
            if(!empty($this->field['L_newPolicyIGSTPremium'])) {$Lead->L_newPolicyTotalPremium = $this->field['L_newPolicyIGSTPremium'];}
            if(!empty($this->field['L_newPolicyRSACoverPremium'])) {$Lead->L_newPolicyRSACoverPremium = $this->field['L_newPolicyRSACoverPremium'];}
            if(!empty($this->field['L_newPolicyNetPremium'])) {$Lead->L_newPolicyTotalPremium = $this->field['L_newPolicyNetPremium'];}
            if(!empty($this->field['L_newPolicyTotalPremium'])) {$Lead->L_newPolicyTotalPremium = $this->field['L_newPolicyTotalPremium'];}
            if(!empty($this->field['L_newPolicyNumber'])) {$Lead->L_newPolicyNumber = $this->field['L_newPolicyNumber'];}
            if(!empty($this->field['L_newPolicyStartDate'])) {$Lead->L_newPolicyStartDate = $this->field['L_newPolicyStartDate'];}
            if(!empty($this->field['L_newPolicyEndDate'])) {$Lead->L_newPolicyEndDate = $this->field['L_newPolicyEndDate'];}
            if(!empty($this->field['L_newPolicyCreationDate'])) {$Lead->L_newPolicyCreationDate = $this->field['L_newPolicyCreationDate'];}
            if(!empty($this->field['L_newPolicyAttachment'])) {$Lead->L_newPolicyAttachment = $this->field['L_newPolicyAttachment'];}
            if(!empty($this->field['L_completedAt'])) {$Lead->L_completedAt = $this->field['L_completedAt'];}
            if(!empty(@$this->field['L_status'])) {$Lead->L_status = $this->field['L_status'];}
            $Lead->L_updatedFrom = Request::ip();
            if($Lead->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Lead updated successfully';
                $return['data'] = $Lead;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to update Lead, please try again!';
            }
        }

        return $return;
    }
    
    public function addPedalLead() {
        $Lead = new Lead;
        $Lead->L_completeLeadDetails = $this->field['L_completeLeadDetails'];
        $Lead->L_invoiceNo = $this->field['L_invoiceNo'];
        $Lead->L_txnId = $this->field['L_txnId'];
        $Lead->save();
    }
    
    public function updatePedalLead() {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['L_id']== null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Lead id is empty';
            }
        }

        if($fn_status == true)
        {
            $Lead = self::find($this->field['L_id']);
            $Lead->L_completeLeadDetails = $this->field['L_completeLeadDetails'];
            if($Lead->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Lead updated successfully';
                $return['data'] = $Lead;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update lead, please try again!';
            }
        }

        return $return;
    }
    
    public static function getLeadByInvoice($txnId = null)
    {
        $fn_status = true;
        $return = null;
        if($txnId == null) {
            $fn_status = false;
            $return['status'] = false;
            $return['code'] = 101;
            $return['message'] = 'Lead id is empty';
        } else {
            $return['status'] = true;
            $return['code'] = 100;
            $return['result'] = self::where('L_txnId', $txnId)->first();
        }
        
        return $return;
    }
    
    /* Used to reject pending request for which payment not made in 24 hours */
    public static function rejectPendingLeads($leads = array()) {
        $rejectedStatus = config('constant.lead_status.REJECTED.value');
        self::whereIn('L_id', $leads)->update(['L_status' => $rejectedStatus]);
    }
}
