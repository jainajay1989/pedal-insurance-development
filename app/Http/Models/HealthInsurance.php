<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use App\Http\Models\Lead;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class HealthInsurance extends Authenticatable
{
    use Notifiable;
    protected $table        = 'tbl_healthinsurance';
    protected $primaryKey   = 'id';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'modified_at';
    public $field;
    
    public function residentState() {
        return $this->belongsTo('App\Http\Models\State', 'resident_state', 'ST_code');
    }
    
    public function residentCity() {
        return $this->belongsTo('App\Http\Models\City', 'resident_city', 'CT_id');
    }
    
    public function insuranceCompany() {
        return $this->belongsTo('App\Http\Models\InsuranceCompany', 'new_policy_company', 'IC_id');
    }
    
    public function insuranceProduct() {
        return $this->belongsTo('App\Http\Models\InsuranceProduct', 'new_policy_product', 'IP_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * Add user.
     *
     * @return string
     */
    public function add()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            // Used to save lead details
            $lead = new Lead;
            $lead->L_policyType = $this->field['L_policyType'];
            $lead->L_AGid = $this->field['L_AGid'];
            $lead->L_customerFirstName = $this->field['L_customerFirstName'];
            $lead->L_customerLastName = $this->field['L_customerLastName'];
            $lead->L_customerEmail = $this->field['L_customerEmail'];
            $lead->L_customerDOB = $this->field['L_customerDOB'];
            $lead->L_customerMobile = $this->field['L_customerMobile'];
            $lead->L_customerPhone = $this->field['L_customerPhone'];
            $lead->L_customerAddress = $this->field['L_customerAddress'];
            $lead->L_customerCity = $this->field['L_customerCity'];
            $lead->L_customerState = $this->field['L_customerState'];
            $lead->L_customerZip = $this->field['L_customerZip'];
            $lead->save();

            // Used to save health insurance details
            $health = new HealthInsurance;
            $health->lead_id = $lead->L_id;
            $health->for_whom = $this->field['for_whom'];
            $health->policy_holder_details = $this->field['policy_holder_details'];
            $health->resident_state = $this->field['resident_state'];
            $health->resident_city = $this->field['resident_city'];
            $health->sum_insured = $this->field['sum_insured'];
            $health->new_policy_company = $this->field['new_policy_company'];
            $health->new_policy_product = $this->field['new_policy_product'];
            $health->estimated_premium = $this->field['estimated_premium'];
            $health->status = $this->field['status'];
            $health->proposal_form = $this->field['proposal_form'];
            $health->old_policy_attachment1 = $this->field['old_policy_attachment1'];
            $health->old_policy_attachment2 = $this->field['old_policy_attachment2'];
            $health->last_year_claim = $this->field['last_year_claim'];
            $health->policy_renewal_new = $this->field['policy_renewal_new'];
            if($health->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'Lead created successfully';
                $return['data'] = $health;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to create lead, please try again!';
            }
        }

        return $return;
    }

    public function updateDetails()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            // Used to save lead details
            $lead = Lead::find($this->field['L_id']);
            $lead->L_id = $this->field['L_id'];
            $lead->L_policyType = 'HEALTH';
            if(!empty($this->field['L_customerFirstName'])) {$lead->L_customerFirstName = $this->field['L_customerFirstName'];}
            if(!empty($this->field['L_customerLastName'])) {$lead->L_customerLastName = $this->field['L_customerLastName'];}
            if(!empty($this->field['L_customerEmail'])) {$lead->L_customerEmail = $this->field['L_customerEmail'];}
            if(!empty($this->field['L_customerDOB'])) {$lead->L_customerDOB = $this->field['L_customerDOB'];}
            if(!empty($this->field['L_customerMobile'])) {$lead->L_customerMobile = $this->field['L_customerMobile'];}
            if(!empty($this->field['L_customerPhone'])) {$lead->L_customerPhone = $this->field['L_customerPhone'];}
            if(!empty($this->field['L_customerAddress'])) {$lead->L_customerAddress = $this->field['L_customerAddress'];}
            if(!empty($this->field['L_customerCity'])) {$lead->L_customerCity = $this->field['L_customerCity'];}
            if(!empty($this->field['L_customerState'])) {$lead->L_customerState = $this->field['L_customerState'];}
            if(!empty($this->field['L_customerZip'])) {$lead->L_customerZip = $this->field['L_customerZip'];}
            $lead->save();
            
            // Used to save health insurance details
            $health = self::find($this->field['id']);
            $health->id = $this->field['id'];
            if(!empty($this->field['L_id'])) {$health->lead_id = $lead->L_id;}
            if(!empty($this->field['for_whom'])) {$health->for_whom = $this->field['for_whom'];}
            if(!empty($this->field['policy_holder_details'])) {$health->policy_holder_details = $this->field['policy_holder_details'];}
            if(!empty($this->field['resident_state'])) {$health->resident_state = $this->field['resident_state'];}
            if(!empty($this->field['resident_city'])) {$health->resident_city = $this->field['resident_city'];}
            if(!empty($this->field['sum_insured'])) {$health->sum_insured = $this->field['sum_insured'];}
            if(!empty($this->field['new_policy_company'])) {$health->new_policy_company = $this->field['new_policy_company'];}
            if(!empty($this->field['new_policy_product'])) {$health->new_policy_product = $this->field['new_policy_product'];}
            if(!empty($this->field['estimated_premium'])) {$health->estimated_premium = $this->field['estimated_premium'];}
            if(!empty($this->field['net_premium'])) {$health->net_premium = $this->field['net_premium'];}
            if(!empty($this->field['gst'])) {$health->gst = $this->field['gst'];}
            if(!empty($this->field['gross_premium'])) {$health->gross_premium = $this->field['gross_premium'];}
            if(!empty($this->field['status'])) {$health->status = $this->field['status'];}
            if(!empty($this->field['paid_by_whom'])) {$health->paid_by_whom = $this->field['paid_by_whom'];}
            if(!empty($this->field['policy_renewal_new'])) {$health->policy_renewal_new = $this->field['policy_renewal_new'];}
            if(!empty($this->field['remark'])) {$health->remark = $this->field['remark'];}
            if(!empty($this->field['quotations'])) {
                $oldQuotes = (!empty($health->quotations)?json_decode($health->quotations, true):array());
                $newQuotes = array_merge($oldQuotes, $this->field['quotations']);
                $health->quotations = (!empty($newQuotes)?json_encode($newQuotes):array());
            }
            if(!empty($this->field['new_policy_number'])) {$health->new_policy_number = $this->field['new_policy_number'];}
            if(!empty($this->field['new_policy_start_date'])) {$health->new_policy_start_date = $this->field['new_policy_start_date'];}
            if(!empty($this->field['new_policy_end_date'])) {$health->new_policy_end_date = $this->field['new_policy_end_date'];}
            if(!empty($this->field['policy_attachment'])) {
                $health->policy_attachment = $this->field['policy_attachment'];
            }
            if(!empty($this->field['proposal_form'])) {$health->proposal_form = $this->field['proposal_form'];}
            if (!empty($this->field['customer_selected_quote'])) { $health->customer_selected_quote = $this->field['customer_selected_quote']; }
            if (!empty($this->field['customer_bank_cheque'])) { $health->customer_bank_cheque = $this->field['customer_bank_cheque']; }
            if (!empty($this->field['last_year_claim'])) { $health->last_year_claim = $this->field['last_year_claim']; }
            if (!empty($this->field['old_policy_attachment1'])) { $health->old_policy_attachment1 = $this->field['old_policy_attachment1']; }
            if (!empty($this->field['old_policy_attachment2'])) { $health->old_policy_attachment2 = $this->field['old_policy_attachment2']; }
            if($health->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'Lead updated successfully';
                $return['data'] = $health;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to create lead, please try again!';
            }
        }

        return $return;
    }
}
