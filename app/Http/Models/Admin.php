<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $table        = 'tbl_admin';
    protected $primaryKey   = 'A_id';
    const CREATED_AT        = 'A_createdAt';
    const UPDATED_AT        = 'A_updatedAt';
    protected $hidden       = ['A_rememberToken'];
    public $field;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'A_firstName', 'A_username', 'A_password'
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->attributes['A_password'];
    }

    /**
     * Get remember token column name.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'A_rememberToken';
    }

    /**
     * Get user detail.
     *
     * @param $user_id
     * @return $return
     */
    public static function getUserDetail($user_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($user_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'User id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($user_id);
        }

        return $return;
    }

    /**
     * Get user list.
     *
     * @return $return
     */
    public static function getAllUsers()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = self::all();
        }

        return $return;
    }

    /**
     * Add user.
     *
     * @return string
     */
    public function addUser()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $Admin = new Admin;
            $Admin->A_type = $this->field['A_type'];
            $Admin->A_firstName = $this->field['A_firstName'];
            $Admin->A_lastName = $this->field['A_lastName'];
            $Admin->A_email = $this->field['A_email'];
            $Admin->A_username = $this->field['A_username'];
            $Admin->A_password = password_hash($this->field['A_password'], PASSWORD_BCRYPT);
            $Admin->A_mobile = $this->field['A_mobile'];
            $Admin->A_phone = $this->field['A_phone'];
            $Admin->A_address = $this->field['A_address'];
            $Admin->A_city = $this->field['A_city'];
            $Admin->A_state = $this->field['A_state'];
            $Admin->A_zip = $this->field['A_zip'];
            $Admin->A_active = $this->field['A_active'];
            $Admin->A_createdFrom = Request::ip();
            $Admin->A_updatedFrom = Request::ip();
            if($Admin->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'User updated successfully';
                $return['data'] = $Admin;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to update User, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update user detail.
     *
     * @return string
     */
    public function updateUserDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['A_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 201;
                $return['message'] = 'User id is empty';
            }
        }

        if($fn_status == true)
        {
            $Admin = self::find($this->field['A_id']);
            $Admin->A_type = $this->field['A_type'];
            $Admin->A_firstName = $this->field['A_firstName'];
            $Admin->A_lastName = $this->field['A_lastName'];
            $Admin->A_email = $this->field['A_email'];
            $Admin->A_username = $this->field['A_username'];
            $Admin->A_mobile = $this->field['A_mobile'];
            $Admin->A_phone = $this->field['A_phone'];
            $Admin->A_address = $this->field['A_address'];
            $Admin->A_city = $this->field['A_city'];
            $Admin->A_state = $this->field['A_state'];
            $Admin->A_zip = $this->field['A_zip'];
            $Admin->A_active = $this->field['A_active'];
            if(@$this->field['A_password'])
            {
                $Admin->A_password = password_hash($this->field['A_password'], PASSWORD_BCRYPT);
            }
            $Admin->A_updatedFrom = Request::ip();
            if($Admin->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 200;
                $return['message'] = 'User updated successfully';
                $return['data'] = $Admin;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 202;
                $return['message'] = 'Unable to update User, please try again!';
            }
        }

        return $return;
    }

    /**
     * Delete user.
     *
     * @return string
     */
    public function deleteUser()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['A_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'User id is empty';
            }
        }

        if($fn_status == true)
        {
            $Admin = self::find($this->field['A_id']);
            if($Admin->delete() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'User deleted successfully';
                $return['data'] = $Admin;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to delete User, please try again!';
            }
        }

        return $return;
    }
    
    public static function findAdminByUsername($code = null) {
        return self::select(['A_id'])->where('A_username', $code)->first();
    }
}
