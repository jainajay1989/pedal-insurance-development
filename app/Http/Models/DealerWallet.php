<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB, Request;

class DealerWallet extends Model
{
    use Notifiable;
    protected $table        = 'tbl_dealerwallet';
    protected $primaryKey   = 'id';
    const CREATED_AT        = 'created';
    const UPDATED_AT        = 'updated';
    public $field;

    public static function addToWallet($data = array()) {
        Self::updateTransaction($data['agent_id']);
        $wallet= new DealerWallet;
        $wallet->agent_id = $data['agent_id'];
        $wallet->amount = $data['amount'];
        $wallet->transaction_id = $data['transaction_id'];
        $wallet->payu_response = $data['payu_response'];
        $wallet->recharge_date = $data['recharge_date'];
        $wallet->is_active = 1;
        if($wallet->save() == true)
        {
            $return['status'] = true;
            $return['code'] = 300;
            $return['message'] = 'Agent added successfully';
            $return['data'] = $wallet;
        }
        else
        {
            $return['status'] = false;
            $return['code'] = 302;
            $return['message'] = 'Unable to add Agent, please try again!';
        }
    }
    
    public static function updateTransaction($agentId = 0) {
        Self::where('agent_id', $agentId)->update(['is_active' => 0]);
    }
}
