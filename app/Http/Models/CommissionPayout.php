<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB, Request;

class CommissionPayout extends Model
{
    use Notifiable;
    protected $table        = 'tbl_commissionpayout';
    protected $primaryKey   = 'CP_id';
    const CREATED_AT        = 'CP_createdAt';
    const UPDATED_AT        = 'CP_updatedAt';
    public $field;

    /**
     * Get payout detail.
     *
     * @param $payout_id
     * @return $return
     */
    public static function getPayoutDetail($payout_id = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($payout_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Payout id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::find($payout_id);
        }

        return $return;
    }

    /**
     * Get agent payout detail.
     *
     * @param $agent_id
     * @param $company_id
     * @param $product_id
     * @return $return
     */
    public static function getAgentPayoutDetail($agent_id = null, $company_id = null, $product_id = null)
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            if($agent_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Agent id is empty';
            }
        }

        if($fn_status == true)
        {
            if($company_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 102;
                $return['message'] = 'Company id is empty';
            }
        }

        if($fn_status == true)
        {
            if($product_id == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 103;
                $return['message'] = 'Product id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['message'] = null;
            $return['data'] = $query->select('*')
                                    ->where(['CP_AGid' => $agent_id, 'CP_ICid' => $company_id, 'CP_IPid' => $product_id])
                                    ->first();

        }

        return $return;
    }

    /**
     * Get payout list.
     *
     * @return $return
     */
    public static function getAllPayouts()
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT CONCAT(AG_firstName, " ", AG_lastName) FROM tbl_agent WHERE AG_id=CP_AGid) AS CP_agent, (SELECT IC_name FROM tbl_insurancecompany WHERE IC_id=CP_ICid) AS CP_company, (SELECT IP_name FROM tbl_insuranceproduct WHERE IP_id=CP_IPid) AS CP_product, (SELECT IP_policyType FROM tbl_insuranceproduct WHERE IP_id=CP_IPid) AS CP_policyType'))
                                    ->get();
        }

        return $return;
    }

    /**
     * Get agent payout list.
     *
     * @param $agent_id
     * @param $where
     * @return $return
     */
    public static function getAgentPayouts($agent_id = null, $where = [])
    {
        $fn_status = true;
        $return = null;
        $query = self::query();

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 200;
            $return['data'] = $query->select(DB::raw('*, (SELECT IC_name FROM tbl_insurancecompany WHERE IC_id=CP_ICid) AS CP_company, (SELECT IP_name FROM tbl_insuranceproduct WHERE IP_id=CP_IPid) AS CP_product, (SELECT IP_policyType FROM tbl_insuranceproduct WHERE IP_id=CP_IPid) AS CP_policyType'))
                                    ->join('tbl_insuranceproduct', 'tbl_commissionpayout.CP_IPid', '=', 'tbl_insuranceproduct.IP_id')
                                    ->where('CP_AGid', $agent_id)
                                    ->where($where)
                                    ->get();
        }

        return $return;
    }

    /**
     * Add payout.
     *
     * @return string
     */
    public function addPayout()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $CommissionPayout = new CommissionPayout;
            $CommissionPayout->CP_AGid = $this->field['CP_AGid'];
            $CommissionPayout->CP_ICid = $this->field['CP_ICid'];
            $CommissionPayout->CP_IPid = $this->field['CP_IPid'];
            $CommissionPayout->CP_payout = $this->field['CP_payout'];
            $CommissionPayout->CP_active = $this->field['CP_active'];
            $CommissionPayout->CP_createdFrom = Request::ip();
            $CommissionPayout->CP_updatedFrom = Request::ip();
            if($CommissionPayout->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Product added successfully';
                $return['data'] = $CommissionPayout;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Payout, please try again!';
            }
        }
        return $return;
    }

    /**
     * Update payout detail.
     *
     * @return string
     */
    public function updatePayoutDetail()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($this->field['CP_id'] == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 301;
                $return['message'] = 'Payout id is empty';
            }
        }

        if($fn_status == true)
        {
            $CommissionPayout = self::find($this->field['CP_id']);
            if(@$this->field['CP_ICid']) {$CommissionPayout->CP_ICid = $this->field['CP_ICid'];}
            if(@$this->field['CP_IPid']) {$CommissionPayout->CP_IPid = $this->field['CP_IPid'];}
            $CommissionPayout->CP_payout = $this->field['CP_payout'];
            $CommissionPayout->CP_active = $this->field['CP_active'];
            $CommissionPayout->CP_updatedFrom = Request::ip();
            if($CommissionPayout->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Payout updated successfully';
                $return['data'] = $CommissionPayout;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to update Payout, please try again!';
            }
        }

        return $return;
    }
}
