<?php //

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Request, \Carbon\Carbon;

class PayuLog extends Model
{
    use Notifiable;
    protected $table        = 'tbl_payulog';
    protected $primaryKey   = 'P_id';
    const CREATED_AT        = 'P_payu_link_date';
    const UPDATED_AT        = 'P_payu_payment_date';
    public $field;

    public function lead() {
        return $this->belongsTo('App\Http\Models\Lead', 'P_lead_id', 'L_id');
    }  
    
    /**
     * Get company detail.
     *
     * @param $company_id
     * @return $return
     */
    public static function getTransactionLog($txnId = null)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($txnId == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 101;
                $return['message'] = 'Transaction id is empty';
            }
        }

        if($fn_status == true)
        {
            $return['status'] = true;
            $return['code'] = 100;
            $return['data'] = self::where('P_txnid', $txnId)->with(['lead'])->first()->toArray();
        }

        return $return;
    }

    /**
     * Add company.
     *
     * @return string
     */
    public function addPaymentLog()
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            $payulog = new PayuLog();
            $payulog->P_lead_id = $this->field['P_lead_id'];
            $payulog->P_txnid = $this->field['P_txnid'];
//            $payulog->P_invoice_no = $this->field['P_invoice_no'];
            $payulog->P_payu_link_request = $this->field['P_payu_link_request'];
            $payulog->P_payu_link_response = $this->field['P_payu_link_response'];
            $payulog->P_payu_link_date = $this->field['P_payu_link_date'];
            if($payulog->save() == true)
            {
                $return['status'] = true;
                $return['code'] = 300;
                $return['message'] = 'Company added successfully';
                $return['data'] = $payulog;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 302;
                $return['message'] = 'Unable to add Company, please try again!';
            }
        }

        return $return;
    }

    /**
     * Update company detail.
     *
     * @return string
     */
    public function updatePayuLog($txnid = NULL, $payuRes = NULL, $payuPaymentStatus = NULL)
    {
        $fn_status = true;
        $return = null;

        if($fn_status == true)
        {
            if($txnid == null)
            {
                $fn_status = false;
                $return['status'] = false;
                $return['code'] = 401;
                $return['message'] = 'Payu is empty';
            }
        }

        if($fn_status == true)
        {
            $payuLog = self::where('P_txnid', $txnid)
            ->update(['P_payu_payment_response' => $payuRes, 'P_payu_payment_status' => $payuPaymentStatus]);
            
            if($payuLog == true)
            {
                $return['status'] = true;
                $return['code'] = 400;
                $return['message'] = 'Company updated successfully';
                $return['data'] = $payuLog;
            }
            else
            {
                $return['status'] = false;
                $return['code'] = 402;
                $return['message'] = 'Unable to update Company, please try again!';
            }
        }

        return $return;
    }
    
    public static function getAllPendingTransactions() {
//        $pendingTransactions = self::whereraw('((P_payu_payment_response IS NULL) OR (P_payu_payment_status = "success" AND lead.L_newPolicyNumber IS NULL)) AND P_txnid IN ("7dda1e9a52c3bf34fdd4")')->select('P_id', 'P_txnid', 'lead.L_id', 'lead.L_completeLeadDetails', 'lead.L_newPolicyCoverage', 'lead.L_appPostData')->join('tbl_lead AS lead', 'lead.L_id', '=', 'tbl_payulog.P_lead_id')->get();
//        $pendingTransactions = self::whereIn('P_txnid', array('fbb14dde8c2cf05d8a9c'))->select('P_id', 'P_txnid', 'lead.L_id', 'lead.L_completeLeaDetails', 'lead.L_newPolicyCoverage')->get();
            $pendingTransactions = self::whereraw('(P_payu_payment_response IS NULL) OR (P_payu_payment_status = "success" AND lead.L_newPolicyNumber IS NULL) OR (P_payu_payment_status = "pending")')->select('P_id', 'P_txnid', 'lead.L_id', 'lead.L_completeLeadDetails', 'lead.L_newPolicyCoverage', 'lead.L_appPostData')->join('tbl_lead AS lead', 'lead.L_id', '=', 'tbl_payulog.P_lead_id')->get();
        return $pendingTransactions;
    }
    
    // Used to get pending request which are create before 24 hours
    public static function getAllPendingRequests() {
        $date = date('Y-m-d H:i:s', strtotime('-1 Day'));
        $pendingTransactions = self::whereraw("((P_payu_payment_response IS NULL OR P_payu_payment_response = 'NOT NULL' OR  P_payu_payment_response = '' OR P_payu_payment_response = 'NOTNULL') OR (P_payu_payment_status = 'Not Found' OR P_payu_payment_status = 'failure')) AND lead.L_createdAt < '$date' AND (lead.L_status = 1 OR lead.L_status IS NULL) AND lead.L_newPolicyNumber IS NULL")->select('P_id', 'P_txnid', 'P_payu_payment_response', 'P_payu_payment_status', 'lead.L_id', 'lead.L_completeLeadDetails', 'lead.L_newPolicyCoverage')->join('tbl_lead AS lead', 'lead.L_id', '=', 'tbl_payulog.P_lead_id')->get();
        return $pendingTransactions;
    }
}
