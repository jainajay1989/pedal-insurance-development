<?php
namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Page;

class CMSController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'cms';
    }

    public function index()
    {
        $page_list = null;

        // Get All Pages
        $return = Page::getAllPages();
        if($return['status'] == true)
        {
            $page_list = $return['data'];
        }
        //---------------

        $this->viewData['page_list'] = $page_list;
        return view('cms.index', $this->viewData);
    }

    /**
     * Add page.
     *
     * @return view
     */
    public function create()
    {
        return view('cms.create', $this->viewData);
    }

    /**
     * Edit page.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $page_id = null;
        $page_detail = null;

        // Fetch Request Variables
        $page_id = intval($request['id']);
        //------------------------

        $return = Page::getPageDetail($page_id);
        if($return['status'] == true)
        {
            $page_detail = $return['data'];
        }

        $this->viewData['page_detail'] = $page_detail;
        return view('cms.edit', $this->viewData);
    }

    /**
     * Add page.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $Page = new Page();
        $Page->field['PG_key'] = $request['key'];
        $Page->field['PG_title'] = $request['name'];
        $Page->field['PG_content'] = $request['content'];
        $Page->field['PG_active'] = $request['active'];
        $return = $Page->addPage();
        if($return['status'] == true)
        {
            $page_detail = $return['data'];
            $redirect['url'] = url('page/edit', ['id' => $page_detail->PG_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('page/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update page.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $page_id = null;

        // Fetch Request Variables
        $page_id = $request['id'];
        //------------------------

        $Page = new Page();
        $Page->field['PG_id'] = $page_id;
        $Page->field['PG_title'] = $request['name'];
        $Page->field['PG_content'] = $request['content'];
        $Page->field['PG_active'] = $request['active'];
        $return = $Page->updatePageDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('page/edit', ['id' => $page_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Delete page.
     *
     * @param Request $request
     * @return redirect
     */
    public function remove(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $return = $Agent->deleteAgent();
        if($return['status'] == true)
        {
            $redirect['url'] = url('agents');
        }
        else
        {
            $redirect['url'] = url('agent/edit', ['id' => $agent_id]);
        }
        $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        return redirect($redirect['url'])->with($redirect['message']);
    }
}
