<?php
namespace App\Http\Controllers\Ledger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Agent;
use App\Http\Models\Ledger;
use App\Http\Models\Notification;
use Auth;

class LedgerController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'ledger';
    }

    /**
     * View transactions.
     *
     * @param $request
     * @return view
     */
    public function index(Request $request)
    {
        $fn_status = true;
        $transaction_type_list = [];
        $agent_list = [];
        $transaction_status_list = [];
        $agent_ids = [];
        $transaction_list = null;

        // Check Permission
        if(check_permission('LEDGER_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $transaction_type = $request['type'];
        $agent_id = $request['agent_id'];
        $lead_id = $request['lead_id'];
        $status = $request['status'];
        //------------------------

        // Get Transaction Type List
        if($fn_status == true)
        {
            $transaction_type_list[''] = 'Transaction Type';
            foreach(config('constant.transaction_type') as $type)
            {
                $transaction_type_list[$type['value']] = $type['message'];
            }
        }
        //--------------------------

        // Get Agent List
        if($fn_status == true)
        {
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value') || Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $where = ['AG_Aid' => Auth::user()->A_id];
                $return = Agent::getAllAgents($where);
            }
            else
            {
                $return = Agent::getAllAgents();
            }
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Agent';
                foreach($agents as $agent)
                {
                    $agent_ids[] = $agent->AG_id;
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Transaction Status List
        if($fn_status == true)
        {
            $transaction_status_list[''] = 'Status';
            foreach(config('constant.transaction_status') as $transaction_status)
            {
                $transaction_status_list[$transaction_status['value']] = $transaction_status['caption'];
            }
        }
        //----------------------------

        // Get All Transactions
        if($fn_status == true)
        {
            if($agent_id == null)
            {
                $where = [];
                if($transaction_type !== null)
                {
                    $where['LG_type'] = $transaction_type;
                }
                if($lead_id !== null)
                {
                    $where['LG_Lid'] = $lead_id;
                }
                if($status !== null)
                {
                    $where['LG_status'] = $status;
                }
                $return = Ledger::getAllTransactions($where);
            }
            else
            {
                $where = [];
                if($transaction_type !== null)
                {
                    $where['LG_type'] = $transaction_type;
                }
                if($lead_id !== null)
                {
                    $where['LG_Lid'] = $lead_id;
                }
                if($status !== null)
                {
                    $where['LG_status'] = $status;
                }
                $return = Ledger::getAgentTransactions($agent_id, $where);
            }
            if($return['status'] == true)
            {
                $transaction_list = $return['data'];
            }
        }
        //---------------------

        $this->viewData['transaction_type'] = $transaction_type;
        $this->viewData['agent_id'] = $agent_id;
        $this->viewData['lead_id'] = $lead_id;
        $this->viewData['status'] = $status;
        $this->viewData['transaction_type_list'] = $transaction_type_list;
        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['transaction_status_list'] = $transaction_status_list;
        $this->viewData['transaction_list'] = $transaction_list;
        return view('ledger.index', $this->viewData);
    }

    /**
     * Add transaction.
     *
     * @return view
     */
    public function create()
    {
        $fn_status = true;
        $agent_list = null;
        $transaction_status_list = [];

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Select';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Transaction Status List
        if($fn_status == true)
        {
            foreach(config('constant.transaction_status') as $status)
            {
                $transaction_status_list[$status['value']] = $status['caption'];
            }
        }
        //---------------------

        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['transaction_status_list'] = $transaction_status_list;
        return view('ledger.create', $this->viewData);
    }

    /**
     * Edit transaction.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $fn_status = true;
        $transaction_id = null;
        $agent_list = null;
        $transaction_detail = null;
        $transaction_status_list = [];

        // Fetch Request Variables
        $transaction_id = intval($request['id']);
        //------------------------

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Select';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Transaction Detail
        if($fn_status == true)
        {
            $return = Ledger::getTransactionDetail($transaction_id);
            $fn_status = $return['status'];
            if($fn_status == true)
            {
                $transaction_detail = $return['data'];
            }
        }
        //-----------------------

        // Get Transaction Status List
        if($fn_status == true)
        {
            foreach(config('constant.transaction_status') as $status)
            {
                $transaction_status_list[$status['value']] = $status['caption'];
            }
            if($transaction_detail->LG_type != config('constant.transaction_type.WITHDRAW.value'))
            {
                unset($transaction_status_list[config('constant.transaction_status.PAYMENTRELEASED.value')]);
            }
        }
        //---------------------

        // Delete Notification
        if($fn_status == true)
        {
            Notification::deleteNotification(['N_LGid' => $transaction_id]);
        }
        //--------------------

        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['transaction_detail'] = $transaction_detail;
        $this->viewData['transaction_status_list'] = $transaction_status_list;
        return view('ledger.edit', $this->viewData);
    }

    /**
     * Add transaction.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $Ledger = new Ledger();
        $Ledger->field['LG_Lid'] = $request['lead'];
        $Ledger->field['LG_AGid'] = $request['agent'];
        $Ledger->field['LG_type'] = $request['type'];
        $Ledger->field['LG_message'] = config('constant.transaction_type.'.$request['type'].'.message');
        $Ledger->field['LG_amount'] = $request['amount'];
        $Ledger->field['LG_note'] = $request['note'];
        $Ledger->field['LG_attachment'] = $request['attachment'];
        $Ledger->field['LG_status'] = $request['status'];
        $return = $Ledger->addTransaction();
        if($return['status'] == true)
        {
            $transaction_detail = $return['data'];
            $redirect['url'] = url('ledger/edit', ['id' => $transaction_detail->LG_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('ledger/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update transaction detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $transaction_id = null;

        // Fetch Request Variables
        $transaction_id = $request['id'];
        //------------------------

        $Ledger = new Ledger();
        $Ledger->field['LG_id'] = $transaction_id;
        //$Ledger->field['LG_Lid'] = $request['lead'];
        //$Ledger->field['LG_AGid'] = $request['agent'];
        //$Ledger->field['LG_type'] = $request['type'];
        //$Ledger->field['LG_message'] = config('constant.transaction_type.'.$request['type'].'.message');
        //$Ledger->field['LG_amount'] = $request['amount'];
        //$Ledger->field['LG_note'] = $request['note'];
        //$Ledger->field['LG_attachment'] = $request['attachment'];
        $Ledger->field['LG_status'] = $request['status'];
        $return = $Ledger->updateTransactionDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('ledger/edit', ['id' => $transaction_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }
}
