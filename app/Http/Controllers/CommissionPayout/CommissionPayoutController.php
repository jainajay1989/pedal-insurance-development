<?php

namespace App\Http\Controllers\CommissionPayout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Agent;
use App\Http\Models\InsuranceCompany;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\CommissionPayout;
use Auth;

class CommissionPayoutController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'agent';
    }

    /**
     * View payouts.
     *
     * @param $request
     * @return view
     */
    public function index(Request $request)
    {
        $fn_status = true;
        $policy_type_list = [];
        $payout_list = [];
        $agent_list = [];
        $company_list = [];
        $product_list = [];

        // Check Permission
        if(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $agent_id = $request['agent_id'];
        $policy_type = $request['type'];
        $company_id = $request['company_id'];
        $product_id = $request['product_id'];
        $status = intval(($request['status']==''?1:$request['status']));
        //------------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            $policy_type_list[''] = 'Policy Type';
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Agent';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Company List
        if($fn_status == true)
        {
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $companies = $return['data'];
                $company_list[''] = 'Company';
                foreach($companies as $company)
                {
                    $company_list[$company->IC_id] = $company->IC_name;
                }
            }
        }
        //-----------------

        // Get Product List
        if($fn_status == true)
        {
            $return = InsuranceProduct::getAllProducts();
            if($return['status'] == true)
            {
                $products = $return['data'];
                $product_list[''] = 'Product';
                foreach($products as $product)
                {
                    $product_list[$product->IP_id] = $product->IP_name;
                }
            }
        }
        //-----------------

        // Get All Commission Payouts
        if($fn_status == true)
        {
            if($agent_id == null)
            {
                $return = CommissionPayout::getAllPayouts();
            }
            else
            {
                $where = [];
                if($policy_type)
                {
                    $where['tbl_insuranceproduct.IP_PolicyType'] = $policy_type;
                }
                if($company_id)
                {
                    $where['CP_ICid'] = $company_id;
                }
                if($product_id)
                {
                    $where['CP_IPid'] = $product_id;
                }
                $where['CP_active'] = $status;
                $return = CommissionPayout::getAgentPayouts($agent_id, $where);
            }

            $fn_status = $return['status'];
            if($fn_status == true)
            {
                $payout_list = $return['data'];
            }
        }
        //---------------------------

        $this->viewData['agent_id'] = $agent_id;
        $this->viewData['policy_type'] = $policy_type;
        $this->viewData['company_id'] = $company_id;
        $this->viewData['product_id'] = $product_id;
        $this->viewData['status'] = $status;
        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['company_list'] = $company_list;
        $this->viewData['product_list'] = $product_list;
        $this->viewData['payout_list'] = $payout_list;
        return view('commission_payout.index', $this->viewData);
    }

    /**
     * Add payout.
     *
     * @param $request
     * @return view
     */
    public function create(Request $request)
    {
        $fn_status = true;
        $agent_id = null;
        $agent_list = [];
        $company_list = [];
        $product_list = [];

        // Check Permission
        if(check_permission('COMMISSION_PAYOUT_CREATE', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $agent_id = $request['agent_id'];
        //------------------------

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Select';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Company List
        if($fn_status == true)
        {
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $companies = $return['data'];
                $company_list[''] = 'Select';
                foreach($companies as $company)
                {
                    $company_list[$company->IC_id] = $company->IC_name;
                }
            }
        }
        //-----------------

        // Get Product List
        if($fn_status == true)
        {
            $return = InsuranceProduct::getAllProducts();
            if($return['status'] == true)
            {
                $products = $return['data'];
                $product_list[''] = 'Select';
                foreach($products as $product)
                {
                    $product_list[$product->IP_id] = $product->IP_name;
                }
            }
        }
        //-----------------

        $this->viewData['agent_id'] = $agent_id;
        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['company_list'] = $company_list;
        $this->viewData['product_list'] = $product_list;
        return view('commission_payout.create', $this->viewData);
    }

    /**
     * Edit payout.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $fn_status = true;
        $payout_id = null;
        $agent_list = [];
        $company_list = [];
        $product_list = [];
        $payout_detail = null;

        // Check Permission
        if(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type) == false && check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $payout_id = intval($request['id']);
        //------------------------

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Select';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Company List
        if($fn_status == true)
        {
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $companies = $return['data'];
                $company_list[''] = 'Select';
                foreach($companies as $company)
                {
                    $company_list[$company->IC_id] = $company->IC_name;
                }
            }
        }
        //-----------------

        // Get Product List
        if($fn_status == true)
        {
            $return = InsuranceProduct::getAllProducts();
            if($return['status'] == true)
            {
                $products = $return['data'];
                $product_list[''] = 'Select';
                foreach($products as $product)
                {
                    $product_list[$product->IP_id] = $product->IP_name;
                }
            }
        }
        //-----------------

        // Get Payout Detail
        if($fn_status == true)
        {
            $return = CommissionPayout::getPayoutDetail($payout_id);
            if($return['status'] == true)
            {
                $payout_detail = $return['data'];
            }
        }
        //------------------

        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['company_list'] = $company_list;
        $this->viewData['product_list'] = $product_list;
        $this->viewData['payout_detail'] = $payout_detail;
        return view('commission_payout.edit', $this->viewData);
    }

    /**
     * Add payout.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $CommissionPayout = new CommissionPayout();
        $CommissionPayout->field['CP_ICid'] = $request['company'];
        $CommissionPayout->field['CP_IPid'] = $request['product'];
        $CommissionPayout->field['CP_payout'] = $request['payout'];
        $CommissionPayout->field['CP_active'] = $request['active'];
        $return = $CommissionPayout->addPayout();
        if($return['status'] == true)
        {
            $payout_detail = $return['data'];
            $redirect['url'] = url('commission-payout/edit', ['id' => $payout_detail->CP_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('commission-payout/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update product detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $payout_id = null;

        // Fetch Request Variables
        $payout_id = $request['id'];
        //------------------------

        $CommissionPayout = new CommissionPayout();
        $CommissionPayout->field['CP_id'] = $payout_id;
        $CommissionPayout->field['CP_ICid'] = $request['company'];
        $CommissionPayout->field['CP_IPid'] = $request['product'];
        $CommissionPayout->field['CP_payout'] = $request['payout'];
        $CommissionPayout->field['CP_active'] = $request['active'];
        $return = $CommissionPayout->updatePayoutDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('commission-payout/edit', ['id' => $payout_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }
    
    public function setCommission() {
        $return = Agent::getAllAgents(['AG_type' => 'AGENT']);
        $agents = $return['data'];
        $return = InsuranceCompany::getAllCompanies();
        $insuranceCompany = $return['data'];
        $return = InsuranceProduct::getAllProducts();
        $insuranceProducts = $return['data'];
        foreach($agents as $key => $val) {
            foreach($insuranceCompany as $key1 => $val1) {
                foreach($insuranceProducts as $key2 => $val2) {
                    $CommissionPayout = new CommissionPayout();
                    $CommissionPayout->field['CP_AGid'] = $val->AG_id;
                    $CommissionPayout->field['CP_ICid'] = $val1->IC_id;
                    $CommissionPayout->field['CP_IPid'] = $val2->IP_id;
                    $CommissionPayout->field['CP_payout'] = 15;
                    $CommissionPayout->field['CP_active'] = 1;
                    $return = $CommissionPayout->addPayout();
                }
            }
        }
    }
}
