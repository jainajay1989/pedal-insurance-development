<?php

namespace App\Http\Controllers\API\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Page;
use Carbon;

class PageController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('api_auth');
    }

    /**
     * Detail
     *
     * @param Request $request
     * @return Mixed
     */
    public function detail(Request $request)
    {
        $page_detail = null;

        // Get Request Variables
        $page_key = $request['key'];
        //----------------------

        $return = Page::getPageContent($page_key);
        if($return['status'] == true)
        {
            $page_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S200';
            $response['message'] = 'Successful';
            $response['data'] = $page_detail;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }
    
    public function getTermsConditions() {
//        $view = View::make('page.terms_conditions');
//        $view = \Illuminate\View\View::make('page.terms_conditions');
        $view = \Illuminate\Support\Facades\View::make('page.terms_conditions');
        $contents = (string) $view;
        $response['status'] = 1;
        $response['code'] = 'S200';
        $response['message'] = 'Successful';
        $response['content'] = $contents;
        return response()->json($response);
    }
}