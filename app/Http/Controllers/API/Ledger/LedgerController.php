<?php

namespace App\Http\Controllers\API\Ledger;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Ledger;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon, Storage;

class LedgerController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index
     *
     * @param Request $request
     * @return Mixed
     */
    public function index(Request $request)
    {
        $transaction_list = null;

        // Get Request Variables
        $agent_id = $request['agent_id'];
        //----------------------

        $return = Ledger::getAgentTransactions($agent_id);
        if($return['status'] == true)
        {
            $transaction_list = $return['data'];
            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Successful';
            $response['data'] = $transaction_list;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Detail
     *
     * @param Request $request
     * @return Mixed
     */
    public function detail(Request $request)
    {
        $transaction_detail = null;

        // Get Request Variables
        $transaction_id = $request['id'];
        //----------------------

        $return = Ledger::getTransactionDetail($transaction_id);
        if($return['status'] == true)
        {
            $transaction_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S200';
            $response['message'] = 'Successful';
            $response['data'] = $transaction_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }


    /**
     * Add balance.
     *
     * @param Request $request
     * @return redirect
     */
    public function add(Request $request)
    {
        $input = $request->all();
        Mail::raw(json_encode($input), function($message)
        {
            $message->from('us@example.com', 'Laravel');
            $message->to('engineering@fondostech.in');
        });
        $redirect = null;
        $agent_id = null;

        // Get Request Variables
        $agent_id = $request['agent_id'];
        //----------------------

        $Ledger = new Ledger();
        $Ledger->field['LG_Lid'] = null;
        $Ledger->field['LG_AGid'] = $agent_id;
        $Ledger->field['LG_type'] = config('constant.transaction_type.RECHARGE.value');
        $Ledger->field['LG_message'] = config('constant.transaction_type.RECHARGE.message');
        $Ledger->field['LG_amount'] = $request['amount'];
        $Ledger->field['LG_note'] = $request['note'];
        $Ledger->field['LG_attachment'] = $request['receipt_photo'];
        $Ledger->field['LG_status'] = config('constant.transaction_status.PENDING.value');
        $return = $Ledger->addTransaction();
        if($return['status'] == true)
        {
            $transaction_detail = $return['data'];
            // Dealer Wallet Balance
            $dealerWalletBalance = \App\Http\Models\Ledger::getAgentAccountBalance($agent_id);
            $dealerWalletBalance = (!empty($dealerWalletBalance['data']->LG_balance)?$dealerWalletBalance['data']->LG_balance:0);

            $response['status'] = 1;
            $response['code'] = 'S300';
            $response['message'] = 'Payment added successfully';
            $response['data'] = $transaction_detail;
            $response['data']['dealer_wallet_balance'] = (float)$dealerWalletBalance;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E300';
            $response['message'] = 'Unable to add payment, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }


    /**
     * Withdrawn balance.
     *
     * @param Request $request
     * @return redirect
     */
    public function withdraw(Request $request)
    {
        $redirect = null;
        $agent_id = null;

        // Get Request Variables
        $agent_id = $request['agent_id'];
        //----------------------

        $Ledger = new Ledger();
        $Ledger->field['LG_Lid'] = null;
        $Ledger->field['LG_AGid'] = $agent_id;
        $Ledger->field['LG_type'] = config('constant.transaction_type.WITHDRAW.value');
        $Ledger->field['LG_message'] = config('constant.transaction_type.WITHDRAW.message');
        $Ledger->field['LG_amount'] = $request['amount'];
        $Ledger->field['LG_note'] = $request['note'];
        $Ledger->field['LG_status'] = config('constant.transaction_status.PENDING.value');
        $return = $Ledger->addTransaction();
        if($return['status'] == true)
        {
            $transaction_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Withdrawn request submitted successfully';
            $response['data'] = $transaction_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Unable to submit withdrawn request, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Upload Attachments
     *
     * @param Request $request
     * @return Mixed
     */
    public function upload(Request $request)
    {
//        Mail::raw(json_encode($request->all()), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
//        $fn_status = false;
//
//        //$image_name = Carbon::now()->timestamp.'.jpg';
//        //$fn_status = Storage::disk('ledger')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $request['image'])));
//		
//		$image_name = $request->file('image')->hashName();
//		$fn_status = Storage::disk('ledger')->putFileAs('', $request->file('image'), $image_name);
//		
//        if($fn_status == true)
//        {
//            $response['status'] = 1;
//            $response['code'] = 'S500';
//            $response['message'] = 'Attachment uploaded successfully';
//            $response['data'] = ['image_name' => $image_name, 'image_url' => url('transaction-attachment', ['image' => $image_name])];
//            $response['timestamp'] = Carbon::now()->toDateTimeString();
//        }
//        else
//        {
//            $response['status'] = 0;
//            $response['code'] = 'E500';
//            $response['message'] = 'Network issue, please upload again!';
//            $response['data'] = null;
//            $response['timestamp'] = Carbon::now()->toDateTimeString();
//        }
//        return response()->json($response);
        
        $fn_status = false;
        $image = $request['image'];
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('ledger')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $image)));
//        if ($request->file('image')) {
//            $image_name = 'L_' . $request->file('image')->hashName();
//            $fn_status = Storage::disk('lead')->putFileAs('', $request->file('image'), $image_name);
//        }

        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $image_name, 'image_url' => url('transaction-attachment', ['file' => $image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
}