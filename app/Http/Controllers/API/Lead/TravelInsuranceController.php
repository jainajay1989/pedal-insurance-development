<?php

namespace App\Http\Controllers\API\Lead;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Models\TravelInsurance;
use Carbon\Carbon,
    Storage;

class TravelInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function add(Request $request) {
        $input = $request->all();
//        print_r($input);
        Mail::raw(json_encode($input), function($message)
        {
            $message->from('us@example.com', 'Laravel');
            $message->to('engineering@fondostech.in');
        });
//        exit;
//        print_r($input);exit;
        try{
            if(empty($request['customer_first_name'])) { throw new \Exception('Please enter first name'); }
            if(empty($request['customer_last_name'])) { throw new \Exception('Please enter last name'); }
            if(empty($request['customer_email'])) { throw new \Exception('Please enter email'); }
            if(empty($request['customer_dob'])) { 
                throw new \Exception('Please select DOB');
            } else {
                $dob = explode('/', $request['customer_dob']);
                $dd = $dob[0];
                $mm = $dob[1];
                $yy = $dob[2];
                if(!checkdate($mm, $dd, $yy)) {
                    throw new \Exception('Please select valid DOB');
                }
            }
            if(empty($request['customer_mobile'])) { throw new \Exception('Please enter mobile number'); }
            if(empty($request['customer_address'])) { throw new \Exception('Please enter customer address'); }
            if(empty($request['customer_state'])) { throw new \Exception('Please select customer state'); }
            if(empty($request['customer_city'])) { throw new \Exception('Please select customer city'); }
            if(empty($request['customer_zip'])) { throw new \Exception('Please enter pincode'); }
           
            if(empty($request['policy_for'])) { throw new \Exception('Please select whom'); }
            if(empty($request['dest_country'])) { throw new \Exception('Please select whom'); }
            if($request['policy_for'] != 'Student Policy') {
                if(empty($request['for_whom'])) { throw new \Exception('Please select for whom'); }
            }
            if(empty($request['policy_holder_details'])) { throw new \Exception('Please select policy holder details'); }
            if($request['policy_for'] != 'Student Policy') {
                if(empty($request['annual_trip'])) { throw new \Exception('Please select annual trip'); }
            }
            if(!empty($request['annual_trip']) && $request['annual_trip'] == 'Yes' && empty($request['longest_trip_duration'])) { 
                throw new \Exception('Please select trip duration'); 
            } elseif($request['annual_trip'] == 'No') {
                if(empty($request['annual_trip_start'])) { throw new \Exception('Please enter trip start date'); }
                if(empty($request['annual_trip_end'])) { throw new \Exception('Please enter trip end date'); }
                if(empty($request['annual_trip_days'])) { throw new \Exception('Please enter trip days'); }
            }
            if(empty($request['communication_state'])) { throw new \Exception('Please select communication state'); }
            if(empty($request['sum_insured'])) { throw new \Exception('Please enter sum insured'); }
            if(empty($request['insurance_company'])) { throw new \Exception('Please select insurance company'); }
            if(empty($request['estimated_premium'])) { throw new \Exception('Please enter estimated premium'); }
            if(empty($request['departed_from_india'])) { throw new \Exception('Please select whether departed from India or Not'); }
            
            // Insert Lead
            $travel = new TravelInsurance;
            $travel->field['L_AGid'] = $request['agent_id'];
            $travel->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $travel->field['L_customerFirstName'] = $request['customer_first_name'];
            $travel->field['L_customerLastName'] = $request['customer_last_name'];
            $travel->field['L_customerEmail'] = $request['customer_email'];
            $travel->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString() : null);
            $travel->field['L_customerMobile'] = $request['customer_mobile'];
            $travel->field['L_customerPhone'] = $request['customer_phone'];
            $travel->field['L_customerAddress'] = $request['customer_address'];
            $travel->field['L_customerCity'] = $request['customer_city'];
            $travel->field['L_customerState'] = $request['customer_state'];
            $travel->field['L_customerZip'] = $request['customer_zip'];
            
            //Vehicle Details
            $travel->field['policy_for'] = $request['policy_for'];
            $travel->field['dest_country'] = $request['dest_country'];
            $travel->field['for_whom'] = $request['for_whom'];
            $travel->field['policy_holder_details'] = json_encode($request['policy_holder_details']);
            $travel->field['annual_trip'] = $request['annual_trip'];
            $travel->field['longest_trip_duration'] = $request['longest_trip_duration'];
            $travel->field['annual_trip_start'] = ($request['annual_trip_start'] ? Carbon::createFromFormat('j/n/Y', $request['annual_trip_start'])->toDateString() : null);
            $travel->field['annual_trip_end'] = ($request['annual_trip_end'] ? Carbon::createFromFormat('j/n/Y', $request['annual_trip_end'])->toDateString() : null);
            $travel->field['annual_trip_days'] = $request['annual_trip_days'];
            $travel->field['communication_state'] = $request['communication_state'];
            $travel->field['sum_insured'] = $request['sum_insured'];
            $travel->field['new_policy_company'] = $request['insurance_company'];
            $travel->field['new_policy_product'] = $request['new_policy_product'];
            $travel->field['estimated_premium'] = $request['estimated_premium'];
            $travel->field['departed_from_india'] = $request['departed_from_india'];
            $travel->field['status'] = config('constant.lead_status.NEW.value');
            /* End */
//            print_r($travel);exit;
            $return = $travel->add();
            
            /* Used to add zip code */
            $cityPincode = new \App\Http\Models\CityPincode();
            $cityPincode->field['CP_city_id'] = $request['customer_city'];
            $cityPincode->field['CP_pincode'] = $request['customer_zip'];
            $cityPincode->addCityPincodes();
            /* Used to add zip code */
        
            if ($return['status'] == true) {
                $lead_detail = $return['data'];
                $lead_detail->L_appPostData = NULL;
                $response['status'] = 1;
                $response['code'] = 'S300';
                $response['message'] = 'Lead added successfully';
                $response['timestamp'] = Carbon::now()->toDateTimeString();
            } else {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = 'Unable to add lead, please try again!';
                $response['timestamp'] = Carbon::now()->toDateTimeString();
            }
            
        } catch (\Exception $ex) {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = $ex->getMessage();
                $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
}
