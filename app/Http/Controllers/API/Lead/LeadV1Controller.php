<?php

namespace App\Http\Controllers\API\Lead;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Controllers\Lead\MotorInsuranceController;
use App\Http\Models\Lead;
use App\Http\Models\InsuranceCompany;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\State;
use App\Http\Models\City;
use App\Http\Models\Country;
use App\Http\Models\CityPincode;
use App\Http\Models\PayuLog;
use \App\Http\Models\Vehicle;
use \App\Http\Models\Agent;
use Carbon\Carbon,
    Storage;

class LeadV1Controller extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Index
     *
     * @param Request $request
     * @return Mixed
     */
    public function index(Request $request) {

        $lead_list = null;

        // Get Request Variables
        $input = $request->all();
                
//        Mail::raw(json_encode($input), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
        $agentId = $input['agent_id'];
        $agentType = $input['ag_type'];
        
        //----------------------
        $agentTypeConst = config('constant.agent_type.DEALER.value');
        if($agentType == $agentTypeConst) {
            $agentList = Agent::getRMAgentList($agentId);
            if(!empty($agentList)) {
                foreach($agentList as $key => $val) {
                    $dealers[] = $val['AG_id'];
                }
            }
        } else {
            $dealers[] = $agentId;
        }
        $return = Lead::getAgentLeads($dealers, $input);
        if ($return['status'] == true) {
            $lead_list = $return['data'];
            $leadList = array();
            $leadStatus = config('constant.lead_policy_status');
            foreach($lead_list as $key => $val) {
                $leadList[$key]['L_id'] = (!empty($val->L_id)?$val->L_id:NULL);
                $leadList[$key]['L_AGid'] = (!empty($val->L_AGid)?$val->L_AGid:NULL);
                $leadList[$key]['L_customerFirstName'] = (!empty($val->L_customerFirstName)?$val->L_customerFirstName:NULL);
                $leadList[$key]['L_customerLastName'] = (!empty($val->L_customerLastName)?$val->L_customerLastName:NULL);
                $leadList[$key]['L_policyType'] = (!empty($val->L_policyType)?$val->L_policyType:NULL);
                if($val->L_policyType=='PEDAL_CYCLE') {
                    $leadList[$key]['L_status'] = (!empty($val->L_status)?$leadStatus[$val->L_status]:$leadStatus[1]);
                } elseif($val->L_policyType=='MOTOR') {
                    $leadList[$key]['L_status'] = (!empty($val->motorInsurance->status)?$leadStatus[$val->motorInsurance->status]:$leadStatus[1]);
                } elseif($val->L_policyType=='HEALTH') {
                    $leadList[$key]['L_status'] = (!empty($val->healthInsurance->status)?$leadStatus[$val->healthInsurance->status]:$leadStatus[1]);
                } elseif($val->L_policyType=='TRAVEL') {
                    $leadList[$key]['L_status'] = (!empty($val->travelInsurance->status)?$leadStatus[$val->travelInsurance->status]:$leadStatus[1]);
                } else {
                    $leadList[$key]['L_status'] = (!empty($val->otherInsurance->status)?$leadStatus[$val->otherInsurance->status]:$leadStatus[1]);
                }
                
            }
            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Successful';
            $response['data'] = $leadList;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Detail
     *
     * @param Request $request
     * @return Mixed
     */
    public function detail(Request $request) {
        $lead_detail = null;

        // Get Request Variables
        $lead_id = $request['id'];
        //----------------------

        $return = Lead::getLeadDetail($lead_id);
        if ($return['status'] == true) {
            $appUrl = env('APP_URL');
            $lead_detail = $return['data'];
            if($lead_detail->L_policyType == 'PEDAL_CYCLE') {
                $completeOrderDetail = (!empty($lead_detail->L_completeLeadDetails)?json_decode($lead_detail->L_completeLeadDetails, true):NULL);
                $accessories = (!empty($lead_detail->L_vehicleAccessoryDetails)?json_decode($lead_detail->L_vehicleAccessoryDetails, true):NULL);
                if(!empty($accessories)) {
                    foreach($accessories as $key => $val) {
                        $accessories[$key]['accessory_image'] = (!empty($val['accessory_image'])?$appUrl.'lead-attachment?file='.$val['accessory_image']:NULL);
                    }
                }
                $appPostedData = (!empty($lead_detail->L_appPostData)?json_decode($lead_detail->L_appPostData, true):NULL);
    //            print_r($appPostedData);
                $lead_detail->L_vehicleMake = (!empty($appPostedData['vehicle_make_text'])?$appPostedData['vehicle_make_text']:NULL);
                $lead_detail->L_vehicleModel = (!empty($appPostedData['vehicle_model_text'])?$appPostedData['vehicle_model_text']:NULL);
                $lead_detail->L_customerIncome = (!empty($appPostedData['customer_income'])?(float)$appPostedData['customer_income']:NULL);
                $lead_detail->L_customerDOB = (!empty($appPostedData['customer_dob'])?$appPostedData['customer_dob']:NULL);
                $lead_detail->L_vehicleAccessoryDetails = $accessories;
                $lead_detail->L_vehicleInvoicePhoto = (!empty($lead_detail->L_vehicleInvoicePhoto)?$appUrl.'/lead-attachment?file='.$lead_detail->L_vehicleInvoicePhoto:NULL);
                $lead_detail->L_vehicleChassisPhoto = (!empty($lead_detail->L_vehicleChassisPhoto)?$appUrl.'/lead-attachment?file='.$lead_detail->L_vehicleChassisPhoto:NULL);
                $lead_detail->L_purchaseDate = $completeOrderDetail['PurchaseDate'];
                $lead_detail->L_customerState = $completeOrderDetail['CustmerObj']['StateName'];
                $lead_detail->L_customerCity = $completeOrderDetail['CustmerObj']['CityDistrictName'];
                $lead_detail->L_nomineeName = $completeOrderDetail['NomineeFirstName'];
                $lead_detail->L_nomineeAge = (int)$appPostedData['new_policy_nominee_age'];
                $lead_detail->L_nomineeRelation = $completeOrderDetail['NomineeRelationship'];
                $lead_detail->L_customerCity = $completeOrderDetail['CustmerObj']['CityDistrictName'];
                $lead_detail->L_vehicleSumInsured = $completeOrderDetail['TotalSumInsured'];
                $agentDetail = Agent::getAgentDetailByFields($lead_detail->L_AGid, array('AG_firstName', 'AG_lastName'));
                $agentName = $agentDetail['AG_firstName'].' '.$agentDetail['AG_lastName'];
                $lead_detail->L_agentName = $agentName;
                $lead_detail->L_completeLeadDetails = NULL;
                $lead_detail->L_customerOccupation = (!empty($appPostedData['customer_occupation'])?$appPostedData['customer_occupation']:NULL);
                $lead_detail->L_guardianName = (!empty($appPostedData['new_policy_guardian_name'])?$appPostedData['new_policy_guardian_name']:NULL);
                $lead_detail->L_guardianAge = (!empty($appPostedData['guardian_age'])?(int)$appPostedData['guardian_age']:NULL);
                $lead_detail->L_guardianRelation = (!empty($appPostedData['new_policy_guardian_relation'])?$appPostedData['new_policy_guardian_relation']:NULL);
            } elseif($lead_detail->L_policyType == 'MOTOR') {
                $lead_detail = $this->getMotorLeadDetail($lead_detail);
            } elseif($lead_detail->L_policyType == 'HEALTH') {
                $lead_detail = $this->getHealthLeadDetail($lead_detail);
            } elseif($lead_detail->L_policyType == 'TRAVEL') {
                $lead_detail = $this->getTravelLeadDetail($lead_detail);
            } else {
                $lead_detail = $this->getOtherLeadDetail($lead_detail);
            }
            $response['status'] = 1;
            $response['code'] = 'S200';
            $response['message'] = 'Successful';
            $response['data'] = $lead_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function add(Request $request) {
        
        $input = $request->all();
//        print_r($input);
//        Mail::raw(json_encode($input), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
        try{
            if(empty($request['customer_first_name'])) { throw new \Exception('Please enter first name'); }
            if(empty($request['customer_last_name'])) { throw new \Exception('Please enter last name'); }
            if(empty($request['customer_email'])) { throw new \Exception('Please enter email'); }
            if(empty($request['customer_dob'])) { 
                throw new \Exception('Please select DOB');
            } else {
                $dob = explode('/', $request['customer_dob']);
                $dd = $dob[0];
                $mm = $dob[1];
                $yy = $dob[2];
                if(!checkdate($mm, $dd, $yy)) {
                    throw new \Exception('Please select valid DOB');
                }
            }
            if(empty($request['customer_mobile'])) { throw new \Exception('Please enter mobile number'); }
            if(empty($request['customer_address'])) { throw new \Exception('Please enter customer address'); }
            if(empty($request['customer_state'])) { throw new \Exception('Please select customer state'); }
            if(empty($request['customer_city'])) { throw new \Exception('Please select customer city'); }
            if(empty($request['customer_zip'])) { throw new \Exception('Please enter pincode'); }
            if(empty($request['customer_income'])) { throw new \Exception('Please enter customer income'); }
            if(empty($request['customer_occupation'])) { throw new \Exception('Please select customer occupation'); }
            //Cycle Details
            if(empty($request['vehicle_make'])) { throw new \Exception('Please select make'); }
            if(empty($request['vehicle_model'])) { throw new \Exception('Please select model'); }
            if(empty($request['vehicle_chassis_number'])) { throw new \Exception('Please enter chassis number'); }
            if(empty($request['vehicle_year'])) { throw new \Exception('Please select manufacturing year'); }
            if(empty($request['vehicle_purchase_date'])) { throw new \Exception('Please select purchase date'); }
            if(empty($request['vehicle_invoice_number'])) { throw new \Exception('Please enter invoice number'); }
            if(empty($request['vehicle_net_price'])) { throw new \Exception('Please enter cycle price'); }
            if(empty($request['vehicle_invoice_photo'])) { throw new \Exception('Please capture invoice photo'); }
            if(empty($request['vehicle_chassis_photo'])) { throw new \Exception('Please capture chassis photo'); }
            if((empty($request['vehicle_cgst']) || empty($request['vehicle_sgst'])) && empty($request['vehicle_igst'])) { 
                throw new \Exception('Please check GST amount'); 
            }
            if(empty($request['vehicle_gross_price'])) { 
                throw new \Exception('Please enter gross price');
            } else {
                $totalPrice = ($request['vehicle_gross_price']-$request['vehicle_cgst']-$request['vehicle_sgst']-$request['vehicle_igst']);
                $minCyclePrice = config('constant.calculation_config.MIN_PURCHASE_PRICE');
                $maxCyclePrice = config('constant.calculation_config.MAX_PURCHASE_PRICE');
                if($totalPrice < $minCyclePrice || $totalPrice > $maxCyclePrice) {
                    throw new \Exception("New Lead Can Only Be Generated If Cycle Purchase Price Is Between Rs.$minCyclePrice And Rs.$maxCyclePrice.");
                }
            }
            if(empty($request['vehicle_sum_insured'])) { throw new \Exception('Please enter Bicyle sum insured'); }
            //Add On Cover
            if(empty($request['new_policy_coverage'])) { throw new \Exception('Please select policy tenure'); }
            if(empty($request['new_policy_basic_cover'])) { throw new \Exception('Please enter basic cover'); }
            if(!empty($request['new_policy_ad'])) { 
                if(empty($request['new_policy_nominee_name'])) { 
                    throw new \Exception('Please enter nominee full name'); 
                } else {
                    $nomineeFirstName= strstr($request['new_policy_nominee_name'], ' ', true);
                    $nomineeLastName = trim(strstr($request['new_policy_nominee_name'], ' ', false));
                    if(empty($nomineeLastName)) {
                        throw new \Exception('Please enter nominee full name'); 
                    }
                }
                if(empty($request['new_policy_nominee_relation'])) { throw new \Exception('Please select nominee relation'); }
                if(empty($request['new_policy_nominee_age'])) { 
                    throw new \Exception('Please select nominee DOB'); 
                } elseif($request['new_policy_nominee_age'] < 18) {
                    if(empty($request['new_policy_guardian_name'])) { 
                        throw new \Exception('Please enter guarian name'); 
                    } else {
                        $repFirstName= strstr($request['new_policy_guardian_name'], ' ', true);
                        $repLastName = strstr($request['new_policy_guardian_name'], ' ', false);
                        if(empty($repLastName)) {
                            throw new \Exception('Please enter guardian full name'); 
                        }
                    }
                    if(empty($request['new_policy_guardian_relation'])) { throw new \Exception('Please select guardian relation'); }
                    if(empty($request['guardian_age'])) { throw new \Exception('Please select guardian age'); }
                }
            }
            if(empty($request['new_policy_ad_cover'])) { throw new \Exception('Please enter PA cover amount'); }
            if(empty($request['new_policy_ad_premium'])) { throw new \Exception('Please enter PA premium amount'); }
            if(!empty($request['new_policy_pl'])) { 
                if(empty($request['new_policy_pl_cover'])) { throw new \Exception('Please enter PL cover amount'); }
                if(empty($request['new_policy_pl_premium'])) { throw new \Exception('Please enter PL premium amount'); }
            }
            if(empty($request['new_policy_net_premium'])) { throw new \Exception('Please enter premium amount'); }
            if((empty($request['new_policy_cgst_premium']) || empty($request['new_policy_sgst_premium'])) && empty($request['new_policy_igst_premium'])) { 
                throw new \Exception('Please check premium GST amount'); 
            }
            if(empty($request['new_policy_total_premium'])) { throw new \Exception('Please enter total premium'); }

            $lead_detail = null;
            // Insert Lead
            $Lead = new Lead();
            $Lead->field['L_AGid'] = $request['agent_id'];
            $Lead->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $Lead->field['L_customerFirstName'] = $request['customer_first_name'];
            $Lead->field['L_customerLastName'] = $request['customer_last_name'];
            $Lead->field['L_customerEmail'] = $request['customer_email'];
            $Lead->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString() : null);
            $Lead->field['L_customerMobile'] = $request['customer_mobile'];
            $Lead->field['L_customerPhone'] = $request['customer_phone'];
            $Lead->field['L_customerAddress'] = $request['customer_address'];
            $Lead->field['L_customerCity'] = $request['customer_city'];
            $Lead->field['L_customerState'] = $request['customer_state'];
            $Lead->field['L_customerZip'] = $request['customer_zip'];
            //Vehicle Details
            $Lead->field['L_vehicleMake'] = $request['vehicle_make'];
            $Lead->field['L_vehicleModel'] = $request['vehicle_model'];
            $Lead->field['L_vehicleYear'] = $request['vehicle_year'];
            $Lead->field['L_vehicleEngineNo'] = $request['vehicle_engine_number'];
            $Lead->field['L_vehicleChassisNo'] = $request['vehicle_chassis_number'];
            if(!empty($request['vehicle_invoice_photo'])) {
//                $imgUpload = $this->upload($request['vehicle_invoice_photo'], 'invoice_');
//                if($imgUpload['code'] =='S400') {
//                    $request['vehicle_invoice_photo'] = $imgUpload['data']['image_name'];
//                } else {
//                    throw new Exception('Error occured while uploading invoice photo');
//                }
                $imgUpload = 'invoice_'.$request['vehicle_invoice_photo'];
            }
            if(!empty($request['vehicle_chassis_photo'])) {
//                $imgUpload = $this->upload($request['vehicle_chassis_photo'], 'chassis_');
//                if($imgUpload['code'] == 'S400') {
//                    $request['vehicle_chassis_photo'] = $imgUpload['data']['image_name'];
//                } else {
//                    throw new Exception('Error occured while uploading invoice photo');
//                }
                $imgUpload = 'chassis_'.$request['vehicle_chassis_photo'];
            }
            $Lead->field['L_vehicleInvoicePhoto'] = $request['vehicle_invoice_photo'];
            $Lead->field['L_vehicleChassisPhoto'] = $request['vehicle_chassis_photo'];
            $Lead->field['L_vehicleAccessories'] = $request['vehicle_accessories'];
            //Accessories Details
            
            $accessoryDetails = (!empty($request['vehicle_accessories_details'])?$request['vehicle_accessories_details']:array());
            $accessoryList = array();
            $electricAcsList = array('ElectricalAccessories' => 'No');
            $nonElectricAcsList = array('NonElectricalAccessories' => 'No');
            $electricAccPrice = 0;
            $nonElectricAccPrice = 0;
            if(!empty($accessoryDetails)) {
                foreach($accessoryDetails as $akey => $aval) {
                    if($aval['accessory_type'] == 'Electrical') {
                        $electricAcsList['ElectricalAccessories'] = 'Yes';
                        $electricAcsList['lstAccessories'][] = array(
                            'Description' => $aval['accessory_detail'],
    //                        'PurchasePrice' => $aval['accessory_price'],
//                            'PurchaseDate' => date('d/m/Y')
                        );
//                        $electricAcsList['TotalElectricalAccessoriesSI'] = (!empty($electricAcsList['TotalElectricalAccessoriesSI'])?($electricAcsList['TotalElectricalAccessoriesSI']+$aval['accessory_price']):$aval['accessory_price']); //OLD working code commented on 01-10-2018
                        $electricAccPrice = (!empty($electricAccPrice)?($electricAccPrice+$aval['accessory_price']):$aval['accessory_price']);
                    } elseif($aval['accessory_type'] == 'Non Electrical') {
                        $nonElectricAcsList['NonElectricalAccessories'] = 'Yes';
                        $nonElectricAcsList['lstNonElecAccessories'][] = array(
                            'Description' => $aval['accessory_detail'],
    //                        'PurchasePrice' => $aval['accessory_price'],
//                            'PurchaseDate' => date('d/m/Y')
                        );
//                        $nonElectricAcsList['TotalNonElectricalAccessoriesSI'] = (!empty($nonElectricAcsList['TotalNonElectricalAccessoriesSI'])?($nonElectricAcsList['TotalNonElectricalAccessoriesSI']+$aval['accessory_price']):$aval['accessory_price']); //OLD working code commented on 01-10-2018
                        $nonElectricAccPrice = (!empty($nonElectricAccPrice)?($nonElectricAccPrice+$aval['accessory_price']):$aval['accessory_price']);
                    }
//                    $imgUpload = $this->upload($aval['accessory_image'], "accessory$akey"."_");
//                    if($imgUpload['code'] == 'S400') {
//                        $aval['accessory_image'] = $imgUpload['data']['image_name'];
//                        $accessoryList[$akey] = $aval;
//                    } else {
//                        throw new Exception('Error occured while uploading invoice photo');
//                    }
                    $imgUpload = "accessory$akey"."_".$aval['accessory_image'];
                    $accessoryList[$akey] = $aval;
                }
                $electricAcsList['TotalElectricalAccessoriesSI'] = (!empty($electricAccPrice)?($electricAccPrice*95)/100:0.00);
                $nonElectricAcsList['TotalNonElectricalAccessoriesSI'] = (!empty($nonElectricAccPrice)?($nonElectricAccPrice*95)/100:0.00);
            }
            $new_policy_cgst_premium = number_format($request['new_policy_cgst_premium'], 2, '.', '');
            $new_policy_sgst_premium = number_format($request['new_policy_sgst_premium'], 2, '.', '');
            $new_policy_igst_premium = number_format($request['new_policy_igst_premium'], 2, '.', '');
            $new_policy_net_premium = number_format($request['new_policy_net_premium'], 2, '.', '');
            $new_policy_total_premium = number_format($request['new_policy_total_premium'], 0, '.', '');

            $Lead->field['L_vehicleAccessoryDetails'] = json_encode($accessoryList);
            $Lead->field['L_vehicleNetPrice'] = $request['vehicle_net_price'];
            $Lead->field['L_vehicleCGST'] = $request['vehicle_cgst'];
            $Lead->field['L_vehicleSGST'] = $request['vehicle_sgst'];
            $Lead->field['L_vehicleIGST'] = $request['vehicle_igst'];
            $Lead->field['L_vehicleGrossPrice'] = $request['vehicle_gross_price'];
            $Lead->field['L_newPolicyCoverage'] = $request['new_policy_coverage'];

            $Lead->field['L_newPolicyAD'] = $request['new_policy_ad'];
            $Lead->field['L_newPolicyADCover'] = $request['new_policy_ad_cover'];
            $Lead->field['L_newPolicyADCoverPremium'] = $request['new_policy_ad_premium'];
            $Lead->field['L_newPolicyPL'] = $request['new_policy_pl'];
            $Lead->field['L_newPolicyPLCover'] = $request['new_policy_pl_cover'];
            $Lead->field['L_newPolicyPLPremium'] = $request['new_policy_pl_premium'];
            $Lead->field['L_newPolicyBasicCover'] = $request['new_policy_basic_cover'];

            $Lead->field['L_newPolicyCGSTPremium'] = $new_policy_cgst_premium;
            $Lead->field['L_newPolicySGSTPremium'] = $new_policy_sgst_premium;
            $Lead->field['L_newPolicyIGSTPremium'] = $new_policy_igst_premium;
            $Lead->field['L_newPolicyNetPremium'] = $new_policy_net_premium;
            $Lead->field['L_newPolicyTotalPremium'] = $new_policy_total_premium;
            $Lead->field['L_newPolicyPaymentMode'] = $request['new_policy_payment_mode'];
            $Lead->field['L_status'] = config('constant.lead_status.NEW.value');

            $Lead->field['L_newPolicyNumber'] = $request['new_policy_number'];
            $Lead->field['L_newPolicyStartDate'] = null;
            $Lead->field['L_newPolicyEndDate'] = null;
            $Lead->field['L_newPolicyAttachment'] = $request['new_policy_attachment'];
            $Lead->field['L_invoiceNo'] = $request['vehicle_invoice_number'];
            $Lead->field['L_location'] = $request['location'];
            $Lead->field['L_read'] = 0;

            /* Using to save all posted data from App */
            $input['vehicle_invoice_photo'] = NULL;
            $input['vehicle_chassis_photo'] = NULL;
            $input['vehicle_accessories_details'] = json_encode($accessoryList);;
            $Lead->field['L_appPostData'] = json_encode($input);
            /* End */
            $return = $Lead->addLead();
            if ($return['status'] == true) {
                /*
                * Added by Ajay Jain
                * To save the send create data modal for PayU
                */
                $leadId = $return['data']->L_id;
                $policyDetails = array();
                $policyDetails['Salutation'] = $request['customer_salutation'];
                $policyDetails['FirstName'] = $request['customer_first_name'];
                $policyDetails['LastName'] = $request['customer_last_name'];
                $policyDetails['EmailID'] = $request['customer_email'];
                $policyDetails['DOB'] = $request['customer_dob'];
                $policyDetails['MobileNumber'] = $request['customer_mobile'];
                $policyDetails['AddressLine1'] = $request['customer_address'];
                $policyDetails['StateName'] = $request['customer_state_text'];
                $policyDetails['Area'] = $request['customer_city_text'];
                $policyDetails['CityDistrictName'] = $request['customer_city_text'];
                $policyDetails['PinCode'] = $request['customer_zip'];
                $policyDetails['MailingPinCodeLocality'] = $request['customer_city_text'];
                $policyDetails['MailingPinCode'] = $request['customer_zip'];
                $policyDetails['BuyerState'] = $request['customer_state_text'];
                $policyDetails['InvoiceNo'] = $request['vehicle_invoice_number'];

                $policyDetails['NomineeFirstName'] = (!empty($nomineeFirstName)?$nomineeFirstName:$request['new_policy_nominee_name']);
                $policyDetails['NomineelastName'] = (!empty($nomineeLastName)?$nomineeLastName:'');
                $policyDetails['NomineeAge'] = (!empty($request['new_policy_nominee_age'])?$request['new_policy_nominee_age']:'');
                $policyDetails['NomineeRelationship'] = (!empty($request['new_policy_nominee_relation'])?$request['new_policy_nominee_relation']:'');
                $policyDetails['IsMinor'] = (!empty($request['new_policy_nominee_age']) && $request['new_policy_nominee_age'] < 18)?"true":"false";
                $policyDetails['RepFirstName'] = (!empty($repFirstName)?$repFirstName:$request['new_policy_guardian_name']);
                $policyDetails['RepLastName'] = (!empty($repLastName)?$repLastName:'');
                $policyDetails['RepRelationWithMinor'] = (!empty($request['new_policy_guardian_relation'])?$request['new_policy_guardian_relation']:'');

                $policyDetails['PolicyTenure'] = $request['new_policy_coverage'];
                $policyDetails['TotalPremium'] = $new_policy_total_premium;//$request['new_policy_total_premium'];
                $policyDetails['SGST'] = $new_policy_sgst_premium;//(!empty($request['new_policy_sgst_premium'])?$request['new_policy_sgst_premium']:"");
                $policyDetails['CGST'] = $new_policy_cgst_premium;//(!empty($request['new_policy_cgst_premium'])?$request['new_policy_cgst_premium']:"");
                $policyDetails['IGST'] = $new_policy_igst_premium;//(!empty($request['new_policy_igst_premium'])?$request['new_policy_igst_premium']:"");
                $policyDetails['NetPremium'] = $new_policy_net_premium;//$request['new_policy_net_premium'];

                $policyDetails['Make'] = $request['vehicle_make'];
                $policyDetails['Model'] = $request['vehicle_modelcode'];
                //$policyDetails['PurchasePrice'] = ($request['vehicle_gross_price']-$request['vehicle_cgst']-$request['vehicle_sgst']-$request['vehicle_igst']); // Old Logic of calculating total price after deducting GST taxes
                $policyDetails['PurchasePrice'] = round($request['vehicle_net_price'] + $electricAccPrice + $nonElectricAccPrice);
                $policyDetails['SerialNoofFrame'] = $request['vehicle_chassis_number'];
                $policyDetails['BicycleSumInsured'] = ($request['vehicle_net_price']*95)/100;
                $policyDetails['TotalSumInsured'] = $request['vehicle_sum_insured'];

                $policyDetails['ManfYear'] = $request['vehicle_year'];

                $rateCalculator = \App\Http\Models\RateCalculator::getRateCalculator($request['new_policy_coverage'], $totalPrice);
                $BurglaryorHouseBreakingorTheftUpto75Premium = ($request['vehicle_sum_insured']*$rateCalculator->RC_burglury)/100;
                $BurglaryorHouseBreakingorTheftUpto75Premium = number_format($BurglaryorHouseBreakingorTheftUpto75Premium, 2, '.', '');
                $LossorDamagetoInsuredBicyclePremium = ($request['vehicle_sum_insured']*$rateCalculator->RC_lossDamage)/100;
                $LossorDamagetoInsuredBicyclePremium = number_format($LossorDamagetoInsuredBicyclePremium, 2, '.', '');
                $PermanentTotalDisabilitySumInsured = (float)$rateCalculator->RC_ptd;
                $policyDetails['LongTermPolicyDiscountPremium'] = number_format($request['new_policy_lt_discount_premium'], 2, '.', '');
                $policyDetails['BurglaryorHouseBreakingorTheftUpto75SumInsured'] = $request['vehicle_sum_insured'];
                $policyDetails['BurglaryorHouseBreakingorTheftUpto75Premium'] = $BurglaryorHouseBreakingorTheftUpto75Premium;
                $policyDetails['LossorDamagetoInsuredBicycleSumInsured'] = $request['vehicle_sum_insured'];
                $policyDetails['LossorDamagetoInsuredBicyclePremium'] = $LossorDamagetoInsuredBicyclePremium;
                $policyDetails['PermanentTotalDisabilitySumInsured'] = $PermanentTotalDisabilitySumInsured;
                $policyDetails['AccidentalDeathSumInsured'] = $request['new_policy_ad_cover'];
                $policyDetails['AccidentalDeathPremium'] = $request['new_policy_ad_premium'];
                $policyDetails['PublicLiabilitySumInsured'] = $request['new_policy_pl_cover'];
                $policyDetails['PublicLiabilityPremium'] = $request['new_policy_pl_premium'];
                $fields = array('AG_firstName', 'AG_lastName', 'AG_city', 'AG_code', 'AG_IMDcode');
                $agentDetails = Agent::getAgentDetailByFields($request['agent_id'], $fields);
                if(!empty($agentDetails)) {
                    $policyDetails['OutletName'] = $agentDetails['AG_firstName'].' '.$agentDetails['AG_lastName'];
                    $policyDetails['PlaceOfPurchase'] = (!empty($agentDetails['AG_city'])?$agentDetails['AG_city']:'');
                    $policyDetails['DealerCode'] = $agentDetails['AG_code'];
                    $policyDetails['IMDNumber'] = $agentDetails['AG_IMDcode'];
                }
                $policyDetails['PurchaseDate'] = $request['vehicle_purchase_date'];
                $policyDetails['DeliveryDate'] = $request['vehicle_purchase_date'];

                $policyDetails['Occupation'] = $request['customer_occupation'];
                $policyDetails['MonthlyIncome'] = $request['customer_income'];

                $policyFieldsArr = array_merge($policyDetails, $electricAcsList, $nonElectricAcsList);
                $this->getPolicyPedal($leadId, $policyFieldsArr);
                /* Ends */
                //------------

                if ($return['status'] == true) {
                    $lead_detail = $return['data'];
                    $lead_detail->L_appPostData = NULL;
                    $response['status'] = 1;
                    $response['code'] = 'S300';
                    $response['message'] = 'Lead added successfully';
                    $response['timestamp'] = Carbon::now()->toDateTimeString();
                } else {
                    $response['status'] = 0;
                    $response['code'] = 'E300';
                    $response['message'] = 'Unable to add lead, please try again!';
                    $response['timestamp'] = Carbon::now()->toDateTimeString();
                }
            }
            
        } catch (\Exception $ex) {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = $ex->getMessage();
                $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }

    /**
     * Upload Attachments
     *
     * @param Request $request
     * @return Mixed
     */
//    public function upload($imageName = '', $slug = '') {
//        $fn_status = false;
//        $image_name = md5(microtime()) . '.jpg';
//        $fn_status = Storage::disk('lead')->put($slug.$image_name, base64_decode(str_replace('data:image/png;base64,', '', $imageName)));
//        if ($fn_status == true) {
//            $response['status'] = 1;
//            $response['code'] = 'S400';
//            $response['message'] = 'Image uploaded successfully';
//            $response['data'] = ['image_name' => $slug.$image_name, 'image_url' => url('lead-attachment', ['file' => $slug.$image_name])];
//            $response['timestamp'] = Carbon::now()->toDateTimeString();
//        } else {
//            $response['status'] = 0;
//            $response['code'] = 'E400';
//            $response['message'] = 'Network issue, please upload again!';
//            $response['data'] = null;
//            $response['timestamp'] = Carbon::now()->toDateTimeString();
//        }
//        return $response;
//    }

    /**
     * Upload Attachments
     *
     * @param Request $request
     * @return Mixed
     */
    public function upload(Request $request) {
        $fn_status = false;
        $image = $request['image'];
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('lead')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $image)));
//        if ($request->file('image')) {
//            $image_name = 'L_' . $request->file('image')->hashName();
//            $fn_status = Storage::disk('lead')->putFileAs('', $request->file('image'), $image_name);
//        }

        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $image_name, 'image_url' => url('lead-attachment', ['file' => $image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }

    /*
     * @Date        : 14 June, 2017
     * @CreatedBy   : SG
     * @Purpose     : Restaurant Signin
     * @Param       : 
     * @Return      : 
     */

//    public functio PolicyPedal(Request $request) {
    public function getPolicyPedal($leadId = 0, $request = array()) {
//        print_r($request);
//        $input = $request->all();
////        Mail::raw(json_encode($input), function($message)
////        {
////            $message->from('us@example.com', 'Laravel');
////            $message->to('engineering@fondostech.in');
////        });
////        exit;
        try {
            $QuotationNumber = (!empty($request['QuotationNumber']) ? $request['QuotationNumber'] : substr(hash('sha256', mt_rand() . microtime()), 0, 10));
            $PaymentSource = (!empty($request['PaymentSource']) ? $request['PaymentSource'] : config('constant.PaymentSource'));
            $PaymentDate = (!empty($request['PaymentDate']) ? $request['PaymentDate'] : '');
            $TransactionID = '';
            $TPSourceName = (!empty($request['TPSourceName']) ? $request['TPSourceName'] : config('constant.TPSourceName'));
            $TPEmailID = (!empty($request['TPEmailID']) ? $request['TPEmailID'] : config('constant.TPEmailID'));
            $SendEmailtoCustomer = (!empty($request['SendEmailtoCustomer']) ? $request['SendEmailtoCustomer'] : true);
            $OTP = (isset($request['OTP']) ? $request['OTP'] : '123456');
            $OTPcreatedDate = (!empty($request['OTPcreatedDate']) ? $request['OTPcreatedDate'] : date('d/m/Y H:i:s'));
            $OTPEnteredDate = (!empty($request['OTPEnteredDate']) ? $request['OTPEnteredDate'] : date('d/m/Y H:i:s'));
            $dealerCode = (!empty($request['DealerCode']) ? $request['DealerCode'] : '');
            $BiCycleType = (!empty($request['BiCycleType']) ? $request['BiCycleType'] : config('constant.BiCycleType'));
            $Purchased = (!empty($request['Purchased']) ? $request['Purchased'] : config('constant.Purchased'));
            $ElectricalAccessories = $request['ElectricalAccessories'];
            $TotalElectricalAccessoriesSI = (!empty($request['TotalElectricalAccessoriesSI'])?$request['TotalElectricalAccessoriesSI']:0.00);
            $lstAccessories = (!empty($request['lstAccessories']) ? $request['lstAccessories'] : array());
            $NonElectricalAccessories = $request['NonElectricalAccessories'];
            $TotalNonElectricalAccessoriesSI = (!empty($request['TotalNonElectricalAccessoriesSI'])?$request['TotalNonElectricalAccessoriesSI']:0.00);
            $lstNonElecAccessories = (!empty($request['lstNonElecAccessories']) ? $request['lstNonElecAccessories'] : array());
            
            $Origin = (!empty($request['Origin']) ? $request['Origin'] : '');
            $Make = (!empty($request['Make']) ? $request['Make'] : '');
            $Model = (!empty($request['Model']) ? $request['Model'] : '');
            $ManfMonth = (!empty($request['ManfMonth']) ? $request['ManfMonth'] : date('m', strtotime('-2 Month')));;
            $ManfYear = (!empty($request['ManfYear']) ? $request['ManfYear'] : '');
            $BicycleBodyType = (!empty($request['BicycleBodyType']) ? $request['BicycleBodyType'] : config('constant.BicycleBodyType'));
            $OutletName = (!empty($request['OutletName']) ? $request['OutletName'] : "");
            $PlaceOfPurchase = (!empty($request['PlaceOfPurchase']) ? $request['PlaceOfPurchase'] : '');
            $SerialNoofFrame = (!empty($request['SerialNoofFrame']) ? $request['SerialNoofFrame'] : '');
            $FrameType = (!empty($request['FrameType']) ? $request['FrameType'] : config('constant.FrameType'));
            $PurchaseDate = (!empty($request['PurchaseDate']) ? $request['PurchaseDate'] : '');
            $InvoiceNo = (!empty($request['InvoiceNo']) ? $request['InvoiceNo'] : '');
            $PurchasePrice = (!empty($request['PurchasePrice']) ? $request['PurchasePrice'] : '');
            $DeliveryDate = (!empty($request['DeliveryDate']) ? $request['DeliveryDate'] : '');
//            $AddressWhereBicycleisUsuallyKept = (!empty($request['AddressWhereBicycleisUsuallyKept']) ? $request['AddressWhereBicycleisUsuallyKept'] : "");
            $isAntiTheftSafetyTrackingDevice = (!empty($request['isAntiTheftSafetyTrackingDevice']) ? $request['isAntiTheftSafetyTrackingDevice'] : 'No');
//            $RoadSideAsstCover = (!empty($request['RoadSideAsstCover']) ? $request['RoadSideAsstCover'] : '');
//            $DeviceDetails = (!empty($request['DeviceDetails']) ? $request['DeviceDetails'] : "");
//            $isMemberofCyclingAssociation = (!empty($request['isMemberofCyclingAssociation']) ? $request['isMemberofCyclingAssociation'] : "");
//            $MemberShipDetails = (!empty($request['MemberShipDetails']) ? $request['MemberShipDetails'] : "");
//            $DetailsoflossesorClaimsforLast3Years = (!empty($request['DetailsoflossesorClaimsforLast3Years']) ? $request['DetailsoflossesorClaimsforLast3Years'] : "");
            $NoNomineeDetails = (empty($request['NomineeFirstName']) ? true : false);
            $NomineeFirstName = (!empty($request['NomineeFirstName']) ? $request['NomineeFirstName'] : '');
            $NomineeMiddleName = (!empty($request['NomineeMiddleName']) ? $request['NomineeMiddleName'] : '');
            $NomineeAge = (!empty($request['NomineeAge']) ? $request['NomineeAge'] : '');
            $NomineelastName = (!empty($request['NomineelastName']) ? $request['NomineelastName'] : '');
            $NomineeRelationship = (!empty($request['NomineeRelationship']) ? $request['NomineeRelationship'] : '');
//            $OtherRelation = (!empty($request['OtherRelation']) ? $request['OtherRelation'] : "");
            $IsMinor = (!empty($request['IsMinor']) ? $request['IsMinor'] : FALSE);
            $RepFirstName = (!empty($request['RepFirstName']) ? $request['RepFirstName'] : "");
            $RepLastName = (!empty($request['RepLastName']) ? $request['RepLastName'] : "");
            $RepRelationWithMinor = (!empty($request['RepRelationWithMinor']) ? $request['RepRelationWithMinor'] : '');
//            $RepOtherRelation = (!empty($request['RepOtherRelation']) ? $request['RepOtherRelation'] : "");
            $PolicyStartDate = '';
            $PolicyEndDate = '';
            $IMDNumber = (!empty($request['IMDNumber']) ? $request['IMDNumber'] : '');
            $PolicyTenure = (!empty($request['PolicyTenure']) ? $request['PolicyTenure'] : '1');
            $BusinessType = (!empty($request['BusinessType']) ? $request['BusinessType'] : config('constant.BusinessType'));
            $Occupation = (!empty($request['Occupation']) ? $request['Occupation'] : '');
            $MonthlyIncome = (!empty($request['MonthlyIncome']) ? $request['MonthlyIncome'] : '');
//            $NoPreviousPolicyHistory = (!empty($request['NoPreviousPolicyHistory']) ? $request['NoPreviousPolicyHistory'] : false);
//            $PreviousPolicyName = (!empty($request['PreviousPolicyName']) ? $request['PreviousPolicyName'] : '');
//            $PreviousPolicyType = (!empty($request['PreviousPolicyType']) ? $request['PreviousPolicyType'] : '');
//            $PreviousPolicyStartDate = (!empty($request['PreviousPolicyStartDate']) ? $request['PreviousPolicyStartDate'] : '');
//            $PreviousPolicyEndDate = (!empty($request['PreviousPolicyEndDate']) ? $request['PreviousPolicyEndDate'] : '');
//            $PreviousPolicyNumber = (!empty($request['PreviousPolicyNumber']) ? $request['PreviousPolicyNumber'] : '');
//            $PreviousPolicyInsurerName = (!empty($request['PreviousPolicyInsurerName']) ? $request['PreviousPolicyInsurerName'] : '');
//            $NoOfClaims = (!empty($request['NoOfClaims']) ? $request['NoOfClaims'] : '');
//            $ClaimAmount = (!empty($request['ClaimAmount']) ? $request['ClaimAmount'] : '');
//            $PreviousYearNCBPercentage = (!empty($request['PreviousYearNCBPercentage']) ? $request['PreviousYearNCBPercentage'] : '');
            // CustmerObj => START
            $CustomerType = (!empty($request['CustomerType']) ? $request['CustomerType'] : 'I');
            $Salutation = (!empty($request['Salutation']) ? $request['Salutation'] : '');
            $FirstName = (!empty($request['FirstName']) ? $request['FirstName'] : '');
            $LastName = (!empty($request['LastName']) ? $request['LastName'] : '');
//            $MiddleName = (!empty($request['MiddleName']) ? $request['MiddleName'] : '');
            $DOB = (!empty($request['DOB']) ? $request['DOB'] : '');
            $EmailID = (!empty($request['EmailID']) ? $request['EmailID'] : '');
//            $PanNo = (!empty($request['PanNo']) ? $request['PanNo'] : '');
//            $MobileCode = (!empty($request['MobileCode']) ? $request['MobileCode'] : '');
//            $LandlineNumber = (!empty($request['LandlineNumber']) ? $request['LandlineNumber'] : '');
            $MobileNumber = (!empty($request['MobileNumber']) ? $request['MobileNumber'] : '');
            $AddressLine1 = (!empty($request['AddressLine1']) ? $request['AddressLine1'] : '');
//            $AddressLine2 = (!empty($request['AddressLine2']) ? $request['AddressLine2'] : '');
//            $AddressLine3 = (!empty($request['AddressLine3']) ? $request['AddressLine3'] : '');
            $Area = (!empty($request['Area']) ? $request['Area'] : '');
//            $CityDistrictCode = (!empty($request['CityDistrictCode']) ? $request['CityDistrictCode'] : '');
            $CityDistrictName = (!empty($request['CityDistrictName']) ? $request['CityDistrictName'] : '');
//            $StateCode = (!empty($request['StateCode']) ? $request['StateCode'] : '');
            $StateName = (!empty($request['StateName']) ? $request['StateName'] : '');
            $PinCode = (!empty($request['PinCode']) ? $request['PinCode'] : '');
//            $PinCodeLocality = (!empty($request['PinCodeLocality']) ? $request['PinCodeLocality'] : '');
            $TPSource = (!empty($request['TPSource']) ? $request['TPSource'] : config('constant.TPSource'));
            $PermanentLocationSameAsMailLocation = (!empty($request['PermanentLocationSameAsMailLocation']) ? $request['PermanentLocationSameAsMailLocation'] : true);
            $MailingAddressLine1 = (!empty($request['MailingAddressLine1']) ? $request['MailingAddressLine1'] : '');
            $MailingPinCode = (!empty($request['MailingPinCode']) ? $request['MailingPinCode'] : '');
            $MailingPinCodeLocality = (!empty($request['MailingPinCodeLocality']) ? $request['MailingPinCodeLocality'] : '');
//            $IsEIAAvailable = (!empty($request['IsEIAAvailable']) ? $request['IsEIAAvailable'] : '');
//            $EIAAccNo = (!empty($request['EIAAccNo']) ? $request['EIAAccNo'] : '');
//            $EIAAccWith = (!empty($request['EIAAccWith']) ? $request['EIAAccWith'] : '');
//            $EIAPanNo = (!empty($request['EIAPanNo']) ? $request['EIAPanNo'] : '');
//            $EIAUIDNo = (!empty($request['EIAUIDNo']) ? $request['EIAUIDNo'] : '');
//            $IsEIAPolicy = (!empty($request['IsEIAPolicy']) ? $request['IsEIAPolicy'] : '');
//            $GCCustomerID = (!empty($request['GCCustomerID']) ? $request['GCCustomerID'] : '');
            // CustmerObj => END
            $BuyerState = (!empty($request['BuyerState']) ? $request['BuyerState'] : '');
            $GSTIN = (!empty($request['GSTIN']) ? $request['GSTIN'] : '');

            $BicycleSumInsured = (!empty($request['BicycleSumInsured']) ? $request['BicycleSumInsured'] : '');
            $TotalSumInsured = (!empty($request['TotalSumInsured']) ? $request['TotalSumInsured'] : '');

            $TotalPremium = (!empty($request['TotalPremium']) ? $request['TotalPremium'] : '');
            $NetPremium = (!empty($request['NetPremium']) ? $request['NetPremium'] : '');
            $CGST = (!empty($request['CGST']) ? $request['CGST'] : '');
            $SGST = (!empty($request['SGST']) ? $request['SGST'] : '');
            $IGST = (!empty($request['IGST']) ? $request['IGST'] : '');
            $UTGST = (!empty($request['UTGST']) ? $request['UTGST'] : '');

            $LongTermPolicyDiscountPremium = (!empty($request['LongTermPolicyDiscountPremium']) ? $request['LongTermPolicyDiscountPremium'] : 0.00);
            $BurglaryorHouseBreakingorTheftUpto75SumInsured = (!empty($request['BurglaryorHouseBreakingorTheftUpto75SumInsured']) ? $request['BurglaryorHouseBreakingorTheftUpto75SumInsured'] : '');
            $BurglaryorHouseBreakingorTheftUpto75Premium = (!empty($request['BurglaryorHouseBreakingorTheftUpto75Premium']) ? $request['BurglaryorHouseBreakingorTheftUpto75Premium'] : '');
            $LossorDamagetoInsuredBicycleSumInsured = (!empty($request['LossorDamagetoInsuredBicycleSumInsured']) ? $request['LossorDamagetoInsuredBicycleSumInsured'] : '');
            $LossorDamagetoInsuredBicyclePremium = (!empty($request['LossorDamagetoInsuredBicyclePremium']) ? $request['LossorDamagetoInsuredBicyclePremium'] : '');
            $PermanentTotalDisabilitySumInsured = (!empty($request['PermanentTotalDisabilitySumInsured']) ? $request['PermanentTotalDisabilitySumInsured'] : '');
            $AccidentalDeathSumInsured = (!empty($request['AccidentalDeathSumInsured']) ? $request['AccidentalDeathSumInsured'] : '');
            $AccidentalDeathPremium = (!empty($request['AccidentalDeathPremium']) ? $request['AccidentalDeathPremium'] : '');
            $PublicLiabilitySumInsured = (!empty($request['PublicLiabilitySumInsured']) ? $request['PublicLiabilitySumInsured'] : '');
            $PublicLiabilityPremium = (!empty($request['PublicLiabilityPremium']) ? $request['PublicLiabilityPremium'] : '');

//            $requestMethod = 'POST';
//            $requestPath = '/pedal/API/IMDTPService/GetPolicyPedal';
            $postFields = array(
                "TPSourceName" => $TPSourceName,
                "QuotationNumber" => $QuotationNumber,
                "PaymentSource" => $PaymentSource,
                "PaymentDate" => $PaymentDate,
                "TransactionID" => $TransactionID,
                "TPEmailID" => $TPEmailID,
                "SendEmailtoCustomer" => $SendEmailtoCustomer,
                "OTP" => $OTP,
                "OTPEnteredDate" => $OTPEnteredDate,
                "OTPcreatedDate" => $OTPcreatedDate,
                'DealerCode' => $dealerCode,
                "BiCycleType" => $BiCycleType,
                "Purchased" => $Purchased,
//                "Origin" => $Origin,
                "MakeCode" => $Make,
                "ModelCode" => $Model,
                "ManfMonth" => $ManfMonth,
                "ManfYear" => $ManfYear,
//                "BicycleBodyType" => $BicycleBodyType,
                "OutletName" => $OutletName,
                "PlaceOfPurchase" => $PlaceOfPurchase,
                "SerialNoofFrame" => $SerialNoofFrame,
                "FrameType" => $FrameType,
                "PurchaseDate" => $PurchaseDate,
                "InvoiceNo" => $InvoiceNo,
                "PurchasePrice" => $PurchasePrice,
                "DeliveryDate" => $DeliveryDate,
                "BicycleSumInsured" => $BicycleSumInsured,
                "TotalSumInsured" => $TotalSumInsured,
//                "AddressWhereBicycleisUsuallyKept" => $AddressWhereBicycleisUsuallyKept,
                "isAntiTheftSafetyTrackingDevice" => $isAntiTheftSafetyTrackingDevice,
//                "RoadSideAsstCover" => $RoadSideAsstCover,
//                "DeviceDetails" => $DeviceDetails,
//                "isMemberofCyclingAssociation" => $isMemberofCyclingAssociation,
//                "MemberShipDetails" => $MemberShipDetails,
//                "DetailsoflossesorClaimsforLast3Years" => $DetailsoflossesorClaimsforLast3Years,
                "NoNomineeDetails" => $NoNomineeDetails,
                "NomineeFirstName" => $NomineeFirstName,
//                "NomineeMiddleName" => $NomineeMiddleName,
                "NomineelastName" => $NomineelastName,
                "NomineeAge" => $NomineeAge,
                "NomineeRelationship" => $NomineeRelationship,
//                "OtherRelation" => $OtherRelation,
                "IsMinor" => $IsMinor,
                "RepFirstName" => $RepFirstName,
                "RepLastName" => $RepLastName,
                "RepRelationWithMinor" => $RepRelationWithMinor,
//                "RepOtherRelation" => $RepOtherRelation,
                "PolicyStartDate" => $PolicyStartDate,
                "PolicyEndDate" => $PolicyEndDate,
//                "IMDNumber" => $IMDNumber,
                "IMDNumber" => 'IMD1096263',//$IMDNumber,
                "PolicyTenure" => $PolicyTenure,
                "BusinessType" => $BusinessType,
                'ElectricalAccessories' => $ElectricalAccessories,
                'TotalElectricalAccessoriesSI' => $TotalElectricalAccessoriesSI,
                'lstAccessories' => $lstAccessories,
                'NonElectricalAccessories' => $NonElectricalAccessories,
                'TotalNonElectricalAccessoriesSI' => $TotalNonElectricalAccessoriesSI,
                'lstNonElecAccessories' => $lstNonElecAccessories,
//                "NoPreviousPolicyHistory" => $NoPreviousPolicyHistory,
//                "PreviousPolicyName" => $PreviousPolicyName,
//                "PreviousPolicyType" => $PreviousPolicyType,
//                "PreviousPolicyStartDate" => $PreviousPolicyStartDate,
//                "PreviousPolicyEndDate" => $PreviousPolicyEndDate,
//                "PreviousPolicyNumber" => $PreviousPolicyNumber,
//                "PreviousPolicyInsurerName" => $PreviousPolicyInsurerName,
//                "NoOfClaims" => $NoOfClaims,
//                "ClaimAmount" => $ClaimAmount,
//                "PreviousYearNCBPercentage" => "$PreviousYearNCBPercentage",
                "Occupation" => $Occupation,
                "MonthlyIncome" => $MonthlyIncome,
                "CustmerObj" => array(
                    "CustomerType" => $CustomerType,
                    "Salutation" => $Salutation,
                    "FirstName" => $FirstName,
                    "LastName" => $LastName,
//                    "MiddleName" => $MiddleName,
                    "DOB" => $DOB,
                    "EmailID" => $EmailID,
//                    "PanNo" => $PanNo,
//                    "MobileCode" => $MobileCode,
//                    "LandlineNumber" => $LandlineNumber,
                    "MobileNumber" => $MobileNumber,
                    "AddressLine1" => $AddressLine1,
//                    "AddressLine2" => $AddressLine2,
//                    "AddressLine3" => $AddressLine3,
                    "Area" => $Area,
//                    "CityDistrictCode" => $CityDistrictCode,
                    "CityDistrictName" => $CityDistrictName,
//                    "StateCode" => $StateCode,
                    "StateName" => $StateName,
                    "PinCode" => $PinCode,
//                    "PinCodeLocality" => $PinCodeLocality,
                    "TPSource" => $TPSource,
                    "PermanentLocationSameAsMailLocation" => $PermanentLocationSameAsMailLocation,
                    "MailingAddressLine1" => $AddressLine1,
                    "MailingPinCode" => $MailingPinCode,
                    "MailingPinCodeLocality" => $MailingPinCodeLocality,
//                    "IsEIAAvailable" => $IsEIAAvailable,
//                    "EIAAccNo" => $EIAAccNo,
//                    "EIAAccWith" => $EIAAccWith,
//                    "EIAPanNo" => $EIAPanNo,
//                    "EIAUIDNo" => $EIAUIDNo,
//                    "IsEIAPolicy" => $IsEIAPolicy,
//                    "GSTIN" => $GSTIN,
//                    "GCCustomerID" => $GCCustomerID
                ),
                "BuyerState" => $BuyerState,
                "GSTIN" => $GSTIN,
                "TotalPremium" => $TotalPremium,
                'CGST' => $CGST,
                'SGST' => $SGST,
                'IGST' => $IGST,
                'UTGST' => $UTGST,
                'NetPremium' => $NetPremium,
                'lstDomPedalCovers' => array(
                    array(
                        "LongTermPolicyDiscountPremium"=>$LongTermPolicyDiscountPremium,
                        "BurglaryorHouseBreakingorTheftUpto75SumInsured"=>$BurglaryorHouseBreakingorTheftUpto75SumInsured,
                        "BurglaryorHouseBreakingorTheftUpto75Premium"=>$BurglaryorHouseBreakingorTheftUpto75Premium,
                        "LossorDamagetoInsuredBicycleSumInsured"=>$LossorDamagetoInsuredBicycleSumInsured,
                        "LossorDamagetoInsuredBicyclePremium"=>$LossorDamagetoInsuredBicyclePremium,
                        "PermanentTotalDisabilitySumInsured"=>$PermanentTotalDisabilitySumInsured,
                        "AccidentalDeathSumInsured"=>$AccidentalDeathSumInsured,
                        "AccidentalDeathPremium"=>$AccidentalDeathPremium,
                        "PublicLiabilitySumInsured"=>$PublicLiabilitySumInsured,
                        "PublicLiabilityPremium"=>$PublicLiabilityPremium
                    )
                )
            );

            $customerDetails = array();
            $customerDetails['lead_id'] = $leadId;
            $customerDetails['name'] = $FirstName . ' ' . $LastName;
            $customerDetails['email'] = $EmailID;
            $customerDetails['mobile'] = $MobileNumber;
            $customerDetails['total_premium'] = 1;//$TotalPremium;
            $fnStatus = $this->sendPayULinkToCustomer($customerDetails);
            if ($fnStatus['success'] == 1) {
                $Lead = new Lead();
                $postFields['TransactionID'] = $fnStatus['result']['Transaction Id'];
                $Lead->field['L_id'] = $leadId;
                $Lead->field['L_completeLeadDetails'] = json_encode($postFields);
                $Lead->updatePedalLead();

                $response['status'] = 1;
                $response['code'] = 'S400';
                $response['message'] = 'Payment link sent successfully';
            } else {
                $response['status'] = 0;
                $response['code'] = 'E400';
                $response['message'] = 'Error occured, please try again';
            }
            //}
        } catch (\Exception $exc) {
            echo $exc;exit;
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = $exc->getMessage();
        }
        return $response;
    }

    public function restRequest($requestMethod = NULL, $requestPath = NULL, $queryData = NULL, $postFields = array()) {
        $arrResp = array();
        try {

            if (!function_exists('curl_init')) {
                throw new Exception('Sorry cURL is not installed!');
            } else {
                // Host URL
                $this->_api_url = config('constant.API_ENDPOINT');;
                $requestData = (!empty($postFields) ? json_encode($postFields) : '');
                // echo "<pre>";
                // echo $requestData;
                $this->_curl_handle = curl_init();
                $curl_url = $this->_api_url . $requestPath . '?' . $queryData;
                curl_setopt($this->_curl_handle, CURLOPT_URL, $curl_url);
                curl_setopt($this->_curl_handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                curl_setopt($this->_curl_handle, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($this->_curl_handle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($this->_curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($this->_curl_handle, CURLOPT_SSL_VERIFYPEER, 0);

                if (!empty($postFields)) {
                    curl_setopt($this->_curl_handle, CURLOPT_POST, 1);
                    curl_setopt($this->_curl_handle, CURLOPT_POSTFIELDS, $requestData);
                }

                $contents = curl_exec($this->_curl_handle);
                // print_r($contents);
                $headers = curl_getinfo($this->_curl_handle);
                curl_close($this->_curl_handle);
                $arrResp = json_decode($contents, true);
                // print_r($arrResp);
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit;
        }
        return $arrResp;
    }

    public function sendPayULinkToCustomer($customerDetails = array()) {
        $this->autoRender = false;
        $arrResp = array();
        try {
            $leadId = $customerDetails['lead_id'];
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $merchantKey = config('constant.payu_money.merchant_key');
            $salt = config('constant.payu_money.salt');
            $command = "create_invoice";
            if (!function_exists('curl_init')) {
                throw new Exception('Sorry cURL is not installed!');
            } else {
                $var1 = '{"amount":"' . $customerDetails['total_premium'] . '","txnid":"' . $txnid . '","productinfo":"Insurance Payment","firstname":"' . $customerDetails['name'] . '","email":"' . $customerDetails['email'] . '","phone":"' . $customerDetails['mobile'] . '","address1":"","city":"","state":"","country":"","zipcode":"","template_id":"","validation_period":1,"send_email_now":"1","send_sms":"1"}';
                $hash_str = $merchantKey . '|' . $command . '|' . $var1 . '|' . $salt;
                $hash = strtolower(hash('sha512', $hash_str));
                $r = array('key' => $merchantKey, 'hash' => $hash, 'var1' => $var1, 'command' => $command);
                $qs = http_build_query($r);
                $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
                $c = curl_init();
                curl_setopt($c, CURLOPT_URL, $wsUrl);
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
                curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
                $o = curl_exec($c);
                if (curl_errno($c)) {
                    $sad = curl_error($c);
                    throw new \Exception($sad);
                }
                curl_close($c);
                $valueSerialized = @unserialize($o);
                if ($o === 'b:0;' || $valueSerialized !== false) {
                    throw new \Exception('Error in sending payment link to customer');
                }
                $objResp = json_decode($o);
                foreach ($objResp as $key => $value) {
                    $arrResp[$key] = $value;
                }
            }

            if ($arrResp['Status'] == 'Success') {
                /* Used to keep PayU logs */
                $payulog = new PayuLog();
                $payuRes = json_encode($arrResp);
                $payulog->field['P_lead_id'] = $leadId;
                $payulog->field['P_txnid'] = $txnid;
                $payulog->field['P_payu_link_request'] = $var1;
                $payulog->field['P_payu_link_response'] = $payuRes;
                $payulog->field['P_payu_link_date'] = date('Y-m-d H:i:s');
                $payulog->addPaymentLog();
                /* End */

                $response['success'] = 1;
                $response['message'] = 'Paymnet link sent successfully to customer';
                $response['result'] = $arrResp;
            } else {
                throw new \Exception('Error in sending payment link to customer');
            }
        } catch (\Exception $exc) {
            $response['success'] = 0;
            $response['message'] = $exc->getMessage();
            echo $exc->getMessage();
            ;
            exit;
        }
        return $response;
    }

    public function payuMoneyPayResponse(Request $request) {
        $input = $request->all();
        $paymentStatus = $input["status"];
        $firstname = $input["firstname"];
        $amount = $input["amount"];
        $txnid = $input["txnid"];
        $posted_hash = $input["hash"];
        $key = $input["key"];
        $productinfo = $input["productinfo"];
        $email = $input["email"];
        $addedOn = $input['addedon'];
        $salt = config('constant.payu_money.salt');
        $retHashSeq = $salt . '|' . $paymentStatus . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        $hash = hash("sha512", $retHashSeq);
        if ($hash != $posted_hash) {
            $status = 'payment_error';
            return redirect(url('/api/thank-you', ['status'=>$status]));
        } else {
            $txnid = $input["txnid"];
            $payuLog = new PayuLog();
            $data = $payuLog->getTransactionLog($txnid);
            $payuRes = json_encode($input);
            $updatePayLog = $payuLog->updatePayuLog($txnid, $payuRes);
            if (!empty($data['data']['lead']['L_completeLeadDetails'])) {
                if($paymentStatus == 'success') {
                    $paymentDate = date('d/m/Y', strtotime($addedOn));
                    $policyTenure = $data['data']['lead']['L_newPolicyCoverage'];
                    $PolicyStartDate = date('d/m/Y', strtotime('+1 Day', strtotime($addedOn)));
                    $changedDate = str_replace('/', '-', $paymentDate);
                    $changedDate = date('Y-m-d', strtotime('+1 Day', strtotime($changedDate)));
                    $PolicyEndDate = date('Y-m-d', strtotime("+$policyTenure Year", strtotime($changedDate)));
                    $PolicyEndDate = date('d/m/Y', strtotime('-1 Day', strtotime($PolicyEndDate)));

                    $leadId = $data['data']['P_lead_id'];
                    $leadDetails = $data['data']['lead']['L_completeLeadDetails'];
                    $postFields = json_decode($leadDetails, true);
                    $postFields['PaymentDate'] = $paymentDate;
                    $postFields['PolicyStartDate'] = $PolicyStartDate;
                    $postFields['PolicyEndDate'] = $PolicyEndDate;
                    $queryData = http_build_query($postFields);
                    $requestMethod = 'POST';
                    $requestPath = '/pedal/API/IMDTPService/GetPolicyPedal';
                    $responce = $this->restRequest($requestMethod, $requestPath, $queryData, $postFields);

                    if($responce['PolicyNumber']) {
                        /* Used to update policy details after policy generation */
                        $Lead = new Lead();
                        $Lead->field['L_id'] = $leadId;
                        $Lead->field['L_newPolicyNumber'] = $responce['PolicyNumber'];
                        $Lead->field['L_newPolicyStartDate'] = ($PolicyStartDate ? Carbon::createFromFormat('j/n/Y', $PolicyStartDate)->toDateString() : null);
                        $Lead->field['L_newPolicyEndDate'] = ($PolicyEndDate ? Carbon::createFromFormat('j/n/Y', $PolicyEndDate)->toDateString() : null);
                        $Lead->field['L_newPolicyCreationDate'] = date('Y-m-d H:i:s');
                        $Lead->field['L_status'] = config('constant.lead_status.COMPLETED.value');
                        $Lead->updateLeadDetail();
                        $message = "Thank you! You will receive your policy shortly on your email.";
                        $status = 'success';
                        /* End */
                    } else {
                        $message = "Error! {$responce['ErrorText']}";
                        $status = 'error';
                    }
    //                $this->viewData['message'] = $message;
                    /* Sending as log for reference */
                    $arr['server'] = "Development";
                    $arr['request'] = $postFields;
                    $arr['response'] = $responce;
                    Mail::raw(json_encode($arr), function($m)
                    {
                        $m->from('noreply@pedal.com', '');
                        $m->to('engineering@fondostech.in');
                        $m->cc('shailendra.kulria@fondostech.in');
                    });
                    /* End */
                    return redirect(url('/api/thank-you', ['status'=>$status]));
                } else {
                    $status = 'payment_error';
                    return redirect(url('/api/thank-you', ['status'=>$status]));
                }
            } else {
                echo "<br>No details found associated with Transaction ID : $txnid";
            }
        }
    }

    public function getCities(Request $request) {
        try{
            $stateCode = $request->state_code;
            if(empty($stateCode)) { throw new \Exception('Please select state'); }
            $cities = City::getCityByState($stateCode);
            if(!empty($cities)) {
                $response['status'] = 1;
                $response['code'] = 'S200';
                $response['message'] = 'City List';
                $response['data'] = $cities;
            } else {
                throw new \Exception('Unable to get city list');
            }
        } catch (\Exception $ex) {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = $ex->getMessage();
            $response['data'] = array();
        }
        return response()->json($response);
    }
    
    public function getPincodes(Request $request) {
        try{
            $cityId = $request->city;
            if(empty($cityId)) { throw new \Exception('Please select city'); }
            $pincodes = CityPincode::getCityPincodes($cityId);
            if(!empty($pincodes)) {
                $response['status'] = 1;
                $response['code'] = 'S200';
                $response['message'] = 'Pincode List';
                $response['data'] = $pincodes;
            } else {
                throw new \Exception('Unable to get city pincodes');
            }
        } catch (\Exception $ex) {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = $ex->getMessage();
            $response['data'] = array();
        }
        return response()->json($response);
    }
    
    public function getModel(Request $request) {
        try{
            $makeId = $request->make_id;
            if(empty($makeId)) { throw new \Exception('Please select city'); }
            $models = Vehicle::getMakeModels($makeId);
            if(!empty($models)) {
                $response['status'] = 1;
                $response['code'] = 'S200';
                $response['message'] = 'Model List';
                $response['data'] = $models;
            } else {
                throw new \Exception('Unable to get models');
            }
        } catch (\Exception $ex) {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = $ex->getMessage();
            $response['data'] = array();
        }
        return response()->json($response);
    }
    
    public function getRateCalculator(Request $request) {
        try{
            $policyTenure = $request->policy_tenure;
            $purchaseAmt = $request->purchase_amount;
            if(empty($policyTenure)) { throw new \Exception('Please select policy tenure'); }
            if(empty($purchaseAmt)) { throw new \Exception('Please enter purchase amount'); }
            $calculator = \App\Http\Models\RateCalculator::getRateCalculator($policyTenure, $purchaseAmt);
            if(!empty($calculator)) {
                $response['status'] = 1;
                $response['code'] = 'S200';
                $response['message'] = 'Calculator List';
                $response['data'] = $calculator;
            } else {
                throw new \Exception('Unable to get calculator');
            }
        } catch (\Exception $ex) {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = $ex->getMessage();
//            $response['data'] = [];
        }
        return response()->json($response);
    }
    
    /* Added by Ajay Jain
     * Used to calculate age of person
     */
    function ageCalculator($dob){
        if(!empty($dob)){
            $birthdate = new \DateTime($dob);
            $today   = new \DateTime('today');
            $age = $birthdate->diff($today)->y;
            return $age;
        }else{
            return 0;
        }
    }
    
    /* Added by Ajay Jain
     * Used to get app latest version
     */
    public function getAppLatestVersion() {
        $appVersion = config('constant.APP_VERSION');
        $appUrl = config('constant.APP_URL');
        $response['status'] = 1;
        $response['code'] = 'S200';
        $response['message'] = 'Calculator List';
        $response['data']['app_version'] = $appVersion;
        $response['data']['app_url'] = $appUrl;
        return response()->json($response);
    }
    
    public function thankYou(Request $request) {
//        Mail::raw(json_encode($request->all()), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
        $status = $request->status;
        if($status == 'success') {
            $this->viewData['message'] = "Thank you! You will receive your policy shortly on your email.";
        } elseif($status == 'error') {
            $this->viewData['message'] = "Ops! There seems to be an error in policy Generation. But don't worry we will sort this out soon. In case of any difficulty you may contact our customer care helpline 18002665844";
        } elseif($status == 'payment_error') {
            $this->viewData['message'] = "Invalid Transaction. Please try again";
        }
        return view('lead.policy_success', $this->viewData);
    }
    
    public function getMotorLeadDetail($lead_detail = array()) {
        $motor = array();
        $appUrl = env('APP_URL');
        if(!empty($lead_detail)) {
            $motor['customer_first_name'] = (!empty($lead_detail->L_customerFirstName)?$lead_detail->L_customerFirstName:NULL);
            $motor['customer_last_name'] = (!empty($lead_detail->L_customerLastName)?$lead_detail->L_customerLastName:NULL);
            $motor['customer_email'] = (!empty($lead_detail->L_customerEmail)?$lead_detail->L_customerEmail:NULL);
            $motor['customer_dob'] = (!empty($lead_detail->L_customerDOB)?$lead_detail->L_customerDOB:NULL);
            $motor['customer_mobile'] = (!empty($lead_detail->L_customerMobile)?$lead_detail->L_customerMobile:NULL);
            $motor['customer_address'] = (!empty($lead_detail->L_customerAddress)?$lead_detail->L_customerAddress:NULL);
            $stateText = State::getStateByCode($lead_detail->L_customerState);
            $motor['customer_state'] = (!empty($stateText['data']->ST_name)?$stateText['data']->ST_name:NULL);
            $cityText = City::getCityDetail($lead_detail->L_customerCity);
            $motor['customer_city'] = (!empty($cityText['data']->CT_name)?$cityText['data']->CT_name:NULL);
            $motor['customer_zip'] = (!empty($lead_detail->L_customerZip)?$lead_detail->L_customerZip:NULL);

            $motor['vehicle_photo1'] = (!empty($lead_detail->motorInsurance->vehicle_photo1)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->vehicle_photo1:NULL);
            $motor['vehicle_photo2'] = (!empty($lead_detail->motorInsurance->vehicle_photo2)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->vehicle_photo2:NULL);
            $motor['vehicle_photo3'] = (!empty($lead_detail->motorInsurance->vehicle_photo3)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->vehicle_photo3:NULL);
            $motor['vehicle_photo4'] = (!empty($lead_detail->motorInsurance->vehicle_photo4)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->vehicle_photo4:NULL);
            $motor['old_policy_attachment1'] = (!empty($lead_detail->motorInsurance->old_policy_attachment1)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->old_policy_attachment1:NULL);
            $motor['old_policy_attachment2'] = (!empty($lead_detail->motorInsurance->old_policy_attachment2)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->old_policy_attachment2:NULL);
            $motor['rc_photo'] = (!empty($lead_detail->motorInsurance->rc_photo)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->rc_photo:NULL);
            $motor['puc_photo'] = (!empty($lead_detail->motorInsurance->puc_photo)?$appUrl.'motor-attachment?file='.$lead_detail->motorInsurance->puc_photo:NULL);
            
            $motor['id'] = (!empty($lead_detail->motorInsurance->id)?$lead_detail->motorInsurance->id:NULL);
            $motor['lead_id'] = (!empty($lead_detail->motorInsurance->lead_id)?$lead_detail->motorInsurance->lead_id:NULL);
            $motor['vehicle_type'] = (!empty($lead_detail->motorInsurance->vehicle_type)?$lead_detail->motorInsurance->vehicle_type:NULL);
            $motor['vehicle_make'] = (!empty($lead_detail->motorInsurance->vehicle_make)?$lead_detail->motorInsurance->vehicle_make:NULL);
            $motor['vehicle_model'] = (!empty($lead_detail->motorInsurance->vehicle_model)?$lead_detail->motorInsurance->vehicle_model:NULL);
            $motor['vehicle_year'] = (!empty($lead_detail->motorInsurance->vehicle_year)?$lead_detail->motorInsurance->vehicle_year:NULL);
            $motor['vehicle_engine_no'] = (!empty($lead_detail->motorInsurance->vehicle_engine_no)?$lead_detail->motorInsurance->vehicle_engine_no:NULL);
            $motor['vehicle_chassis_no'] = (!empty($lead_detail->motorInsurance->vehicle_chassis_no)?$lead_detail->motorInsurance->vehicle_chassis_no:NULL);
            $motor['vehicle_reg_no'] = (!empty($lead_detail->motorInsurance->vehicle_reg_no)?$lead_detail->motorInsurance->vehicle_reg_no:NULL);
            $motor['vehicle_net_price'] = (!empty($lead_detail->motorInsurance->vehicle_net_price)?$lead_detail->motorInsurance->vehicle_net_price:NULL);
            $motor['vehicle_cgst'] = (!empty($lead_detail->motorInsurance->vehicle_cgst)?$lead_detail->motorInsurance->vehicle_cgst:NULL);
            $motor['vehicle_sgst'] = (!empty($lead_detail->motorInsurance->vehicle_sgst)?$lead_detail->motorInsurance->vehicle_sgst:NULL);
            $motor['vehicle_gross_price'] = (!empty($lead_detail->motorInsurance->vehicle_gross_price)?$lead_detail->motorInsurance->vehicle_gross_price:NULL);
            $motor['old_policy_number'] = (!empty($lead_detail->motorInsurance->old_policy_number)?$lead_detail->motorInsurance->old_policy_number:NULL);
            $insuranceCompany = InsuranceCompany::getCompanyDetail($lead_detail->motorInsurance->old_policy_company);
            $motor['old_policy_company'] = (!empty($insuranceCompany['data']->IC_name)?$insuranceCompany['data']->IC_name:NULL);
            $motor['old_policy_start_date'] = (!empty($lead_detail->motorInsurance->old_policy_start_date)?$lead_detail->motorInsurance->old_policy_start_date:NULL);
            $motor['old_policy_end_date'] = (!empty($lead_detail->motorInsurance->old_policy_end_date)?$lead_detail->motorInsurance->old_policy_end_date:NULL);
            $insuranceCompany = InsuranceCompany::getCompanyDetail($lead_detail->motorInsurance->new_policy_company);
            $motor['new_policy_company'] = (!empty($insuranceCompany['data']->IC_name)?$insuranceCompany['data']->IC_name:NULL);
            $insuranceProd = InsuranceProduct::getProductDetail($lead_detail->motorInsurance->new_policy_product);
            $motor['new_policy_product'] = (!empty($insuranceProd['data']->IP_name)?$insuranceProd['data']->IP_name:NULL);
            $motor['new_policy_estimated_premium'] = (!empty($lead_detail->motorInsurance->new_policy_estimated_premium)?$lead_detail->motorInsurance->new_policy_estimated_premium:NULL);
            $motor['new_policy_coverage'] = (!empty($lead_detail->motorInsurance->new_policy_coverage)?$lead_detail->motorInsurance->new_policy_coverage:NULL);
            $motor['new_policy_od_premium'] = (!empty($lead_detail->motorInsurance->new_policy_od_premium)?$lead_detail->motorInsurance->new_policy_od_premium:NULL);
            $motor['new_policy_tp_premium'] = (!empty($lead_detail->motorInsurance->new_policy_tp_premium)?$lead_detail->motorInsurance->new_policy_tp_premium:NULL);
            $motor['new_policy_tax_premium'] = (!empty($lead_detail->motorInsurance->new_policy_tax_premium)?$lead_detail->motorInsurance->new_policy_tax_premium:NULL);
            $motor['new_policy_csgt'] = (!empty($lead_detail->motorInsurance->new_policy_csgt)?$lead_detail->motorInsurance->new_policy_csgt:NULL);
            $motor['new_policy_sgst'] = (!empty($lead_detail->motorInsurance->new_policy_sgst)?$lead_detail->motorInsurance->new_policy_sgst:NULL);
            $motor['new_policy_total_premium'] = (!empty($lead_detail->motorInsurance->new_policy_total_premium)?$lead_detail->motorInsurance->new_policy_total_premium:NULL);
            $motor['new_policy_payment_mode'] = (!empty($lead_detail->motorInsurance->new_policy_payment_mode)?$lead_detail->motorInsurance->new_policy_payment_mode:NULL);
            $motor['new_policy_number'] = (!empty($lead_detail->motorInsurance->new_policy_number)?$lead_detail->motorInsurance->new_policy_number:NULL);
            $motor['new_policy_start_date'] = (!empty($lead_detail->motorInsurance->new_policy_start_date)?$lead_detail->motorInsurance->new_policy_start_date:NULL);
            $motor['new_policy_end_date'] = (!empty($lead_detail->motorInsurance->new_policy_end_date)?$lead_detail->motorInsurance->new_policy_end_date:NULL);
            $motor['new_policy_attachment'] = (!empty($lead_detail->motorInsurance->new_policy_attachment)?$appUrl.'policies-attachment?file='.$lead_detail->motorInsurance->new_policy_attachment:NULL);
            $motor['policy_status'] = (!empty($lead_detail->motorInsurance->status)?$lead_detail->motorInsurance->status:NULL);
        }
        return $motor;
    }
    
    public function getOtherLeadDetail($lead_detail = array()) {
        $motor = array();
        $appUrl = env('APP_URL');
        if(!empty($lead_detail)) {
            $motor['customer_first_name'] = (!empty($lead_detail->L_customerFirstName)?$lead_detail->L_customerFirstName:NULL);
            $motor['customer_last_name'] = (!empty($lead_detail->L_customerLastName)?$lead_detail->L_customerLastName:NULL);
            $motor['customer_email'] = (!empty($lead_detail->L_customerEmail)?$lead_detail->L_customerEmail:NULL);
            $motor['customer_dob'] = (!empty($lead_detail->L_customerDOB)?$lead_detail->L_customerDOB:NULL);
            $motor['customer_mobile'] = (!empty($lead_detail->L_customerMobile)?$lead_detail->L_customerMobile:NULL);
            $motor['customer_address'] = (!empty($lead_detail->L_customerAddress)?$lead_detail->L_customerAddress:NULL);
            $stateText = State::getStateByCode($lead_detail->L_customerState);
            $motor['customer_state'] = (!empty($stateText['data']->ST_name)?$stateText['data']->ST_name:NULL);
            $cityText = City::getCityDetail($lead_detail->L_customerCity);
            $motor['customer_city'] = (!empty($cityText['data']->CT_name)?$cityText['data']->CT_name:NULL);
            $motor['customer_zip'] = (!empty($lead_detail->L_customerZip)?$lead_detail->L_customerZip:NULL);
            $motor['policy_type'] = (!empty($lead_detail->L_policyType)?$lead_detail->L_policyType:NULL);
            $insuranceProd = InsuranceProduct::getProductDetail($lead_detail->otherInsurance->product_offered);
            $motor['product_offered'] = (!empty($insuranceProd['data']->IP_name)?$insuranceProd['data']->IP_name:NULL);
            $motor['proposer_type'] = (!empty($lead_detail->otherInsurance->proposer_type)?$lead_detail->otherInsurance->proposer_type:NULL);
            $motor['sum_insured'] = (!empty($lead_detail->otherInsurance->sum_insured)?$lead_detail->otherInsurance->sum_insured:NULL);
            $insuranceCompany = InsuranceCompany::getCompanyDetail($lead_detail->otherInsurance->insurance_company);
            $motor['insurance_company'] = (!empty($insuranceCompany['data']->IC_name)?$insuranceCompany['data']->IC_name:NULL);
            $motor['estimated_premium'] = (!empty($lead_detail->otherInsurance->estimated_premium)?$lead_detail->otherInsurance->estimated_premium:NULL);
            $motor['net_premium'] = (!empty($lead_detail->otherInsurance->net_premium)?$lead_detail->otherInsurance->net_premium:NULL);
            $motor['gst'] = (!empty($lead_detail->otherInsurance->gst)?$lead_detail->otherInsurance->gst:NULL);
            $motor['gross_premium'] = (!empty($lead_detail->otherInsurance->gross_premium)?$lead_detail->otherInsurance->gross_premium:NULL);
            $motor['policy_attachment'] = (!empty($lead_detail->otherInsurance->policy_attachment)?$appUrl.'policies-attachment?file='.$lead_detail->otherInsurance->policy_attachment:NULL);
            $motor['status'] = (!empty($lead_detail->otherInsurance->status)?$lead_detail->otherInsurance->status:NULL);        }
        return $motor;
    }
    
    public function getHealthLeadDetail($lead_detail = array()) {
        $motor = array();
        $appUrl = env('APP_URL');
        if(!empty($lead_detail)) {
            $motor['customer_first_name'] = (!empty($lead_detail->L_customerFirstName)?$lead_detail->L_customerFirstName:NULL);
            $motor['customer_last_name'] = (!empty($lead_detail->L_customerLastName)?$lead_detail->L_customerLastName:NULL);
            $motor['customer_email'] = (!empty($lead_detail->L_customerEmail)?$lead_detail->L_customerEmail:NULL);
            $motor['customer_dob'] = (!empty($lead_detail->L_customerDOB)?$lead_detail->L_customerDOB:NULL);
            $motor['customer_mobile'] = (!empty($lead_detail->L_customerMobile)?$lead_detail->L_customerMobile:NULL);
            $motor['customer_address'] = (!empty($lead_detail->L_customerAddress)?$lead_detail->L_customerAddress:NULL);
            $stateText = State::getStateByCode($lead_detail->L_customerState);
            $motor['customer_state'] = (!empty($stateText['data']->ST_name)?$stateText['data']->ST_name:NULL);
            $cityText = City::getCityDetail($lead_detail->L_customerCity);
            $motor['customer_city'] = (!empty($cityText['data']->CT_name)?$cityText['data']->CT_name:NULL);
            $motor['customer_zip'] = (!empty($lead_detail->L_customerZip)?$lead_detail->L_customerZip:NULL);
            $motor['policy_type'] = (!empty($lead_detail->L_policyType)?$lead_detail->L_policyType:NULL);
            
            $motor['for_whom'] = (!empty($lead_detail->healthInsurance->for_whom)?$lead_detail->healthInsurance->for_whom:NULL);
            $motor['policy_holder_details'] = (!empty($lead_detail->healthInsurance->policy_holder_details)?json_decode($lead_detail->healthInsurance->policy_holder_details, true):NULL);
            $stateText = State::getStateByCode($lead_detail->healthInsurance->resident_state);
            $motor['resident_state'] = (!empty($stateText['data']->ST_name)?$stateText['data']->ST_name:NULL);
            $cityText = City::getCityDetail($lead_detail->L_customerCity);
            $motor['resident_city'] = (!empty($cityText['data']->CT_name)?$cityText['data']->CT_name:NULL);
            $motor['sum_insured'] = (!empty($lead_detail->healthInsurance->sum_insured)?$lead_detail->healthInsurance->sum_insured:NULL);
            $insuranceCompany = InsuranceCompany::getCompanyDetail($lead_detail->healthInsurance->insurance_company);
            $motor['insurance_company'] = (!empty($insuranceCompany['data']->IC_name)?$insuranceCompany['data']->IC_name:NULL);
            $motor['estimated_premium'] = (!empty($lead_detail->healthInsurance->estimated_premium)?$lead_detail->healthInsurance->estimated_premium:NULL);
            $motor['net_premium'] = (!empty($lead_detail->healthInsurance->net_premium)?$lead_detail->healthInsurance->net_premium:NULL);
            $motor['gst'] = (!empty($lead_detail->healthInsurance->gst)?$lead_detail->healthInsurance->gst:NULL);
            $motor['gross_premium'] = (!empty($lead_detail->healthInsurance->gross_premium)?$lead_detail->healthInsurance->gross_premium:NULL);
            $motor['policy_attachment'] = (!empty($lead_detail->healthInsurance->policy_attachment)?$appUrl.'policies-attachment?file='.$lead_detail->healthInsurance->policy_attachment:NULL);
            $motor['status'] = (!empty($lead_detail->healthInsurance->status)?$lead_detail->healthInsurance->status:NULL);        }
        return $motor;
    }
    
    public function getTravelLeadDetail($lead_detail = array()) {
        $motor = array();
        $appUrl = env('APP_URL');
        if(!empty($lead_detail)) {
            $motor['customer_first_name'] = (!empty($lead_detail->L_customerFirstName)?$lead_detail->L_customerFirstName:NULL);
            $motor['customer_last_name'] = (!empty($lead_detail->L_customerLastName)?$lead_detail->L_customerLastName:NULL);
            $motor['customer_email'] = (!empty($lead_detail->L_customerEmail)?$lead_detail->L_customerEmail:NULL);
            $motor['customer_dob'] = (!empty($lead_detail->L_customerDOB)?$lead_detail->L_customerDOB:NULL);
            $motor['customer_mobile'] = (!empty($lead_detail->L_customerMobile)?$lead_detail->L_customerMobile:NULL);
            $motor['customer_address'] = (!empty($lead_detail->L_customerAddress)?$lead_detail->L_customerAddress:NULL);
            $stateText = State::getStateByCode($lead_detail->L_customerState);
            $motor['customer_state'] = (!empty($stateText['data']->ST_name)?$stateText['data']->ST_name:NULL);
            $cityText = City::getCityDetail($lead_detail->L_customerCity);
            $motor['customer_city'] = (!empty($cityText['data']->CT_name)?$cityText['data']->CT_name:NULL);
            $motor['customer_zip'] = (!empty($lead_detail->L_customerZip)?$lead_detail->L_customerZip:NULL);
            $motor['policy_type'] = (!empty($lead_detail->L_policyType)?$lead_detail->L_policyType:NULL);            
            $motor['policy_for'] = (!empty($lead_detail->travelInsurance->policy_for)?$lead_detail->travelInsurance->policy_for:NULL);
            $countryTxt = Country::getCountryDetail($lead_detail->travelInsurance->dest_country);
            $motor['dest_country'] = (!empty($countryTxt['data']->name)?$countryTxt['data']->name:NULL);
            $motor['for_whom'] = (!empty($lead_detail->travelInsurance->for_whom)?$lead_detail->travelInsurance->for_whom:NULL);
            $motor['policy_holder_details'] = (!empty($lead_detail->travelInsurance->policy_holder_details)?json_decode($lead_detail->travelInsurance->policy_holder_details, true):NULL);
            $motor['annual_trip'] = (!empty($lead_detail->travelInsurance->annual_trip)?$lead_detail->travelInsurance->annual_trip:NULL);
            $motor['longest_trip_duration'] = (!empty($lead_detail->travelInsurance->longest_trip_duration)?$lead_detail->travelInsurance->longest_trip_duration:NULL);
            $motor['annual_trip_start'] = (!empty($lead_detail->travelInsurance->annual_trip_start)?$lead_detail->travelInsurance->annual_trip_start:NULL);
            $motor['annual_trip_end'] = (!empty($lead_detail->travelInsurance->annual_trip_end)?$lead_detail->travelInsurance->annual_trip_end:NULL);
            $motor['annual_trip_days'] = (!empty($lead_detail->travelInsurance->annual_trip_days)?$lead_detail->travelInsurance->annual_trip_days:NULL);
            $stateText = State::getStateDetail($lead_detail->travelInsurance->communication_state);
            $motor['communication_state'] = (!empty($stateText['data']->ST_name)?$stateText['data']->ST_name:NULL);           
            $motor['sum_insured'] = (!empty($lead_detail->travelInsurance->sum_insured)?$lead_detail->travelInsurance->sum_insured:NULL);
            $insuranceCompany = InsuranceCompany::getCompanyDetail($lead_detail->travelInsurance->insurance_company);
            $motor['insurance_company'] = (!empty($insuranceCompany['data']->IC_name)?$insuranceCompany['data']->IC_name:NULL);
            $motor['estimated_premium'] = (!empty($lead_detail->travelInsurance->estimated_premium)?$lead_detail->travelInsurance->estimated_premium:NULL);
            $motor['net_premium'] = (!empty($lead_detail->travelInsurance->net_premium)?$lead_detail->travelInsurance->net_premium:NULL);
            $motor['gst'] = (!empty($lead_detail->travelInsurance->gst)?$lead_detail->travelInsurance->gst:NULL);
            $motor['gross_premium'] = (!empty($lead_detail->travelInsurance->gross_premium)?$lead_detail->travelInsurance->gross_premium:NULL);
            $motor['policy_attachment'] = (!empty($lead_detail->travelInsurance->policy_attachment)?$appUrl.'policies-attachment?file='.$lead_detail->travelInsurance->policy_attachment:NULL);
            $motor['departed_from_india'] = (!empty($lead_detail->travelInsurance->departed_from_india)?$lead_detail->travelInsurance->departed_from_india:NULL);
            $motor['status'] = (!empty($lead_detail->travelInsurance->status)?$lead_detail->travelInsurance->status:NULL);
            return $motor;
        }
    }
}
