<?php

namespace App\Http\Controllers\API\Lead;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Models\OtherInsurance;
use Carbon\Carbon,
    Storage;

class OtherInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function add(Request $request) {
        $input = $request->all();
//        print_r($input);
//        Mail::raw(json_encode($input), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
        try{
            if(empty($request['customer_first_name'])) { throw new \Exception('Please enter first name'); }
            if(empty($request['customer_last_name'])) { throw new \Exception('Please enter last name'); }
            if(empty($request['customer_email'])) { throw new \Exception('Please enter email'); }
            if(empty($request['customer_dob'])) { 
                throw new \Exception('Please select DOB');
            } else {
                $dob = explode('/', $request['customer_dob']);
                $dd = $dob[0];
                $mm = $dob[1];
                $yy = $dob[2];
                if(!checkdate($mm, $dd, $yy)) {
                    throw new \Exception('Please select valid DOB');
                }
            }
            if(empty($request['customer_mobile'])) { throw new \Exception('Please enter mobile number'); }
            if(empty($request['customer_address'])) { throw new \Exception('Please enter customer address'); }
            if(empty($request['customer_state'])) { throw new \Exception('Please select customer state'); }
            if(empty($request['customer_city'])) { throw new \Exception('Please select customer city'); }
            if(empty($request['customer_zip'])) { throw new \Exception('Please enter pincode'); }
           
            if(empty($request['policy_type'])) { throw new \Exception('Please select whom'); }
            if(empty($request['product_offered'])) { throw new \Exception('Please select whom'); }
            if(empty($request['proposer_type'])) { throw new \Exception('Please enter policy holder details'); }
            if(empty($request['sum_insured'])) { throw new \Exception('Please select sum insured'); }
            if(empty($request['insurance_company'])) { throw new \Exception('Please select insurance company'); }
            if(empty($request['estimated_premium'])) { throw new \Exception('Please enter estimated premium'); }

            $lead_detail = null;
            // Insert Lead
            $others = new OtherInsurance;
            $others->field['L_AGid'] = $request['agent_id'];
            $others->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $others->field['L_customerFirstName'] = $request['customer_first_name'];
            $others->field['L_customerLastName'] = $request['customer_last_name'];
            $others->field['L_customerEmail'] = $request['customer_email'];
            $others->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString() : null);
            $others->field['L_customerMobile'] = $request['customer_mobile'];
            $others->field['L_customerPhone'] = $request['customer_phone'];
            $others->field['L_customerAddress'] = $request['customer_address'];
            $others->field['L_customerCity'] = $request['customer_city'];
            $others->field['L_customerState'] = $request['customer_state'];
            $others->field['L_customerZip'] = $request['customer_zip'];
            
            //Vehicle Details
            $others->field['proposer_type'] = $request['proposer_type'];
            $others->field['new_policy_product'] = $request['product_offered'];
            $others->field['sum_insured'] = $request['sum_insured'];
            $others->field['new_policy_company'] = $request['insurance_company'];
            $others->field['estimated_premium'] = $request['estimated_premium'];
            $others->field['status'] = config('constant.lead_status.NEW.value');
            /* End */
            $return = $others->add();
            
            /* Used to add zip code */
            $cityPincode = new \App\Http\Models\CityPincode();
            $cityPincode->field['CP_city_id'] = $request['customer_city'];
            $cityPincode->field['CP_pincode'] = $request['customer_zip'];
            $cityPincode->addCityPincodes();
            /* Used to add zip code */
            if ($return['status'] == true) {
                $lead_detail = $return['data'];
                $lead_detail->L_appPostData = NULL;
                $response['status'] = 1;
                $response['code'] = 'S300';
                $response['message'] = 'Lead added successfully';
                $response['timestamp'] = Carbon::now()->toDateTimeString();
            } else {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = 'Unable to add lead, please try again!';
                $response['timestamp'] = Carbon::now()->toDateTimeString();
            }
            
        } catch (\Exception $ex) {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = $ex->getMessage();
                $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
}
