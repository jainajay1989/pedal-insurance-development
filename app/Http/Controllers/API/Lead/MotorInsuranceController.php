<?php

namespace App\Http\Controllers\API\Lead;

use Illuminate\Http\Request;
use App\Http\Models\MotorInsurance;
use App\Http\Controllers\API\Controller;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon,
    Storage;

class MotorInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function add(Request $request)
    {
        $lead_detail = null;
        $appUrl = env('APP_URL');
        // Insert Lead
        $motor = new MotorInsurance;

        $motor->field['L_id'] = (!empty($request['lead_id'])?$request['lead_id']:NULL);
        $motor->field['L_AGid'] = (!empty($request['agent_id'])?$request['agent_id']:NULL);
        $motor->field['L_policyType'] = (!empty($request['policy_type'])?$request['policy_type']:NULL);
        $motor->field['L_customerFirstName'] = (!empty($request['customer_first_name'])?$request['customer_first_name']:NULL);
        $motor->field['L_customerLastName'] = (!empty($request['customer_last_name'])?$request['customer_last_name']:NULL);
        $motor->field['L_customerEmail'] = (!empty($request['customer_email'])?$request['customer_email']:NULL);
        $motor->field['L_customerDOB'] = (!empty($request['customer_dob'])?Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString():null);
        $motor->field['L_customerMobile'] = (!empty($request['customer_mobile'])?$request['customer_mobile']:NULL);
        $motor->field['L_customerPhone'] = (!empty($request['customer_phone'])?$request['customer_phone']:NULL);
        $motor->field['L_customerAddress'] = (!empty($request['customer_address'])?$request['customer_address']:NULL);
        $motor->field['L_customerCity'] = (!empty($request['customer_city'])?$request['customer_city']:NULL);
        $motor->field['L_customerState'] = (!empty($request['customer_state'])?$request['customer_state']:NULL);
        $motor->field['L_customerZip'] = (!empty($request['customer_zip'])?$request['customer_zip']:NULL);

        $motor->field['id'] = (!empty($request['id'])?$request['id']:NULL);
        $motor->field['vehicle_type'] = (!empty($request['vehicle_type'])?$request['vehicle_type']:NULL);
        $motor->field['vehicle_make'] = (!empty($request['vehicle_make'])?$request['vehicle_make']:NULL);
        $motor->field['vehicle_model'] = (!empty($request['vehicle_model'])?$request['vehicle_model']:NULL);
        $motor->field['vehicle_year'] = (!empty($request['vehicle_year'])?$request['vehicle_year']:NULL);
        $motor->field['vehicle_engine_no'] = (!empty($request['vehicle_engine_number'])?$request['vehicle_engine_number']:NULL);
        $motor->field['vehicle_chassis_no'] = (!empty($request['vehicle_chassis_number'])?$request['vehicle_chassis_number']:NULL);
        $motor->field['vehicle_reg_no'] = (!empty($request['vehicle_reg_number'])?$request['vehicle_reg_number']:NULL);
        $motor->field['vehicle_net_price'] = (!empty($request['vehicle_net_price'])?$request['vehicle_net_price']:NULL);
        $motor->field['vehicle_cgst'] = (!empty($request['vehicle_cgst'])?$request['vehicle_cgst']:NULL);
        $motor->field['vehicle_sgst'] = (!empty($request['vehicle_sgst'])?$request['vehicle_sgst']:NULL);
        $motor->field['vehicle_gross_price'] = (!empty($request['vehicle_gross_price'])?$request['vehicle_gross_price']:NULL);
        $motor->field['rc_photo'] = (!empty($request['rc_photo'])?$request['rc_photo']:NULL);
        $motor->field['puc_photo'] = (!empty($request['puc_photo'])?$request['puc_photo']:NULL);
        $motor->field['vehicle_photo1'] = (!empty($request['vehicle_photo1'])?$request['vehicle_photo1']:NULL);
        $motor->field['vehicle_photo2'] = (!empty($request['vehicle_photo2'])?$request['vehicle_photo2']:NULL);
        $motor->field['vehicle_photo3'] = (!empty($request['vehicle_photo3'])?$request['vehicle_photo3']:NULL);
        $motor->field['vehicle_photo4_'] = (!empty($request['vehicle_photo4_'])?$request['vehicle_photo4_']:NULL);
        $motor->field['old_policy_number'] = (!empty($request['old_policy_number'])?$request['old_policy_number']:NULL);
        $motor->field['old_policy_company'] = (!empty($request['old_policy_company'])?$request['old_policy_company']:NULL);
        $motor->field['last_year_claim'] = (!empty($request['last_year_claim'])?$request['last_year_claim']:NULL);
        $motor->field['old_policy_end_date'] = (!empty($request['old_policy_end_date'])?Carbon::createFromFormat('j/n/Y', $request['old_policy_end_date'])->toDateString():null);
        $startDate = (!empty($request['old_policy_end_date'])?Carbon::createFromFormat('j/n/Y', $request['old_policy_end_date'])->subDays(364)->toDateString():NULL);
        $motor->field['old_policy_start_date'] = ($startDate?$startDate:null);
        $motor->field['old_policy_attachment1'] = (!empty($request['old_policy_attachment1'])?$request['old_policy_attachment1']:NULL);
        $motor->field['old_policy_attachment2'] = (!empty($request['old_policy_attachment2'])?$request['old_policy_attachment2']:NULL);
        $motor->field['new_policy_company'] = (!empty($request['new_policy_company'])?$request['new_policy_company']:NULL);
        $motor->field['new_policy_product'] = (!empty($request['new_policy_product'])?$request['new_policy_product']:NULL);
        $motor->field['new_policy_estimated_premium'] = (!empty($request['new_policy_estimated_premium'])?$request['new_policy_estimated_premium']:NULL);
        $motor->field['remark'] = (!empty($request['remark'])?$request['remark']:NULL);
        $motor->field['status'] = config('constant.lead_status.NEW.value');
        if(!empty($request['id']) && !empty($request['lead_id'])) {
            $customerQuote = (!empty($request['quotations'])?explode('=', $request['quotations']):NULL);
            $motor->field['customer_selected_quote'] = (!empty($customerQuote[1])?$customerQuote[1]:$request['quotations']);
            $motor->field['customer_bank_cheque'] = (!empty($request['cheque'])?$request['cheque']:NULL);
            $motor->field['paid_by_whom'] = (!empty($request['paid_by_whom'])?$request['paid_by_whom']:NULL);
            $return = $motor->updateDetails();
        } else {
            $return = $motor->add();
        }
        //------------
        
        /* Used to add zip code */
        $cityPincode = new \App\Http\Models\CityPincode();
        $cityPincode->field['CP_city_id'] = $request['customer_city'];
        $cityPincode->field['CP_pincode'] = $request['customer_zip'];
        $cityPincode->addCityPincodes();
        /* Used to add zip code */

        if($return['status'] == true)
        {
            $lead_detail = $return['data'];
            $quotesList = array();
            if(!empty($lead_detail->quotations)) {
                $quotes = (!empty($lead_detail->quotations)?json_decode($lead_detail->quotations, true):array());
                foreach($quotes as $key=>$val) {
                    $quotesList[$key] = $appUrl.'quotes-attachment?file='.$val;
                }
                $lead_detail->quotations = $quotesList;
            } else {
                $lead_detail->quotations = array();
            }
            $response['status'] = 1;
            $response['code'] = 'S300';
            $response['message'] = 'Lead added successfully';
            $response['data'] = $lead_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E300';
            $response['message'] = 'Unable to add lead, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }
    
    public function upload(Request $request) {
        $fn_status = false;
        $image = $request['image'];
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('motorinsurance')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $image)));
        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $image_name, 'image_url' => url('motor-attachment', ['file' => $image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
    
    public function uploadCheque(Request $request) {
        $fn_status = false;
        $image = $request['image'];
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('motorinsurance')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $image)));
        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $image_name, 'image_url' => url('motor-attachment', ['file' => $image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
    
    public function getMake(Request $request) {
        
        //Vehicle Type:
        $type = $request['vehicle_type'];
        $vehicleMake = \App\Http\Models\MotorMake::where('type',$type)->get();
        $response['status'] = 1;
        $response['code'] = 'S400';
        $response['message'] = 'Image uploaded successfully';
        $response['data'] = $vehicleMake;
        $response['timestamp'] = Carbon::now()->toDateTimeString();
        return response()->json($response);
    }
    
    public function getModel(Request $request) {
        //Vehicle Type:
        $vehicleMake = $request['vehicle_make'];
        $vehicleMake = \App\Http\Models\MotorModel::where('make_id',$vehicleMake)->get();
        $response['status'] = 1;
        $response['code'] = 'S400';
        $response['message'] = 'Image uploaded successfully';
        $response['data'] = $vehicleMake;
        $response['timestamp'] = Carbon::now()->toDateTimeString();
        return response()->json($response);
    }
    
    public function updateQuote(Request $request) {
//        $input = $request->all();
//        Mail::raw(json_encode($input), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
//      
        //Vehicle Type:
        $motor = new MotorInsurance;
        $customerQuote = (!empty($request['quotations'])?explode('=', $request['quotations']):NULL);
        $motor->field['customer_selected_quote'] = (!empty($customerQuote)?$customerQuote[1]:NULL);
        $motor->field['customer_bank_cheque'] = $request['cheque'];
        $motor->field['id'] = $request['id'];
        $result = $motor->updateDetails();
        if($result['status']) {
        $response['status'] = 1;
        $response['code'] = 'S400';
            $response['message'] = 'Lead Updated Successfully';
        $response['data'] = [];
        $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        
        return response()->json($response);
    }
}
