<?php

namespace App\Http\Controllers\API\Lead;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Lead;
use App\Http\Models\State;
use App\Http\Models\City;
use App\Http\Models\CityPincode;
use App\Http\Models\PayuLog;
use \App\Http\Models\Vehicle;
use \App\Http\Models\Agent;
use App\Http\Models\HealthInsurance;
use Carbon\Carbon,
    Storage;

class HealthInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function add(Request $request) {
        try{
            $lead_detail = null;
            // Insert Lead
            $health = new HealthInsurance;
            $health->field['L_id'] = (!empty($request['lead_id'])?$request['lead_id']:NULL);
            $health->field['L_AGid'] = $request['agent_id'];
            $health->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $health->field['L_customerFirstName'] = $request['customer_first_name'];
            $health->field['L_customerLastName'] = $request['customer_last_name'];
            $health->field['L_customerEmail'] = $request['customer_email'];
            $health->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString() : NULL);
            $health->field['L_customerMobile'] = $request['customer_mobile'];
            $health->field['L_customerPhone'] = $request['customer_phone'];
            $health->field['L_customerAddress'] = $request['customer_address'];
            $health->field['L_customerCity'] = $request['customer_city'];
            $health->field['L_customerState'] = $request['customer_state'];
            $health->field['L_customerZip'] = $request['customer_zip'];
            
            //Vehicle Details
            $health->field['id'] = (!empty($request['id'])?$request['id']:NULL);
            $health->field['for_whom'] = $request['for_whom'];
            $health->field['policy_holder_details'] = (!empty($request['policy_holder_details'])?json_encode($request['policy_holder_details']):NULL);
            $health->field['resident_state'] = $request['resident_state'];
            $health->field['resident_city'] = $request['resident_city'];
            $health->field['sum_insured'] = $request['sum_insured'];
            $health->field['new_policy_company'] = $request['insurance_company'];
            $health->field['new_policy_product'] = $request['new_policy_product'];
            $health->field['estimated_premium'] = $request['estimated_premium'];
            $health->field['status'] = config('constant.lead_status.NEW.value');
            
            $health->field['proposal_form'] = $request['proposal_form'];
            $health->field['old_policy_attachment1'] = $request['old_policy_attachment1'];
            $health->field['old_policy_attachment2'] = $request['old_policy_attachment2'];
            $health->field['last_year_claim'] = (!empty($request['last_year_claim'])?$request['last_year_claim']:'No');
            $health->field['policy_renewal_new'] = (!empty($request['policy_renewal_new'])?$request['policy_renewal_new']:NULL);
            $health->field['remark'] = (!empty($request['remark'])?$request['remark']:NULL);
            /* End */            
            if(!empty($request['id']) && !empty($request['lead_id'])) {
                $customerQuote = (!empty($request['quotations'])?explode('=', $request['quotations']):NULL);
                $health->field['customer_selected_quote'] = (!empty($customerQuote[1])?$customerQuote[1]:$request['quotations']);
                $health->field['customer_bank_cheque'] = (!empty($request['cheque'])?$request['cheque']:NULL);
                $health->field['paid_by_whom'] = (!empty($request['paid_by_whom'])?$request['paid_by_whom']:NULL);
                $return = $health->updateDetails();
            } else {
                $return = $health->add();
            }
            /* Used to add zip code */
            $cityPincode = new \App\Http\Models\CityPincode();
            $cityPincode->field['CP_city_id'] = $request['customer_city'];
            $cityPincode->field['CP_pincode'] = $request['customer_zip'];
            $cityPincode->addCityPincodes();
            /* Used to add zip code */
            
            if ($return['status'] == true) {
                $lead_detail = $return['data'];
                $lead_detail->L_appPostData = NULL;
                $response['status'] = 1;
                $response['code'] = 'S300';
                $response['message'] = 'Lead added successfully';
                $response['timestamp'] = Carbon::now()->toDateTimeString();
            } else {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = 'Unable to add lead, please try again!';
                $response['timestamp'] = Carbon::now()->toDateTimeString();
            }
            
        } catch (\Exception $ex) {
                $response['status'] = 0;
                $response['code'] = 'E300';
                $response['message'] = $ex->getMessage();
                $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
    
    public function uploadPurposalForm(Request $request) {
//        Mail::raw(json_encode($request->all()), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
        $fn_status = false;
        $image = $request['image'];
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('health_insurance_purpose')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $image)));
        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $image_name, 'image_url' => url('health-purpose-attachment', ['file' => $image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
    
    public function upload(Request $request) {
        $fn_status = false;
        $image = $request['image'];
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('healthinsurance')->put($image_name, base64_decode(str_replace('data:image/png;base64,', '', $image)));
        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $image_name, 'image_url' => url('health-attachment', ['file' => $image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return response()->json($response);
    }
}
