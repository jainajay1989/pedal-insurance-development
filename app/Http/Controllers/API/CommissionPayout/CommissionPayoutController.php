<?php

namespace App\Http\Controllers\API\CommissionPayout;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\CommissionPayout;
use Carbon;

class CommissionPayoutController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index
     *
     * @param Request $request
     * @return Mixed
     */
    public function index(Request $request)
    {
        $payout_list = null;

        // Get Request Variables
        $agent_id = $request['agent_id'];
        //----------------------

        $return = CommissionPayout::getAgentPayouts($agent_id);
        if($return['status'] == true)
        {
            $payout_list = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Successful';
            $response['data'] = $payout_list;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }
}