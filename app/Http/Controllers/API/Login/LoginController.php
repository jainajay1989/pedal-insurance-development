<?php

namespace App\Http\Controllers\API\Login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Agent;
use App\Http\Models\InsuranceCompany;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\CommissionPayout;
use App\Http\Models\Vehicle;
use App\Http\Models\State;
use App\Http\Models\City;
use Carbon;

class LoginController extends Controller
{
    protected $guard = 'api';

    public function __construct()
    {
        parent::__construct();
        $this->middleware('api_auth');
    }

    /**
     * Handle an authentication attempt
     *
     * @param Request $request
     * @return Mixed
     */
    public function authenticate(Request $request)
    {
        $vehicle_list = [];
        $state_list = [];
        $agentList = [];
        $dealerWalletBalance = 0;

        // Get Request Variables
        $username = $request['username'];
        $password = $request['password'];
        //----------------------

        if(Auth::guard($this->guard)->attempt(['AG_username' => $username, 'password' => $password]))
        {
            $user_detail = Auth::guard($this->guard)->user()->toArray();
            // Get All Vehicles
            $makes = Vehicle::getMakes();
            if(!empty($makes))
            {
                $vehicle_list = $makes;
            }
            //-----------------

            // Get All States
            $countryId = config('constant.COUNTRY_ID_INDIA');
            $return = State::getStateByCountry($countryId);
            if(!empty($return))
            {
                $state_list = $return;
            }
            //-----------------
            
            // Get All Dealers in case of RM
            if($user_detail['AG_type'] == 'DEALER') {
                $rmId = $user_detail['AG_id'];
                $agentList = Agent::getRMAgentList($rmId);
            }
            
            //Nominee relationship
            $nomineeRelationship = config('constant.nominee_relations');
            
            //calculation config
            $calculationConfig = config('constant.calculation_config');
            
            /* Use incase of special IMD code */
            $specialImdCode = config('constant.SPECIAL_IMDCODE');
            $specialMinPrice = config('constant.SPECIAL_MIN_PURCHASE_PRICE');
            $specialMaxPrice = config('constant.SPECIAL_MAX_PURCHASE_PRICE');
            $imdCode = $user_detail['AG_ImdCode'];
            if(in_array($imdCode, $specialImdCode)) {
                $calculationConfig['MIN_PURCHASE_PRICE'] = $specialMinPrice;
                $calculationConfig['MAX_PURCHASE_PRICE'] = $specialMaxPrice;
            }
            /* Use incase of special IMD code */
            
            //calculation config
            $policyTenure = config('constant.policy_tenure');
            
            //Other Policy Type
            $otherPolicyType = config('constant.other_policy_type');
            
            //Policy Proposar
            $policyProposer = config('constant.other_policy_proposer');
            
            //Policy Relationship
            $policyRelationship = config('constant.policy_relationship');
            
            //Vehicle Type:
            $vehicleType = config('constant.VEHICLE_TYPE');
            
            //Policy Status
            $policyStatus = config('constant.lead_status_api');
            
            //Policy Type
            $policyTypeArr = array();
            $policyType = config('constant.policy_type');
            $agentPolicyType = $user_detail['AG_policyType'];
            if(!empty($agentPolicyType)) {
                $agentPolicyType = explode(',', $agentPolicyType);
                foreach($agentPolicyType as $val) {
                    $policyTypeArr[] = $policyType[$val];
                }
            }
            
            // Get All Companies
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $company_list = $return['data'];
            }
            //------------------
            
            if($user_detail['AG_type'] == 'AGENT') {
            // Dealer Wallet Balance
                $dealerWalletBalance = \App\Http\Models\Ledger::getAgentAccountBalance($user_detail['AG_id']);
                $dealerWalletBalance = $dealerWalletBalance['data']['LG_balance'];
            }

            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Login successful';
            $response['data']['insurance_companies'] = $company_list;
            $response['data']['other_policy_type'] = $otherPolicyType;
            $response['data']['other_policy_proposer'] = $policyProposer;
            $response['data']['policy_relationship'] = $policyRelationship;
            $response['data']['user_detail'] = $user_detail;
            $response['data']['nominee_relationship'] = $nomineeRelationship;
            $response['data']['calculation_config'] = $calculationConfig;
            $response['data']['policy_tenure'] = $policyTenure;
            $response['data']['vehicles'] = $vehicle_list;
            $response['data']['states'] = $state_list;
            $response['data']['agents'] = $agentList;
            $response['data']['motor_type'] = $vehicleType;
            $response['data']['app_policy_status'] = $policyStatus;
            $response['data']['agent_policy_type'] = $policyTypeArr;
            $response['data']['insurance_company_state'] = config('constant.INUSRANCE_COMPANY_STATE');
            $response['data']['dealer_payment_success_url'] = config('constant.DEALER_PAYMENT_SUCCESS_URL');
            $response['data']['dealer_payment_failure_url'] = config('constant.DEALER_PAYMENT_FAILURE_URL');
            $response['data']['dealer_wallet_balance'] = $dealerWalletBalance;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'Invalid login details provided';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Handle an update device id
     *
     * @param Request $request
     * @return Mixed
     */
    public function updateDeviceId(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $Agent->field['AG_deviceId'] = $request['device_id'];
        $return = $Agent->updateDeviceId();

        if($return['status'] == true)
        {
            $agent_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S200';
            $response['message'] = 'Device Id updated successfully';
            $response['data'] = $agent_detail;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = 'Unable to update Device Id, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }
}