<?php

namespace App\Http\Controllers\API\Cron;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\Controller;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\Country;
use App\Http\Models\State;
use Carbon;

class CommonController extends Controller
{
    protected $guard = 'api';

    public function __construct()
    {
        parent::__construct();
        $this->middleware('api_auth');
    }

    /**
     * Handle an authentication attempt
     *
     * @param Request $request
     * @return Mixed
     */
    public function getInsuranceProducts(Request $request)
    {
        // Get Request Variables
        $policyType = $request['policy_type'];
        //----------------------
        $policyProducts = InsuranceProduct::getInsuranceProducts($policyType);
        if(!empty($policyProducts))
        {
            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Login successful';
            $response['data'] = $policyProducts;
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'No products are available.';
            $response['data'] = null;
        }

        return response()->json($response);
    }
    
    public function getCountries() {
        $countries = Country::getAllCountries();
        if(!empty($countries))
        {
            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Countries List';
            $response['data'] = $countries;
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'No country are available.';
            $response['data'] = null;
        }

        return response()->json($response);
    }
    
    public function getCountryState(Request $request) {
        $countryId = $request['country_id'];
        $states = State::getStateByCountry($countryId);
        if(!empty($states))
        {
            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Countries List';
            $response['data'] = $states;
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'No country are available.';
            $response['data'] = null;
        }

        return response()->json($response);
    }
}