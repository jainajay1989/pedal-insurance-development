<?php

namespace App\Http\Controllers\API\Cron;

use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Lead;
use App\Http\Models\PayuLog;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Session;

class CronController extends Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function verifyPayUTransaction($txnId = array()) {
        $this->autoRender = false;
        $arrResp = array();
        try {
            $merchantKey = config('constant.payu_money.merchant_key');
            $salt = config('constant.payu_money.salt');
            $command = "verify_payment";
            if (!function_exists('curl_init')) {
                throw new Exception('Sorry cURL is not installed!');
            } else {
                $var1 = implode('|', $txnId);
                $hash_str = $merchantKey . '|' . $command . '|' . $var1 . '|' . $salt;
                $hash = strtolower(hash('sha512', $hash_str));
                $r = array('key' => $merchantKey, 'hash' => $hash, 'var1' => $var1, 'command' => $command);
                $qs = http_build_query($r);
                $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
                $c = curl_init();
                curl_setopt($c, CURLOPT_URL, $wsUrl);
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
                curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
                $o = curl_exec($c);
                if (curl_errno($c)) {
                    $sad = curl_error($c);
                    throw new Exception($sad);
                }
                curl_close($c);
                $valueSerialized = @unserialize($o);
                if ($o === 'b:0;' || $valueSerialized !== false) {
                    throw new \Exception('Error in sending payment link to customer');
                }
                $objResp = json_decode($o, true);
                foreach ($objResp as $key => $value) {
                    $arrResp[$key] = $value;
                }
            }
            $response['success'] = 1;
            $response['message'] = "Transaction Details";
            $response['result'] = $arrResp;
        } catch (\Exception $exc) {
            $response['success'] = 0;
            $response['message'] = $exc->getMessage();
            $response['result'] = array();
        }
        return $response;
    }
    
    public function verifyTransaction() {
        $this->autoRender = false;
        $arrResp = array();
        try {
            $pendingTransactions = PayuLog::getAllPendingTransactions();
            if($pendingTransactions->toArray()) {
                foreach($pendingTransactions as $key => $val) {
                    $txnId[] = $val->P_txnid;
                }
                $payUResponse = $this->verifyPayUTransaction($txnId);
                $payUResponse = $payUResponse['result'];
//                print_r($txnId);
//                print_r($payUResponse);exit;
                if($payUResponse['status'] == 1) {
                    $payUTransactionDetails = $payUResponse['transaction_details'];
                    foreach($pendingTransactions as $key => $val) {
                        $payuLog = new PayuLog();
                        $txnId = $val->P_txnid;
//                        echo $val->P_txnid;
//                        print_r($payUTransactionDetails);exit;
                        if(array_key_exists($txnId, $payUTransactionDetails)) {
                            $leadId = $val->L_id;
                            if($payUTransactionDetails[$txnId]['status'] == 'success') {
                                $completeOrderDetails = json_decode($val['L_completeLeadDetails'], true);
                                if (!empty($completeOrderDetails)) {
                                    $addedOn = date("Y-m-d H:i:s");//$payUTransactionDetails[$val->P_txnid]['addedon']; //"2018-12-05 18:38:56";//
                                    $paymentDate = date('d/m/Y', strtotime($addedOn));
                                    $policyTenure = $val['L_newPolicyCoverage'];
                                    $PolicyStartDate = date('d/m/Y', strtotime('+1 Day', strtotime($addedOn)));
                                    $changedDate = str_replace('/', '-', $paymentDate);
                                    $changedDate = date('Y-m-d', strtotime('+1 Day', strtotime($changedDate)));
                                    $PolicyEndDate = date('Y-m-d', strtotime("+$policyTenure Year", strtotime($changedDate)));
                                    $PolicyEndDate = date('d/m/Y', strtotime('-1 Day', strtotime($PolicyEndDate)));                    
                                    $postFields = $completeOrderDetails;
                                    $postFields['PaymentDate'] = $paymentDate;
                                    $postFields['PolicyStartDate'] = $PolicyStartDate;
                                    $postFields['PolicyEndDate'] = $PolicyEndDate;
                                    $queryData = http_build_query($postFields);
                                    $requestMethod = 'POST';
                                    $requestPath = '/pedal/API/IMDTPService/GetPolicyPedal';
                                    $responce = restRequest($requestMethod, $requestPath, $queryData, $postFields);
//                                    print_r($responce);
//                                    exit;
                                    if(!empty($responce['PolicyNumber'])) {
                                        /* Used to update policy details after policy generation */
                                        $Lead = new Lead();
                                        $Lead->field['L_id'] = $leadId;
                                        $Lead->field['L_newPolicyNumber'] = $responce['PolicyNumber'];
                                        $Lead->field['L_newPolicyStartDate'] = ($PolicyStartDate ? Carbon::createFromFormat('j/n/Y', $PolicyStartDate)->toDateString() : null);
                                        $Lead->field['L_newPolicyEndDate'] = ($PolicyEndDate ? Carbon::createFromFormat('j/n/Y', $PolicyEndDate)->toDateString() : null);
                                        $Lead->field['L_newPolicyCreationDate'] = date('Y-m-d H:i:s');
                                        $Lead->field['L_status'] = config('constant.lead_status.COMPLETED.value');
                                        $Lead->updateLeadDetail();
                                        
                                        $payuRes = json_encode($payUTransactionDetails[$val->P_txnid]);
                                        $payuLog->updatePayuLog($txnId, $payuRes, $payUTransactionDetails[$txnId]['status']);
                                        /* End */
                                    } elseif(!empty($responce['ErrorText']) && $responce['ErrorText'] == 'Please enter the unique Quotation number.') {
                                        $payuRes = json_encode($payUTransactionDetails[$val->P_txnid]);
                                        $payuLog->updatePayuLog($txnId, $payuRes, $payUTransactionDetails[$txnId]['status']);
                                        
                                    }
                                    /* Sending log for reference */
                                    $arr['request'] = $postFields;
                                    $arr['response'] = $responce;
                                    Mail::raw(json_encode($arr), function($m) use ($txnId)
                                    {
                                        $m->from('noreply@pedal.com', 'Pedal Cycle');
                                        $m->subject("Live - verifyTransaction API - Response from Liberty - $txnId");
                                        $m->to('milbinsure@gmail.com');
                                        $m->cc(array('karthikeyan.iyer@libertyinsurance.in', 'vikas.kelaskar@libertyinsurance.in'));
                                    });
                                    /* End */
                                } else {
                                    echo "<br>No details found associated with Transaction ID : $txnId";
                                }
                            } else {
//                                $Lead = new Lead();
//                                $Lead->field['L_id'] = $leadId;
//                                $Lead->field['L_status'] = config('constant.lead_status.REJECTED.value');
//                                $Lead->updateLeadDetail();
////                                
//                                $payuRes = (!empty($payUTransactionDetails[$val->P_txnid])?json_encode($payUTransactionDetails[$val->P_txnid]):NULL);
//                                $payuPaymentStatus = (!empty($payUTransactionDetails[$txnId]['status'])?$payUTransactionDetails[$txnId]['status']:'NOT PAID');
//                                $payuLog->updatePayuLog($txnId, $payuRes, $payuPaymentStatus);
                            }
                        }
                    }
                }
            }
        } catch (\Exception $exc) {
            $res['success'] = 0;
            $res['message'] = $exc->getMessage();
            $res['request'] = $postFields;
            $res['response'] = $responce;
            /* Sending error log for reference */
            Mail::raw(json_encode($res), function($m)
            {
                $m->from('noreply@pedal.com', 'Pedal Cycle');
                $m->subject("Live - verifyTransaction API Error- Response from Liberty");
                $m->to('milbinsure@gmail.com');
            });
            /* End */
        }        
    }
    
    /* Used to reject pending request for which payment not made in 24 hours */
    public function rejectPendingRequest() {        
        $pendignRequest = PayuLog::getAllPendingRequests();
//        print_r($pendignRequest);exit;
        $leads = array_column($pendignRequest->toArray(), 'L_id');
        if(!empty($leads)) {
            Mail::raw(json_encode($leads), function($message)
            {
                $message->from('noreply@pedal.com', 'rejectPendingRequest Cron');
                $message->to('engineering@fondostech.in');
            });
            Lead::rejectPendingLeads($leads);
        }
    }
    
    /* Added by Ajay Jain
     * Date: 10 oct 2018
     * Used to generate daily report
     */
    public function generateReport() {
        /* Logic build as per month - Start */
//        $todayDate = date('d');
//        if($todayDate == '01' || $todayDate == '1') {
//            $currentDate = date('Y-m-d');
//            $d1 = new \DateTime($currentDate);
//            $d2 = new \DateTime($currentDate);
//            $d1->modify('first day of previous month');
//            $startDate = $d1->format('Y-m-d');
//            $d2->modify('last day of previous month');
//            $endDate = $d2->format('Y-m-d');
//        } else {
//            $startDate = date('Y-m-01');
//            $endDate = date('Y-m-t');
//        }
//        $leadList = \App\Http\Models\Lead::whereNotNull('L_newPolicyNumber')->where('L_newPolicyCreationDate', '>=', $startDate)->where('L_newPolicyCreationDate', '<=', $endDate)->where('L_policyType', 'PEDAL_CYCLE')->with('agents')->orderBy('L_newPolicyCreationDate', 'ASC')->get();
        /* End */
        
        /* Logic build as per year - Start*/
//        $startDate = date('Y-01-01');
//        $endDate = date('Y-12-31');
        $startDate = date('2018-08-29');
        $endDate = date('2019-08-28');
        $leadList = \App\Http\Models\Lead::whereNotNull('L_newPolicyNumber')->where('L_newPolicyCreationDate', '>=', $startDate)->where('L_newPolicyCreationDate', '<=', $endDate)->whereNotIn('L_AGid', [12,17])->where('L_policyType', 'PEDAL_CYCLE')->with('agents')->orderBy('L_newPolicyCreationDate', 'ASC')->get();
//        echo "<pre>";
//        print_r($leadList->toArray());exit;
        /* End */
        
        $spreadsheet = new Spreadsheet();
        $repeatedEntriesSheet = new Worksheet($spreadsheet, 'Repeated Entries');
        $repeatedEntriesSheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $repeatedEntriesSheet->getStyle('A1:BY1')->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFFF00');
        $repeatedEntriesSheet->getStyle('A1:BY1')
                ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BY1')
                ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BY1')
                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BY1')
                ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BY1')->getAlignment()->applyFromArray(
                                    array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
                            );
        $repeatedEntriesSheet->setCellValue("A$i", "RM Name");
        $repeatedEntriesSheet->setCellValue("B$i", "State of Purchase");
        $repeatedEntriesSheet->setCellValue("C$i", "PolicyNumber");
        $repeatedEntriesSheet->setCellValue("D$i", "PolicyStartDate");
        $repeatedEntriesSheet->setCellValue("E$i", "PolicyEndDate");
        $repeatedEntriesSheet->setCellValue("F$i", "IMDNumber");
        $repeatedEntriesSheet->setCellValue("G$i", "PolicyTenure");
        $repeatedEntriesSheet->setCellValue("H$i", "Salutation");
        $repeatedEntriesSheet->setCellValue("I$i", "FirstName");
        $repeatedEntriesSheet->setCellValue("J$i", "LastName");
        $repeatedEntriesSheet->setCellValue("K$i", "MiddleName");
        $repeatedEntriesSheet->setCellValue("L$i", "DOB");
        $repeatedEntriesSheet->setCellValue("M$i", "EmailID");
        $repeatedEntriesSheet->setCellValue("N$i", "PanNo");
        $repeatedEntriesSheet->setCellValue("O$i", "MobileNumber");
        $repeatedEntriesSheet->setCellValue("P$i", "AddressLine1");
        $repeatedEntriesSheet->setCellValue("Q$i", "AddressLine2");
        $repeatedEntriesSheet->setCellValue("R$i", "AddressLine3");
        $repeatedEntriesSheet->setCellValue("S$i", "PinCode");
        $repeatedEntriesSheet->setCellValue("T$i", "GSTIN");
        $repeatedEntriesSheet->setCellValue("U$i", "BuyerState");
        $repeatedEntriesSheet->setCellValue("V$i", "SGST");
        $repeatedEntriesSheet->setCellValue("W$i", "CGST");
        $repeatedEntriesSheet->setCellValue("X$i", "UTGST");
        $repeatedEntriesSheet->setCellValue("Y$i", "IGST");
        $repeatedEntriesSheet->setCellValue("Z$i", "BurglaryorHouseBreakingorTheftUpto75SumInsured");
        $repeatedEntriesSheet->setCellValue("AA$i", "BurglaryorHouseBreakingorTheftUpto75Premium");
        $repeatedEntriesSheet->setCellValue("AB$i", "BurglaryorHouseBreakingorTheftUpto100SumInsured");
        $repeatedEntriesSheet->setCellValue("AC$i", "BurglaryorHouseBreakingorTheftUpto100Premium");
        $repeatedEntriesSheet->setCellValue("AD$i", "LossorDamagetoInsuredBicycleSumInsured");
        $repeatedEntriesSheet->setCellValue("AE$i", "LossorDamagetoInsuredBicyclePremium");
        $repeatedEntriesSheet->setCellValue("AG$i", "LossofPersonalBelongingsSumInsured");
        $repeatedEntriesSheet->setCellValue("AH$i", "LossofPersonalBelongingsPremium");
        $repeatedEntriesSheet->setCellValue("AH$i", "UsageforHireOrRewardCover");
        $repeatedEntriesSheet->setCellValue("AI$i", "ExtensionofGeographicalArea");
        $repeatedEntriesSheet->setCellValue("AJ$i", "RoadSideAsstCoverSumInsured");
        $repeatedEntriesSheet->setCellValue("AK$i", "RoadSideAsstCoverPremium");
        $repeatedEntriesSheet->setCellValue("AL$i", "PermanentTotalDisabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AM$i", "PermanentTotalDisabilityPremium");
        $repeatedEntriesSheet->setCellValue("AN$i", "PermanentPartialDisabilityPremium");
        $repeatedEntriesSheet->setCellValue("AO$i", "PermanentPartialDisabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AP$i", "TemporaryTotalDisabilityPremium");
        $repeatedEntriesSheet->setCellValue("AQ$i", "TemporaryTotalDisabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AR$i", "AccidentalDeathSumInsured");
        $repeatedEntriesSheet->setCellValue("AS$i", "AccidentalDeathPremium");
        $repeatedEntriesSheet->setCellValue("AT$i", "PublicLiabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AU$i", "TotalPremium");
        $repeatedEntriesSheet->setCellValue("AV$i", "BicycleMake");
        $repeatedEntriesSheet->setCellValue("AW$i", "BicycleModel");
        $repeatedEntriesSheet->setCellValue("AX$i", "TPSourceName");
        $repeatedEntriesSheet->setCellValue("AY$i", "QuotationNumber");
        $repeatedEntriesSheet->setCellValue("AZ$i", "PaymentSource");
        $repeatedEntriesSheet->setCellValue("BA$i", "PaymentDate");
        $repeatedEntriesSheet->setCellValue("BB$i", "TransactionID");
        $repeatedEntriesSheet->setCellValue("BC$i", "TPEmailID");
        $repeatedEntriesSheet->setCellValue("BD$i", "DealerCode");
        $repeatedEntriesSheet->setCellValue("BE$i", "BiCycleType");
        $repeatedEntriesSheet->setCellValue("BF$i", "Purchased");
        $repeatedEntriesSheet->setCellValue("BG$i", "Origin");
        $repeatedEntriesSheet->setCellValue("BH$i", "MakeCode");
        $repeatedEntriesSheet->setCellValue("BI$i", "ModelCode");
        $repeatedEntriesSheet->setCellValue("BJ$i", "ManfMonth");
        $repeatedEntriesSheet->setCellValue("BK$i", "ManfYear");
        $repeatedEntriesSheet->setCellValue("BL$i", "Chassis Number");
        $repeatedEntriesSheet->setCellValue("BM$i", "BicycleBodyType");
        $repeatedEntriesSheet->setCellValue("BN$i", "OutletName");
        $repeatedEntriesSheet->setCellValue("BO$i", "PlaceOfPurchase");
        $repeatedEntriesSheet->setCellValue("BP$i", "FrameType");
        $repeatedEntriesSheet->setCellValue("BQ$i", "PurchaseDate");
        $repeatedEntriesSheet->setCellValue("BR$i", "InvoiceNo");
        $repeatedEntriesSheet->setCellValue("BS$i", "PurchasePrice");
        $repeatedEntriesSheet->setCellValue("BT$i", "DeliveryDate");
        $repeatedEntriesSheet->setCellValue("BU$i", "BicycleSumInsured");
        $repeatedEntriesSheet->setCellValue("BV$i", "TotalSumInsured");
        $repeatedEntriesSheet->setCellValue("BW$i", "PolicyCreatedOn");
        $repeatedEntriesSheet->setCellValue("BX$i", "Accessories");
        $repeatedEntriesSheet->setCellValue("BY$i", "Accessories Details");
        $i = 2;
        foreach($leadList as $key => $val) {
            $rmName = '';
            if(!is_null($val->agents->rm)) {
                $rmName = $val->agents->rm->AG_firstName.' '.$val->agents->rm->AG_lastName;
            }
            $completeOrderDetails = json_decode($val['L_completeLeadDetails'], true);
            $postData = json_decode($val['L_appPostData'], true);
            $repeatedEntriesSheet->setCellValue("A$i", $rmName);
            $repeatedEntriesSheet->setCellValue("B$i", "{$completeOrderDetails['BuyerState']}");
            $repeatedEntriesSheet->setCellValue("C$i", $val['L_newPolicyNumber']);
            $repeatedEntriesSheet->setCellValue("D$i", $val['L_newPolicyStartDate']);
            $repeatedEntriesSheet->setCellValue("E$i", $val['L_newPolicyEndDate']);
            $repeatedEntriesSheet->setCellValue("F$i","{$completeOrderDetails['IMDNumber']}");
            $repeatedEntriesSheet->setCellValue("G$i","{$completeOrderDetails['PolicyTenure']}");
            $repeatedEntriesSheet->setCellValue("H$i","{$completeOrderDetails['CustmerObj']['Salutation']}");
            $repeatedEntriesSheet->setCellValue("I$i","{$completeOrderDetails['CustmerObj']['FirstName']}");
            $repeatedEntriesSheet->setCellValue("J$i","{$completeOrderDetails['CustmerObj']['LastName']}");
            $repeatedEntriesSheet->setCellValue("K$i","");
            $repeatedEntriesSheet->setCellValue("L$i","{$completeOrderDetails['CustmerObj']['DOB']}");
            $repeatedEntriesSheet->setCellValue("M$i","{$completeOrderDetails['CustmerObj']['EmailID']}");
            $repeatedEntriesSheet->setCellValue("N$i","");
            $repeatedEntriesSheet->setCellValue("O$i","{$completeOrderDetails['CustmerObj']['MobileNumber']}");
            $repeatedEntriesSheet->setCellValue("P$i","{$completeOrderDetails['CustmerObj']['AddressLine1']}");
            $repeatedEntriesSheet->setCellValue("Q$i","");
            $repeatedEntriesSheet->setCellValue("R$i","");
            $repeatedEntriesSheet->setCellValue("S$i","{$completeOrderDetails['CustmerObj']['PinCode']}");
            $repeatedEntriesSheet->setCellValue("T$i","{$completeOrderDetails['GSTIN']}");
            $repeatedEntriesSheet->setCellValue("U$i","{$completeOrderDetails['BuyerState']}");
            $repeatedEntriesSheet->setCellValue("V$i","{$completeOrderDetails['SGST']}");
            $repeatedEntriesSheet->setCellValue("W$i","{$completeOrderDetails['CGST']}");
            $repeatedEntriesSheet->setCellValue("X$i","{$completeOrderDetails['UTGST']}");
            $repeatedEntriesSheet->setCellValue("Y$i","{$completeOrderDetails['IGST']}");
            $repeatedEntriesSheet->setCellValue("Z$i","{$completeOrderDetails['lstDomPedalCovers'][0]['BurglaryorHouseBreakingorTheftUpto75SumInsured']}");
            $repeatedEntriesSheet->setCellValue("AA$i","{$completeOrderDetails['lstDomPedalCovers'][0]['BurglaryorHouseBreakingorTheftUpto75Premium']}");
            $repeatedEntriesSheet->setCellValue("AB$i","");
            $repeatedEntriesSheet->setCellValue("AC$i","");
            $repeatedEntriesSheet->setCellValue("AD$i","");
            $repeatedEntriesSheet->setCellValue("AE$i","");
            $repeatedEntriesSheet->setCellValue("AF$i","");
            $repeatedEntriesSheet->setCellValue("AG$i","");
            $repeatedEntriesSheet->setCellValue("AH$i","");
            $repeatedEntriesSheet->setCellValue("AI$i","");
            $repeatedEntriesSheet->setCellValue("AJ$i","");
            $repeatedEntriesSheet->setCellValue("AK$i","");
            $repeatedEntriesSheet->setCellValue("AL$i","");
            $repeatedEntriesSheet->setCellValue("AM$i","");
            $repeatedEntriesSheet->setCellValue("AN$i","");
            $repeatedEntriesSheet->setCellValue("AO$i","");
            $repeatedEntriesSheet->setCellValue("AP$i","");
            $repeatedEntriesSheet->setCellValue("AQ$i","");
            $repeatedEntriesSheet->setCellValue("AR$i","{$completeOrderDetails['lstDomPedalCovers'][0]['AccidentalDeathSumInsured']}");
            $repeatedEntriesSheet->setCellValue("AS$i","{$completeOrderDetails['lstDomPedalCovers'][0]['AccidentalDeathPremium']}");
            $repeatedEntriesSheet->setCellValue("AT$i","{$completeOrderDetails['lstDomPedalCovers'][0]['PublicLiabilitySumInsured']}");
            $repeatedEntriesSheet->setCellValue("AU$i","{$completeOrderDetails['TotalPremium']}");
            $repeatedEntriesSheet->setCellValue("AV$i","{$postData['vehicle_make_text']}");
            $repeatedEntriesSheet->setCellValue("AW$i","{$postData['vehicle_model_text']}");
            $repeatedEntriesSheet->setCellValue("AX$i","{$completeOrderDetails['TPSourceName']}");
            $repeatedEntriesSheet->setCellValue("AY$i","{$completeOrderDetails['QuotationNumber']}");
            $repeatedEntriesSheet->setCellValue("AZ$i","{$completeOrderDetails['PaymentSource']}");
//            $repeatedEntriesSheet->setCellValue("BA$i","{$completeOrderDetails['PaymentDate']}");
            $repeatedEntriesSheet->setCellValue("BA$i","{$val->L_newPolicyCreationDate}");
            $repeatedEntriesSheet->setCellValue("BB$i","{$completeOrderDetails['TransactionID']}");
            $repeatedEntriesSheet->setCellValue("BC$i","{$completeOrderDetails['TPEmailID']}");
            $repeatedEntriesSheet->setCellValue("BD$i","{$completeOrderDetails['DealerCode']}");
            $repeatedEntriesSheet->setCellValue("BE$i","{$completeOrderDetails['BiCycleType']}");
            $repeatedEntriesSheet->setCellValue("BF$i","{$completeOrderDetails['Purchased']}");
            $repeatedEntriesSheet->setCellValue("BG$i","");
            $repeatedEntriesSheet->setCellValue("BH$i","{$completeOrderDetails['MakeCode']}");
            $repeatedEntriesSheet->setCellValue("BI$i","{$completeOrderDetails['ModelCode']}");
            $repeatedEntriesSheet->setCellValue("BJ$i","{$completeOrderDetails['ManfMonth']}");
            $repeatedEntriesSheet->setCellValue("BK$i","{$completeOrderDetails['ManfYear']}");
            $repeatedEntriesSheet->setCellValue("BL$i","{$val->L_vehicleChassisNo}");
            $repeatedEntriesSheet->setCellValue("BM$i","");
            $repeatedEntriesSheet->setCellValue("BN$i","");
            $repeatedEntriesSheet->setCellValue("BO$i","");
            $repeatedEntriesSheet->setCellValue("BP$i","{$completeOrderDetails['FrameType']}");
            $repeatedEntriesSheet->setCellValue("BQ$i","{$completeOrderDetails['PurchaseDate']}");
            $repeatedEntriesSheet->setCellValue("BR$i","{$completeOrderDetails['InvoiceNo']}");
            $repeatedEntriesSheet->setCellValue("BS$i","{$completeOrderDetails['PurchasePrice']}");
            $repeatedEntriesSheet->setCellValue("BT$i","{$completeOrderDetails['DeliveryDate']}");
            $repeatedEntriesSheet->setCellValue("BU$i","{$completeOrderDetails['BicycleSumInsured']}");
            $repeatedEntriesSheet->setCellValue("BV$i", "{$completeOrderDetails['TotalSumInsured']}");
            $repeatedEntriesSheet->setCellValue("BW$i", "{$val->L_newPolicyCreationDate}");
            $repeatedEntriesSheet->setCellValue("BX$i", (($val->L_vehicleAccessories==1)?'Yes':'No'));
            $accessoriesDetails = json_decode($val->L_vehicleAccessoryDetails);
            $accessories = array();
            if(!empty($accessoriesDetails)) {
                foreach($accessoriesDetails as $akey => $aval) {
                    $accessories['accessory_detail'] = $aval->accessory_detail;;
                    $accessories['accessory_price'] = $aval->accessory_price;
                    $accessories['accessory_type'] = $aval->accessory_type;
                }
            }
            $repeatedEntriesSheet->setCellValue("BY$i", json_encode($accessories));
            $i++;
        } 
//        exit;
        $writer = new Xlsx($spreadsheet);
        $fileName = time() . '-PolicyReport' . '.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save("MIS_report/$fileName");
        
        if(!empty($fileName)) {
            $emails = config('constant.MIS_REPORT_EMAILS');
            $bccEmails = config('constant.MIS_REPORT_EMAILS_BCC');
            $data = array();
            Mail::send('emails.mis_report', $data, function($message) use ($emails, $bccEmails, $fileName)
            {
                $message->subject('Pedal Cycle - MIS Report');
                $message->from('noreply@pedalcycle.com', 'Pedal Cycle');
//                $message->to('jainajay1989@gmail.com');
                $message->to($emails);
                $message->bcc($bccEmails);
//                $message->to('shailendra.kulria@fondostech.in');
//                $message->cc('jainajay1989@gmail.com');
                $message->attach("MIS_report/$fileName");
            });
        }
        exit;
    }
}