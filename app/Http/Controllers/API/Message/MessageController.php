<?php

namespace App\Http\Controllers\API\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Message;
use Carbon\Carbon;

class MessageController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index
     *
     * @param Request $request
     * @return Mixed
     */
    public function index(Request $request)
    {
        $message_list = null;

        // Get Request Variables
        $agent_id = $request['agent_id'];
        //----------------------

        $return = Message::getAgentMessages($agent_id);
        if($return['status'] == true)
        {
            $message_list = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S100';
            $response['message'] = 'Successful';
            $response['data'] = $message_list;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Detail
     *
     * @param Request $request
     * @return Mixed
     */
    public function detail(Request $request)
    {
        $message_detail = null;

        // Get Request Variables
        $message_id = $request['id'];
        //----------------------

        $return = Message::getMessageDetail($message_id);
        if($return['status'] == true)
        {
            $message_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S200';
            $response['message'] = 'Successful';
            $response['data'] = $message_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E200';
            $response['message'] = 'Unsuccessful';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }


    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function add(Request $request)
    {
        $message_detail = null;
        
        // Insert Message
        $Message = new Message();
        $Message->field['MG_Lid'] = $request['lead_id'];
        $Message->field['MG_type'] = config('constant.message_type.ATB.value');
        $Message->field['MG_from'] = $request['agent_id'];
        $Message->field['MG_to'] = 0;
        $Message->field['MG_subject'] = $request['subject'];
        $Message->field['MG_message'] = $request['message'];
        $Message->field['MG_read'] = 1;
        $return = $Message->addMessage();
        //------------

        if($return['status'] == true)
        {
            $vehicle_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S300';
            $response['message'] = 'Message sent successfully';
            $response['data'] = $vehicle_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E300';
            $response['message'] = 'Unable to send message, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Read Message
     *
     * @param Request $request
     * @return Mixed
     */
    public function read(Request $request)
    {
        $message_id = null;

        // Fetch Request Variables
        $message_id = $request['id'];
        //------------------------

        // Update Message
        $Message = new Message();
        $Message->field['MG_id'] = $message_id;
        $Message->field['MG_read'] = 1;
        $return = $Message->readMessage();
        //------------

        if($return['status'] == true)
        {
            $vehicle_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Message read successfully';
            $response['data'] = $vehicle_detail;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E300';
            $response['message'] = 'Unable to read message, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

}