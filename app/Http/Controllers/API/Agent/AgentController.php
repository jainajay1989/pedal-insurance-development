<?php

namespace App\Http\Controllers\API\Agent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Lead;
use App\Http\Models\State;
use App\Http\Models\City;
use App\Http\Models\CityPincode;
use App\Http\Models\PayuLog;
use \App\Http\Models\Vehicle;
use \App\Http\Models\Agent;
use Carbon\Carbon,
    Storage;

class AgentController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }
    
    public function walletPaymentResponse(Request $request) {
        Mail::raw(json_encode($request->all()), function($message)
        {
            $message->from('us@example.com', 'Laravel');
            $message->to('engineering@fondostech.in');
        });
        $response['status'] = 1;
        $response['code'] = 'S200';
        $response['message'] = 'Amount added to wallet successfully';
        return response()->json($response);
    }
    
    public function addMoney(Request $request) {
        Mail::raw(json_encode($request->all()), function($message)
        {
            $message->from('us@example.com', 'Laravel');
            $message->to('engineering@fondostech.in');
        });
        $response['status'] = 1;
        $response['code'] = 'S200';
        $response['message'] = 'Amount added to wallet successfully';
        return response()->json($response);
    }

}
