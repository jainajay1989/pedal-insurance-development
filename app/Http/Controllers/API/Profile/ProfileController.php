<?php

namespace App\Http\Controllers\API\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Agent;
use Carbon;

class ProfileController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Update Profile
     *
     * @param Request $request
     * @return Mixed
     */
    public function updateProfile(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $Agent->field['AG_firstName'] = $request['first_name'];
        $Agent->field['AG_lastName'] = $request['last_name'];
        $Agent->field['AG_email'] = $request['email'];
        $Agent->field['AG_password'] = $request['password'];
        $Agent->field['AG_mobile'] = $request['mobile'];
        $Agent->field['AG_phone'] = $request['phone'];
        $Agent->field['AG_address'] = $request['address'];
        $Agent->field['AG_city'] = $request['city'];
        $Agent->field['AG_state'] = $request['state'];
        $Agent->field['AG_zip'] = $request['zip'];
        $return = $Agent->updateAgentDetail();

        if($return['status'] == true)
        {
            $agent_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Profile updated successfully';
            $response['data'] = $agent_detail;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Unable to update profile, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    /**
     * Update Password
     *
     * @param Request $request
     * @return Mixed
     */
    public function updatePassword(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $Agent->field['AG_password'] = $request['password'];
        $return = $Agent->updateAgentPassword();

        if($return['status'] == true)
        {
            $agent_detail = $return['data'];

            $response['status'] = 1;
            $response['code'] = 'S500';
            $response['message'] = 'Password updated successfully';
            $response['data'] = $agent_detail;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E500';
            $response['message'] = 'Unable to update password, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }

    public function getWalletBalance(Request $request) {
        // Fetch Request Variables
        $agent_id = $request['agent_id'];
        //------------------------

        $return = \App\Http\Models\Ledger::getAgentAccountBalance($agent_id);
        if($return['status'] == true)
        {

            $response['status'] = 1;
            $response['code'] = 'S500';
            $response['message'] = 'Wallet balance fetached successfully';
            $response['data'] = $return['data'];
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E500';
            $response['message'] = 'Unable to get wallet balance, please try again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
        }

        return response()->json($response);
    }
}