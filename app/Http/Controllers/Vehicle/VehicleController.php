<?php

namespace App\Http\Controllers\Vehicle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use App\Http\Models\State;
use App\Http\Models\City;
use App\Http\Models\UploadModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use \App\Http\Models\Vehicle;
use Auth;
use Carbon\Carbon,
    Storage;

class VehicleController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'Vehicle-Upload';
    }

    public function uploadedList() {
        // Check Permission
        if (check_permission('VEHICLE_UPLOAD_LIST', Auth::user()->A_type) == false) {
            return redirect(url());
        }
        //-----------------
        // Get List
        $return = UploadModel::getList();
        //-----------------
        $this->viewData['list'] = $return['data'];
        return view('vehicle.uploadlist', $this->viewData);
    }

    public function uploadVehicle1(Request $request) {
        try {
            if ($request->isMethod('post')) {
                if (!empty($request['vehicle_models'])) {
                    $vehicleModels = $request['vehicle_models'];
                    if ($vehicleModels->getClientOriginalExtension() != 'xlsx' && $vehicleModels->getClientOriginalExtension() != 'XLSX') {
                        throw new \Exception('Only xlsx files are allowed');
                    }
                    $fileName = 'vehicle_models_' . Carbon::now()->timestamp . '.' . $vehicleModels->getClientOriginalExtension();
                    Storage::disk('models_list')->putFileAs('', $vehicleModels, $fileName);
                    $filePath = storage_path('app/public/vehicle/uploaded/').$fileName;//env('APP_URL')."storage/app/public/vehicle/".$fileName;// . storage_path('app/public/vehicle');
                    //Upload Code
                    $reader = IOFactory::createReader('Xlsx');
                    $reader->setReadDataOnly(TRUE);
                    $spreadsheet = $reader->load($filePath);
                    $worksheet = $spreadsheet->getActiveSheet();
                    $totalRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();

                    $spreadsheet = new Spreadsheet();
                    $repeatedEntriesSheet = new \PhpOffice\PhpSpreadsheet\Worksheet($spreadsheet, 'Repeated Models');
                    $spreadsheet->addSheet($repeatedEntriesSheet, 0);
                    $invalidEntriesSheet = new \PhpOffice\PhpSpreadsheet\Worksheet($spreadsheet, 'Invalid Entries');
                    $spreadsheet->addSheet($invalidEntriesSheet, 1);

                    $invalidEntriesSheet = $spreadsheet->getSheetByName('Invalid Entries');
                    $repeatedEntriesSheet = $spreadsheet->getSheetByName('Repeated Models');
                    $i = 2;
                    $j = 2;
                    $invalidCount = 0;
                    $repeatedCount = 0;
                    $successCount = 0;
                    $motorInsuranceFormCount = 0;
                    $error = false;
                    $excelColumnArr = array();
                    $error = false;
                    foreach ($worksheet->getRowIterator() as $rowKey => $row) {

                        if ($rowKey == 1) {
                            $excelColumnArr["A"] = 'Make';
                            $excelColumnArr["B"] = 'Make Code';
                            $excelColumnArr["C"] = 'Model';
                            $excelColumnArr["D"] = 'Model Code';
                            $excelColumnArr["E"] = 'Price';
                            $excelColumnArr["F"] = 'Status';

                            foreach ($excelColumnArr as $excelKey => $excelVal) {

                                /* Used to set invalid leads excel columns */
                                $invalidEntriesSheet->getStyle($excelKey . $rowKey)->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFFF00');
                                $invalidEntriesSheet->setCellValue($excelKey . $rowKey, $excelVal);

                                /* Used to set repeat entry lead excel columns */
                                $repeatedEntriesSheet->getStyle($excelKey . $rowKey)->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFFF00');
                                $repeatedEntriesSheet->setCellValue($excelKey . $rowKey, $excelVal);
                            }
                            continue;
                        }

                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(FALSE);
                        $data = array();
                        $makeEmpty = false;
                        $makeCodeEmpty = false;
                        $modelEmpty = false;
                        $modelCodeEmpty = false;
                        $count = 0;
                        $makeError = false;
                        $makeCodeError = false;
                        foreach ($cellIterator as $key => $cell) {
                            if ($key == 'A') {
                                $make = trim($cell->getValue());
                                if (empty($make)) {
                                    $makeEmpty = true;
                                }
                            }
                            if ($key == 'B') {
                                $makeCode = trim($cell->getValue());
                                if (empty($makeCode)) {
                                    $makeCodeEmpty = true;
                                } else {
                                    $isMakeCorrect = Vehicle::checkMakeExist($make, $makeCode);
                                    $isMakeCodeCorrect = Vehicle::checkMakeCodeExist($make, $makeCode);
                                    if($isMakeCorrect) {
                                        $makeError = true;
                                    }
                                    if($isMakeCodeCorrect) {
                                        $makeCodeError = true;
                                    }
                                }
                            }
                            if ($key == 'C') {
                                $model= trim($cell->getValue());
                                if (empty($model)) {
                                    $modelEmpty = true;
                                }
                            }
                            if ($key == 'D') {
                                $modelCode = trim($cell->getValue());
                                if (empty($modelCode)) {
                                    $modelCodeEmpty = true;
                                } else {
                                    $count = Vehicle::checkModelExist($modelCode);
                                }
                            }
                            $data[$key] = trim($cell->getValue());
                        }
                        if (!empty($makeCodeEmpty) || !empty($makeEmpty) || !empty($modelCodeEmpty) || !empty($modelEmpty) || !empty($makeError) || !empty($makeCodeError)) {
                            foreach ($data as $dataKey => $dataVal) {
                                $errorFlag = false;
                                if ((!empty($makeEmpty) && $dataKey == 'A') || ($makeError && $dataKey == 'A')) {
                                    $errorFlag = true;
                                }
                                if ((!empty($makeCodeEmpty) && $dataKey == 'B') || ($makeCodeError && $dataKey == 'B')) {
                                    $errorFlag = true;
                                }
                                if (!empty($modelEmpty) && $dataKey == 'C') {
                                    $errorFlag = true;
                                }
                                if (!empty($modelCodeEmpty) && $dataKey == 'D') {
                                    $errorFlag = true;
                                }

                                if ($errorFlag) {
                                    $invalidEntriesSheet->getStyle($dataKey . $i)->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FFFF0000');
                                }
                                $invalidEntriesSheet->setCellValue($dataKey . $i, $dataVal);
                            }
                            $i++;
                            $invalidCount++;
                            $error = true;
                        } elseif ($makeError || $count >= 1) {
                            foreach ($data as $dataKey => $dataVal) {
                                $errorFlag = false;
                                if ($dataKey == 'D' && $count >= 1) {
                                    $errorFlag = true;
                                }
                                if ($errorFlag) {
                                    $repeatedEntriesSheet->getStyle($dataKey . $j)->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FFFF0000');
                                }
                                $repeatedEntriesSheet->setCellValue($dataKey . $j, $dataVal);
                                $errorFlag = false;
                            }
                            $j++;
                            $repeatedCount++;
                            $error = true;
                        } else {
                            $Vehicle = new Vehicle;
                            $Vehicle->field['V_make'] = trim($data['A']);
                            $Vehicle->field['V_code'] = trim($data['B']);
                            $Vehicle->field['V_model'] = trim($data['C']);
                            $Vehicle->field['V_modelcode'] = trim($data['D']);
                            $Vehicle->field['V_price'] = trim($data['E']);
                            $Vehicle->field['V_active'] = 1;
                            $Vehicle->addVehicle();
                            $successCount++;
                        }
                    }
                    $errorFileName = '';
                    if ($error) {
                        $writer = new Xlsx($spreadsheet);
                        $errorFileName = time() . '_Upload-Error-Report' . '.xlsx';
                        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
//                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//                        header('Content-Disposition: attachment; filename="Vehicle_Models.xlsx"');
//                        $writer->save("php://output");
                        $writer->save(storage_path('app/public/vehicle/error/').$errorFileName);
                    }
                    
                    $uploadVehicle = new UploadModel();
                    $uploadVehicle->field['file'] = $fileName;
                    $uploadVehicle->field['created_at'] = date('Y-m-d H:i:s');
                    $uploadVehicle->field['total'] = $totalRow-1;
                    $uploadVehicle->field['success'] = $successCount;
                    $uploadVehicle->field['error'] = $invalidCount;
                    $uploadVehicle->field['repeated'] = $repeatedCount;
                    $uploadVehicle->field['error_report'] = $errorFileName;
                    $uploadVehicle->addRow();
                    
                    $redirect['url'] = url('/vehicle/upload-list');
                    $redirect['message'] = ['msg' => 'Vehicle Model Uploaded Successfully', 'type' => 'success'];
                    return redirect($redirect['url'])->with($redirect['message']);
                }
            }
        } catch (\Exception $ex) {
            $redirect['url'] = url('/vehicle/upload-vehicle');
            $redirect['message'] = ['msg' => $ex->getMessage(), 'type' => 'error'];
            return redirect($redirect['url'])->with($redirect['message']);
        }
        return view('vehicle.uploadvehicle', $this->viewData);
    }
    
    public function uploadVehicle(Request $request) {
        try {
            if ($request->isMethod('post')) {
                if (!empty($request['vehicle_models'])) {
                    $vehicleModels = $request['vehicle_models'];
                    if ($vehicleModels->getClientOriginalExtension() != 'xlsx' && $vehicleModels->getClientOriginalExtension() != 'XLSX') {
                        throw new \Exception('Only xlsx files are allowed');
                    }
                    $fileName = 'vehicle_models_' . Carbon::now()->timestamp . '.' . $vehicleModels->getClientOriginalExtension();
                    Storage::disk('models_list')->putFileAs('', $vehicleModels, $fileName);
                    $filePath = storage_path('app/public/vehicle/uploaded/').$fileName;//env('APP_URL')."storage/app/public/vehicle/".$fileName;// . storage_path('app/public/vehicle');
                    //Upload Code
                    $reader = IOFactory::createReader('Xlsx');
                    $reader->setReadDataOnly(TRUE);
                    $spreadsheet = $reader->load($filePath);
                    $worksheet = $spreadsheet->getActiveSheet();
                    $totalRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $maxCell = $worksheet->getHighestRowAndColumn();
                    $filterData = $worksheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row'], NULL, TRUE, TRUE, TRUE);
                    $filterData = array_map('array_filter', $filterData);
                    $filterData = array_filter($filterData);
                    
                    $spreadsheet = new Spreadsheet();
                    $repeatedEntriesSheet = new \PhpOffice\PhpSpreadsheet\Worksheet($spreadsheet, 'Repeated Models');
                    $spreadsheet->addSheet($repeatedEntriesSheet, 0);
                    $invalidEntriesSheet = new \PhpOffice\PhpSpreadsheet\Worksheet($spreadsheet, 'Invalid Entries');
                    $spreadsheet->addSheet($invalidEntriesSheet, 1);

                    $invalidEntriesSheet = $spreadsheet->getSheetByName('Invalid Entries');
                    $repeatedEntriesSheet = $spreadsheet->getSheetByName('Repeated Models');
                    $i = 2;
                    $j = 2;
                    $invalidCount = 0;
                    $repeatedCount = 0;
                    $successCount = 0;
                    $motorInsuranceFormCount = 0;
                    $error = false;
                    $excelColumnArr = array();
                    $error = false;
                    /* Error Report Columns */
                    $excelColumnArr["A"] = 'Make';
                    $excelColumnArr["B"] = 'Make Code';
                    $excelColumnArr["C"] = 'Model';
                    $excelColumnArr["D"] = 'Model Code';
                    $excelColumnArr["E"] = 'Price';
                    $excelColumnArr["F"] = 'Status';

                    $rowKey = 1;
                    foreach ($excelColumnArr as $excelKey => $excelVal) {

                        /* Used to set invalid leads excel columns */
                        $invalidEntriesSheet->getStyle($excelKey . $rowKey)->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFFF00');
                        $invalidEntriesSheet->setCellValue($excelKey . $rowKey, $excelVal);

                        /* Used to set repeat entry lead excel columns */
                        $repeatedEntriesSheet->getStyle($excelKey . $rowKey)->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFFF00');
                        $repeatedEntriesSheet->setCellValue($excelKey . $rowKey, $excelVal);
                    }
                    /* End */

                    foreach ($filterData as $key => $val) {
                        $data = array();
                        $makeEmpty = false;
                        $makeCodeEmpty = false;
                        $modelEmpty = false;
                        $modelCodeEmpty = false;
                        $count = 0;
                        $makeError = false;
                        $makeCodeError = false;
                        if($key == 1) {
                            continue;
                        }

                        $make = (!empty(trim($val['A']))?trim($val['A']):NULL);
                        if (empty($make)) {
                            $makeEmpty = true;
                        }
                        
                        $makeCode = (!empty(trim($val['B']))?trim($val['B']):NULL);
                        if (empty($makeCode)) {
                            $makeCodeEmpty = true;
                        } else {
                            $isMakeCorrect = Vehicle::checkMakeExist($make, $makeCode);
                            $isMakeCodeCorrect = Vehicle::checkMakeCodeExist($make, $makeCode);
                            if($isMakeCorrect) {
                                $makeError = true;
                            }
                            if($isMakeCodeCorrect) {
                                $makeCodeError = true;
                            }
                        }
                        
                        $model= (!empty(trim($val['C']))?trim($val['C']):NULL);
                        if (empty($model)) {
                            $modelEmpty = true;
                        }
                        
                        $modelCode = (!empty(trim($val['D']))?trim($val['D']):NULL);
                        if (empty($modelCode)) {
                            $modelCodeEmpty = true;
                        } else {
                            $count = Vehicle::checkModelExist($modelCode);
                        }
                        
                        if (!empty($makeCodeEmpty) || !empty($makeEmpty) || !empty($modelCodeEmpty) || !empty($modelEmpty) || !empty($makeError) || !empty($makeCodeError)) {
                        foreach ($val as $dataKey => $dataVal) {
                            $errorFlag = false;
                            if ((!empty($makeEmpty) && $dataKey == "A") || ($makeError && $dataKey == "A")) {
                                $errorFlag = true;
                            }
                            if ((!empty($makeCodeEmpty) && $dataKey == "B") || ($makeCodeError && $dataKey == "B")) {
                                $errorFlag = true;
                            }
                            if (!empty($modelEmpty) && $dataKey == "C") {
                                $errorFlag = true;
                            }
                            if (!empty($modelCodeEmpty) && $dataKey == "D") {
                                $errorFlag = true;
                            }

                            if ($errorFlag) {
                                $invalidEntriesSheet->getStyle($dataKey . $i)->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFFF0000');
                            }
                            $invalidEntriesSheet->setCellValue($dataKey . $i, $dataVal);
                        }
                        $i++;
                        $invalidCount++;
                        $error = true;
                    } elseif ($makeError || $count >= 1) {
                        foreach ($val as $dataKey => $dataVal) {
                            $errorFlag = false;
                            if ($dataKey == 'D' && $count >= 1) {
                                $errorFlag = true;
                            }
                            if ($errorFlag) {
                                $repeatedEntriesSheet->getStyle($dataKey . $j)->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFFF0000');
                            }
                            $repeatedEntriesSheet->setCellValue($dataKey . $j, $dataVal);
                            $errorFlag = false;
                        }
                        $j++;
                        $repeatedCount++;
                        $error = true;
                    } else {
                        $Vehicle = new Vehicle;
                        $Vehicle->field['V_make'] = trim($val['A']);
                        $Vehicle->field['V_code'] = trim($val['B']);
                        $Vehicle->field['V_model'] = trim($val['C']);
                        $Vehicle->field['V_modelcode'] = trim($val['D']);
                        $Vehicle->field['V_price'] = (!empty($val['E'])?trim($val['E']):NULL);
                        $Vehicle->field['V_active'] = 1;
                        $Vehicle->addVehicle();
                        $successCount++;
                    }
                }
                $errorFileName = '';
                if ($error) {
                    $writer = new Xlsx($spreadsheet);
                    $errorFileName = time() . '_Upload-Error-Report' . '.xlsx';
                    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
//                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//                        header('Content-Disposition: attachment; filename="Vehicle_Models.xlsx"');
//                        $writer->save("php://output");
                    $writer->save(storage_path('app/public/vehicle/error/').$errorFileName);
                }
                    
                    $uploadVehicle = new UploadModel();
                    $uploadVehicle->field['file'] = $fileName;
                    $uploadVehicle->field['created_at'] = date('Y-m-d H:i:s');
                    $uploadVehicle->field['total'] = count($filterData)-1;
                    $uploadVehicle->field['success'] = $successCount;
                    $uploadVehicle->field['error'] = $invalidCount;
                    $uploadVehicle->field['repeated'] = $repeatedCount;
                    $uploadVehicle->field['error_report'] = $errorFileName;
                    $uploadVehicle->addRow();
                    
                    $redirect['url'] = url('/vehicle/upload-list');
                    $redirect['message'] = ['msg' => 'Vehicle Model Uploaded Successfully', 'type' => 'success'];
                    return redirect($redirect['url'])->with($redirect['message']);
                }
            }
        } catch (\Exception $ex) {
            $redirect['url'] = url('/vehicle/upload-vehicle');
            $redirect['message'] = ['msg' => $ex->getMessage(), 'type' => 'error'];
            return redirect($redirect['url'])->with($redirect['message']);
        }
        return view('vehicle.uploadvehicle', $this->viewData);
    }
}
