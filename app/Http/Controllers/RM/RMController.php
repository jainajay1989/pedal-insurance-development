<?php
namespace App\Http\Controllers\RM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Agent;
use App\Http\Models\InsuranceCompany;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\CommissionPayout;
use App\Http\Models\Admin;
use App\Http\Models\State;
use App\Http\Models\City;
use Auth;

class RMController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'rm';
    }

    /**
     * View dealers.
     *
     * @return view
     */
    public function index()
    {
        $fn_status = true;
        $agent_list = null;
        $where = ['AG_type' => config('constant.agent_type.DEALER.value')];

        // Get All Agents
        if($fn_status == true)
        {
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value') || Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $where = ['AG_type' => config('constant.agent_type.DEALER.value'), 'AG_Aid' => Auth::user()->A_id];
            }

            $return = Agent::getAllAgents($where);
            if($return['status'] == true)
            {
                $agent_list = $return['data'];
            }
        }
        //---------------

        $this->viewData['agent_list'] = $agent_list;
        return view('rm.index', $this->viewData);
    }

    /**
     * Add agent.
     *
     * @return view
     */
    public function create()
    {
        $fn_status = true;
        $policy_type_list = [];
        $user_list = [];
        $state_list = [];
        $city_list = [];

        // Check Permission
        if(check_permission('AGENT_CREATE', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Get Agent Type List
        if($fn_status == true)
        {
            foreach(config('constant.agent_type') as $status)
            {
                $agent_type_list[$status['value']] = $status['caption'];
            }
        }
        //--------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        // Get Admin List
        if($fn_status == true)
        {
            $return = Admin::getAllUsers();
            if($return['status'] == true)
            {
                $users = $return['data'];
                $user_list[''] = 'Select';
                foreach($users as $user)
                {
                    $user_list[$user->A_id] = $user->A_firstName.' '.$user->A_lastName;
                }
            }
        }
        //-----------------

        // Get State List
        if($fn_status == true)
        {
            $return = State::getAllStates();
            if($return['status'] == true)
            {
                $states = $return['data'];
                $state_list[''] = 'Select';
                foreach($states as $state)
                {
                    $state_list[$state->ST_code] = $state->ST_name;
                }
            }
        }
        //-----------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
                $city_list = $return['data'];
                /*$cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }*/
            }
        }
        //--------------

        $this->viewData['agent_type_list'] = $agent_type_list;
        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['user_list'] = $user_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        return view('rm.create', $this->viewData);
    }

    /**
     * Edit agent.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $fn_status = true;
        $policy_type_list = [];
        $agent_id = null;
        $state_list = [];
        $city_list = [];
        $agent_detail = null;
        $user_list = [];

        // Check Permission
        if(check_permission('AGENT_CREATE', Auth::user()->A_type) == false && check_permission('AGENT_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $agent_id = intval($request['id']);
        //------------------------

        // Get Agent Type List
        if($fn_status == true)
        {
            foreach(config('constant.agent_type') as $status)
            {
                $agent_type_list[$status['value']] = $status['caption'];
            }
        }
        //--------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        // Get Admin List
        if($fn_status == true)
        {
            $return = Admin::getAllUsers();
            if($return['status'] == true)
            {
                $users = $return['data'];
                $user_list[''] = 'Select';
                foreach($users as $user)
                {
                    $user_list[$user->A_id] = $user->A_firstName.' '.$user->A_lastName;
                }
            }
        }
        //-----------------

        // Get State List
        if($fn_status == true)
        {
//            $return = State::getAllStates();
//            if($return['status'] == true)
//            {
//                $states = $return['data'];
//                $state_list[''] = 'Select';
//                foreach($states as $state)
//                {
//                    $state_list[$state->ST_code] = $state->ST_name;
//                }
//            }
            $countryId = config('constant.COUNTRY_ID_INDIA');
            $states = State::getStateByCountry($countryId);
            $state_list[''] = 'Select';
            foreach($states as $state)
            {
                $state_list[$state->ST_code] = $state->ST_name;
            }
        }
        //---------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
                //$city_list = $return['data'];
                $cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }
            }
        }
        //--------------

        // Get Agent Detail
        if($fn_status == true)
        {
            $return = Agent::getAgentDetail($agent_id);
            if($return['status'] == true)
            {
                $agent_detail = $return['data'];
                $agent_detail->AG_policyType = explode(',', $agent_detail->AG_policyType);
            }
        }
        //-----------------

        $this->viewData['agent_type_list'] = $agent_type_list;
        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['user_list'] = $user_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        $this->viewData['agent_detail'] = $agent_detail;
        return view('rm.edit', $this->viewData);
    }

    /**
     * Add agent.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $company_list = null;
        $product_list = null;
        $redirect = null;

        $Agent = new Agent();
        $Agent->field['AG_type'] = config('constant.agent_type.DEALER.value');
        $Agent->field['AG_code'] = $request['code'];
        $Agent->field['AG_policyType'] = config('constant.policy_type.PEDAL_CYCLE.value');//$request['policy_type'];
        $Agent->field['AG_firstName'] = $request['first_name'];
        $Agent->field['AG_lastName'] = $request['last_name'];
        $Agent->field['AG_email'] = $request['email'];
        $Agent->field['AG_username'] = $request['username'];
        $Agent->field['AG_password'] = $request['password'];
        $Agent->field['AG_mobile'] = $request['mobile'];
        $Agent->field['AG_phone'] = $request['phone'];
        $Agent->field['AG_address'] = $request['address'];
        $Agent->field['AG_city'] = $request['city'];
        $Agent->field['AG_state'] = $request['state'];
        $Agent->field['AG_zip'] = $request['zip'];
        $Agent->field['AG_active'] = $request['active'];
        $return = $Agent->addAgent();
        if($return['status'] == true)
        {
            $agent_detail = $return['data'];

            // Get All Companies
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $company_list = $return['data'];
            }
            //------------------

            // Get All Products
            $return = InsuranceProduct::getAllProducts();
            if($return['status'] == true)
            {
                $product_list = $return['data'];
            }
            //-----------------

            // Add Agents Payout
            foreach($company_list as $company)
            {
                foreach($product_list as $product)
                {
                    if($product->IP_policyType == $Agent->field['AG_policyType'])
                    {
                        // Insert Commission Payout
                        $CommissionPayout = new CommissionPayout();
                        $CommissionPayout->field['CP_AGid'] = $agent_detail->AG_id;
                        $CommissionPayout->field['CP_ICid'] = $company->IC_id;
                        $CommissionPayout->field['CP_IPid'] = $product->IP_id;
                        $CommissionPayout->field['CP_payout'] = config('constant.commission_payout.DEFAULT.value');
                        $CommissionPayout->field['CP_active'] = $request['active'];
                        $return = $CommissionPayout->addPayout();
                        //-------------------------
                    }
                }
            }
            //------------------

            $redirect['url'] = url('dealer/edit', ['id' => $agent_detail->AG_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('dealer/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update agent detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $Agent->field['AG_policyType'] = config('constant.policy_type.PEDAL_CYCLE.value');//$request['policy_type'];
        $Agent->field['AG_firstName'] = $request['first_name'];
        $Agent->field['AG_lastName'] = $request['last_name'];
        $Agent->field['AG_email'] = $request['email'];
        $Agent->field['AG_username'] = $request['username'];
        $Agent->field['AG_password'] = $request['password'];
        $Agent->field['AG_mobile'] = $request['mobile'];
        $Agent->field['AG_phone'] = $request['phone'];
        $Agent->field['AG_address'] = $request['address'];
        $Agent->field['AG_city'] = $request['city'];
        $Agent->field['AG_state'] = $request['state'];
        $Agent->field['AG_zip'] = $request['zip'];
        $Agent->field['AG_active'] = $request['active'];
        $return = $Agent->updateAgentDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('dealer/edit', ['id' => $agent_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Delete agent.
     *
     * @param Request $request
     * @return redirect
     */
    public function remove(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $return = $Agent->deleteAgent();
        if($return['status'] == true)
        {
            $redirect['url'] = url('dealers');
        }
        else
        {
            $redirect['url'] = url('dealer/edit', ['id' => $agent_id]);
        }
        $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        return redirect($redirect['url'])->with($redirect['message']);
    }
}
