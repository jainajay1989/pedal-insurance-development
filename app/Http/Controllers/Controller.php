<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Models\Message;
use App\Http\Models\Notification;
use Session, Config, Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $viewData = [];
    protected $config = null;

    /**
     * Controller constructor.
     *
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next)
        {
            $this->config = Session::get('general_setting');
            return $next($request);
        });

        // Get Unread Messages
        $return = Message::getUnreadMessages();
        if($return['status'] == true)
        {
            $this->viewData['unread_message_list'] = $return['data'];
        }
        //--------------------

        // Get Notifications
        if(Auth::user())
        {
            $where = [];
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value'))
            {
                $where['N_type'] = 'LEAD';
            }
            elseif(Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $where['N_type'] = 'LEAD';
            }
            $return = Notification::getAllNotifications($where);
            if($return['status'] == true)
            {
                $this->viewData['notification_list'] = $return['data'];
            }
        }
        //------------------
    }
}
