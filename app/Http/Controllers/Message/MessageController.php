<?php
namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Message;
use Auth;

class MessageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'message';
    }

    /**
     * View messages.
     *
     * @return view
     */
    public function index()
    {
        $fn_status = true;
        $message_list = null;

        // Get Message List
        if($fn_status == true)
        {
            $where[] = ['MG_Lid', '=', null];
            $return = Message::getAllMessages($where);
            if($return['status'] == true)
            {
                $message_list = $return['data'];
            }
        }
        //-----------------

        $this->viewData['message_list'] = $message_list;
        return view('inbox.index', $this->viewData);
    }

    /**
     * Add message.
     *
     * @return view
     */
    public function create()
    {
        return view('inbox.create', $this->viewData);
    }

    /**
     * Edit company.
     *
     * @param Request $request
     * @return view
     */
    public function view(Request $request)
    {
        $message_id = null;
        $message_detail = null;

        // Fetch Request Variables
        $message_id = intval($request['id']);
        //------------------------

        $return = Message::getMessageDetail($message_id);
        if($return['status'] == true)
        {
            $message_detail = $return['data'];

            // Mark Message as Read
            $Message = new Message();
            $Message->field['MG_id'] = $message_detail->MG_id;
            $Message->field['MG_read'] = 1;
            $Message->readMessage();
            //---------------------
        }

        $this->viewData['message_detail'] = $message_detail;
        return view('inbox.view', $this->viewData);
    }

    /**
     * Add message.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $Message = new Message();
        $Message->field['MG_Lid'] = $request['lead'];
        $Message->field['MG_type'] = config('constant.message_type.BTA.value');
        $Message->field['MG_from'] = Auth::user()->A_id;
        $Message->field['MG_to'] = $request['to'];
        $Message->field['MG_subject'] = $request['subject'];
        $Message->field['MG_message'] = $request['message'];
        $Message->field['MG_read'] = 0;
        $return = $Message->addMessage();
        if($return['status'] == true)
        {
            $redirect['url'] = url('inbox');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('index/new-message');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Read message.
     *
     * @param Request $request
     * @return redirect
     */
    public function read(Request $request)
    {
        $message_id = null;

        // Fetch Request Variables
        $message_id = $request['id'];
        //------------------------

        $Message = new Message();
        $Message->field['MG_read'] = 1;
        $return = $Message->readMessage();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('message/edit', ['id' => $message_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Unread message.
     *
     * @param Request $request
     * @return redirect
     */
    public function unread(Request $request)
    {
        $message_id = null;

        // Fetch Request Variables
        $message_id = $request['id'];
        //------------------------

        $Message = new Message();
        $Message->field['MG_read'] = 0;
        $return = $Message->unreadMessage();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('message/edit', ['id' => $message_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }
}
