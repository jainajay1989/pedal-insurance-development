<?php
namespace App\Http\Controllers\LeadMessage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Message;
use App\Http\Models\Lead;
use Carbon\Carbon, Auth;

class LeadMessageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'message';
    }

    /**
     * View messages.
     *
     * @param $request
     * @return view
     */
    public function index(Request $request)
    {
        $fn_status = true;
        $lead_id = null;
        $where = [];
        $message_list = null;

        // Fetch Request Variables
        $lead_id = $request['lead_id'];
        //------------------------

        // Get Message List
        if($fn_status == true)
        {
            $where[] = ['MG_Lid', '<>', null];
            if($lead_id)
            {
                $where[] = ['MG_Lid', '=', $lead_id];
            }
            $return = Message::getAllMessages($where);
            if($return['status'] == true)
            {
                $message_list = $return['data'];
            }
        }
        //-----------------

        $this->viewData['message_list'] = $message_list;
        $this->viewData['lead_id'] = $lead_id;
        return view('message.index', $this->viewData);
    }

    /**
     * Refresh messages.
     *
     * @param $request
     * @return view
     */
    public function refresh(Request $request)
    {
        $fn_status = true;
        $lead_id = null;
        $message_id = null;
        $where = [];
        $message_list = null;

        // Fetch Request Variables
        $message_id = $request['message_id'];
        $lead_id = $request['lead_id'];
        //------------------------

        // Get Message List
        if($fn_status == true)
        {
            $where = [['MG_Lid', '=', $lead_id], ['MG_id', '>', $message_id]];
            $return = Message::getAllMessages($where);
            if($return['status'] == true)
            {
                $message_list = $return['data'];
            }
        }
        //-----------------

        $response['status'] = 1;
        $response['code'] = 'S300';
        $response['message'] = 'Message refreshed successfully';
        $response['data'] = $message_list;

        return response()->json($response);
    }

    /**
     * Add message.
     *
     * @param $request
     * @return view
     */
    public function create(Request $request)
    {
        $lead_id = null;

        // Fetch Request Variables
        $lead_id = $request['lead_id'];
        //------------------------

        $this->viewData['lead_id'] = $lead_id;
        return view('message.create', $this->viewData);
    }

    /**
     * Edit company.
     *
     * @param Request $request
     * @return view
     */
    public function view(Request $request)
    {
        $message_id = null;
        $message_detail = null;

        // Fetch Request Variables
        $message_id = intval($request['id']);
        //------------------------

        $return = Message::getMessageDetail($message_id);
        if($return['status'] == true)
        {
            $message_detail = $return['data'];

            // Mark Message as Read
            $Message = new Message();
            $Message->field['MG_id'] = $message_detail->MG_id;
            $Message->field['MG_read'] = 1;
            $Message->readMessage();
            //---------------------
        }

        $this->viewData['message_detail'] = $message_detail;
        return view('message.view', $this->viewData);
    }

    /**
     * Add message.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $fn_status = true;
        $lead_id = null;
        $lead_detail = null;

        // Fetch Request Variables
        $lead_id = $request['lead_id'];
        //------------------------

        // Get Lead Detail
        if($fn_status == true)
        {
            $return = Lead::getLeadDetail($lead_id);
            $fn_status = $return['status'];
            if($fn_status == true)
            {
                $lead_detail = $return['data'];
            }
        }
        //----------------

        // Insert Message
        if($fn_status == true)
        {
            $Message = new Message();
            $Message->field['MG_Lid'] = $request['lead_id'];
            $Message->field['MG_type'] = config('constant.message_type.BTA.value');
            $Message->field['MG_from'] = Auth::user()->A_id;
            $Message->field['MG_to'] = $lead_detail->L_AGid;
            $Message->field['MG_subject'] = 'Lead message: #'.$request['lead_id'];
            $Message->field['MG_message'] = $request['message'];
            $Message->field['MG_read'] = 1;
            $return = $Message->addMessage();
        }
        //------------

        if($return['status'] == true)
        {
            $message_detail = $return['data'];

            // Update Message Detail
            $message_detail->MG_formattedCreatedAt = $message_detail->MG_createdAt->format('d-M-Y h:i A');
            //----------------------

            $response['status'] = 1;
            $response['code'] = 'S300';
            $response['message'] = 'Message sent successfully';
            $response['data'] = $message_detail;
        }
        else
        {
            $response['status'] = 0;
            $response['code'] = 'E300';
            $response['message'] = 'Unable to send message, please try again!';
            $response['data'] = null;
        }

        return response()->json($response);
    }

    /**
     * Read message.
     *
     * @param Request $request
     * @return redirect
     */
    public function read(Request $request)
    {
        $message_id = null;

        // Fetch Request Variables
        $message_id = $request['id'];
        //------------------------

        $Message = new Message();
        $Message->field['MG_read'] = 1;
        $return = $Message->readMessage();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('message/edit', ['id' => $message_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Unread message.
     *
     * @param Request $request
     * @return redirect
     */
    public function unread(Request $request)
    {
        $message_id = null;

        // Fetch Request Variables
        $message_id = $request['id'];
        //------------------------

        $Message = new Message();
        $Message->field['MG_read'] = 0;
        $return = $Message->unreadMessage();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('message/edit', ['id' => $message_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }
}
