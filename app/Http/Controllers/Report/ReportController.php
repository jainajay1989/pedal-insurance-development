<?php
namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'report';
    }

    public function index()
    {
        return view('report.index', $this->viewData);
    }
    
/* Added by Ajay Jain
     * Date: 10 oct 2018
     * Used to generate daily report
     */
    public function generateReport() {
        $todayDate = date('d');
        if($todayDate == '01' || $todayDate == '1') {
            $currentDate = date('Y-m-d');
            $d1 = new \DateTime($currentDate);
            $d2 = new \DateTime($currentDate);
            $d1->modify('first day of previous month');
            $startDate = $d1->format('Y-m-d');
            $d2->modify('last day of previous month');
            $endDate = $d2->format('Y-m-d');
        } else {
            $startDate = date('Y-m-01');
            $endDate = date('Y-m-t');
        }
        
        $leadList = \App\Http\Models\Lead::whereNotNull('L_newPolicyNumber')->where('L_newPolicyCreationDate', '>=', $startDate)->where('L_newPolicyCreationDate', '<=', $endDate)->where('L_policyType', 'PEDAL_CYCLE')->with('agents')->orderBy('L_newPolicyCreationDate', 'ASC')->get();
        $spreadsheet = new Spreadsheet();
        $repeatedEntriesSheet = new Worksheet($spreadsheet, 'Repeated Entries');
        $repeatedEntriesSheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $repeatedEntriesSheet->getStyle('A1:BV1')->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFFF00');
        $repeatedEntriesSheet->getStyle('A1:BV1')
                ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BV1')
                ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BV1')
                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BV1')
                ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $repeatedEntriesSheet->getStyle('A1:BV1')->getAlignment()->applyFromArray(
                                    array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
                            );
        $repeatedEntriesSheet->setCellValue("A$i", "RM Name");
        $repeatedEntriesSheet->setCellValue("B$i", "State of Purchase");
        $repeatedEntriesSheet->setCellValue("C$i", "PolicyNumber");
        $repeatedEntriesSheet->setCellValue("D$i", "PolicyStartDate");
        $repeatedEntriesSheet->setCellValue("E$i", "PolicyEndDate");
        $repeatedEntriesSheet->setCellValue("F$i", "IMDNumber");
        $repeatedEntriesSheet->setCellValue("G$i", "PolicyTenure");
        $repeatedEntriesSheet->setCellValue("H$i", "Salutation");
        $repeatedEntriesSheet->setCellValue("I$i", "FirstName");
        $repeatedEntriesSheet->setCellValue("J$i", "LastName");
        $repeatedEntriesSheet->setCellValue("K$i", "MiddleName");
        $repeatedEntriesSheet->setCellValue("L$i", "DOB");
        $repeatedEntriesSheet->setCellValue("M$i", "EmailID");
        $repeatedEntriesSheet->setCellValue("N$i", "PanNo");
        $repeatedEntriesSheet->setCellValue("O$i", "MobileNumber");
        $repeatedEntriesSheet->setCellValue("P$i", "AddressLine1");
        $repeatedEntriesSheet->setCellValue("Q$i", "AddressLine2");
        $repeatedEntriesSheet->setCellValue("R$i", "AddressLine3");
        $repeatedEntriesSheet->setCellValue("S$i", "PinCode");
        $repeatedEntriesSheet->setCellValue("T$i", "GSTIN");
        $repeatedEntriesSheet->setCellValue("U$i", "BuyerState");
        $repeatedEntriesSheet->setCellValue("V$i", "SGST");
        $repeatedEntriesSheet->setCellValue("W$i", "CGST");
        $repeatedEntriesSheet->setCellValue("X$i", "UTGST");
        $repeatedEntriesSheet->setCellValue("Y$i", "IGST");
        $repeatedEntriesSheet->setCellValue("Z$i", "BurglaryorHouseBreakingorTheftUpto75SumInsured");
        $repeatedEntriesSheet->setCellValue("AA$i", "BurglaryorHouseBreakingorTheftUpto75Premium");
        $repeatedEntriesSheet->setCellValue("AB$i", "BurglaryorHouseBreakingorTheftUpto100SumInsured");
        $repeatedEntriesSheet->setCellValue("AC$i", "BurglaryorHouseBreakingorTheftUpto100Premium");
        $repeatedEntriesSheet->setCellValue("AD$i", "LossorDamagetoInsuredBicycleSumInsured");
        $repeatedEntriesSheet->setCellValue("AE$i", "LossorDamagetoInsuredBicyclePremium");
        $repeatedEntriesSheet->setCellValue("AG$i", "LossofPersonalBelongingsSumInsured");
        $repeatedEntriesSheet->setCellValue("AH$i", "LossofPersonalBelongingsPremium");
        $repeatedEntriesSheet->setCellValue("AH$i", "UsageforHireOrRewardCover");
        $repeatedEntriesSheet->setCellValue("AI$i", "ExtensionofGeographicalArea");
        $repeatedEntriesSheet->setCellValue("AJ$i", "RoadSideAsstCoverSumInsured");
        $repeatedEntriesSheet->setCellValue("AK$i", "RoadSideAsstCoverPremium");
        $repeatedEntriesSheet->setCellValue("AL$i", "PermanentTotalDisabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AM$i", "PermanentTotalDisabilityPremium");
        $repeatedEntriesSheet->setCellValue("AN$i", "PermanentPartialDisabilityPremium");
        $repeatedEntriesSheet->setCellValue("AO$i", "PermanentPartialDisabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AP$i", "TemporaryTotalDisabilityPremium");
        $repeatedEntriesSheet->setCellValue("AQ$i", "TemporaryTotalDisabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AR$i", "AccidentalDeathSumInsured");
        $repeatedEntriesSheet->setCellValue("AS$i", "AccidentalDeathPremium");
        $repeatedEntriesSheet->setCellValue("AT$i", "PublicLiabilitySumInsured");
        $repeatedEntriesSheet->setCellValue("AU$i", "TotalPremium");
        $repeatedEntriesSheet->setCellValue("AV$i", "BicycleMake");
        $repeatedEntriesSheet->setCellValue("AW$i", "BicycleModel");
        $repeatedEntriesSheet->setCellValue("AX$i", "TPSourceName");
        $repeatedEntriesSheet->setCellValue("AY$i", "QuotationNumber");
        $repeatedEntriesSheet->setCellValue("AZ$i", "PaymentSource");
        $repeatedEntriesSheet->setCellValue("AA$i", "PaymentDate");
        $repeatedEntriesSheet->setCellValue("BB$i", "TransactionID");
        $repeatedEntriesSheet->setCellValue("BC$i", "TPEmailID");
        $repeatedEntriesSheet->setCellValue("BD$i", "DealerCode");
        $repeatedEntriesSheet->setCellValue("BE$i", "BiCycleType");
        $repeatedEntriesSheet->setCellValue("BF$i", "Purchased");
        $repeatedEntriesSheet->setCellValue("BG$i", "Origin");
        $repeatedEntriesSheet->setCellValue("BH$i", "MakeCode");
        $repeatedEntriesSheet->setCellValue("BI$i", "ModelCode");
        $repeatedEntriesSheet->setCellValue("BJ$i", "ManfMonth");
        $repeatedEntriesSheet->setCellValue("BK$i", "ManfYear");
        $repeatedEntriesSheet->setCellValue("BL$i", "BicycleBodyType");
        $repeatedEntriesSheet->setCellValue("BM$i", "OutletName");
        $repeatedEntriesSheet->setCellValue("BN$i", "PlaceOfPurchase");
        $repeatedEntriesSheet->setCellValue("BO$i", "FrameType");
        $repeatedEntriesSheet->setCellValue("BP$i", "PurchaseDate");
        $repeatedEntriesSheet->setCellValue("BQ$i", "InvoiceNo");
        $repeatedEntriesSheet->setCellValue("BR$i", "PurchasePrice");
        $repeatedEntriesSheet->setCellValue("BS$i", "DeliveryDate");
        $repeatedEntriesSheet->setCellValue("BT$i", "BicycleSumInsured");
        $repeatedEntriesSheet->setCellValue("BU$i", "TotalSumInsured");
        $repeatedEntriesSheet->setCellValue("BV$i", "PolicyCreatedOn");
        $i = 2;
        foreach($leadList as $key => $val) {
            $rmName = '';
            if(!is_null($val->agents->rm)) {
                $rmName = $val->agents->rm->AG_firstName.' '.$val->agents->rm->AG_lastName;
            }
            $completeOrderDetails = json_decode($val['L_completeLeadDetails'], true);
            $postData = json_decode($val['L_appPostData'], true);
            $repeatedEntriesSheet->setCellValue("A$i", $rmName);
            $repeatedEntriesSheet->setCellValue("B$i", "{$completeOrderDetails['BuyerState']}");
            $repeatedEntriesSheet->setCellValue("C$i", $val['L_newPolicyNumber']);
            $repeatedEntriesSheet->setCellValue("D$i", $val['L_newPolicyStartDate']);
            $repeatedEntriesSheet->setCellValue("E$i", $val['L_newPolicyEndDate']);
            $repeatedEntriesSheet->setCellValue("F$i","{$completeOrderDetails['IMDNumber']}");
            $repeatedEntriesSheet->setCellValue("G$i","{$completeOrderDetails['PolicyTenure']}");
            $repeatedEntriesSheet->setCellValue("H$i","{$completeOrderDetails['CustmerObj']['Salutation']}");
            $repeatedEntriesSheet->setCellValue("I$i","{$completeOrderDetails['CustmerObj']['FirstName']}");
            $repeatedEntriesSheet->setCellValue("J$i","{$completeOrderDetails['CustmerObj']['LastName']}");
            $repeatedEntriesSheet->setCellValue("L$i","{$completeOrderDetails['CustmerObj']['DOB']}");
            $repeatedEntriesSheet->setCellValue("M$i","{$completeOrderDetails['CustmerObj']['EmailID']}");
            $repeatedEntriesSheet->setCellValue("O$i","{$completeOrderDetails['CustmerObj']['MobileNumber']}");
            $repeatedEntriesSheet->setCellValue("P$i","{$completeOrderDetails['CustmerObj']['AddressLine1']}");
            $repeatedEntriesSheet->setCellValue("S$i","{$completeOrderDetails['CustmerObj']['PinCode']}");
            $repeatedEntriesSheet->setCellValue("T$i","{$completeOrderDetails['GSTIN']}");
            $repeatedEntriesSheet->setCellValue("U$i","{$completeOrderDetails['BuyerState']}");
            $repeatedEntriesSheet->setCellValue("V$i","{$completeOrderDetails['SGST']}");
            $repeatedEntriesSheet->setCellValue("W$i","{$completeOrderDetails['CGST']}");
            $repeatedEntriesSheet->setCellValue("X$i","{$completeOrderDetails['UTGST']}");
            $repeatedEntriesSheet->setCellValue("Y$i","{$completeOrderDetails['IGST']}");
            $repeatedEntriesSheet->setCellValue("Z$i","{$completeOrderDetails['lstDomPedalCovers'][0]['BurglaryorHouseBreakingorTheftUpto75SumInsured']}");
            $repeatedEntriesSheet->setCellValue("AA$i","{$completeOrderDetails['lstDomPedalCovers'][0]['BurglaryorHouseBreakingorTheftUpto75Premium']}");
            $repeatedEntriesSheet->setCellValue("AD$i","{$completeOrderDetails['lstDomPedalCovers'][0]['LossorDamagetoInsuredBicycleSumInsured']}");
            $repeatedEntriesSheet->setCellValue("AE$i","{$completeOrderDetails['lstDomPedalCovers'][0]['LossorDamagetoInsuredBicyclePremium']}");
            $repeatedEntriesSheet->setCellValue("AR$i","{$completeOrderDetails['lstDomPedalCovers'][0]['AccidentalDeathSumInsured']}");
            $repeatedEntriesSheet->setCellValue("AS$i","{$completeOrderDetails['lstDomPedalCovers'][0]['AccidentalDeathPremium']}");
            $repeatedEntriesSheet->setCellValue("AT$i","{$completeOrderDetails['lstDomPedalCovers'][0]['PublicLiabilitySumInsured']}");
            $repeatedEntriesSheet->setCellValue("AU$i","{$completeOrderDetails['TotalPremium']}");
            $repeatedEntriesSheet->setCellValue("AV$i","{$postData['vehicle_make_text']}");
            $repeatedEntriesSheet->setCellValue("AW$i","{$postData['vehicle_model_text']}");
            $repeatedEntriesSheet->setCellValue("AX$i","{$completeOrderDetails['TPSourceName']}");
            $repeatedEntriesSheet->setCellValue("AY$i","{$completeOrderDetails['QuotationNumber']}");
            $repeatedEntriesSheet->setCellValue("AZ$i","{$completeOrderDetails['PaymentSource']}");
            $repeatedEntriesSheet->setCellValue("BA$i","{$completeOrderDetails['PaymentDate']}");
            $repeatedEntriesSheet->setCellValue("BB$i","{$completeOrderDetails['TransactionID']}");
            $repeatedEntriesSheet->setCellValue("BC$i","{$completeOrderDetails['TPEmailID']}");
            $repeatedEntriesSheet->setCellValue("BD$i","{$completeOrderDetails['DealerCode']}");
            $repeatedEntriesSheet->setCellValue("BE$i","{$completeOrderDetails['BiCycleType']}");
            $repeatedEntriesSheet->setCellValue("BF$i","{$completeOrderDetails['Purchased']}");
            $repeatedEntriesSheet->setCellValue("BH$i","{$completeOrderDetails['MakeCode']}");
            $repeatedEntriesSheet->setCellValue("BI$i","{$completeOrderDetails['ModelCode']}");
            $repeatedEntriesSheet->setCellValue("BJ$i","{$completeOrderDetails['ManfMonth']}");
            $repeatedEntriesSheet->setCellValue("BK$i","{$completeOrderDetails['ManfYear']}");
            $repeatedEntriesSheet->setCellValue("BO$i","{$completeOrderDetails['FrameType']}");
            $repeatedEntriesSheet->setCellValue("BP$i","{$completeOrderDetails['PurchaseDate']}");
            $repeatedEntriesSheet->setCellValue("BQ$i","{$completeOrderDetails['InvoiceNo']}");
            $repeatedEntriesSheet->setCellValue("BR$i","{$completeOrderDetails['PurchasePrice']}");
            $repeatedEntriesSheet->setCellValue("BS$i","{$completeOrderDetails['DeliveryDate']}");
            $repeatedEntriesSheet->setCellValue("BT$i","{$completeOrderDetails['BicycleSumInsured']}");
            $repeatedEntriesSheet->setCellValue("BU$i", "{$completeOrderDetails['TotalSumInsured']}");
            $repeatedEntriesSheet->setCellValue("BV$i", "{$val->L_newPolicyCreationDate}");
            $i++;
        } 
        $writer = new Xlsx($spreadsheet);
        $fileName = time() . '-PolicyReport' . '.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save("MIS_report/$fileName");
    }
}
