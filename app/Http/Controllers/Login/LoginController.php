<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Models\Config;
use Session;

class LoginController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Show login form
     *
     * @return View
     */
    public function viewLogin()
    {
        $this->middleware('redirect.auth');
        return view('login.index');
    }

    /**
     * Handle an authentication attempt
     *
     * @param Request $request
     * @return Mixed
     */
    public function authenticate(Request $request)
    {
        if(Auth::attempt(['A_username' => $request['username'], 'password' => $request['password']], ($request['remember']==1)))
        {
            $return = Config::getConfigDetail('GENERAL_SETTING');
            if($return['status'] == true)
            {
                Session::put('general_setting', json_decode($return['data']->CN_value));
            }
            return redirect()->intended('/');
        }
        return redirect('login');
    }

    /**
     * Lock
     *
     * @return redirect
     */
    public function lock()
    {
        $Admin = new \stdClass;
        $Admin->name = Auth::user()->A_firstName.' '.Auth::user()->A_lastName;
        $Admin->username = Auth::user()->A_username;
        Session::put('admin_detail', $Admin);
        Auth::logout();
        return redirect('lock');
    }

    /**
     * Logout
     *
     * @return redirect
     */
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}