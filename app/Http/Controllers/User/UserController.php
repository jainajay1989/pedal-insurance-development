<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use App\Http\Models\State;
use App\Http\Models\City;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'user';
    }

    /**
     * Add user.
     *
     * @return view
     */
    public function index()
    {
        $fn_status = true;
        $user_list = null;

        // Check Permission
        if(check_permission('USER_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Get All Users
        if($fn_status == true)
        {
            $return = Admin::getAllUsers();
            if($return['status'] == true)
            {
                $user_list = $return['data'];
            }
        }
        //--------------

        $this->viewData['user_list'] = $user_list;
        return view('user.index', $this->viewData);
    }

    /**
     * Add user.
     *
     * @return view
     */
    public function create()
    {
        $fn_status = true;
        $state_list = [];
        $city_list = [];

        // Check Permission
        if(check_permission('USER_CREATE', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Get Admin Type List
        if($fn_status == true)
        {
            foreach(config('constant.user_type') as $status)
            {
                $user_type_list[$status['value']] = $status['caption'];
            }
        }
        //--------------------

        // Get State List
        if($fn_status == true)
        {
            $return = State::getAllStates();
            if($return['status'] == true)
            {
                $states = $return['data'];
                $state_list[''] = 'Select';
                foreach($states as $state)
                {
                    $state_list[$state->ST_code] = $state->ST_name;
                }
            }
        }
        //-----------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
                $city_list = $return['data'];
                /*$cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }*/
            }
        }
        //--------------
    
        $this->viewData['user_type_list'] = $user_type_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        return view('user.create', $this->viewData);
    }

    /**
     * Edit user.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
    	$fn_status = true;
        $user_id = null;
        $state_list = [];
        $city_list = [];
        $user_detail = null;

        // Check Permission
        if(check_permission('USER_VIEW', Auth::user()->A_type) == false && check_permission('USER_EDIT', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $user_id = intval($request['id']);
        //------------------------

	    // Get Admin Type List
        if($fn_status == true)
        {
            foreach(config('constant.user_type') as $status)
            {
                $user_type_list[$status['value']] = $status['caption'];
            }
        }
        //--------------------

        // Get State List
        if($fn_status == true)
        {
            $return = State::getAllStates();
            if($return['status'] == true)
            {
                $states = $return['data'];
                $state_list[''] = 'Select';
                foreach($states as $state)
                {
                    $state_list[$state->ST_code] = $state->ST_name;
                }
            }
        }
        //---------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
                $city_list = $return['data'];
                /*$cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }*/
            }
        }
        //--------------

        // Get Admin User Detail
        if($fn_status == true)
        {
            $return = Admin::getUserDetail($user_id);
            if($return['status'] == true)
            {
                $user_detail = $return['data'];
            }
        }
        //----------------------

        $this->viewData['user_type_list'] = $user_type_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        $this->viewData['user_detail'] = $user_detail;
        return view('user.edit', $this->viewData);
    }

    /**
     * Add user.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $Admin = new Admin();
        $Admin->field['A_type'] = $request['type'];
        $Admin->field['A_firstName'] = $request['first_name'];
        $Admin->field['A_lastName'] = $request['last_name'];
        $Admin->field['A_username'] = $request['username'];
        $Admin->field['A_password'] = $request['password'];
        $Admin->field['A_email'] = $request['email'];
        $Admin->field['A_mobile'] = $request['mobile'];
        $Admin->field['A_phone'] = $request['phone'];
        $Admin->field['A_address'] = $request['address'];
        $Admin->field['A_city'] = $request['city'];
        $Admin->field['A_state'] = $request['state'];
        $Admin->field['A_zip'] = $request['zip'];
        $Admin->field['A_active'] = $request['active'];
        $return = $Admin->addUser();
        if($return['status'] == true)
        {
            $user_detail = $return['data'];
            $redirect['url'] = url('user/edit', ['id' => $user_detail->A_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('user/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update dealer detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $user_id = null;

        // Fetch Request Variables
        $user_id = $request['id'];
        //------------------------

        $Admin = new Admin();
        $Admin->field['A_id'] = $user_id;
        $Admin->field['A_type'] = $request['type'];
        $Admin->field['A_firstName'] = $request['first_name'];
        $Admin->field['A_lastName'] = $request['last_name'];
        $Admin->field['A_username'] = $request['username'];
        $Admin->field['A_password'] = $request['password'];
        $Admin->field['A_email'] = $request['email'];
        $Admin->field['A_mobile'] = $request['mobile'];
        $Admin->field['A_phone'] = $request['phone'];
        $Admin->field['A_address'] = $request['address'];
        $Admin->field['A_city'] = $request['city'];
        $Admin->field['A_state'] = $request['state'];
        $Admin->field['A_zip'] = $request['zip'];
        $Admin->field['A_active'] = $request['active'];
        $return = $Admin->updateUserDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('user/edit', ['id' => $user_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Delete user.
     *
     * @param Request $request
     * @return redirect
     */
    public function remove(Request $request)
    {
        $user_id = null;

        // Fetch Request Variables
        $user_id = $request['id'];
        //------------------------

        $Admin = new Admin();
        $Admin->field['A_id'] = $user_id;
        $return = $Admin->deleteUser();
        if($return['status'] == true)
        {
            $redirect['url'] = url('users');
        }
        else
        {
            $redirect['url'] = url('user/edit', ['id' => $user_id]);
        }
        $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        return redirect($redirect['url'])->with($redirect['message']);
    }
}
