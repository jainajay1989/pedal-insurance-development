<?php

namespace App\Http\Controllers\Lead;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\OtherInsurance;
use Carbon\Carbon,
    Storage;

class OtherInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function update(Request $request) {
        $input = $request->all();
        
        try{
            if(empty($request['customer_first_name'])) { throw new \Exception('Please enter first name'); }
            if(empty($request['customer_last_name'])) { throw new \Exception('Please enter last name'); }
            if(empty($request['customer_email'])) { throw new \Exception('Please enter email'); }
            if(empty($request['customer_dob'])) { 
                throw new \Exception('Please select DOB');
            } else {
                $dob = explode('/', $request['customer_dob']);
                $dd = $dob[0];
                $mm = $dob[1];
                $yy = $dob[2];
                if(!checkdate($mm, $dd, $yy)) {
                    throw new \Exception('Please select valid DOB');
                }
            }
            
            if(empty($request['customer_mobile'])) { throw new \Exception('Please enter mobile number'); }
            if(empty($request['customer_address'])) { throw new \Exception('Please enter customer address'); }
            if(empty($request['customer_state'])) { throw new \Exception('Please select customer state'); }
            if(empty($request['customer_city'])) { throw new \Exception('Please select customer city'); }
            if(empty($request['customer_zip'])) { throw new \Exception('Please enter pincode'); }
           
            if(empty($request['policy_type'])) { throw new \Exception('Please select policy type'); }
            if(empty($request['new_policy_product'])) { throw new \Exception('Please select policy product offered'); }
            if(empty($request['proposer_type'])) { throw new \Exception('Please enter policy holder details'); }
            if(empty($request['sum_insured'])) { throw new \Exception('Please select sum insured'); }
            if(empty($request['new_policy_company'])) { throw new \Exception('Please select insurance company'); }
            if(empty($request['estimated_premium'])) { throw new \Exception('Please enter estimated premium'); }
            if(empty($request['net_premium']) || $request['net_premium'] == 0.00) { throw new \Exception('Please enter net premium'); }
            if(empty($request['gross_premium']) || $request['gross_premium'] == 0.00) { throw new \Exception('Please enter gross premium'); }
            if(empty($request['gst']) || $request['gst'] == 0.00) { throw new \Exception('Please enter gst premium'); }
            if(empty($request['paid_by_whom'])) { throw new \Exception('Please select the payer'); }
            
            if($request['status'] == config('constant.lead_status.COMPLETED.value')) {
                
                $lead_detail = \App\Http\Models\Lead::where('L_id', $request['lead_id'])->with('otherInsurance')->first();
                $motorInsuranceLead = $lead_detail->otherInsurance;
                $motorInsuranceLead->agent_id = $lead_detail->L_AGid;
                $motorInsuranceLead->policy_type = $lead_detail->L_policyType;
                $motorInsuranceLead->gross_premium = $request['gross_premium'];
                $motorInsuranceLead->new_policy_product = $request['new_policy_product'];
                $motorInsuranceLead->new_policy_company = $request['new_policy_company'];
                $motorInsuranceLead->status = $request['status'];
                $motorInsuranceLead->paid_by_whom = $request['paid_by_whom'];
                /* Commision calculation */
                
                $return = calculateCommision($motorInsuranceLead);
                if($return['status']==false) {
                    $msg = (!empty($return['message'])?$return['message']:"Error occured");
                    throw new \Exception($msg);
                }
                
                /* End */
            }
            $leadId = $request['lead_id'];
            // Insert Lead
            $others = new OtherInsurance;
            $others->field['L_id'] = $request['lead_id'];
            $others->field['L_AGid'] = $request['agent_id'];
            $others->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $others->field['L_customerFirstName'] = $request['customer_first_name'];
            $others->field['L_customerLastName'] = $request['customer_last_name'];
            $others->field['L_customerEmail'] = $request['customer_email'];
            $others->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString() : null);
            $others->field['L_customerMobile'] = $request['customer_mobile'];
            $others->field['L_customerPhone'] = $request['customer_phone'];
            $others->field['L_customerAddress'] = $request['customer_address'];
            $others->field['L_customerCity'] = $request['customer_city'];
            $others->field['L_customerState'] = $request['customer_state'];
            $others->field['L_customerZip'] = $request['customer_zip'];
            
            //Vehicle Details
            $others->field['id'] = $request['id'];
            $others->field['proposer_type'] = $request['proposer_type'];
            $others->field['new_policy_product'] = $request['new_policy_product'];
            $others->field['sum_insured'] = $request['sum_insured'];
            $others->field['new_policy_company'] = $request['new_policy_company'];
            $others->field['estimated_premium'] = $request['estimated_premium'];
            $others->field['net_premium'] = $request['net_premium'];
            $others->field['gst'] = $request['gst'];
            $others->field['gross_premium'] = $request['gross_premium'];
            $others->field['status'] = $request['status'];
            $others->field['new_policy_number'] = $request['new_policy_number'];

            $others->field['new_policy_start_date'] = ($request['new_policy_start_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_start_date'])->toDateString():null);
            $others->field['new_policy_end_date'] = ($request['new_policy_end_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_end_date'])->toDateString():null);
            $others->field['paid_by_whom'] = ($request['paid_by_whom']?$request['paid_by_whom']:null);
            /* End */
            if($request->file('new_policy_attachment'))
            {
                $file_name = 'OTHER_'.$leadId.'_'.Carbon::now()->timestamp.'.'.$request->file('new_policy_attachment')->getClientOriginalExtension();
                Storage::disk('policies')->putFileAs('', $request->file('new_policy_attachment'), $file_name);
                $others->field['policy_attachment'] = $file_name;
            }
            /* End */
            
            // Used to email policy to customer 
            if(!empty($file_name)) {
                 $storagePath = storage_path('app/public/lead/policies');
                $emailData['attachment'] = $storagePath."/".$file_name;
                $emailData['customer_name'] = $request['customer_first_name'].' '.$request['customer_last_name'];
                $emailData['customer_email'] = $request['customer_email'];
                $emailStatus = sendPolicyEmailToCustomer($emailData);
                if(!$emailStatus['status']) {
                    Mail::raw(json_encode($others), function($message)
                    {
                        $message->from('noreply@flashapp.com', 'Email Error');
                        $message->to('engineering@fondostech.in');
                    });
                }
            }
            // End
            $return = $others->updateDetails();
            $status = ($return['status']==true?'success':'error');
            $message = $return['message'];
        } catch (\Exception $ex) {
            $status = 'error';
            $message = $ex->getMessage();
        }
        return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $message, 'type' => $status]);   
    }
}
