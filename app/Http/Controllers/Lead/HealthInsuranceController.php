<?php

namespace App\Http\Controllers\Lead;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\API\Controller;
use App\Http\Models\Lead;
use App\Http\Models\State;
use App\Http\Models\City;
use App\Http\Models\CityPincode;
use App\Http\Models\PayuLog;
use \App\Http\Models\Vehicle;
use \App\Http\Models\Agent;
use App\Http\Models\HealthInsurance;
use Carbon\Carbon,
    Storage;

class HealthInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function update(Request $request) {
        
        $input = $request->all();
        try{
            if($request['status'] == config('constant.lead_status.COMPLETED.value')) {
                
                /* Used to validate the input fields */
                $validator = Validator::make($request->all(), [
                    'paid_by_whom' => 'required',
                    'net_premium' => 'required',
                    'gross_premium' => 'required',
                    'new_policy_company' => 'required',
                    'new_policy_product' => 'required',
                    'new_policy_number' => 'required',
                    'new_policy_start_date' => 'required',
                    'new_policy_end_date' => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect(url('lead/edit', ['id' => $request['lead_id']]))->withErrors($validator)->withInput();
                }
                /* End */
                
                $lead_detail = \App\Http\Models\Lead::where('L_id', $request['lead_id'])->with('healthInsurance')->first();
                $motorInsuranceLead = $lead_detail->healthInsurance;
                $motorInsuranceLead->agent_id = $lead_detail->L_AGid;
                $motorInsuranceLead->policy_type = $lead_detail->L_policyType;
                $motorInsuranceLead->gross_premium = $request['gross_premium'];
                $motorInsuranceLead->new_policy_product = $request['new_policy_product'];
                $motorInsuranceLead->new_policy_company = $request['new_policy_company'];
                $motorInsuranceLead->status = $request['status'];
                $motorInsuranceLead->paid_by_whom = $request['paid_by_whom'];
                /* Commision calculation */
                
                $return = calculateCommision($motorInsuranceLead);
                if($return['status']==false) {
                    $msg = (!empty($return['message'])?$return['message']:"Error occured");
                    throw new \Exception($msg);
                }
                
                /* End */
            }
            
//            print_r($request->all());
//            exit;
            $leadId = $request['lead_id'];
            // Insert Lead
            $health = new HealthInsurance;
            $health->field['L_AGid'] = $request['agent_id'];
            $health->field['L_id'] = $request['lead_id'];
            $health->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $health->field['L_customerFirstName'] = $request['customer_first_name'];
            $health->field['L_customerLastName'] = $request['customer_last_name'];
            $health->field['L_customerEmail'] = $request['customer_email'];
            $health->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j-n-Y', $request['customer_dob'])->toDateString() : null);
            $health->field['L_customerMobile'] = $request['customer_mobile'];
            $health->field['L_customerPhone'] = $request['customer_phone'];
            $health->field['L_customerAddress'] = $request['customer_address'];
            $health->field['L_customerCity'] = $request['customer_city'];
            $health->field['L_customerState'] = $request['customer_state'];
            $health->field['L_customerZip'] = $request['customer_zip'];
            //Vehicle Details
            $health->field['id'] = $request['id'];
            $health->field['for_whom'] = $request['for_whom'];
            $health->field['policy_holder_details'] = (!empty($request['policy_holder_details'])?json_encode($request['policy_holder_details']):NULL);
            $health->field['resident_state'] = $request['resident_state'];
            $health->field['resident_city'] = $request['resident_city'];
            $health->field['sum_insured'] = $request['sum_insured'];
            $health->field['new_policy_company'] = $request['new_policy_company'];
            $health->field['new_policy_product'] = $request['new_policy_product'];
            $health->field['estimated_premium'] = $request['estimated_premium'];
            $health->field['net_premium'] = $request['net_premium'];
            $health->field['gst'] = $request['gst'];
            $health->field['gross_premium'] = $request['gross_premium'];
            $health->field['status'] = $request['status'];
            $health->field['new_policy_number'] = $request['new_policy_number'];

            $health->field['new_policy_start_date'] = ($request['new_policy_start_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_start_date'])->toDateString():null);
            $health->field['new_policy_end_date'] = ($request['new_policy_end_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_end_date'])->toDateString():null);
            $health->field['paid_by_whom'] = ($request['paid_by_whom']?$request['paid_by_whom']:null);
            $health->field['policy_renewal_new'] = (!empty($request['policy_renewal_new'])?$request['policy_renewal_new']:NULL);
            $health->field['remark'] = (!empty($request['remark'])?$request['remark']:NULL);
//            print_r($request->all());exit;
            if($request->file('new_policy_attachment'))
            {
                $file_name = 'HEALTH_'.$leadId.'_'.Carbon::now()->timestamp.'.'.$request->file('new_policy_attachment')->getClientOriginalExtension();
                Storage::disk('policies')->putFileAs('', $request->file('new_policy_attachment'), $file_name);
                $health->field['policy_attachment'] = $file_name;
            }
            /* End */
            
            if($request->file('quotations')) {
                $quoteFiles = $request->file('quotations');
                foreach($quoteFiles as $key => $val) {
                    if($val->getClientOriginalExtension() != 'pdf' && $val->getClientOriginalExtension() != 'PDF') {
                        throw new \Exception('Only PDF files are allowed in quotes');
                    }
                    $quoteFileName = 'HEALTH_QUOTE_'.$key.'_'.$leadId.'_'.Carbon::now()->timestamp.'.'.$val->getClientOriginalExtension();
                    Storage::disk('quotes')->putFileAs('', $val, $quoteFileName);
                    $quoteFiles[$key] = $quoteFileName;
                }
                $health->field['quotations'] = $quoteFiles;
            }
            
            $return = $health->updateDetails();
            
            // Used to email policy to customer 
            if(!empty($file_name)) {
                 $storagePath = storage_path('app/public/lead/policies');
                $emailData['attachment'] = $storagePath."/".$file_name;
                $emailData['customer_name'] = $request['customer_first_name'].' '.$request['customer_last_name'];
                $emailData['customer_email'] = $request['customer_email'];
                $emailStatus = sendPolicyEmailToCustomer($emailData);
                if(!$emailStatus['status']) {
                    Mail::raw(json_encode($health->field), function($message)
                    {
                        $message->from('noreply@flashapp.com', 'Email Error');
                        $message->to('engineering@fondostech.in');
                    });
                }
            }
            // End
            $status = ($return['status']==true?'success':'error');
            $message = (!empty($message)?$message:'Lead Updated Successully');
            return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $return['message'], 'type' => $status]);
            
        } catch (\Exception $ex) {
            $status = 'error';
            $message = $ex->getMessage();
            return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $message, 'type' => $status])->withInput();
        }
    }
}
