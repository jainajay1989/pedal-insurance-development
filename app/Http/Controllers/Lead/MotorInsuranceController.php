<?php

namespace App\Http\Controllers\Lead;

use Validator;
use Illuminate\Http\Request;
use App\Http\Models\MotorInsurance;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon,
    Storage;

class MotorInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }
    
    public function upload($imageName = '', $slug = '') {
        $fn_status = false;
        $image_name = md5(microtime()) . '.jpg';
        $fn_status = Storage::disk('motorinsurance')->put($slug.$image_name, base64_decode(str_replace('data:image/png;base64,', '', $imageName)));
        if ($fn_status == true) {
            $response['status'] = 1;
            $response['code'] = 'S400';
            $response['message'] = 'Image uploaded successfully';
            $response['data'] = ['image_name' => $slug.$image_name, 'image_url' => url('lead-attachment', ['file' => $slug.$image_name])];
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        } else {
            $response['status'] = 0;
            $response['code'] = 'E400';
            $response['message'] = 'Network issue, please upload again!';
            $response['data'] = null;
            $response['timestamp'] = Carbon::now()->toDateTimeString();
        }
        return $response;
    }
    
    public function update(Request $request)
    {
        try {
            if($request['status'] == config('constant.lead_status.COMPLETED.value')) {
                /* Used to validate the input fields */
                $validator = Validator::make($request->all(), [
                    'new_policy_tax_premium' => 'required',
                    'paid_by_whom' => 'required',
                    'new_policy_tp_premium' => 'required',
                    'new_policy_total_premium' => 'required',
                    'new_policy_company' => 'required',
                    'new_policy_product' => 'required',
//                    'new_policy_estimated_premium' => 'required',
//                    'new_policy_attachment' => 'required',
                    'new_policy_number' => 'required',
                    'new_policy_start_date' => 'required',
                    'new_policy_end_date' => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect(url('lead/edit', ['id' => $request['lead_id']]))->withErrors($validator)->withInput();
                }
                /* End */
                
                $lead_detail = \App\Http\Models\Lead::where('L_id', $request['lead_id'])->with('motorInsurance')->first();
                $motorInsuranceLead = $lead_detail->motorInsurance;
                $motorInsuranceLead->agent_id = $lead_detail->L_AGid;
                $motorInsuranceLead->policy_type = $lead_detail->L_policyType;
                $motorInsuranceLead->status = $request['status'];
                $motorInsuranceLead->paid_by_whom = $request['paid_by_whom'];
                $motorInsuranceLead->new_policy_total_premium = $request['new_policy_total_premium'];
                $motorInsuranceLead->new_policy_od_premium = $request['new_policy_od_premium'];
                $motorInsuranceLead->new_policy_tp_premium = $request['new_policy_tp_premium'];
                $motorInsuranceLead->new_policy_product = $request['new_policy_product'];
                $motorInsuranceLead->new_policy_company = $request['new_policy_company'];
                /* Commision calculation */
                
                $return = calculateCommision($motorInsuranceLead);
                if($return['status']==false) {
                    $msg = (!empty($return['message'])?$return['message']:"Error occured");
                    throw new \Exception($msg);
                }
                
                /* End */
            }
            $leadId = $request['lead_id'];
            // Insert Lead
            $motor = new MotorInsurance;
            $motor->field['L_id'] = $request['lead_id'];
            $motor->field['L_AGid'] = $request['agent_id'];
            $motor->field['L_policyType'] = $request['policy_type'];
            $motor->field['L_customerFirstName'] = $request['customer_first_name'];
            $motor->field['L_customerLastName'] = $request['customer_last_name'];
            $motor->field['L_customerEmail'] = $request['customer_email'];
            $motor->field['L_customerDOB'] = ($request['customer_dob']?Carbon::createFromFormat('j-n-Y', $request['customer_dob'])->toDateString():null);
            $motor->field['L_customerMobile'] = $request['customer_mobile'];
            $motor->field['L_customerPhone'] = $request['customer_phone'];
            $motor->field['L_customerAddress'] = $request['customer_address'];
            $motor->field['L_customerCity'] = $request['customer_city'];
            $motor->field['L_customerState'] = $request['customer_state'];
            $motor->field['L_customerZip'] = $request['customer_zip'];


            $motor->field['id'] = $request['id'];
            $motor->field['vehicle_type'] = $request['vehicle_type'];
            $motor->field['vehicle_make'] = $request['vehicle_make'];
            $motor->field['vehicle_model'] = $request['vehicle_model'];
            $motor->field['vehicle_year'] = $request['vehicle_year'];
            $motor->field['vehicle_engine_no'] = $request['vehicle_engine_no'];
            $motor->field['vehicle_chassis_no'] = $request['vehicle_chassis_no'];
            $motor->field['vehicle_reg_no'] = $request['vehicle_reg_no'];
            $motor->field['vehicle_net_price'] = $request['vehicle_net_price'];
            $motor->field['vehicle_cgst'] = $request['vehicle_cgst'];
            $motor->field['vehicle_sgst'] = $request['vehicle_sgst'];
            $motor->field['vehicle_gross_price'] = $request['vehicle_gross_price'];
            if(!empty($request['rc_photo'])) {
                $imgUpload = $this->upload($request['rc_photo'], 'rc_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['rc_photo'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading rc photo');
                }
            }
            if(!empty($request['puc_photo'])) {
                $imgUpload = $this->upload($request['puc_photo'], 'puc_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['puc_photo'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading puc photo');
                }
            }
            if(!empty($request['vehicle_photo1'])) {
                $imgUpload = $this->upload($request['vehicle_photo1'], 'vehicle_photo1_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['vehicle_photo1'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading vehicle photo 1 photo');
                }
            }
            if(!empty($request['vehicle_photo2'])) {
                $imgUpload = $this->upload($request['vehicle_photo2'], 'vehicle_photo2_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['vehicle_photo2'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading vehicle photo 2 photo');
                }
            }
            if(!empty($request['vehicle_photo3'])) {
                $imgUpload = $this->upload($request['vehicle_photo3'], 'vehicle_photo3_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['vehicle_photo3'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading vehicle photo 3 photo');
                }
            }
            if(!empty($request['vehicle_photo4'])) {
                $imgUpload = $this->upload($request['vehicle_photo4'], 'vehicle_photo4_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['vehicle_photo4'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading vehicle photo 4 photo');
                }
            }

            $motor->field['old_policy_number'] = $request['old_policy_number'];
            $motor->field['old_policy_company'] = $request['old_policy_company'];

            $motor->field['old_policy_start_date'] = ($request['old_policy_start_date']?Carbon::createFromFormat('j-n-Y', $request['old_policy_start_date'])->toDateString():null);
            $motor->field['old_policy_end_date'] = ($request['old_policy_end_date']?Carbon::createFromFormat('j-n-Y', $request['old_policy_end_date'])->toDateString():null);
            $motor->field['last_year_claim'] = (!empty($request['last_year_claim'])?$request['last_year_claim']:0);
            if(!empty($request['old_policy_attachment1'])) {
                $imgUpload = $this->upload($request['old_policy_attachment1'], 'old_policy_attachment1_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['old_policy_attachment1'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading vehicle photo 4 photo');
                }
            }
            if(!empty($request['old_policy_attachment2'])) {
                $imgUpload = $this->upload($request['old_policy_attachment2'], 'old_policy_attachment2_');
                if($imgUpload['code'] == 'S400') {
                    $motor->field['old_policy_attachment2'] = $imgUpload['data']['image_name'];
                } else {
                    throw new Exception('Error occured while uploading vehicle photo 4 photo');
                }
            }
            $motor->field['new_policy_company'] = $request['new_policy_company'];
            $motor->field['new_policy_product'] = $request['new_policy_product'];
            $motor->field['new_policy_estimated_premium'] = $request['new_policy_estimated_premium'];
            $motor->field['new_policy_od_premium'] = $request['new_policy_od_premium'];
            $motor->field['new_policy_tp_premium'] = $request['new_policy_tp_premium'];
            $motor->field['new_policy_tax_premium'] = $request['new_policy_tax_premium'];
            $motor->field['new_policy_total_premium'] = $request['new_policy_total_premium'];
            $motor->field['new_policy_number'] = $request['new_policy_number'];

            $motor->field['new_policy_start_date'] = ($request['new_policy_start_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_start_date'])->toDateString():null);
            $motor->field['new_policy_end_date'] = ($request['new_policy_end_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_end_date'])->toDateString():null);
            $motor->field['paid_by_whom'] = ($request['paid_by_whom']?$request['paid_by_whom']:null);
            
            if($request->file('quotations')) {
                $quoteFiles = $request->file('quotations');
                foreach($quoteFiles as $key => $val) {
                    if($val->getClientOriginalExtension() != 'pdf' && $val->getClientOriginalExtension() != 'PDF') {
                        throw new \Exception('Only PDF files are allowed in quotes');
                    }
                    $quoteFileName = 'MOTOR_QUOTE_'.$key.'_'.$leadId.'_'.Carbon::now()->timestamp.'.'.$val->getClientOriginalExtension();
                    Storage::disk('quotes')->putFileAs('', $val, $quoteFileName);
                    $quoteFiles[$key] = $quoteFileName;
                }
                $motor->field['quotations'] = $quoteFiles;
            }
            if($request->file('new_policy_attachment')) {
                $file_name = 'MOTOR_'.$leadId.'_'.Carbon::now()->timestamp.'.'.$request->file('new_policy_attachment')->getClientOriginalExtension();
                Storage::disk('policies')->putFileAs('', $request->file('new_policy_attachment'), $file_name);
                $motor->field['new_policy_attachment'] = $file_name;
            }
            $motor->field['status'] = $request['status'];
            $motor->field['remark'] = $request['remark'];
            // Used to email policy to customer 
            if(!empty($file_name)) {
                 $storagePath = storage_path('app/public/lead/policies');
                $emailData['attachment'] = $storagePath."/".$file_name;
                $emailData['customer_name'] = $request['customer_first_name'].' '.$request['customer_last_name'];
                $emailData['customer_email'] = $request['customer_email'];
                $emailStatus = sendPolicyEmailToCustomer($emailData);
                if(!$emailStatus['status']) {
//                    Mail::raw(json_encode($motor), function($message)
//                    {
//                        $message->from('noreply@flashapp.com', 'Email Error');
//                        $message->to('engineering@fondostech.in');
//                    });
                }
            }
            // End
            $return = $motor->updateDetails();
            $status = ($return['status']==true?'success':'error');
            $message = (!empty($message)?$message:'Lead Updated Successully');
            return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $message, 'type' => $status]);
            /* End */
        } catch (\Exception $ex) {
            $status = 'error';
            $message = $ex->getMessage();
            return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $message, 'type' => $status])->withInput();
        }
        
    }
}
