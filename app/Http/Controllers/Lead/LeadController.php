<?php
namespace App\Http\Controllers\Lead;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Agent;
use App\Http\Models\InsuranceCompany;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\CommissionPayout;
use App\Http\Models\Ledger;
use App\Http\Models\Lead;
use App\Http\Models\State;
use App\Http\Models\City;
use \App\Http\Models\Vehicle;
use App\Http\Models\Notification;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon, Storage, Auth;

class LeadController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'lead';
    }

    /**
     * View leads.
     *
     * @param $request
     * @return view
     */
    public function index(Request $request)
    {
        $fn_status = true;
        $lead_list = null;
        $agent_list = [];
        $lead_status_list = [];
        $policy_type_list = [];
        $agent_ids = [];

        // Check Permission
        if(check_permission('LEAD_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $policy_type = $request['type'];
        $agent_id = $request['agent_id'];
        $search_keyword = $request['keyword'];
        $status = $request['status'];
        //------------------------

        // Get Manager Agents
        if($fn_status == true)
        {
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value') || Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $where = ['AG_Aid' => Auth::user()->A_id];
                $return = Agent::getAllAgents($where);
                if($return['status'] == true)
                {
                    $agents = $return['data'];
                    foreach($agents as $agent)
                    {
                        $agent_ids[] = $agent->AG_id;
                    }
                }
            }
        }
        //-------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            $policy_type_list[''] = 'Policy Type';
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        // Get Agent List
        if($fn_status == true)
        {
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value') || Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $where = ['AG_Aid' => Auth::user()->A_id, 'AG_type' => 'AGENT'];
                $return = Agent::getAllAgents($where);
            }
            else
            {
                $where = ['AG_type' => 'AGENT'];
                $return = Agent::getAllAgents($where);
//                $return = Agent::getAllAgents();
            }
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'IMDcode';
                foreach($agents as $agent)
                {
                    $agent_ids[] = $agent->AG_id;
                    $agent_list[$agent->AG_id] = $agent->AG_ImdCode."(".$agent->AG_companyName.")";
                }
            }
        }
        //-----------------

        // Get Lead Status List
        if($fn_status == true)
        {
            $lead_status_list[''] = 'Status';
            foreach(config('constant.lead_status') as $lead_status)
            {
                $lead_status_list[$lead_status['value']] = $lead_status['caption'];
            }
        }
        //---------------------

        // Get Leads
        if($fn_status == true)
        {
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value') || Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $return = Lead::getAgentsLeads($agent_ids);
            }
            else
            {
                $where = [];
                if($policy_type !== null)
                {
                    $where['L_policyType'] = $policy_type;
                }
                if($agent_id !== null)
                {
                    $where['L_AGid'] = $agent_id;
                }
                if($search_keyword !== null)
                {
                    $where['L_customerMobile'] = $search_keyword;
                }
                if($status !== null)
                {
                    $where['L_status'] = $status;
                }
                $return = Lead::getAllLeads($where);
            }

            if($return['status'] == true)
            {
                $lead_list = $return['data'];
            }
        }
        //----------

        $this->viewData['agent_id'] = $agent_id;
        $this->viewData['policy_type'] = $policy_type;
        $this->viewData['search_keyword'] = $search_keyword;
        $this->viewData['status'] = $status;
        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['lead_status_list'] = $lead_status_list;
        $this->viewData['lead_list'] = $lead_list;
        return view('lead.index', $this->viewData);
    }

    /**
     * Add lead.
     *
     * @return view
     */
    public function create()
    {
        $fn_status = true;
        $agent_list = [];
        $state_list = [];
        $city_list = [];
        $company_list = [];
        $product_list = [];
        $lead_status_list = [];

        // Get State List
        if($fn_status == true)
        {
            $return = State::getAllStates();
            if($return['status'] == true)
            {
                $states = $return['data'];
                $state_list[''] = 'Select';
                foreach($states as $state)
                {
                    $state_list[$state->ST_code] = $state->ST_name;
                }
            }
        }
        //-----------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
                $city_list = $return['data'];
                /*$cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }*/
            }
        }
        //--------------

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Select';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Company List
        if($fn_status == true)
        {
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $companies = $return['data'];
                $company_list[''] = 'Select';
                foreach($companies as $company)
                {
                    $company_list[$company->IC_id] = $company->IC_name;
                }
            }
        }
        //-----------------

        // Get Product List
        if($fn_status == true)
        {
            $return = InsuranceProduct::getAllProducts();
            if($return['status'] == true)
            {
                $products = $return['data'];
                $product_list[''] = 'Select';
                foreach($products as $product)
                {
                    $product_list[$product->IP_id] = $product->IP_name;
                }
            }
        }
        //-----------------

        // Get Lead Status List
        if($fn_status == true)
        {
            foreach(config('constant.lead_status') as $status)
            {
                $lead_status_list[$status['value']] = $status['caption'];
            }
        }
        //---------------------

        $this->viewData['lead_status_list'] = $lead_status_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['company_list'] = $company_list;
        $this->viewData['product_list'] = $product_list;
        return view('lead.create', $this->viewData);
    }

    /**
     * Edit lead.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $fn_status = true;
        $lead_id = null;
        $countryList = [];
        $commStateList = [];
        $state_list = [];
        $city_list = [];
        $agent_list = [];
        $company_list = [];
        $product_list = [];
        $lead_status_list = [];
        $lead_detail = null;

        // Fetch Request Variables
        $lead_id = intval($request['id']);
        //------------------------
        
        // Get Lead Detail
        if($fn_status == true)
        {
            $return = Lead::getLeadDetail($lead_id);
            $fn_status = $return['status'];
            if($fn_status == true)
            {
                $lead_detail = $return['data'];
                $lead_detail->L_location = str_replace('||', ',', $lead_detail->L_location);
            }
        }
        //------------------------
        //
        // Get State List
        if($fn_status == true)
        {
            $countryId = config('constant.COUNTRY_ID_INDIA');
            $states = State::getStateByCountry($countryId);
            $state_list[''] = 'Select';
            foreach($states as $state)
            {
                $state_list[$state->ST_code] = $state->ST_name;
            }
        }
        
        if($lead_detail->L_policyType == 'TRAVEL') {
            $countryListData = \App\Http\Models\Country::getAllCountries();
            foreach($countryListData as $c)
            {
                $countryList[$c['id']] = $c['name'];
            }
            $countryId = (!empty($lead_detail->travelInsurance->dest_country)?$lead_detail->travelInsurance->dest_country:config('constant.COUNTRY_ID_INDIA'));
            $states = State::getStateByCountry($countryId);
            foreach($states as $state)
            {
                $commStateList[$state->ST_id] = $state->ST_name;
            }
        }
//        print_r($state_list);exit;
        //-----------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getCityByState($lead_detail->L_customerState);
            if($return['status'] == true)
            {
                $city_list = $return['data'];
            }
        }
        //--------------
        
        // Get Make List
        if($fn_status == true)
        {
            $make_list = Vehicle::getMakes();
        }
        //--------------
        
        // Get Make Model List
        if($fn_status == true)
        {
            $model_list = Vehicle::getMakeModels($lead_detail->L_vehicleMake);
        }
        //--------------

        // Get Agent List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents();
            if($return['status'] == true)
            {
                $agents = $return['data'];
                $agent_list[''] = 'Select';
                foreach($agents as $agent)
                {
                    $agent_list[$agent->AG_id] = $agent->AG_firstName.' '.$agent->AG_lastName;
                }
            }
        }
        //-----------------

        // Get Company List
        if($fn_status == true)
        {
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $companies = $return['data'];
                $company_list[''] = 'Select';
                foreach($companies as $company)
                {
                    $company_list[$company->IC_id] = $company->IC_name;
                }
            }
        }
        //-----------------

        // Get Product List
        if($fn_status == true)
        {
            $insuranceProd = InsuranceProduct::getInsuranceProducts($lead_detail->L_policyType);
            if(!empty($return)) {
                $product_list[''] = 'Select';
//                print_r($insuranceProd);exit;
                foreach($insuranceProd as $product)
                {
                    $product_list[$product['IP_id']] = $product['IP_name'];
                }
            }
        }
        //-----------------

        // Get Lead Status List
        if($fn_status == true)
        {
            foreach(config('constant.lead_status') as $status)
            {
                $lead_status_list[$status['value']] = $status['caption'];
            }
        }
        //---------------------

        //----------------

        // Delete Notification
        if($fn_status == true)
        {
            Notification::deleteNotification(['N_Lid' => $lead_id]);
        }
        //--------------------
        
        // Dealer wallet balance
        $return = Ledger::getAgentAccountBalance($lead_detail->L_AGid);
        $dealerWalletBalance = $return['data']->LG_balance;
        //--------------------
        
        $this->viewData['dealerWalletBalance'] = $dealerWalletBalance;
        $this->viewData['lead_status_list'] = $lead_status_list;
        $this->viewData['countryList'] = $countryList;
        $this->viewData['commStateList'] = $commStateList;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        $this->viewData['agent_list'] = $agent_list;
        $this->viewData['company_list'] = $company_list;
        $this->viewData['product_list'] = $product_list;
        $this->viewData['make_list'] = $make_list;
        $this->viewData['model_list'] = $model_list;
        $this->viewData['lead_detail'] = $lead_detail;
        //Vehicle Type:
        if($lead_detail->L_policyType == 'MOTOR') {
        $this->viewData['vehicle_type'] = config('constant.VEHICLE_TYPE');
        $this->viewData['vehicle_make'] = \App\Http\Models\MotorMake::where('type_id', $lead_detail->motorInsurance->vehicle_type)->get();
        $this->viewData['vehicle_model'] = \App\Http\Models\MotorModel::where('make_id', $lead_detail->motorInsurance->vehicle_make)->get();
        }
        
        $otherPolicyType = config('constant.other_policy_type'); 
        if(in_array($lead_detail->L_policyType, $otherPolicyType)) {
            return view('lead.other', $this->viewData);
        } else {
            return view('lead.'.strtolower($lead_detail->L_policyType), $this->viewData);
        }
    }

    /**
     * Add lead.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $Lead = new Lead();
        $Lead->field['L_AGid'] = $request['agent'];
        $Lead->field['L_policyType'] = $request['policy_type'];
        $Lead->field['L_ICid'] = $request['company'];
        $Lead->field['L_IPid'] = $request['product'];
        $Lead->field['L_customerFirstName'] = $request['customer_first_name'];
        $Lead->field['L_customerLastName'] = $request['customer_last_name'];
        $Lead->field['L_customerEmail'] = $request['customer_email'];
        $Lead->field['L_customerMobile'] = $request['customer_mobile'];
        $Lead->field['L_customerPhone'] = $request['customer_phone'];
        $Lead->field['L_customerAddress'] = $request['customer_address'];
        $Lead->field['L_customerCity'] = $request['customer_city'];
        $Lead->field['L_customerState'] = $request['customer_state'];
        $Lead->field['L_customerZip'] = $request['customer_zip'];

        $Lead->field['L_vehicleType'] = $request['vehicle_type'];
        $Lead->field['L_vehicleModel'] = $request['vehicle_model'];
        $Lead->field['L_vehicleVariant'] = $request['vehicle_variant'];
        $Lead->field['L_vehicleYear'] = $request['vehicle_year'];
        $Lead->field['L_vehicleEngineNo'] = $request['vehicle_engine_no'];
        $Lead->field['L_vehicleChassisNo'] = $request['vehicle_chassis_no'];
        $Lead->field['L_vehicleRegNo'] = $request['vehicle_reg_no'];
        $Lead->field['L_vehicleAccessories'] = $request['vehicle_accessories'];
        $Lead->field['L_vehicleNetPrice'] = $request['vehicle_net_price'];
        $Lead->field['L_vehicleCGST'] = $request['vehicle_cgst'];
        $Lead->field['L_vehicleSGST'] = $request['vehicle_sgst'];
        $Lead->field['L_vehicleGrossPrice'] = $request['vehicle_gross_price'];

        $Lead->field['L_oldPolicyCompany'] = $request['old_policy_company'];
        $Lead->field['L_oldPolicyNumber'] = $request['old_policy_number'];
        $Lead->field['L_oldPolicyStartDate'] = $request['old_policy_start_date'];
        $Lead->field['L_oldPolicyEndDate'] = $request['old_policy_end_date'];
        $Lead->field['L_oldPolicyAttachment'] = $request['old_policy_attachment'];

        $Lead->field['L_newPolicyCompany'] = $request['new_policy_company'];
        $Lead->field['L_newPolicyProduct'] = $request['new_policy_product'];
        $Lead->field['L_newPolicyEstimatedPremium'] = $request['new_policy_estimated_premium'];
        $Lead->field['L_newPolicyODPremium'] = $request['new_policy_od_premium'];
        $Lead->field['L_newPolicyTPPremium'] = $request['new_policy_tp_premium'];
        $Lead->field['L_newPolicyTaxPremium'] = $request['new_policy_tax_premium'];
        $Lead->field['L_newPolicyCGSTPremium'] = $request['new_policy_cgst_premium'];
        $Lead->field['L_newPolicySGSTPremium'] = $request['new_policy_sgst_premium'];
        $Lead->field['L_newPolicyTotalPremium'] = $request['new_policy_total_premium'];
        $Lead->field['L_newPolicyNumber'] = $request['new_policy_number'];
        $Lead->field['L_newPolicyStartDate'] = $request['new_policy_start_date'];
        $Lead->field['L_newPolicyEndDate'] = $request['new_policy_end_date'];
        $Lead->field['L_newPolicyAttachment'] = $request['new_policy_attachment'];
        $Lead->field['L_location'] = $request['location'];

        $Lead->field['L_status'] = $request['status'];
        $return = $Lead->addLead();
        if($return['status'] == true)
        {
            $lead_detail = $return['data'];
            $redirect['url'] = url('lead/edit', ['id' => $lead_detail->L_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('lead/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update lead detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $fn_status = true;
        $lead_id = null;
        $agent_id = null;

        // Fetch Request Variables
        if($fn_status == true)
        {
            $lead_id = $request['id'];
        }
        //------------------------

        // Fetch Lead Detail
        if($fn_status == true)
        {
            $return = Lead::getLeadDetail($lead_id);
            if($return['status'] == true)
            {
                $agent_id = $return['data']->L_AGid;
            }
        }
        //------------------

        // Check agent's wallet balance
        if($fn_status == true)
        {
//            if($request['status'] == config('constant.lead_status.COMPLETED.value'))
//            {
//                $return = Ledger::getAgentAccountBalance($agent_id);
//                if($return['status'] == true)
//                {
//                    if($return['data']->LG_balance < $request['new_policy_total_premium'])
//                    {
//                        $fn_status = false;
//                        $return['status'] = $fn_status;
//                        $return['message'] = "Insufficient balance in Agent's wallet";
//                }
//            }
//        }
        }
        //-----------------------------

        // Update Lead
        if($fn_status == true)
        {
            $Lead = new Lead();
            $Lead->field['L_id'] = $lead_id;
            $Lead->field['L_customerFirstName'] = $request['customer_first_name'];
            $Lead->field['L_customerLastName'] = $request['customer_last_name'];
            $Lead->field['L_customerEmail'] = $request['customer_email'];
            $Lead->field['L_customerMobile'] = $request['customer_mobile'];
            $Lead->field['L_customerPhone'] = $request['customer_phone'];
            $Lead->field['L_customerAddress'] = $request['customer_address'];
            $Lead->field['L_customerCity'] = $request['customer_city'];
            $Lead->field['L_customerState'] = $request['customer_state'];
            $Lead->field['L_customerZip'] = $request['customer_zip'];

            $Lead->field['L_vehicleType'] = $request['vehicle_type'];
            $Lead->field['L_vehicleModel'] = $request['vehicle_model'];
            $Lead->field['L_vehicleVariant'] = $request['vehicle_variant'];
            $Lead->field['L_vehicleYear'] = $request['vehicle_year'];
            $Lead->field['L_vehicleEngineNo'] = $request['vehicle_engine_no'];
            $Lead->field['L_vehicleChassisNo'] = $request['vehicle_chassis_no'];
            $Lead->field['L_vehicleRegNo'] = $request['vehicle_reg_no'];
            $Lead->field['L_vehicleAccessories'] = $request['vehicle_accessories'];
            $Lead->field['L_vehicleNetPrice'] = $request['vehicle_net_price'];
            $Lead->field['L_vehicleCGST'] = $request['vehicle_cgst'];
            $Lead->field['L_vehicleSGST'] = $request['vehicle_sgst'];
            $Lead->field['L_vehicleGrossPrice'] = $request['vehicle_gross_price'];

            $Lead->field['L_oldPolicyCompany'] = $request['old_policy_company'];
            $Lead->field['L_oldPolicyNumber'] = $request['old_policy_number'];
            $Lead->field['L_oldPolicyStartDate'] = ($request['old_policy_start_date']?Carbon::createFromFormat('d-m-Y', $request['old_policy_start_date'])->toDateString():null);
            $Lead->field['L_oldPolicyEndDate'] = ($request['old_policy_end_date']?Carbon::createFromFormat('d-m-Y', $request['old_policy_end_date'])->toDateString():null);

            $Lead->field['L_newPolicyCompany'] = $request['new_policy_company'];
            $Lead->field['L_newPolicyProduct'] = $request['new_policy_product'];
            $Lead->field['L_newPolicyPremium'] = $request['new_policy_premium'];
            $Lead->field['L_newPolicyEstimatedPremium'] = $request['new_policy_estimated_premium'];
            $Lead->field['L_newPolicyODPremium'] = $request['new_policy_od_premium'];
            $Lead->field['L_newPolicyTPPremium'] = $request['new_policy_tp_premium'];
            $Lead->field['L_newPolicyTaxPremium'] = $request['new_policy_tax_premium'];
            $Lead->field['L_newPolicyCGSTPremium'] = $request['new_policy_cgst_premium'];
            $Lead->field['L_newPolicySGSTPremium'] = $request['new_policy_sgst_premium'];
            $Lead->field['L_newPolicyTotalPremium'] = $request['new_policy_total_premium'];

            $Lead->field['L_newPolicyNumber'] = $request['new_policy_number'];
            $Lead->field['L_newPolicyStartDate'] = ($request['new_policy_start_date']?Carbon::createFromFormat('d-m-Y', $request['new_policy_start_date'])->toDateString():null);
            $Lead->field['L_newPolicyEndDate'] = ($request['new_policy_end_date']?Carbon::createFromFormat('d-m-Y', $request['new_policy_end_date'])->toDateString():null);

//            if($request->file('new_policy_attachment'))
//            {
//                $file_name = 'NPA_'.$lead_id.'_'.Carbon::now()->timestamp.'.'.$request->file('new_policy_attachment')->getClientOriginalExtension();
//                Storage::disk('lead')->putFileAs('', $request->file('new_policy_attachment'), $file_name);
//                $Lead->field['L_newPolicyAttachment'] = $file_name;
//            }

            if($request['status'] == config('constant.lead_status.COMPLETED.value'))
            {
                $Lead->field['L_completedAt'] = Carbon::now()->toDateTimeString();
            }

            $Lead->field['L_status'] = $request['status'];
            $return = $Lead->updateLeadDetail();
            $fn_status = $return['status'];
            if($fn_status == true)
            {
                $lead_detail = $return['data'];
            }
        }
        //------------

        // Fetch Agent's Payouts
        if($fn_status == true)
        {
            if($lead_detail->L_status == config('constant.lead_status.COMPLETED.value'))
            {
                $return_commission = CommissionPayout::getAgentPayoutDetail($lead_detail->L_AGid, $lead_detail->L_newPolicyCompany, $lead_detail->L_newPolicyProduct);
                $fn_status = $return_commission['status'];
                if($fn_status == true)
                {
                    $payout_detail = $return_commission['data'];
                    if($payout_detail == null)
                    {
                        $fn_status = false;
                    }
                }
            }
            else
            {
                $fn_status = false;
            }
        }
        //----------------------

        // Update Transaction
        if($fn_status == true)
        {
            $return_transaction = Ledger::getLeadTransactionDetail(['LG_Lid' => $lead_id, 'LG_type' => config('constant.transaction_type.PAIDFORPOLICY.value')]);
            if($return_transaction['data'] == null)
            {
                $Ledger = new Ledger();
                $Ledger->field['LG_Lid'] = $lead_detail->L_id;
                $Ledger->field['LG_AGid'] = $lead_detail->L_AGid;
                $Ledger->field['LG_type'] = config('constant.transaction_type.PAIDFORPOLICY.value');
                $Ledger->field['LG_message'] = config('constant.transaction_type.PAIDFORPOLICY.message');
                $Ledger->field['LG_amount'] = $lead_detail->L_newPolicyTotalPremium;
                $Ledger->field['LG_note'] = 'Payment paid on lead '.$lead_detail->L_id;
                $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                $Ledger->addTransaction();
            }
            else
            {
                $transaction_detail = $return_transaction['data'];
                $Ledger = new Ledger();
                $Ledger->field['LG_id'] = $transaction_detail->LG_id;
                $Ledger->field['LG_Lid'] = $lead_detail->L_id;
                $Ledger->field['LG_AGid'] = $lead_detail->L_AGid;
                $Ledger->field['LG_type'] = config('constant.transaction_type.PAIDFORPOLICY.value');
                $Ledger->field['LG_message'] = config('constant.transaction_type.PAIDFORPOLICY.message');
                $Ledger->field['LG_amount'] = $lead_detail->L_newPolicyTotalPremium;
                $Ledger->field['LG_note'] = 'Payment paid on lead '.$lead_detail->L_id;
                $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                $Ledger->updateTransactionDetail();
            }

            if($lead_detail->L_newPolicyODPremium || $lead_detail->L_newPolicyTPPremium)
            {
                $commission_premium = ($lead_detail->L_newPolicyODPremium>0?$lead_detail->L_newPolicyODPremium:$lead_detail->L_newPolicyTPPremium);
                $return_transaction = Ledger::getLeadTransactionDetail(['LG_Lid' => $lead_id, 'LG_type' => config('constant.transaction_type.COMMISSION.value')]);
                if($return_transaction['data'] == null)
                {
                    $Ledger = new Ledger();
                    $Ledger->field['LG_Lid'] = $lead_detail->L_id;
                    $Ledger->field['LG_AGid'] = $lead_detail->L_AGid;
                    $Ledger->field['LG_type'] = config('constant.transaction_type.COMMISSION.value');
                    $Ledger->field['LG_message'] = config('constant.transaction_type.COMMISSION.message');
                    $Ledger->field['LG_amount'] = ($commission_premium*$payout_detail->CP_payout)/100;
                    $Ledger->field['LG_note'] = 'Commission earned on lead '.$lead_detail->L_id;
                    $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                    $Ledger->addTransaction();
                }
                else
                {
                    $transaction_detail = $return_transaction['data'];
                    $Ledger = new Ledger();
                    $Ledger->field['LG_id'] = $transaction_detail->LG_id;
                    $Ledger->field['LG_Lid'] = $lead_detail->L_id;
                    $Ledger->field['LG_AGid'] = $lead_detail->L_AGid;
                    $Ledger->field['LG_type'] = config('constant.transaction_type.COMMISSION.value');
                    $Ledger->field['LG_message'] = config('constant.transaction_type.COMMISSION.message');
                    $Ledger->field['LG_amount'] = ($commission_premium*$payout_detail->CP_payout)/100;
                    $Ledger->field['LG_note'] = 'Commission earned on lead '.$lead_detail->L_id;
                    $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                    $Ledger->updateTransactionDetail();
                }
            }
        }
        //-------------------

        $status = ($return['status']==true?'success':'error');
        return redirect(url('lead/edit', ['id' => $lead_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }
}
