<?php

namespace App\Http\Controllers\Lead;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Models\TravelInsurance;
use Carbon\Carbon,
    Storage;

class TravelInsuranceController extends Controller {

    public function __construct() {
        parent::__construct();
//        $this->middleware('api_auth');
    }

    /**
     * Add
     *
     * @param Request $request
     * @return Mixed
     */
    public function update(Request $request) {
        $input = $request->all();
//        print_r($input);
//        Mail::raw(json_encode($input), function($message)
//        {
//            $message->from('us@example.com', 'Laravel');
//            $message->to('engineering@fondostech.in');
//        });
//        exit;
//        print_r($input);exit;
        try{
            if(empty($request['customer_first_name'])) { throw new \Exception('Please enter first name'); }
            if(empty($request['customer_last_name'])) { throw new \Exception('Please enter last name'); }
            if(empty($request['customer_email'])) { throw new \Exception('Please enter email'); }
            if(empty($request['customer_dob'])) { 
                throw new \Exception('Please select DOB');
            } else {
                $dob = explode('/', $request['customer_dob']);
                $dd = $dob[0];
                $mm = $dob[1];
                $yy = $dob[2];
                if(!checkdate($mm, $dd, $yy)) {
                    throw new \Exception('Please select valid DOB');
                }
            }
            if(empty($request['customer_mobile'])) { throw new \Exception('Please enter mobile number'); }
            if(empty($request['customer_address'])) { throw new \Exception('Please enter customer address'); }
            if(empty($request['customer_state'])) { throw new \Exception('Please select customer state'); }
//            if(empty($request['customer_city'])) { throw new \Exception('Please select customer city'); }
            if(empty($request['customer_zip'])) { throw new \Exception('Please enter pincode'); }
           
            if(empty($request['policy_for'])) { throw new \Exception('Please select whom'); }
            if(empty($request['dest_country'])) { throw new \Exception('Please select whom'); }
            if($request['policy_for'] != 'Student Policy') {
                if(empty($request['for_whom'])) { throw new \Exception('Please select for whom'); }
            }
            if(empty($request['policy_holder_details'])) { throw new \Exception('Please select policy holder details'); }
            if($request['policy_for'] != 'Student Policy') {
                if(empty($request['annual_trip'])) { throw new \Exception('Please select annual trip'); }
            }
            if(!empty($request['annual_trip']) && $request['annual_trip'] == 'Yes' && empty($request['longest_trip_duration'])) { 
                throw new \Exception('Please select trip duration'); 
            } elseif($request['annual_trip'] == 'No') {
                if(empty($request['annual_trip_start'])) { throw new \Exception('Please enter trip start date'); }
                if(empty($request['annual_trip_end'])) { throw new \Exception('Please enter trip end date'); }
                if(empty($request['annual_trip_days'])) { throw new \Exception('Please enter trip days'); }
            }
            if(empty($request['communication_state'])) { throw new \Exception('Please select communication state'); }
            if(empty($request['sum_insured'])) { throw new \Exception('Please enter sum insured'); }
            if(empty($request['new_policy_company'])) { throw new \Exception('Please select insurance company'); }
            if(empty($request['new_policy_product'])) { throw new \Exception('Please select policy product'); }
            if(empty($request['estimated_premium'])) { throw new \Exception('Please enter estimated premium'); }
            if(empty($request['net_premium']) || $request['net_premium'] == 0.00) { throw new \Exception('Please enter net premium'); }
            if(empty($request['gross_premium']) || $request['gross_premium'] == 0.00) { throw new \Exception('Please enter gross premium'); }
            if(empty($request['gst']) || $request['gst'] == 0.00) { throw new \Exception('Please enter gst premium'); }
            if(empty($request['paid_by_whom'])) { throw new \Exception('Please select the payer'); }
//            if(empty($request['departed_from_india'])) { throw new \Exception('Please select whether departed from India or Not'); }
            
            if($request['status'] == config('constant.lead_status.COMPLETED.value')) {
                
                $lead_detail = \App\Http\Models\Lead::where('L_id', $request['lead_id'])->with('travelInsurance')->first();
                $motorInsuranceLead = $lead_detail->travelInsurance;
                $motorInsuranceLead->agent_id = $lead_detail->L_AGid;
                $motorInsuranceLead->policy_type = $lead_detail->L_policyType;
                $motorInsuranceLead->gross_premium = $request['gross_premium'];
                $motorInsuranceLead->new_policy_product = $request['new_policy_product'];
                $motorInsuranceLead->new_policy_company = $request['new_policy_company'];
                $motorInsuranceLead->status = $request['status'];
                $motorInsuranceLead->paid_by_whom = $request['paid_by_whom'];
                /* Commision calculation */
                
                $return = calculateCommision($motorInsuranceLead);
                if($return['status']==false) {
                    $msg = (!empty($return['message'])?$return['message']:"Error occured");
                    throw new \Exception($msg);
                }
                
                /* End */
            }
            
            // Insert Lead
            $leadId = $request['lead_id'];
            $travel = new TravelInsurance;
            $travel->field['L_id'] = $leadId;
            $travel->field['L_AGid'] = $request['agent_id'];
            $travel->field['L_policyType'] = $request['policy_type'];
            //Customer Details
            $travel->field['L_customerFirstName'] = $request['customer_first_name'];
            $travel->field['L_customerLastName'] = $request['customer_last_name'];
            $travel->field['L_customerEmail'] = $request['customer_email'];
            $travel->field['L_customerDOB'] = ($request['customer_dob'] ? Carbon::createFromFormat('j/n/Y', $request['customer_dob'])->toDateString() : null);
            $travel->field['L_customerMobile'] = $request['customer_mobile'];
            $travel->field['L_customerPhone'] = $request['customer_phone'];
            $travel->field['L_customerAddress'] = $request['customer_address'];
            $travel->field['L_customerCity'] = $request['customer_city'];
            $travel->field['L_customerState'] = $request['customer_state'];
            $travel->field['L_customerZip'] = $request['customer_zip'];
            
            //Vehicle Details
            $travel->field['id'] = $request['id'];
            $travel->field['policy_for'] = $request['policy_for'];
            $travel->field['dest_country'] = $request['dest_country'];
            $travel->field['for_whom'] = $request['for_whom'];
            $travel->field['policy_holder_details'] = json_encode($request['policy_holder_details']);
            $travel->field['annual_trip'] = $request['annual_trip'];
            $travel->field['longest_trip_duration'] = $request['longest_trip_duration'];
            $travel->field['annual_trip_start'] = ($request['annual_trip_start'] ? Carbon::createFromFormat('j/n/Y', $request['annual_trip_start'])->toDateString() : null);
            $travel->field['annual_trip_end'] = ($request['annual_trip_end'] ? Carbon::createFromFormat('j/n/Y', $request['annual_trip_end'])->toDateString() : null);
            $travel->field['annual_trip_days'] = $request['annual_trip_days'];
            $travel->field['communication_state'] = $request['communication_state'];
            $travel->field['sum_insured'] = $request['sum_insured'];
            $travel->field['new_policy_company'] = $request['new_policy_company'];
            $travel->field['new_policy_product'] = $request['new_policy_product'];
            $travel->field['estimated_premium'] = $request['estimated_premium'];
            $travel->field['departed_from_india'] = $request['departed_from_india'];
            $travel->field['net_premium'] = $request['net_premium'];
            $travel->field['gst'] = $request['gst'];
            $travel->field['gross_premium'] = $request['gross_premium'];
            $travel->field['status'] = $request['status'];
            $travel->field['new_policy_number'] = $request['new_policy_number'];

            $travel->field['new_policy_start_date'] = ($request['new_policy_start_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_start_date'])->toDateString():null);
            $travel->field['new_policy_end_date'] = ($request['new_policy_end_date']?Carbon::createFromFormat('j-n-Y', $request['new_policy_end_date'])->toDateString():null);
            $travel->field['paid_by_whom'] = ($request['paid_by_whom']?$request['paid_by_whom']:null);
            if($request->file('new_policy_attachment'))
            {
                $file_name = 'TRAVEL_'.$leadId.'_'.Carbon::now()->timestamp.'.'.$request->file('new_policy_attachment')->getClientOriginalExtension();
                Storage::disk('policies')->putFileAs('', $request->file('new_policy_attachment'), $file_name);
                $travel->field['policy_attachment'] = $file_name;
            }
            /* End */
            
            // Used to email policy to customer 
            if(!empty($file_name)) {
                 $storagePath = storage_path('app/public/lead/policies');
                $emailData['attachment'] = $storagePath."/".$file_name;
                $emailData['customer_name'] = $request['customer_first_name'].' '.$request['customer_last_name'];
                $emailData['customer_email'] = $request['customer_email'];
                $emailStatus = sendPolicyEmailToCustomer($emailData);
                if(!$emailStatus['status']) {
                    Mail::raw(json_encode($travel), function($message)
                    {
                        $message->from('noreply@flashapp.com', 'Email Error');
                        $message->to('engineering@fondostech.in');
                    });
                }
            }
            // End
            $return = $travel->updateDetails();
            /* End */
            $status = ($return['status']==true?'success':'error');
            return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $return['message'], 'type' => $status]);  
            
        } catch (\Exception $ex) {
            $status = 'error';
            return redirect(url('lead/edit', ['id' => $request['lead_id']]))->with(['msg' => $ex->getMessage(), 'type' => $status]);
        }
    }
}
