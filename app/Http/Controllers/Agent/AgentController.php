<?php
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Agent;
use App\Http\Models\InsuranceCompany;
use App\Http\Models\InsuranceProduct;
use App\Http\Models\CommissionPayout;
use App\Http\Models\Admin;
use App\Http\Models\State;
use App\Http\Models\City;
use Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class AgentController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'agent';
    }

    /**
     * View agents.
     *
     * @return view
     */
    public function index()
    {
        $fn_status = true;
        $agent_list = null;
        $where = ['AG_type' => config('constant.agent_type.AGENT.value')];

        // Get All Agents
        if($fn_status == true)
        {
            if(Auth::user()->A_type == config('constant.user_type.MANAGER.value') || Auth::user()->A_type == config('constant.user_type.EXECUTIVE.value'))
            {
                $where = ['AG_type' => config('constant.agent_type.AGENT.value'), 'AG_Aid' => Auth::user()->A_id];
            }

            $return = Agent::getAllAgents($where);
            if($return['status'] == true)
            {
                $agent_list = $return['data'];
            }
        }
        //---------------

        $this->viewData['agent_list'] = $agent_list;
        return view('agent.index', $this->viewData);
    }

    /**
     * Add agent.
     *
     * @return view
     */
    public function create()
    {
	    $fn_status = true;
        $policy_type_list = [];
	    $user_list = [];
	    $dealer_list = [];
        $state_list = [];
        $city_list = [];

        // Check Permission
        if(check_permission('AGENT_CREATE', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Get Agent Type List
        if($fn_status == true)
        {
            foreach(config('constant.agent_type') as $status)
            {
                $agent_type_list[$status['value']] = $status['caption'];
            }
        }
        //--------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

    	// Get Admin List
        if($fn_status == true)
        {
            $return = Admin::getAllUsers();
            if($return['status'] == true)
            {
                $users = $return['data'];
                $user_list[''] = 'Select';
                foreach($users as $user)
                {
                    $user_list[$user->A_id] = $user->A_firstName.' '.$user->A_lastName;
                }
            }
        }
        //-----------------

        // Get Dealer List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents(['AG_type' => config('constant.agent_type.DEALER.value')]);
            if($return['status'] == true)
            {
                $dealers = $return['data'];
                $dealer_list[''] = 'Select';
                foreach($dealers as $dealer)
                {
                    $dealer_list[$dealer->AG_id] = $dealer->AG_firstName.' '.$dealer->AG_lastName;
                }
            }
        }
        //-----------------

        // Get State List
        if($fn_status == true)
        {
//            $return = State::getAllStates();
//            if($return['status'] == true)
//            {
//                $states = $return['data'];
//                $state_list[''] = 'Select';
//                foreach($states as $state)
//                {
//                    $state_list[$state->ST_code] = $state->ST_name;
//                }
//            }
            $countryId = config('constant.COUNTRY_ID_INDIA');
            $states = State::getStateByCountry($countryId);
            $state_list[''] = 'Select';
            foreach($states as $state)
            {
                $state_list[$state->ST_code] = $state->ST_name;
            }
        }
        //-----------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
//                $city_list = $return['data'];
                $cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }
            }
        }
        //--------------

        $this->viewData['agent_type_list'] = $agent_type_list;
        $this->viewData['policy_type_list'] = $policy_type_list;
    	$this->viewData['user_list'] = $user_list;
    	$this->viewData['dealer_list'] = $dealer_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        return view('agent.create', $this->viewData);
    }

    /**
     * Edit agent.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
    	$fn_status = true;
        $policy_type_list = [];
        $agent_id = null;
        $state_list = [];
        $city_list = [];
        $agent_detail = null;
        $user_list = [];
        $dealer_list = [];

        // Check Permission
        if(check_permission('AGENT_CREATE', Auth::user()->A_type) == false && check_permission('AGENT_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $agent_id = intval($request['id']);
        //------------------------

        // Get Agent Type List
        if($fn_status == true)
        {
            foreach(config('constant.agent_type') as $status)
            {
                $agent_type_list[$status['value']] = $status['caption'];
            }
        }
        //--------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

	    // Get Admin List
        if($fn_status == true)
        {
            $return = Admin::getAllUsers();
            if($return['status'] == true)
            {
                $users = $return['data'];
                $user_list[''] = 'Select';
                foreach($users as $user)
                {
                    $user_list[$user->A_id] = $user->A_firstName.' '.$user->A_lastName;
                }
            }
        }
        //-----------------

        // Get Dealer List
        if($fn_status == true)
        {
            $return = Agent::getAllAgents(['AG_type' => config('constant.agent_type.DEALER.value')]);
            if($return['status'] == true)
            {
                $dealers = $return['data'];
                $dealer_list[''] = 'Select';
                foreach($dealers as $dealer)
                {
                    $dealer_list[$dealer->AG_id] = $dealer->AG_firstName.' '.$dealer->AG_lastName;
                }
            }
        }
        //-----------------

        // Get State List
        if($fn_status == true)
        {
//            $return = State::getAllStates();
//            if($return['status'] == true)
//            {
//                $states = $return['data'];
//                $state_list[''] = 'Select';
//                foreach($states as $state)
//                {
//                    $state_list[$state->ST_code] = $state->ST_name;
//                }
//            }
            $countryId = config('constant.COUNTRY_ID_INDIA');
            $states = State::getStateByCountry($countryId);
            $state_list[''] = 'Select';
            foreach($states as $state)
            {
                $state_list[$state->ST_code] = $state->ST_name;
            }
        }
        //---------------

        // Get City List
        if($fn_status == true)
        {
            $return = City::getAllCities();
            if($return['status'] == true)
            {
                //$city_list = $return['data'];
                $cities = $return['data'];
                $city_list[''] = 'Select';
                foreach($cities as $city)
                {
                    $city_list[$city->CT_name] = $city->CT_name;
                }
            }
        }
        //--------------

        // Get Agent Detail
        if($fn_status == true)
        {
            $return = Agent::getAgentDetail($agent_id);
            if($return['status'] == true)
            {
                $agent_detail = $return['data'];
                $agent_detail->AG_policyType = explode(',', $agent_detail->AG_policyType);
            }
        }
        //-----------------

        $this->viewData['agent_type_list'] = $agent_type_list;
        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['user_list'] = $user_list;
        $this->viewData['dealer_list'] = $dealer_list;
        $this->viewData['state_list'] = $state_list;
        $this->viewData['city_list'] = $city_list;
        $this->viewData['agent_detail'] = $agent_detail;
        return view('agent.edit', $this->viewData);
    }

    /**
     * Add agent.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $company_list = null;
        $product_list = null;
        $redirect = null;
        $Agent = new Agent();
        $Agent->field['AG_Aid'] = $request['manager'];
        $Agent->field['AG_AGid'] = $request['dealer'];
        $Agent->field['AG_type'] = config('constant.agent_type.AGENT.value');
        $Agent->field['AG_code'] = $request['code'];
        $Agent->field['AG_policyType'] = config('constant.policy_type.PEDAL_CYCLE.value');//$request['policy_type'];
        $Agent->field['AG_firstName'] = $request['first_name'];
        $Agent->field['AG_lastName'] = $request['last_name'];
        $Agent->field['AG_email'] = $request['email'];
        $Agent->field['AG_username'] = $request['username'];
        $Agent->field['AG_password'] = $request['password'];
        $Agent->field['AG_mobile'] = $request['mobile'];
        $Agent->field['AG_phone'] = $request['phone'];
        $Agent->field['AG_address'] = $request['address'];
        $Agent->field['AG_city'] = $request['city'];
        $Agent->field['AG_state'] = $request['state'];
        $Agent->field['AG_zip'] = $request['zip'];
        
        $Agent->field['AG_ImdCode'] = $request['imd_code'];
        $Agent->field['AG_ImdName'] = $request['imd_name'];
        $Agent->field['AG_companyName'] = $request['company_name'];
        $Agent->field['AG_pan'] = $request['pan'];
        $Agent->field['AG_paymentModeType'] = $request['payment_mode'];
        $Agent->field['AG_accountType'] = $request['account_type'];
        $Agent->field['AG_bankName'] = $request['bank_name'];
        $Agent->field['AG_branchName'] = $request['branch_name'];
        $Agent->field['AG_bankAddress'] = $request['bank_address'];
        $Agent->field['AG_bankAccountNumber'] = $request['bank_account_number'];
        $Agent->field['AG_bankIfscCode'] = $request['bank_ifsc_code'];
        $Agent->field['AG_MicrCode'] = $request['micr_code'];
        $Agent->field['AG_active'] = $request['active'];
        $return = $Agent->addAgent();
        if($return['status'] == true)
        {
            $agent_detail = $return['data'];

            // Get All Companies
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $company_list = $return['data'];
            }
            //------------------

            // Get All Products
            $return = InsuranceProduct::getAllProducts();
            if($return['status'] == true)
            {
                $product_list = $return['data'];
            }
            //-----------------

            // Add Agents Payout
            foreach($company_list as $company)
            {
                foreach($product_list as $product)
                {
                    if($product->IP_policyType == $Agent->field['AG_policyType'])
                    {
                        // Insert Commission Payout
                        $CommissionPayout = new CommissionPayout();
                        $CommissionPayout->field['CP_AGid'] = $agent_detail->AG_id;
                        $CommissionPayout->field['CP_ICid'] = $company->IC_id;
                        $CommissionPayout->field['CP_IPid'] = $product->IP_id;
                        $CommissionPayout->field['CP_payout'] = config('constant.commission_payout.DEFAULT.value');
                        $CommissionPayout->field['CP_active'] = $request['active'];
                        $return = $CommissionPayout->addPayout();
                        //-------------------------
                    }
                }
            }
            //------------------

            $redirect['url'] = url('agent/edit', ['id' => $agent_detail->AG_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('agent/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update agent detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $Agent->field['AG_Aid'] = $request['manager'];
        $Agent->field['AG_AGid'] = $request['dealer'];
        $Agent->field['AG_policyType'] = config('constant.policy_type.PEDAL_CYCLE.value');//$request['policy_type'];
        $Agent->field['AG_firstName'] = $request['first_name'];
        $Agent->field['AG_lastName'] = $request['last_name'];
        $Agent->field['AG_email'] = $request['email'];
        $Agent->field['AG_username'] = $request['username'];
        $Agent->field['AG_password'] = $request['password'];
        $Agent->field['AG_mobile'] = $request['mobile'];
        $Agent->field['AG_phone'] = $request['phone'];
        $Agent->field['AG_address'] = $request['address'];
        $Agent->field['AG_city'] = $request['city'];
        $Agent->field['AG_state'] = $request['state'];
        $Agent->field['AG_zip'] = $request['zip'];
        
        $Agent->field['AG_ImdCode'] = $request['imd_code'];
        $Agent->field['AG_ImdName'] = $request['imd_name'];
        $Agent->field['AG_companyName'] = $request['company_name'];
        $Agent->field['AG_pan'] = $request['pan'];
        $Agent->field['AG_paymentModeType'] = $request['payment_mode'];
        $Agent->field['AG_accountType'] = $request['account_type'];
        $Agent->field['AG_bankName'] = $request['bank_name'];
        $Agent->field['AG_branchName'] = $request['branch_name'];
        $Agent->field['AG_bankAddress'] = $request['bank_address'];
        $Agent->field['AG_bankAccountNumber'] = $request['bank_account_number'];
        $Agent->field['AG_bankIfscCode'] = $request['bank_ifsc_code'];
        $Agent->field['AG_MicrCode'] = $request['micr_code'];        
        $Agent->field['AG_active'] = $request['active'];
        $return = $Agent->updateAgentDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('agent/edit', ['id' => $agent_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Delete agent.
     *
     * @param Request $request
     * @return redirect
     */
    public function remove(Request $request)
    {
        $agent_id = null;

        // Fetch Request Variables
        $agent_id = $request['id'];
        //------------------------

        $Agent = new Agent();
        $Agent->field['AG_id'] = $agent_id;
        $return = $Agent->deleteAgent();
        if($return['status'] == true)
        {
            $redirect['url'] = url('agents');
        }
        else
        {
            $redirect['url'] = url('agent/edit', ['id' => $agent_id]);
        }
        $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        return redirect($redirect['url'])->with($redirect['message']);
    }
    
    public function uploadDealer() {
        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);
        $spreadsheet = $reader->load('DealerList.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();
        $highestColumn = $worksheet->getHighestColumn();

        $data = array();
        $dated = array();
        $repeated = array();
        $emailError = false;
        $contactError = false;
        $assignedError = false;
        $callerNameError = false;
//        $columnArr['A'] = 'RM_Employee_Code';
//        $columnArr['B'] = 'Admin_User';
        $columnArr['C'] = 'AG_ImdCode';
        $columnArr['D'] = 'AG_ImdName';
        $columnArr['E'] = 'AG_companyName';
        $columnArr['F'] = 'AG_firstName';
        $columnArr['G'] = 'AG_mobile';
        $columnArr['H'] = 'AG_email';
        $columnArr['I'] = 'AG_address';
//        $columnArr['J'] = 'AG_state';
        $columnArr['K'] = 'AG_city';
        $columnArr['L'] = 'AG_zip';
        $columnArr['M'] = 'AG_pan';
        $columnArr['N'] = 'AG_paymentModeType';
        $columnArr['O'] = 'AG_accountType';
        $columnArr['P'] = 'AG_bankName';
        $columnArr['Q'] = 'AG_branchName';
        $columnArr['R'] = 'AG_bankAddress';
        $columnArr['S'] = 'AG_bankAccountNumber';
        $columnArr['T'] = 'AG_bankIfscCode';
        $columnArr['U'] = 'AG_MicrCode';
        
        $spreadsheet = new Spreadsheet();
        $repeatedEntriesSheet = new Worksheet($spreadsheet, 'Repeated Entries');
        $repeatedEntriesSheet = $spreadsheet->getActiveSheet();
        $errorRows = array();
        $i = 1;
        $error = false;
        foreach($worksheet->getRowIterator() as $rowKey => $row) {
            if($rowKey == 1) { continue; }
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $isExist = false;
            $connectedWith = 0;
            $rmCodeError = false;
            $adminCodeError = false;
            $isExistError = false;
            $stateCodeError = false;
            foreach ($cellIterator as $key => $cell) {
                if($key == 'A') {
                    $rmCode = $cell->getValue();
                    if(!empty($rmCode)) {
                        $rmDetail = Agent::findRMByRmcode($cell->getValue());
                        if(!empty($rmDetail)) {
                            $AG_AGid = $rmDetail->Ag_id;
                            $connectedWith = 1;
                        } else {
                            $rmCodeError = true;
                        }
                    }
                }
                if($key == 'B') {
                    $userCode = $cell->getValue();
                    if(!empty($userCode)) {
                        $userDetail = Admin::findAdminByUsername($cell->getValue());
                        if(!empty($userDetail)) {
                            $AG_AGid = $userDetail->A_id;
                            $connectedWith = 2;
                        } else {
                            $adminCodeError = true;
                        }
                    }
                }
                if($key == 'C') {
                    $isExist = Agent::findAgentByImdCode($cell->getValue());
                    if(!empty($isExist)) {
                        $isExistError = true;
                    }
                }
                if($key == 'J') {
                    $stateName = $cell->getValue();
                    echo '<br>'.$stateName;
                    if(!empty($stateName)) {
                        $conditions = [
                            ['ST_name', $stateName]
                        ];
                        $fields = '*';
                        $stateDetail = State::getState($conditions, $fields);
                        if(!empty($stateDetail)) {
                            $stateCode = $stateDetail->ST_code;
                        } else {
                            $stateCode = '';
                            $stateCodeError = true;
                        }
                    }
                }
                if(array_key_exists($key, $columnArr)) {
                    $column = $columnArr[$key];
                    $data[$rowKey][$column] = $cell->getValue();
                }
            }
            $data[$rowKey]['AG_Aid'] = 8; // Hard coded for liberty insurance user
            $data[$rowKey]['AG_state'] = (!empty($stateCode)?$stateCode:'');
            $data[$rowKey]['AG_AGid'] = (!empty($AG_AGid)?$AG_AGid:0);
            $data[$rowKey]['AG_connectedWith'] = $connectedWith;
            $data[$rowKey]['AG_username'] = $data[$rowKey]['AG_ImdCode'];
            $data[$rowKey]['AG_password'] = password_hash($data[$rowKey]['AG_ImdCode'], PASSWORD_BCRYPT);
            $data[$rowKey]['AG_type'] = config('constant.agent_type.AGENT.value');
            $data[$rowKey]['AG_code'] = $data[$rowKey]['AG_ImdCode'];
            $data[$rowKey]['AG_policyType'] = config('constant.policy_type.PEDAL_CYCLE.value');
            $data[$rowKey]['AG_active'] = 1;
            $data[$rowKey]['AG_createdAt'] = date('Y-m-d H:i:s');
            $data[$rowKey]['AG_updatedAt'] = date('Y-m-d H:i:s');
            if($rmCodeError || $adminCodeError || $isExistError || $stateCodeError) {
                $errorRow = $data[$rowKey];
                if($rmCodeError) {
                    $repeatedEntriesSheet->getStyle("A$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('FFFF0000');
                    
                }
                if($adminCodeError) {
                    $repeatedEntriesSheet->getStyle("B$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('FFFF0000');
                }
                if($isExistError) {
                    $repeatedEntriesSheet->getStyle("C$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('FFFF0000');
                }
                if($stateCodeError) {
                    $repeatedEntriesSheet->getStyle("J$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('FFFF0000');
                }
                $repeatedEntriesSheet->setCellValue("A$i", $rmCode);
                $repeatedEntriesSheet->setCellValue("B$i", $userCode);
                $repeatedEntriesSheet->setCellValue("C$i", $errorRow['AG_ImdCode']);
                $repeatedEntriesSheet->setCellValue("D$i", $errorRow['AG_ImdName']);
                $repeatedEntriesSheet->setCellValue("E$i", $errorRow['AG_companyName']);
                $repeatedEntriesSheet->setCellValue("F$i", $errorRow['AG_firstName']);
                $repeatedEntriesSheet->setCellValue("G$i", $errorRow['AG_mobile']);
                $repeatedEntriesSheet->setCellValue("H$i", $errorRow['AG_email']);
                $repeatedEntriesSheet->setCellValue("I$i", $errorRow['AG_address']);
                $repeatedEntriesSheet->setCellValue("J$i", $stateName);
                $repeatedEntriesSheet->setCellValue("K$i", $errorRow['AG_city']);
                $repeatedEntriesSheet->setCellValue("L$i", $errorRow['AG_zip']);
                $repeatedEntriesSheet->setCellValue("M$i", $errorRow['AG_pan']);
                $repeatedEntriesSheet->setCellValue("N$i", $errorRow['AG_paymentModeType']);
                $repeatedEntriesSheet->setCellValue("O$i", $errorRow['AG_accountType']);
                $repeatedEntriesSheet->setCellValue("P$i", $errorRow['AG_bankName']);
                $repeatedEntriesSheet->setCellValue("Q$i", $errorRow['AG_branchName']);
                $repeatedEntriesSheet->setCellValue("R$i", $errorRow['AG_bankAddress']);
                $repeatedEntriesSheet->setCellValue("S$i", $errorRow['AG_bankAccountNumber']);
                $repeatedEntriesSheet->setCellValue("T$i", $errorRow['AG_bankIfscCode']);
                $repeatedEntriesSheet->setCellValue("U$i", $errorRow['AG_MicrCode']);
                $i++;
                unset($data[$rowKey]);
                $error = true;
            }
        }
//        echo "<pre>";
//        print_r($data);exit;
        
        if($error) {
            $agent = new Agent;
            $agent::insert($data);        
            $writer = new Xlsx($spreadsheet);
            $fileName = time() . '_Upload-Error-Report' . '.xlsx';
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save("MIS_report/$fileName");
        }
    }
    
    public function updateDealer() {
        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);
        $spreadsheet = $reader->load('DealerUpdateList.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();
        $highestColumn = $worksheet->getHighestColumn();

        $data = array();
        $dated = array();
        $repeated = array();
        $emailError = false;
        $contactError = false;
        $assignedError = false;
        $callerNameError = false;
        $columnArr['A'] = 'AG_ImdCode';
        $columnArr['B'] = 'AG_ImdName';
        $columnArr['C'] = 'AG_companyName';
//        $columnArr['D'] = 'AG_firstName';
        $columnArr['E'] = 'AG_mobile';
        $columnArr['F'] = 'AG_email';
        $columnArr['G'] = 'AG_address';
        $columnArr['H'] = 'AG_state';
        $columnArr['I'] = 'AG_city';
        $columnArr['J'] = 'AG_zip';
        $columnArr['K'] = 'AG_pan';
        $columnArr['L'] = 'AG_paymentModeType';
        $columnArr['M'] = 'AG_accountType';
        $columnArr['N'] = 'AG_bankName';
        $columnArr['O'] = 'AG_branchName';
        $columnArr['P'] = 'AG_bankAddress';
        $columnArr['Q'] = 'AG_bankAccountNumber';
        $columnArr['R'] = 'AG_bankIfscCode';
        $columnArr['S'] = 'AG_MicrCode';
        
        $spreadsheet = new Spreadsheet();
        $repeatedEntriesSheet = new Worksheet($spreadsheet, 'Repeated Entries');
        $repeatedEntriesSheet = $spreadsheet->getActiveSheet();
        $errorRows = array();
        $i = 1;
        $error = false;
        foreach($worksheet->getRowIterator() as $rowKey => $row) {
            if($rowKey == 1) { continue; }
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $isExist = false;
            $connectedWith = 0;
            $rmCodeError = false;
            $adminCodeError = false;
            $isNotExistError = false;
            $stateCodeError = false;
            foreach ($cellIterator as $key => $cell) {
                if($key == 'A') {
                    $isNotExist = Agent::findAgentByImdCode($cell->getValue());
                    if(empty($isNotExist)) {
                        $isNotExistError = true;
                    }
                }
                if($key == 'H') {
                    $stateName = $cell->getValue();
                    if(!empty($stateName)) {
                        $conditions = [
                            ['ST_name', $stateName]
                        ];
                        $fields = '*';
                        $stateDetail = State::getState($conditions, $fields);
                        if(!empty($stateDetail)) {
                            $stateCode = $stateDetail->ST_code;
                        } else {
                            $stateCode = '';
                            $stateCodeError = true;
                        }
                    }
                }
                if($key == 'D') {
                    $concernedName = $cell->getValue();
                    $concernedName = (!empty($concernedName)?explode(' ', $concernedName, 2):NULL);
                    $firstName = (!empty($concernedName[0])?$concernedName[0]:'');
                    $lastName = (!empty($concernedName[1])?$concernedName[1]:'');
                }
                if(array_key_exists($key, $columnArr)) {
                    $column = $columnArr[$key];
                    $data[$column] = $cell->getValue();
                }
            }
            $data['AG_state'] = (!empty($stateCode)?$stateCode:'');
            $data['AG_firstName'] = (!empty($firstName)?$firstName:'');
            $data['AG_lastName'] = (!empty($lastName)?$lastName:'');
            if($isNotExistError || $stateCodeError) {
                $errorRow = $data;
                if($isNotExistError) {
                    $repeatedEntriesSheet->getStyle("A$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('FFFF0000');
                }
                if($stateCodeError) {
                    $repeatedEntriesSheet->getStyle("H$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('FFFF0000');
                }
                $repeatedEntriesSheet->setCellValue("A$i", $errorRow['AG_ImdCode']);
                $repeatedEntriesSheet->setCellValue("B$i", $errorRow['AG_ImdName']);
                $repeatedEntriesSheet->setCellValue("C$i", $errorRow['AG_companyName']);
                $repeatedEntriesSheet->setCellValue("D$i", $errorRow['AG_firstName']);
                $repeatedEntriesSheet->setCellValue("E$i", $errorRow['AG_mobile']);
                $repeatedEntriesSheet->setCellValue("F$i", $errorRow['AG_email']);
                $repeatedEntriesSheet->setCellValue("G$i", $errorRow['AG_address']);
                $repeatedEntriesSheet->setCellValue("H$i", $stateName);
                $repeatedEntriesSheet->setCellValue("I$i", $errorRow['AG_city']);
                $repeatedEntriesSheet->setCellValue("J$i", $errorRow['AG_zip']);
                $repeatedEntriesSheet->setCellValue("K$i", $errorRow['AG_pan']);
                $repeatedEntriesSheet->setCellValue("L$i", $errorRow['AG_paymentModeType']);
                $repeatedEntriesSheet->setCellValue("M$i", $errorRow['AG_accountType']);
                $repeatedEntriesSheet->setCellValue("N$i", $errorRow['AG_bankName']);
                $repeatedEntriesSheet->setCellValue("O$i", $errorRow['AG_branchName']);
                $repeatedEntriesSheet->setCellValue("P$i", $errorRow['AG_bankAddress']);
                $repeatedEntriesSheet->setCellValue("Q$i", $errorRow['AG_bankAccountNumber']);
                $repeatedEntriesSheet->setCellValue("R$i", $errorRow['AG_bankIfscCode']);
                $repeatedEntriesSheet->setCellValue("S$i", $errorRow['AG_MicrCode']);
                $i++;
                $error = true;
            } else {
                Agent::where('AG_ImdCode', $data['AG_ImdCode'])->update($data);
            }
        }       
        if($error) {
            $writer = new Xlsx($spreadsheet);
            $fileName = time() . '_Upload-Error-Report' . '.xlsx';
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save("MIS_report/$fileName");
        }
    }
}
