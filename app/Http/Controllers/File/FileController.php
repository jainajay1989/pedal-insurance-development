<?php
namespace App\Http\Controllers\File;

use Illuminate\Http\Request;
use Storage;

class FileController
{
    public function lead(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('lead')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'png')
            {
                return(response(Storage::disk('lead')->get($file))->header('Content-type','image/png'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpeg')
            {
                return(response(Storage::disk('lead')->get($file))->header('Content-type','image/jpg'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'pdf')
            {
                return(response(Storage::disk('lead')->get($file))->header('Content-type','application/pdf'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'docx')
            {
                return(response(Storage::disk('lead')->get($file))->header('Content-type','application/msword'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function motorInsuranceAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('motorinsurance')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'png')
            {
                return(response(Storage::disk('motorinsurance')->get($file))->header('Content-type','image/png'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpeg')
            {
                return(response(Storage::disk('motorinsurance')->get($file))->header('Content-type','image/jpg'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'pdf')
            {
                echo "fadsfadsf";exit;
                return(response(Storage::disk('motorinsurance')->get($file))->header('Content-type','application/pdf'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'docx')
            {
                return(response(Storage::disk('motorinsurance')->get($file))->header('Content-type','application/msword'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function healthInsuranceAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('healthinsurance')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'png')
            {
                return(response(Storage::disk('healthinsurance')->get($file))->header('Content-type','image/png'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpeg')
            {
                return(response(Storage::disk('healthinsurance')->get($file))->header('Content-type','image/jpg'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'pdf')
            {
                return(response(Storage::disk('healthinsurance')->get($file))->header('Content-type','application/pdf'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'docx')
            {
                return(response(Storage::disk('healthinsurance')->get($file))->header('Content-type','application/msword'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function healthInsurancePurposalAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('health_insurance_purpose')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'png')
            {
                return(response(Storage::disk('health_insurance_purpose')->get($file))->header('Content-type','image/png'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpeg')
            {
                return(response(Storage::disk('health_insurance_purpose')->get($file))->header('Content-type','image/jpg'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'pdf')
            {
                return(response(Storage::disk('health_insurance_purpose')->get($file))->header('Content-type','application/pdf'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'docx')
            {
                return(response(Storage::disk('health_insurance_purpose')->get($file))->header('Content-type','application/msword'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function policiesAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('policies')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'png')
            {
                return(response(Storage::disk('policies')->get($file))->header('Content-type','image/png'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'jpeg')
            {
                return(response(Storage::disk('policies')->get($file))->header('Content-type','image/jpg'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'pdf')
            {
                return(response(Storage::disk('policies')->get($file))->header('Content-type','application/pdf'));
            }
            elseif(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'docx')
            {
                return(response(Storage::disk('policies')->get($file))->header('Content-type','application/msword'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function quotesAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('quotes')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'pdf')
            {
                return(response(Storage::disk('quotes')->get($file))->header('Content-type','application/pdf'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }

    public function ledger(Request $request)
    {
        $image_name = $request['image'];
        if(Storage::disk('ledger')->has($image_name))
        {
            return(response(Storage::disk('ledger')->get($image_name))->header('Content-type','image/png'));
        }
        else
        {
            return(response(Storage::disk('ledger')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function vehicleUploadErrorAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('models_upload_error')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'xlsx')
            {
                return(response(Storage::disk('models_upload_error')->get($file))->header('Content-type','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
    
    public function vehicleUploadAttachment(Request $request)
    {
        $file = $request['file'];
        if(Storage::disk('models_list')->has($file))
        {
            if(strtolower(pathinfo($file, PATHINFO_EXTENSION)) == 'xlsx')
            {
                return(response(Storage::disk('models_list')->get($file))->header('Content-type','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            }
        }
        else
        {
            return(response(Storage::disk('lead')->get('no-image.jpg'))->header('Content-type','image/jpg'));
        }
    }
}
