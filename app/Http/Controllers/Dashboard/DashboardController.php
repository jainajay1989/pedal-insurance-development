<?php
namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'dashboard';
    }

    public function index()
    {
        return view('dashboard.index', $this->viewData);
    }
}
