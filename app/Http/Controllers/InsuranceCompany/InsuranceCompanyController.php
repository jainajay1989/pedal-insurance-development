<?php
namespace App\Http\Controllers\InsuranceCompany;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\InsuranceCompany;
use Auth;

class InsuranceCompanyController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'insurance-company';
    }

    /**
     * View companies.
     *
     * @return view
     */
    public function index()
    {
        $fn_status = true;
        $company_list = null;

        // Check Permission
        if(check_permission('INSURANCE_COMPANY_VIEW', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Get All Companies
        if($fn_status == true)
        {
            $return = InsuranceCompany::getAllCompanies();
            if($return['status'] == true)
            {
                $company_list = $return['data'];
            }
        }
        //------------------

        $this->viewData['company_list'] = $company_list;
        return view('insurance_company.index', $this->viewData);
    }

    /**
     * Add company.
     *
     * @return view
     */
    public function create()
    {
        // Check Permission
        if(check_permission('INSURANCE_COMPANY_CREATE', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        return view('insurance_company.create', $this->viewData);
    }

    /**
     * Edit company.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $fn_status = true;
        $company_id = null;
        $company_detail = null;

        // Check Permission
        if(check_permission('INSURANCE_COMPANY_VIEW', Auth::user()->A_type) == false && check_permission('INSURANCE_COMPANY_EDIT', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $company_id = intval($request['id']);
        //------------------------

        // Get Company Detail
        if($fn_status == true)
        {
            $return = InsuranceCompany::getCompanyDetail($company_id);
            if($return['status'] == true)
            {
                $company_detail = $return['data'];
            }
        }
        //-------------------

        $this->viewData['company_detail'] = $company_detail;
        return view('insurance_company.edit', $this->viewData);
    }

    /**
     * Add company.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $InsuranceCompany = new InsuranceCompany();
        $InsuranceCompany->field['IC_name'] = $request['name'];
        $InsuranceCompany->field['IC_active'] = $request['active'];
        $return = $InsuranceCompany->addCompany();
        if($return['status'] == true)
        {
            $company_detail = $return['data'];
            $redirect['url'] = url('insurance-company/edit', ['id' => $company_detail->IC_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('insurance-company/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update company detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $company_id = null;

        // Fetch Request Variables
        $company_id = $request['id'];
        //------------------------

        $InsuranceCompany = new InsuranceCompany();
        $InsuranceCompany->field['IC_id'] = $company_id;
        $InsuranceCompany->field['IC_name'] = $request['name'];
        $InsuranceCompany->field['IC_active'] = $request['active'];
        $return = $InsuranceCompany->updateCompanyDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('insurance-company/edit', ['id' => $company_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Delete company.
     *
     * @param Request $request
     * @return redirect
     */
    public function remove(Request $request)
    {
        $company_id = null;

        // Fetch Request Variables
        $company_id = $request['id'];
        //------------------------

        $InsuranceCompany = new InsuranceCompany();
        $InsuranceCompany->field['IC_id'] = $company_id;
        $return = $InsuranceCompany->deleteCompany();
        if($return['status'] == true)
        {
            $redirect['url'] = url('insurance-companies');
        }
        else
        {
            $redirect['url'] = url('insurance-company/edit', ['id' => $company_id]);
        }
        $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        return redirect($redirect['url'])->with($redirect['message']);
    }
}
