<?php
namespace App\Http\Controllers\InsuranceProduct;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\InsuranceProduct;
use Auth;

class InsuranceProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->viewData['page_name'] = 'insurance-product';
    }

    /**
     * View products.
     *
     * @param Request $request
     * @return view
     */
    public function index(Request $request)
    {
        $fn_status = true;
        $policy_type_list = [];
        $product_list = null;
        $status = null;

        // Fetch Request Variables
        $policy_type = $request['policy_type'];
        $status = intval(($request['status']==''?1:$request['status']));
        //------------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            $policy_type_list[''] = 'Policy Type';
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        // Get All Products
        if($fn_status == true)
        {
            $where = [];
            if($policy_type)
            {
                $where['IP_policyType'] = $policy_type;
            }
            $where['IP_active'] = $status;
            $return = InsuranceProduct::getAllProducts($where);
            if($return['status'] == true)
            {
                $product_list = $return['data'];
            }
        }
        //-----------------

        $this->viewData['status'] = $status;
        $this->viewData['policy_type'] = $policy_type;
        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['product_list'] = $product_list;
        return view('insurance_product.index', $this->viewData);
    }

    /**
     * Add product.
     *
     * @return view
     */
    public function create()
    {
        $fn_status = true;
        $policy_type_list = [];

        // Check Permission
        if(check_permission('INSURANCE_PRODUCT_CREATE', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Get Policy Type List
        if($fn_status == true)
        {
            $policy_type_list[''] = 'Select';
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        $this->viewData['policy_type_list'] = $policy_type_list;
        return view('insurance_product.create', $this->viewData);
    }

    /**
     * Edit product.
     *
     * @param Request $request
     * @return view
     */
    public function edit(Request $request)
    {
        $fn_status = true;
        $product_id = null;
        $policy_type_list = [];
        $product_detail = null;

        // Check Permission
        if(check_permission('INSURANCE_PRODUCT_VIEW', Auth::user()->A_type) == false && check_permission('INSURANCE_PRODUCT_EDIT', Auth::user()->A_type) == false)
        {
            return redirect(url());
        }
        //-----------------

        // Fetch Request Variables
        $product_id = intval($request['id']);
        //------------------------

        // Get Policy Type List
        if($fn_status == true)
        {
            $policy_type_list[''] = 'Select';
            foreach(config('constant.policy_type') as $type)
            {
                $policy_type_list[$type['value']] = $type['caption'];
            }
        }
        //---------------------

        // Get Product Detail
        if($fn_status == true)
        {
            $return = InsuranceProduct::getProductDetail($product_id);
            if($return['status'] == true)
            {
                $product_detail = $return['data'];
            }
        }
        //-------------------

        $this->viewData['policy_type_list'] = $policy_type_list;
        $this->viewData['product_detail'] = $product_detail;
        return view('insurance_product.edit', $this->viewData);
    }

    /**
     * Add product.
     *
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $redirect = null;

        $InsuranceProduct = new InsuranceProduct();
        $InsuranceProduct->field['IP_policyType'] = $request['policy_type'];
        $InsuranceProduct->field['IP_name'] = $request['name'];
        $InsuranceProduct->field['IP_active'] = $request['active'];
        $return = $InsuranceProduct->addProduct();
        if($return['status'] == true)
        {
            $product_detail = $return['data'];
            $redirect['url'] = url('insurance-product/edit', ['id' => $product_detail->IP_id]);
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }
        else
        {
            $redirect['url'] = url('insurance-product/add');
            $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        }

        return redirect($redirect['url'])->with($redirect['message']);
    }

    /**
     * Update product detail.
     *
     * @param Request $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $product_id = null;

        // Fetch Request Variables
        $product_id = $request['id'];
        //------------------------

        $InsuranceProduct = new InsuranceProduct();
        $InsuranceProduct->field['IP_id'] = $product_id;
        $InsuranceProduct->field['IP_policyType'] = $request['policy_type'];
        $InsuranceProduct->field['IP_name'] = $request['name'];
        $InsuranceProduct->field['IP_active'] = $request['active'];
        $return = $InsuranceProduct->updateProductDetail();
        $status = ($return['status']==true?'success':'error');
        return redirect(url('insurance-product/edit', ['id' => $product_id]))->with(['msg' => $return['message'], 'type' => $status]);
    }

    /**
     * Delete product.
     *
     * @param Request $request
     * @return redirect
     */
    public function remove(Request $request)
    {
        $product_id = null;

        // Fetch Request Variables
        $product_id = $request['id'];
        //------------------------

        $InsuranceProduct = new InsuranceProduct();
        $InsuranceProduct->field['IP_id'] = $product_id;
        $InsuranceProduct->field['IP_policyType'] = $request['policy_type'];
        $InsuranceProduct->field['IP_name'] = $request['name'];
        $InsuranceProduct->field['IP_active'] = $request['active'];
        $return = $InsuranceProduct->deleteProduct();
        if($return['status'] == true)
        {
            $redirect['url'] = url('insurance-products');
        }
        else
        {
            $redirect['url'] = url('insurance-product/edit', ['id' => $product_id]);
        }
        $redirect['message'] = ['msg' => $return['message'], 'type' => $return['status']==true?'success':'error'];
        return redirect($redirect['url'])->with($redirect['message']);
    }
}
