<?php

namespace App\Http\Middleware\API;

use Closure, Carbon;;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('App-Token') !== config('app.app_token'))
        {
            $response['status'] = 0;
            $response['code'] = 'E100';
            $response['message'] = 'Invalid token';
            $response['data'] = null;
            $response['timestamp'] = Carbon\Carbon::now()->toDateTimeString();
            return response()->json($response);
        }
        return $next($request);
    }
}
