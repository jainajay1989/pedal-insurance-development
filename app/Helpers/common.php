<?php

use App\Http\Models\Ledger;
use Illuminate\Support\Facades\Mail;

if (!function_exists('format_date')) {

    /**
     * Format Date/Time
     *
     * @param $date
     * @param $from_format
     * @param $to_format
     * @return string
     */
    function format_date($date, $from_format = 'Y-m-d H:i:s', $to_format = 'M d, Y|g:i a') {
        $return_date = null;
        if ($date) {
            $return_date = \Carbon\Carbon::createFromFormat($from_format, $date)->format($to_format);
        }
        return($return_date);
    }

}

if (!function_exists('check_permission')) {

    /**
     * Check Permission
     *
     * @param $permission
     * @param $user_type
     * @return boolean
     */
    function check_permission($permission, $user_type) {
        $permission_array['ADMIN'] = ['DASHBOARD',
            'MESSAGE',
            'USER_VIEW', 'USER_CREATE', 'USER_EDIT',
            'AGENT_VIEW', 'AGENT_CREATE', 'AGENT_EDIT',
            'LEAD_VIEW', 'LEAD_EDIT',
            'LEDGER_VIEW', 'LEDGER_EDIT',
            'COMMISSION_PAYOUT_VIEW', 'COMMISSION_PAYOUT_CREATE', 'COMMISSION_PAYOUT_EDIT',
            'INSURANCE_COMPANY_VIEW', 'INSURANCE_COMPANY_CREATE', 'INSURANCE_COMPANY_EDIT',
            'INSURANCE_PRODUCT_VIEW', 'INSURANCE_PRODUCT_CREATE', 'INSURANCE_PRODUCT_EDIT',
            'CMS_VIEW', 'CMS_CREATE', 'CMS_EDIT',
            'VEHICLE_UPLOAD_LIST', 'VEHICLE_UPLOAD'];

        $permission_array['MANAGER'] = ['DASHBOARD',
            'MESSAGE',
            'AGENT_VIEW',
            'LEAD_VIEW', 'LEAD_EDIT', 'LEAD_MESSAGE_VIEW', 'LEAD_MESSAGE_SEND'];

        $permission_array['EXECUTIVE'] = ['DASHBOARD',
            'MESSAGE',
            'AGENT_VIEW', 'AGENT_CREATE', 'AGENT_EDIT',
            'LEDGER_VIEW',
            'VEHICLE_UPLOAD_LIST', 'VEHICLE_UPLOAD',
            'LEAD_VIEW', 'LEAD_EDIT', 'LEAD_MESSAGE_VIEW', 'LEAD_MESSAGE_SEND'];


        return(in_array($permission, $permission_array[$user_type]));
    }

}

if (!function_exists('get_listbox_array')) {

    /**
     * Get Listbox array
     *
     * @param $list
     * @return array
     */
    function get_listbox_array($list) {
        $return_list = [];

        foreach ($list as $item) {
            $return_list[$item['value']] = $item['caption'];
        }

        return($return_list);
    }

}

if (!function_exists('sendRequest')) {

    /**
     * Get Listbox array
     *
     * @param $list
     * @return array
     */
    function restRequest($requestMethod = NULL, $requestPath = NULL, $queryData = NULL, $postFields = array()) {
        $arrResp = array();
        try {

            if (!function_exists('curl_init')) {
                throw new Exception('Sorry cURL is not installed!');
            } else {
                // Host URL
                $apiUrl = config('constant.API_ENDPOINT');
                $requestData = (!empty($postFields) ? json_encode($postFields) : '');
                $curlHandle = curl_init();
                $curlUrl = $apiUrl . $requestPath . '?' . $queryData;
                curl_setopt($curlHandle, CURLOPT_URL, $curlUrl);
                curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);

                if (!empty($postFields)) {
                    curl_setopt($curlHandle, CURLOPT_POST, 1);
                    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $requestData);
                }

                $contents = curl_exec($curlHandle);
                $headers = curl_getinfo($curlHandle);
                curl_close($curlHandle);
                $arrResp = json_decode($contents, true);
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit;
        }
        return $arrResp;
    }

}

function calculateCommision($lead_detail = NULL) {
    $accountBalanceCheck = true;
    if($lead_detail->policy_type == 'MOTOR') {
        $commission_premium = (!empty($lead_detail->new_policy_od_premium) && $lead_detail->new_policy_od_premium != 0.00?$lead_detail->new_policy_od_premium:$lead_detail->new_policy_tp_premium);
    } else {
        $commission_premium = $lead_detail->gross_premium;
    }
    if($lead_detail->paid_by_whom == 'PAID_BY_DEALER') {
        $accountBalanceCheck = checkAgentAccountBalance($commission_premium, $lead_detail->agent_id);
    }
    
    if($accountBalanceCheck) {
        // Fetch Agent's Payouts
        $fn_status = true;
        if ($fn_status == true) {
            if ($lead_detail->status == config('constant.lead_status.COMPLETED.value')) {
                $return_commission = App\Http\Models\CommissionPayout::getAgentPayoutDetail($lead_detail->agent_id, $lead_detail->new_policy_company, $lead_detail->new_policy_product);
                $fn_status = $return_commission['status'];
                if ($fn_status == true) {
                    $payout_detail = $return_commission['data'];
                    if ($payout_detail == null) {
                        $fn_status = false;
                    }
                }
            } else {
                $fn_status = false;
            }
        }
//        print_r($lead_detail);exit;
//        ECHO $lead_detail->status;
//        echo $lead_detail->agent_id.'|'.$lead_detail->new_policy_company.'|'.$lead_detail->new_policy_product;
//        var_dump($fn_status);exit;
        //----------------------
        // Update Transaction
        if ($fn_status == true) {
            if($lead_detail->paid_by_whom == 'PAID_BY_DEALER') {
                $return_transaction = Ledger::getLeadTransactionDetail(['LG_Lid' => $lead_detail->lead_id, 'LG_type' => config('constant.transaction_type.PAIDFORPOLICY.value')]);
                if ($return_transaction['data'] == null) {
                    $Ledger = new Ledger();
                    $Ledger->field['LG_Lid'] = $lead_detail->lead_id;
                    $Ledger->field['LG_AGid'] = $lead_detail->agent_id;
                    $Ledger->field['LG_type'] = config('constant.transaction_type.PAIDFORPOLICY.value');
                    $Ledger->field['LG_message'] = config('constant.transaction_type.PAIDFORPOLICY.message');
                    $Ledger->field['LG_amount'] = $commission_premium;
                    $Ledger->field['LG_note'] = 'Payment paid on lead ' . $lead_detail->lead_id;
                    $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                    $Ledger->addTransaction();
                } else {
                    $transaction_detail = $return_transaction['data'];
                    $Ledger = new Ledger();
                    $Ledger->field['LG_id'] = $transaction_detail->LG_id;
                    $Ledger->field['LG_Lid'] = $lead_detail->lead_id;
                    $Ledger->field['LG_AGid'] = $lead_detail->agent_id;
                    $Ledger->field['LG_type'] = config('constant.transaction_type.PAIDFORPOLICY.value');
                    $Ledger->field['LG_message'] = config('constant.transaction_type.PAIDFORPOLICY.message');
                    $Ledger->field['LG_amount'] = $commission_premium;
                    $Ledger->field['LG_note'] = 'Payment paid on lead ' . $lead_detail->lead_id;
                    $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                    $Ledger->updateTransactionDetail();
                }
            }
            
            $return_transaction = Ledger::getLeadTransactionDetail(['LG_Lid' => $lead_detail->lead_id, 'LG_type' => config('constant.transaction_type.COMMISSION.value')]);
            if($return_transaction['data'] == null)
            {
                $Ledger = new Ledger();
                $Ledger->field['LG_Lid'] = $lead_detail->lead_id;
                $Ledger->field['LG_AGid'] = $lead_detail->agent_id;
                $Ledger->field['LG_type'] = config('constant.transaction_type.COMMISSION.value');
                $Ledger->field['LG_message'] = config('constant.transaction_type.COMMISSION.message');
                $Ledger->field['LG_amount'] = ($commission_premium*$payout_detail->CP_payout)/100;
                $Ledger->field['LG_note'] = 'Commission earned on lead '.$lead_detail->lead_id;
                $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                $Ledger->addTransaction();
            }
            else
            {
                $transaction_detail = $return_transaction['data'];
                $Ledger = new Ledger();
                $Ledger->field['LG_id'] = $transaction_detail->LG_id;
                $Ledger->field['LG_Lid'] = $lead_detail->lead_id;
                $Ledger->field['LG_AGid'] = $lead_detail->agent_id;
                $Ledger->field['LG_type'] = config('constant.transaction_type.COMMISSION.value');
                $Ledger->field['LG_message'] = config('constant.transaction_type.COMMISSION.message');
                $Ledger->field['LG_amount'] = ($commission_premium*$payout_detail->CP_payout)/100;
                $Ledger->field['LG_note'] = 'Commission earned on lead '.$lead_detail->lead_id;
                $Ledger->field['LG_status'] = config('constant.transaction_status.APPROVED.value');
                $Ledger->updateTransactionDetail();
            }
            $fn_status = true;
            $message = "Commision added successfully";
        } else {
            $fn_status = false;
            $message = "Please declare commision of agent for insurance company and insurance product";
        }
        //-------------------
    } else {
        $fn_status = false;
        $message = "Insufficient balance in Agent's wallet";
    }
    $return['status'] = $fn_status;
    $return['message'] = $message;
    return $return;
}

function checkAgentAccountBalance($premium = 0.00, $agentId = 0) {
    $return = Ledger::getAgentAccountBalance($agentId);
    if($return['status'] == true) {
        if($return['data']->LG_balance < $premium) {
            return false;
        } else{
            return true;
        }
    }
}

function sendPolicyEmailToCustomer($customerData = array()) {
    try{
        $email = $customerData['customer_email'];
        $name = $customerData['customer_name'];
        $fileName = $customerData['attachment'];
        Mail::send('emails.customer_policy', $customerData, function($message) use ($email, $name, $fileName)
        {
            $message->subject('Policy');
            $message->from('noreply@flashapp.com', 'Flash App');
            $message->to($email);
    //        $message->bcc($bccEmails);
            $message->attach("$fileName");
        });
        $return['status'] = true;
        $return['message'] = 'Policy email successfully';
        return $return;
    } catch(\Exception $e) {
        $return['status'] = false;
        $return['message'] = $e->getMessage();
        return $return;
    }
}
