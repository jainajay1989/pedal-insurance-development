var User = function(){

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            User.handleFormValidation();
            User.handleStateCityChange();
            User.handleUserFormSubmit();
            User.handleUppercase();
        },

        // =========================================================================
        // FORM VALIDATION
        // =========================================================================
        handleFormValidation: function () {
            $("#user_form").validate({
                rules:
                {
                    confirm_password:
                    {
                        equalTo: "#password"
                    }
                },
                highlight:function(element) {
                    $(element).closest('.control-group').addClass('has-error has-feedback');
                },
                unhighlight: function(element) {
                    $(element).closest('.control-group').removeClass('has-error has-feedback');
                }
            });
        },

        // =========================================================================
        // CHANGE STATE CITY
        // =========================================================================
        handleStateCityChange: function () {
            $("#user_form #state").change(function() {
                var state = $(this).val();
                var html = "";
                html += "<option value>Select</option>";
                if(state)
                {
                    $("#city_source option."+state).each(function()
                    {
                        html += "<option value='"+$(this).attr("value")+"'>"+$(this).text()+"</option>";
                    });
                    $("#user_form #city").html(html).trigger("chosen:updated");
                }
            });
            $("#user_form #state").change();
            $("#user_form #city").val($("#user_form #city").data("city")).trigger("chosen:updated");
        },

        // =========================================================================
        // USER FORM SUBMISSION
        // =========================================================================
        handleUserFormSubmit: function() {
            $("#user_form .btn-submit").click(function() {
                if($("#user_form").valid())
                {
                    $("#user_form .form-footer .btn").attr("disabled", "disabled");
                    $(this).text($(this).data("text"));
                    $("#user_form").attr("action", $(this).data("action")).submit();
                }
            });
            $("#user_form .btn-delete").click(function() {
                $("#user_form").validate().cancelSubmit = true;
                $("#user_form .form-footer .btn").attr("disabled", "disabled");
                $(this).text($(this).data("text"));
                $("#user_form").attr("action", $(this).data("action")).submit();
            });
        },

        // =========================================================================
        // UPPER CASE
        // =========================================================================
        handleUppercase: function() {
            $(".form-control.uc").change(function(){
                $(this).val($(this).val().toUpperCase());
            });
        }
    };
}();

// Call main app init
User.init();
