var Lead = function(){

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            Lead.handleFormValidation();
            Lead.handleShowMessages();
            Lead.handleFocusSelection();
            Lead.handleStateCityChange();
            Lead.handlePremiumTotal();
            Lead.handleSetNewPolicyEndDate();
            Lead.handleLeadStatusChange();
            Lead.handleLeadFormSubmit();
            Lead.handleUppercase();
            Lead.handleInsurancePremiumCalculation();
            Lead.handleDealerWalletBalance();
            Lead.addQuotationUploader();
            Lead.getMotorVehicleMake();
            Lead.getMotorVehicleMakeModel();
        },
        
        handleInsurancePremiumCalculation: function() {
            $("#net_premium").blur(function() {
                var net_premium = $("#net_premium").val();
                var gst = (net_premium*18/100);
                net_premium = ((net_premium == '')?0:net_premium);
                gst = ((gst == '')?0:gst);
                var total_premium = parseFloat(net_premium) + parseFloat(gst);
                
                var val = $('#paid_by_whom').val();
                if(val === 'PAID_BY_DEALER') {
                    var wallet_balance = $('#wallet-balance').text();
                    if(wallet_balance < total_premium) {
                        $("#gross_premium").css('border','1px solid red');
                        $("#infoTxt").text("Dealer Wallet balance is less than pmemium amount. Please recharge dealer wallet.");
                        $("#gst").val(0);
                        $("#gross_premium").val(0);
                    } else {
                        $("#gross_premium").css('border','1px solid #DDD');
                        $("#infoTxt").text("");
                        $("#gst").val(parseFloat(gst).toFixed(2));
                        $("#gross_premium").val(parseFloat(total_premium).toFixed(2));
                    }
                } else {
                    $("#gross_premium").css('border','1px solid #DDD');
                    $("#infoTxt").text("");
                    $("#gst").val(parseFloat(gst).toFixed(2));
                    $("#gross_premium").val(parseFloat(total_premium).toFixed(2));
                }
            });
        },

        // =========================================================================
        // FORM VALIDATION
        // =========================================================================
        handleFormValidation: function () {
            $("#lead_form").validate({
                highlight:function(element) {
                    $(element).closest('.control-group').addClass('has-error has-feedback');
                },
                unhighlight: function(element) {
                    $(element).closest('.control-group').removeClass('has-error has-feedback');
                }
            });
        },

        // =========================================================================
        // OPEN MESSAGE MODAL
        // =========================================================================
        handleShowMessages: function () {
            var $ajax_modal   = $("#ajax_modal");
            var $btn_edit;
            $(".panel-body").on("click", ".btn-view-messages", function()
            {
                $btn_edit = $(this);
                var target = $(this).data("target");
                
                $ajax_modal.modal({backdrop: 'static', show: true});
                $ajax_modal.load(target, function()
                {
                    $ajax_modal.find(".modal-body").animate({ scrollTop: $ajax_modal.find(".message-list").height() }, 0);
                    Lead.handleSendMessages($ajax_modal);
                    //Lead.handleRefreshMessages($ajax_modal);
                });

                return(false);
            });

            // Close Modal
            $ajax_modal.on('hidden.bs.modal', function ()
            {
                Lead.handleResetModal($(this), 'md');
            });
            //--------------------
        },

        // =========================================================================
        // SEND MESSAGE
        // =========================================================================
        handleSendMessages: function ($modal) {

            $modal.on("click", ".btn-send-message", function()
            {
                var $this = $(this);
                $.ajax({
                    type: $this.closest("form").attr("method"),
                    url: $this.closest("form").attr("action"),
                    data: $this.closest('form').serialize(),
                    beforeSend: function()
                    {
                        $this.attr("disabled", true);
                        //$("form.workflow-form .btn-submit").addClass("loading").text("Saving...").prop("disabled", true);
                    },
                    success: function(Return)
                    {
                        var html = "";

                        html += "<div class='media inner-all no-margin message' data-message_id='"+Return['data']['MG_id']+"'>";
                        html += "<div class='pull-right'>";
                        html += "<img src='images/default-user-image.png' class='img-circle' alt='' width='60' />";
                        html += "</div>";
                        html += "<div class='media-body text-right'>";
                        html += "<span class='h4'>Me</span>";
                        html += "<small class='block'>"+Return['data']['MG_message']+"</small>";
                        html += "<em class='text-xs text-muted'>"+Return['data']['MG_formattedCreatedAt']+"</em>";
                        html += "</div>";
                        html += "</div>";
                        $modal.find('.modal-body .message-list').append(html);
                        $modal.find(".modal-body").animate({ scrollTop: $modal.find(".message-list").height() }, 1000);
                        $this.closest('form').find('#message').val('');
                    },
                    error: function(Error)
                    {
                        // Show error message
                        //App.handleShowMessage(Error['statusText'], 'error');
                        ///-------------------
                    },
                    complete: function()
                    {
                        $this.removeAttr("disabled");
                        //$("form.workflow-form .btn-submit").removeClass("loading").text("Save").removeAttr("disabled");
                    }
                });

                return(false);
            });

            $modal.on('keyup keypress', '#message', function(e)
            {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    $(this).parent().find('.btn-send-message').click();
                    return(false);
                }
            });
        },

        // =========================================================================
        // REFRESH MESSAGES
        // =========================================================================
        handleRefreshMessages: function ($modal) {

            var message_id = $modal.find(".message-list .message:last").data("message_id");
            if(message_id != undefined)
            {
                $.ajax({
                    type: $modal.find("form").attr("method"),
                    url: $modal.find("form").data("refresh_url"),
                    data: {"message_id": message_id, "lead_id": $modal.find("form #lead_id").val(), "_token": $modal.find("form input[name=_token]").val()},
                    success: function(Return)
                    {
                        var html = "";
                        if(Return.status == 1 && Return['data'].length > 0)
                        {
                            for(var m=0;m<Return['data'].length;m++)
                            {
                                html += "<div class='media inner-all no-margin message' data-message_id='"+Return['data'][m]['MG_id']+"'>";
                                html += "<div class='pull-left'>";
                                html += "<img src='images/default-user-image.png' alt='' width='60' />";
                                html += "</div>";
                                html += "<div class='media-body'>";
                                html += "<span class='h4'>"+Return['data'][m]['MG_agent']+"</span>";
                                html += "<small class='block'>"+Return['data'][m]['MG_message']+"</small>";
                                html += "<em class='text-xs text-muted'>"+Return['data'][m]['MG_createdAt']+"</em>";
                                html += "</div>";
                                html += "</div>";
                            }
                            $modal.find('.modal-body .message-list').append(html);
                            $modal.find(".modal-body").animate({ scrollTop: $modal.find(".message-list").height() }, 1000);
                        }
                    },
                    complete: function()
                    {
                        setTimeout(function(){
                            Lead.handleRefreshMessages($modal);
                        }, 5000);
                    }
                });

            }
        },

        // =========================================================================
        // FOCUS INPUT SELECTION
        // =========================================================================
        handleFocusSelection: function () {
            $(".focus-select").focus(function() {
                $(this).select();
            });
        },

        // =========================================================================
        // CHANGE STATE CITY
        // =========================================================================
        handleStateCityChange: function () {
//            $("#lead_form #customer_state").change(function() {
//                var state = $(this).val();
//                var html = "";
//                html += "<option value>Select</option>";
//                if(state)
//                {
//                    $("#city_source option."+state).each(function()
//                    {
//                        html += "<option value='"+$(this).attr("value")+"'>"+$(this).text()+"</option>";
//                    });
//                    $("#lead_form #customer_city").html(html).trigger("chosen:updated");
//                }
//            });
//            $("#lead_form #customer_state").change();
//            $("#lead_form #customer_city").val($("#lead_form #customer_city").data("city")).trigger("chosen:updated");
            
            $("#lead_form #customer_state").change(function() {
                var value = $('#lead_form #customer_state').val();
                $.ajax({
                    url: '/api/get-cities',
                    data: 'state_code='+value,
                    type: 'post',
                    dataType: 'json',
                    success: function(result) {
                        var option = `<option value="">Select an option</option>`;;
                        $.each(result.data.data, function(index, element) {
                            option += `<option value="${element.CT_id}">${element.CT_name}</option>`;
                        });
                        console.log(option);
                        $('#customer_city').html(option).trigger("chosen:updated");;
                    }
                });
            });
        },
        
        // =========================================================================
        // PREMIUM TOTAL
        // =========================================================================
        handlePremiumTotal: function () {
            $("#new_policy_od_premium, #new_policy_tp_premium").blur(function() {
                var tax_premium = Lead.handleTaxTotal();
                var od_premium = $("#new_policy_od_premium").val();
                var tp_premium = $("#new_policy_tp_premium").val();
                od_premium = ((od_premium == '')?0:od_premium);
                tp_premium = ((tp_premium == '')?0:tp_premium);
                tax_premium = ((tax_premium == '')?0:tax_premium);
                var total_premium = parseFloat(od_premium) + parseFloat(tp_premium) + parseFloat(tax_premium);
                
                var val = $('#paid_by_whom').val();
                if(val === 'PAID_BY_DEALER') {
                    var wallet_balance = $('#wallet-balance').text();
                    if(wallet_balance < total_premium) {
                        $("#new_policy_total_premium").css('border','1px solid red');
                        $("#infoTxt").text("Dealer Wallet balance is less than pmemium amount. Please recharge dealer wallet.");
                        $("#new_policy_tax_premium").val(0);
                        $("#new_policy_total_premium").val(0);
                    } else {
                        $("#new_policy_total_premium").css('border','1px solid #DDD');
                        $("#infoTxt").text("");
                        $("#new_policy_tax_premium").val(parseFloat(tax_premium).toFixed(2));
                        $("#new_policy_total_premium").val(parseFloat(total_premium).toFixed(2));
                    }
                } else {
                    $("#new_policy_total_premium").css('border','1px solid #DDD');
                    $("#infoTxt").text("");
                    $("#new_policy_tax_premium").val(parseFloat(tax_premium).toFixed(2));
                    $("#new_policy_total_premium").val(parseFloat(total_premium).toFixed(2));
                }
            });
        },
        
        handleTaxTotal: function () {
            var od_premium = $("#new_policy_od_premium").val();
            var tp_premium = $("#new_policy_tp_premium").val();
            od_premium = ((od_premium == '')?0:od_premium);
            tp_premium = ((tp_premium == '')?0:tp_premium);
            var tax_premium = parseFloat(od_premium) + parseFloat(tp_premium);
            var tax = (tax_premium*18)/100;
            return tax;
        },
        
        handleDealerWalletBalance: function() {
            $('#paid_by_whom').on('change', function() {
                var val = $(this).val();
                if(val === 'PAID_BY_DEALER') {
                    var wallet_balance = parseFloat($('#wallet-balance').text());
                    var total_premium = parseFloat($("#new_policy_total_premium").val());
                    if(wallet_balance < total_premium) {
                        alert("Dealer Wallet balance is less than pmemium amount. Please recharge dealer wallet.");
                    }
                }
            });
        },
        
        // =========================================================================
        // SET NEW POLICY END DATE
        // =========================================================================
        handleSetNewPolicyEndDate: function () {
            $("#new_policy_start_date").datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
            }).on('changeDate', function(ev) {
                var start_date = $(this).data("datepicker").getDate();
                var end_date = new Date(start_date.getFullYear()+1, start_date.getMonth(), start_date.getDate());
                end_date.setDate(end_date.getDate()-1);
                $("#new_policy_end_date").datepicker('setDate', end_date);
                $("#new_policy_end_date")[0].focus();
            });
        },

        // =========================================================================
        // RESET MODAL
        // =========================================================================
        handleResetModal: function ($modal, size) {
            var html = "";
            html += "<div class='modal-dialog modal-" + size + "'>";
                html += "<div class='modal-content'>";
                    html += "<div class='modal-body'>";
                        html += "<h1 class='text-center mb-20'>Loading...</h1>";
                    html += "</div>";
                html += "</div>";
            html += "</div>";
            $modal.html(html);
        },

        // =========================================================================
        // CHANGE LEAD STATUS
        // =========================================================================
        handleLeadStatusChange: function() {
            $("#lead_form #lead_status").change(function() {
                if($(this).val() == 21) {
                    $("#lead_form #new_policy_number, #lead_form #new_policy_start_date, #lead_form #new_policy_end_date, #lead_form #new_policy_attachment, #lead_form #new_policy_tp_premium, #lead_form #paid_by_whom").addClass('required');
                    $("#lead_form .new-policy-attachment-container").show();
                }
                else
                {
                    $("#lead_form #new_policy_number, #lead_form #new_policy_start_date, #lead_form #new_policy_end_date, #lead_form #new_policy_attachment, #lead_form #new_policy_tp_premium, #lead_form #paid_by_whom").removeClass('required');
                    $("#lead_form .new-policy-attachment-container").hide();
                }
            });
        },

        // =========================================================================
        // LEAD FORM SUBMISSION
        // =========================================================================
        handleLeadFormSubmit: function() {
            $("#lead_form .btn-submit").click(function() {
                if($("#lead_form").valid()) {
                    $("#lead_form").submit();
                }
            });
        },

        // =========================================================================
        // UPPER CASE
        // =========================================================================
        handleUppercase: function() {
            $(".form-control.uc").change(function(){
                $(this).val($(this).val().toUpperCase());
            });
        },
        
        // ==========================================================================
        // Add Quote Uploader
        // ==========================================================================
        addQuotationUploader: function() {            
            $('.add-quotation-upload').click(function() {
                var html = '<div class="col-md-12 new-policy-attachment-container"><br/>'+
                                '<div class="control-group">'+
                                    '<div class="fileinput-new input-group" data-provides="fileinput">'+
                                        '<div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>'+
                                        '<span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="quotations[]" id="new_policy_attachment"></span>'+
                                        '<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
//                $( ".quotes:first" ).clone().appendTo(".quote-div");
                $('.quote-div').append(html);
            });
        },
        
        getMotorVehicleMake: function() {            
            $('#vehicle_type').change(function() {
                var value = $('#vehicle_type option:selected').html();
                $.ajax({
                    url: '/api/get-make',
                    data: 'vehicle_type='+value,
                    type: 'post',
                    dataType: 'json',
                    success: function(result) {
                        var option = `<option value="">Select an option</option>`;;
                        $.each(result.data, function(index, element) {
                            option += `<option value="${element.id}">${element.make}</option>`;
                        });
                        $('#vehicle_make').html(option);
                        $('#vehicle_model').html('');
                    }
                });
            });
        },
        
        getMotorVehicleMakeModel: function() {            
            $('#vehicle_make').change(function() {
                var value = $('#vehicle_make').val();
                $.ajax({
                    url: '/api/get-make-model',
                    data: 'vehicle_make='+value,
                    type: 'post',
                    dataType: 'json',
                    success: function(result) {
                        var option = `<option value="">Select an option</option>`;;
                        $.each(result.data, function(index, element) {
                            option += `<option value="${element.id}">${element.model}</option>`;
                        });
                        $('#vehicle_model').html(option);
                    }
                });
            });
        }
    };
}();

// Call main app init
Lead.init();
