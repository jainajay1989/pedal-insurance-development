var InsuranceProduct = function(){

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            InsuranceProduct.handleFormValidation();
            InsuranceProduct.handleInsuranceProductFormSubmit();
            InsuranceProduct.handleUppercase();
        },

        // =========================================================================
        // FORM VALIDATION
        // =========================================================================
        handleFormValidation: function () {
            $("#insurance_product_form").validate({
                highlight:function(element) {
                    $(element).closest('.control-group').addClass('has-error has-feedback');
                },
                unhighlight: function(element) {
                    $(element).closest('.control-group').removeClass('has-error has-feedback');
                }
            });
        },

        // =========================================================================
        // INSURANCE PRODUCT FORM SUBMISSION
        // =========================================================================
        handleInsuranceProductFormSubmit: function() {
            $("#insurance_product_form .btn-submit").click(function() {
                if($("#insurance_product_form").valid())
                {
                    $("#insurance_product_form .form-footer .btn").attr("disabled", "disabled");
                    $(this).text($(this).data("text"));
                    $("#insurance_product_form").attr("action", $(this).data("action")).submit();
                }
            });
            $("#insurance_product_form .btn-delete").click(function() {
                $("#insurance_product_form").validate().cancelSubmit = true;
                $("#insurance_product_form .form-footer .btn").attr("disabled", "disabled");
                $(this).text($(this).data("text"));
                $("#insurance_product_form").attr("action", $(this).data("action")).submit();
            });
        },

        // =========================================================================
        // UPPER CASE
        // =========================================================================
        handleUppercase: function() {
            $(".form-control.uc").change(function(){
                $(this).val($(this).val().toUpperCase());
            });
        }
    };
}();

// Call main app init
InsuranceProduct.init();
