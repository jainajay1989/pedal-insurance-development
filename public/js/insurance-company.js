var InsuranceCompany = function(){

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            InsuranceCompany.handleFormValidation();
            InsuranceCompany.handleInsuranceCompanyFormSubmit();
            InsuranceCompany.handleUppercase();
        },

        // =========================================================================
        // FORM VALIDATION
        // =========================================================================
        handleFormValidation: function () {
            $("#insurance_company_form").validate({
                highlight:function(element) {
                    $(element).closest('.control-group').addClass('has-error has-feedback');
                },
                unhighlight: function(element) {
                    $(element).closest('.control-group').removeClass('has-error has-feedback');
                }
            });
        },

        // =========================================================================
        // INSURANCE COMPANY FORM SUBMISSION
        // =========================================================================
        handleInsuranceCompanyFormSubmit: function() {
            $("#insurance_company_form .btn-submit").click(function() {
                if($("#insurance_company_form").valid())
                {
                    $("#insurance_company_form .form-footer .btn").attr("disabled", "disabled");
                    $(this).text($(this).data("text"));
                    $("#insurance_company_form").attr("action", $(this).data("action")).submit();
                }
            });
            $("#insurance_company_form .btn-delete").click(function() {
                $("#insurance_company_form").validate().cancelSubmit = true;
                $("#insurance_company_form .form-footer .btn").attr("disabled", "disabled");
                $(this).text($(this).data("text"));
                $("#insurance_company_form").attr("action", $(this).data("action")).submit();
            });
        },

        // =========================================================================
        // UPPER CASE
        // =========================================================================
        handleUppercase: function() {
            $(".form-control.uc").change(function(){
                $(this).val($(this).val().toUpperCase());
            });
        }
    };
}();

// Call main app init
InsuranceCompany.init();
