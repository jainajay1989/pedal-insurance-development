var Agent = function(){

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            Agent.handleFormValidation();
            Agent.handleStateCityChange();
            Agent.handleAgentFormSubmit();
            Agent.handleUppercase();
        },

        // =========================================================================
        // FORM VALIDATION
        // =========================================================================
        handleFormValidation: function () {
            $("#agent_form").validate({
                rules:
                {
                    confirm_password:
                        {
                            equalTo: "#password"
                        }
                },
                highlight:function(element) {
                    $(element).closest('.control-group').addClass('has-error has-feedback');
                },
                unhighlight: function(element) {
                    $(element).closest('.control-group').removeClass('has-error has-feedback');
                }
            });
        },

        // =========================================================================
        // CHANGE STATE CITY
        // =========================================================================
        handleStateCityChange: function () {
            $("#agent_form #state").change(function() {
                var state = $(this).val();
                var html = "";
                html += "<option value>Select</option>";
                if(state)
                {
                    $("#city_source option."+state).each(function()
                    {
                        html += "<option value='"+$(this).attr("value")+"'>"+$(this).text()+"</option>";
                    });
                    $("#agent_form #city").html(html).trigger("chosen:updated");
                }
            });
            $("#agent_form #state").change();
            $("#agent_form #city").val($("#agent_form #city").data("city")).trigger("chosen:updated");
        },

        // =========================================================================
        // AGENT FORM SUBMISSION
        // =========================================================================
        handleAgentFormSubmit: function() {
            $("#agent_form .btn-submit").click(function() {
                if($("#agent_form").valid())
                {
                    $("#agent_form .form-footer .btn").attr("disabled", "disabled");
                    $(this).text($(this).data("text"));
                    $("#agent_form").attr("action", $(this).data("action")).submit();
                }
            });
            $("#agent_form .btn-delete").click(function() {
                $("#agent_form").validate().cancelSubmit = true;
                $("#agent_form .form-footer .btn").attr("disabled", "disabled");
                $(this).text($(this).data("text"));
                $("#agent_form").attr("action", $(this).data("action")).submit();
            });
        },

        // =========================================================================
        // UPPER CASE
        // =========================================================================
        handleUppercase: function() {
            $(".form-control.uc").change(function(){
                $(this).val($(this).val().toUpperCase());
            });
        }
    };
}();

// Call main app init
Agent.init();
