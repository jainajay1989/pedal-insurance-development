<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Dashboard
//Route::get('/', 'Dashboard\DashboardController@index');
//----------
Route::get('/', 'Lead\LeadController@index');

// Login
Route::get('/login', 'Login\LoginController@viewLogin');
Route::get('/lock', 'Login\LoginController@viewLock');
Route::post('/login', 'Login\LoginController@authenticate');
Route::get('/lock-user', 'Login\LoginController@lock');
Route::get('/logout', 'Login\LoginController@logout');
//------

// Message
Route::get('/inbox', 'Message\MessageController@index');
Route::get('/inbox/message', 'Message\MessageController@view');
Route::get('/inbox/new-message', 'Message\MessageController@create');
Route::post('/inbox/send-message', 'Message\MessageController@store');
//--------

// Lead Message
Route::get('/messages', 'LeadMessage\LeadMessageController@index');
Route::get('/message/view', 'LeadMessage\LeadMessageController@view');
Route::get('/message/compose', 'LeadMessage\LeadMessageController@create');
Route::post('/message/send-message', 'LeadMessage\LeadMessageController@store');
Route::post('/message/refresh-messages', 'LeadMessage\LeadMessageController@refresh');
//-------------

// Users
Route::get('/users', 'User\UserController@index');
Route::get('/user/add', 'User\UserController@create');
Route::post('/user/add', 'User\UserController@store');
Route::get('/user/edit', 'User\UserController@edit');
Route::post('/user/update', 'User\UserController@update');
Route::post('/user/delete', 'User\UserController@remove');
//---------

// RM
Route::get('/dealers', 'RM\RMController@index');
Route::get('/dealer/add', 'RM\RMController@create');
Route::post('/dealer/add', 'RM\RMController@store');
Route::get('/dealer/edit', 'RM\RMController@edit');
Route::post('/dealer/update', 'RM\RMController@update');
Route::post('/dealer/delete', 'RM\RMController@remove');
//---

// Agent
Route::get('/agents', 'Agent\AgentController@index');
Route::get('/agent/add', 'Agent\AgentController@create');
Route::post('/agent/add', 'Agent\AgentController@store');
Route::get('/agent/edit', 'Agent\AgentController@edit');
Route::post('/agent/update', 'Agent\AgentController@update');
Route::post('/agent/delete', 'Agent\AgentController@remove');
//---------

// Lead
Route::get('/leads', 'Lead\LeadController@index');
Route::get('/lead/add', 'Lead\LeadController@create');
Route::post('/lead/add', 'Lead\LeadController@store');
//Route::get('/lead/edit', 'Lead\LeadController@edit');
Route::match(['get', 'post'], '/lead/edit', 'Lead\LeadController@edit');
Route::post('/lead/update', 'Lead\LeadController@update');
//-----

// Health Insurance Update
Route::post('/healthInsurance/update', 'Lead\HealthInsuranceController@update');
Route::post('/otherInsurance/update', 'Lead\OtherInsuranceController@update');
Route::post('/motorInsurance/update', 'Lead\MotorInsuranceController@update');
Route::post('/travelInsurance/update', 'Lead\TravelInsuranceController@update');

// Ledger
Route::get('/ledger', 'Ledger\LedgerController@index');
Route::get('/ledger/add', 'Ledger\LedgerController@create');
Route::post('/ledger/add', 'Ledger\LedgerController@store');
Route::get('/ledger/edit', 'Ledger\LedgerController@edit');
Route::post('/ledger/update', 'Ledger\LedgerController@update');
//-------

// Insurance Company
Route::get('/insurance-companies', 'InsuranceCompany\InsuranceCompanyController@index');
Route::get('/insurance-company/add', 'InsuranceCompany\InsuranceCompanyController@create');
Route::post('/insurance-company/add', 'InsuranceCompany\InsuranceCompanyController@store');
Route::get('/insurance-company/edit', 'InsuranceCompany\InsuranceCompanyController@edit');
Route::post('/insurance-company/update', 'InsuranceCompany\InsuranceCompanyController@update');
Route::post('/insurance-company/delete', 'InsuranceCompany\InsuranceCompanyController@remove');
//------------------

// Insurance Product
Route::get('/insurance-products', 'InsuranceProduct\InsuranceProductController@index');
Route::get('/insurance-product/add', 'InsuranceProduct\InsuranceProductController@create');
Route::post('/insurance-product/add', 'InsuranceProduct\InsuranceProductController@store');
Route::get('/insurance-product/edit', 'InsuranceProduct\InsuranceProductController@edit');
Route::post('/insurance-product/update', 'InsuranceProduct\InsuranceProductController@update');
Route::post('/insurance-product/delete', 'InsuranceProduct\InsuranceProductController@remove');
//------------------

// Commission Payout
Route::get('/commission-payouts', 'CommissionPayout\CommissionPayoutController@index');
Route::get('/commission-payout/add', 'CommissionPayout\CommissionPayoutController@create');
Route::post('/commission-payout/add', 'CommissionPayout\CommissionPayoutController@store');
Route::get('/commission-payout/edit', 'CommissionPayout\CommissionPayoutController@edit');
Route::post('/commission-payout/update', 'CommissionPayout\CommissionPayoutController@update');
//------------------

// CMS
Route::get('/pages', 'CMS\CMSController@index');
Route::get('/page/add', 'CMS\CMSController@create');
Route::post('/page/add', 'CMS\CMSController@store');
Route::get('/page/edit', 'CMS\CMSController@edit');
Route::post('/page/update', 'CMS\CMSController@update');
//----

// File
Route::get('/lead-attachment', 'File\FileController@lead');
Route::get('/motor-attachment', 'File\FileController@motorInsuranceAttachment');
Route::get('/policies-attachment', 'File\FileController@policiesAttachment');
Route::get('/quotes-attachment', 'File\FileController@quotesAttachment');
Route::get('/health-attachment', 'File\FileController@healthInsuranceAttachment');
Route::get('/health-purpose-attachment', 'File\FileController@healthInsurancePurposalAttachment');

Route::get('/transaction-attachment', 'File\FileController@ledger');
//-----

Route::get('/upload-dealer', 'Agent\AgentController@uploadDealer');
Route::get('/update-dealer', 'Agent\AgentController@updateDealer');

Route::get('/set-commission', 'CommissionPayout\CommissionPayoutController@setCommission');
Route::get('/lead/generate-report', 'Lead\LeadController@generateReport');

Route::get('/vehicle/upload-list', 'Vehicle\VehicleController@uploadedList');
Route::match(array('GET', 'POST'), '/vehicle/upload-vehicle', 'Vehicle\VehicleController@uploadVehicle');
Route::get('/vehicle/upload-error-report', 'File\FileController@vehicleUploadErrorAttachment');
Route::get('/vehicle/uploaded-file', 'File\FileController@vehicleUploadAttachment');