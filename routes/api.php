<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/update-device-id', 'API\Login\LoginController@updateDeviceId');

Route::post('/update-user-profile', 'API\Profile\ProfileController@updateProfile');
Route::post('/update-user-password', 'API\Profile\ProfileController@updatePassword');

Route::post('/get-messages', 'API\Message\MessageController@index');
Route::post('/get-message-detail', 'API\Message\MessageController@detail');
Route::post('/read-message', 'API\Message\MessageController@read');
Route::post('/send-message', 'API\Message\MessageController@add');




Route::post('/update-lead-detail', 'API\Lead\LeadController@update');

Route::post('/upload-motor-image', 'API\Lead\MotorInsuranceController@upload');
Route::post('/upload-health-image', 'API\Lead\HealthInsuranceController@upload');

Route::post('/get-transactions', 'API\Ledger\LedgerController@index');
Route::post('/get-transaction-detail', 'API\Ledger\LedgerController@detail');
Route::post('/add-balance', 'API\Ledger\LedgerController@add');
Route::post('/withdraw-balance', 'API\Ledger\LedgerController@withdraw');
Route::post('/upload-transaction-image', 'API\Ledger\LedgerController@upload');

Route::post('/get-commission-payouts', 'API\CommissionPayout\CommissionPayoutController@index');

Route::post('/get-page-content', 'API\Page\PageController@detail');

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/* Added by Ajay Jain */
Route::post('/get-policy-pedal', 'API\Lead\LeadController@getPolicyPedal');
Route::post('/payu-money-pay-response', 'API\Lead\LeadController@payuMoneyPayResponse');
Route::get('/thank-you', 'API\Lead\LeadController@thankYou');
Route::post('/get-insurance-products', 'API\Cron\CommonController@getInsuranceProducts');
Route::post('/get-insurance-products', 'API\Cron\CommonController@getInsuranceProducts');
Route::post('/add-health-insurance-lead', 'API\Lead\HealthInsuranceController@add');
Route::post('/add-other-insurance-lead', 'API\Lead\OtherInsuranceController@add');
Route::post('/add-motor-insurance-lead', 'API\Lead\MotorInsuranceController@add');
Route::post('/add-travel-insurance-lead', 'API\Lead\TravelInsuranceController@add');
//Route::post('/get-motor-lead-detail', 'API\Lead\MotorInsuranceController@getMotorLeadDetail');

Route::get('/get-countries', 'API\Cron\CommonController@getCountries');
Route::post('/get-country-state', 'API\Cron\CommonController@getCountryState');

//Cron jobs start
Route::get('/generate-report', 'API\Cron\CronController@generateReport');
Route::get('/verify-transaction', 'API\Cron\CronController@verifyTransaction');
Route::get('/reject-pending-pequest', 'API\Cron\CronController@rejectPendingRequest');
//Cron jobs end


/* PEDAL - V0 API START */
Route::post('/login', 'API\Login\LoginController@authenticate');
Route::post('/get-leads', 'API\Lead\LeadController@index');
Route::post('/add-lead', 'API\Lead\LeadController@add');
Route::post('/get-cities', 'API\Lead\LeadController@getCities');
Route::post('/get-pincodes', 'API\Lead\LeadController@getPincodes');
Route::post('/get-model', 'API\Lead\LeadController@getModel');
Route::post('/get-lead-detail', 'API\Lead\LeadController@detail');
Route::post('/get-rate-calculator', 'API\Lead\LeadController@getRateCalculator');
Route::get('/get-terms-conditions', 'API\Page\PageController@getTermsConditions');
Route::get('/get-app-latest-version', 'API\Lead\LeadController@getAppLatestVersion');
Route::post('/wallet-payment-response', 'API\Agent\AgentController@walletPaymentResponse');
Route::post('/upload-lead-image', 'API\Lead\LeadController@upload');
/* END */

/* PEDAL - V1 API START */
Route::post('/login', 'API\Login\LoginController@authenticate');
Route::post('/get-leads', 'API\Lead\LeadController@index');
Route::post('/add-lead', 'API\Lead\LeadController@add');
Route::post('/get-leadsV1', 'API\Lead\LeadV1Controller@index');
Route::post('/add-leadV1', 'API\Lead\LeadV1Controller@add');
Route::post('/get-citiesV1', 'API\Lead\LeadV1Controller@getCities');
Route::post('/get-pincodesV1', 'API\Lead\LeadV1Controller@getPincodes');
Route::post('/get-modelV1', 'API\Lead\LeadV1Controller@getModel');
Route::post('/get-lead-detailV1', 'API\Lead\LeadV1Controller@detail');
Route::post('/get-rate-calculatorV1', 'API\Lead\LeadV1Controller@getRateCalculator');
Route::get('/get-app-latest-versionV1', 'API\Lead\LeadV1Controller@getAppLatestVersion');
Route::post('/upload-lead-imageV1', 'API\Lead\LeadV1Controller@upload');
/* END */

Route::post('/add-money', 'API\Agent\AgentController@addMoney');
Route::post('/get-wallet-balance', 'API\Profile\ProfileController@getWalletBalance');



Route::post('/get-make', 'API\Lead\MotorInsuranceController@getMake');
Route::post('/get-make-model', 'API\Lead\MotorInsuranceController@getModel');
Route::post('/upload-cheque', 'API\Lead\MotorInsuranceController@uploadCheque');
Route::post('/update-quote', 'API\Lead\MotorInsuranceController@updateQuote');
Route::post('/upload-purposal-form', 'API\Lead\HealthInsuranceController@uploadPurposalForm');
