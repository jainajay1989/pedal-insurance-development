@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-money"></i>Ledger</h2>
            <a href="{!! url('ledger') !!}" class="btn btn-xs btn-theme pull-right mr-10">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Edit Transaction</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('ledger/update', ['id' => $transaction_detail->LG_id]), 'class' => 'form floating-label', 'method' => 'post']) !!}
                        <div class="form-body p-20">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Transaction Type</label>
                                            <div class="text-strong">{!! config('constant.transaction_type.'.$transaction_detail->LG_type.'.message') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Agent</label>
                                            <div class="text-strong">{{ $agent_list[$transaction_detail->LG_AGid] }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Amount</label>
                                            <div class="text-strong">₹{{ $transaction_detail->LG_amount }}</div>
                                            <div class="text-sm">{{ $transaction_detail->LG_note }}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Attachment</label>
                                            <div>
                                                <a href="{!! url('transaction-attachment', ['image' => $transaction_detail->LG_attachment]) !!}" target="_blank">
                                                    <img src="{!! url('transaction-attachment', ['image' => $transaction_detail->LG_attachment]) !!}" style="height: 34px;" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('status', $transaction_status_list, $transaction_detail->LG_status, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($transaction_detail->LG_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($transaction_detail->LG_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(check_permission('LEDGER_EDIT', Auth::user()->A_type) && $transaction_detail->LG_status < config('constant.transaction_status.APPROVED.value'))
                            <div class="form-footer">
                                <button type="submit" class="btn btn-theme">Update</button>
                            </div>
                        @endif
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
