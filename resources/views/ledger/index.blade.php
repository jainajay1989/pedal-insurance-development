@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-money"></i>Ledger</h2>
            @if($agent_id)
                <a href="{!! url('agents') !!}" class="btn btn-theme btn-xs pull-right">Back</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Transactions</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="filter">
                        <form method="get" action="{{ url('ledger') }}">
                            <div class="row">
                                <div class="col-lg-3">
                                    {!! Form::select('type', $transaction_type_list, $transaction_type, ['class' => 'form-control chosen-select']) !!}
                                </div>
                                <div class="col-lg-3">
                                    {!! Form::select('agent_id', $agent_list, $agent_id, ['class' => 'form-control chosen-search-select']) !!}
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" id="lead_id" name="lead_id" class="form-control" value="{{ $lead_id }}" placeholder="Lead Number" />
                                </div>
                                <div class="col-lg-2">
                                    {!! Form::select('status', $transaction_status_list, $status, ['class' => 'form-control chosen-select']) !!}
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-theme">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive mt-10">
                        <table class="table table-striped table-theme table-middle">
                            <thead>
                                <tr role="row">
                                    <th width="50" class="text-center no-sort">
                                        <div class="ckbox ckbox-default">
                                            <input id='trigger_checkboxes_state' class="trigger-checkboxes-state" type="checkbox" value="1" />
                                            <label for='trigger_checkboxes_state'></label>
                                        </div>
                                    </th>
                                    <th>Transaction Type</th>
                                    <th>Agent Name</th>
                                    <th>Lead Number</th>
                                    <th>Amount</th>
                                    <th width="170">Date</th>
                                    <th width="100">Status</th>
                                    <th width="50">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($transaction_list) > 0)
                                @foreach($transaction_list as $index => $transaction)
                                    <tr role="row">
                                        <td width="50" class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id='checkbox_{{ $index }}' class="trigger-checkbox-state" type="checkbox" value="1" />
                                                <label for='checkbox_{{ $index }}'></label>
                                            </div>
                                        </td>
                                        <td>{!! config('constant.transaction_type.'.$transaction->LG_type.'.message') !!}</td>
                                        <td>{{ $transaction->LG_agent }}</td>
                                        <td>
                                            @if($transaction->LG_Lid)
                                                <a href="{!! url('lead/edit', ['id' => $transaction->LG_Lid]) !!}" class="btn btn-theme btn-xs" target="_blank">View ({{ $transaction->LG_Lid }})</a>
                                            @endif
                                        </td>
                                        <td>₹{{ $transaction->LG_amount }}</td>
                                        <td>{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->LG_createdAt)->format('d-M-Y h:i A') !!}</td>
                                        <td width="100" class="text-center">
                                            @if($transaction->LG_status == config('constant.transaction_status.PENDING.value'))
                                                <label class="label label-warning">Pending</label>
                                            @elseif($transaction->LG_status == config('constant.transaction_status.PAYMENTRELEASED.value'))
                                                <label class="label label-primary">Payment Released</label>
                                            @elseif($transaction->LG_status == config('constant.transaction_status.REJECTED.value'))
                                                <label class="label label-danger">Rejected</label>
                                            @elseif($transaction->LG_status == config('constant.transaction_status.APPROVED.value'))
                                                <label class="label label-success">Approved</label>
                                            @endif
                                        </td>
                                        <td width="50" class="text-center">
                                            <a href="{!! url('ledger/edit', ['id' => $transaction->LG_id]) !!}" class="btn btn-xs btn-theme">
                                                @if($transaction->LG_status < config('constant.transaction_status.APPROVED.value'))Edit @else View @endif
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr role="row">
                                    <td colspan="7" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
