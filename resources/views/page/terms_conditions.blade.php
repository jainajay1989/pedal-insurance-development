<p>&ldquo;I am/we are aware that the complete terms and conditions of this insurance policy are available at the official website of the insurer (www.libertyinsurance.in). I/We hereby consent to receiving only the certificate and schedule of insurance upon the undertaking of the insurer that the complete policy terms and conditions will be made available free of cost upon my/our request&rdquo;.</p>

<p>&nbsp;</p>

<p><strong>ANY OTHER MATERIAL INFORMATION DECLARATION AND CONSENT</strong></p>

<p>&nbsp;</p>

<ul>
	<li>I/We hereby declare that the statements, answers given by me /us in this proposal form are true to the best of my knowledge and belief and I/We hereby agree that this declaration shall form the basis of the contract between me/us and the Liberty general Insurance Limited. It is hereby understood and agreed that the statements, answers and particulars provided herein above are the basis on which this insurance is being granted and that if, after the insurance is effected, it is found that any of the statements, answers or particulars are incorrect or untrue in any respect, the company shall have no liability under this Insurance.</li>
	<li>I/We confirm that I/We have read and understood the coverages, the terms and conditions and agree to accept the company&#39;s policy of insurance along with the said conditions prescribed by the Company.</li>
	<li>I/We also declare and undertake that if any additions or alterations are carried out by me/us in this proposal form or if there is any change in the information as submitted by me/us after the submission of this proposal form then the same would be conveyed to Liberty general Insurance Limited<a name="_GoBack"></a>immediately failing which it is agreed and understood by me/us that the benefits under the policy would stand forfeited.</li>
	<li>I/We declare and consent to the company seeking medical information from any doctor or from the hospital who at any time has attended on the life to be insured or from any past or present employer concerning anything which affects the physical and mental health of the life to be insured and seeking information from any insurance company to which an application for insurance on the life to be insured has been made for the purpose of underwriting the proposal and/or claim settlement.</li>
	<li>I/We authorise the Company to share information pertaining to my proposal including the medical records for the sole purpose of proposal underwriting and/or claims settlement and with any Government and/or Regulatory Authority.</li>
	<li>I/We agree to the Company taking appropriate measures to capture the voice log for all such telephonic transactions carried out by me/us as required by the procedures/regulations internal or external to the Company and shall not hold the Company responsible or liable for relying/using such recorded telephonic conversation I/We agree that the insurance would be effective only on acceptance of this application by the Company and the payment of the requisite premium by me/us in advance. In the event of nonrealization of the cheque or non-receipt of the amount of premium by the Company the policy shall be deemed cancelled &#39;ab-initio&#39; and the Company shall not be responsible for any liabilities of whatsoever nature under this Policy.</li>
	<li>I/We have insurable interest in the subject matter of this insurance and we hereby declare that the Cost of the same and the premium for this insurance is paid from legal sources of funds.</li>
</ul>

<p style="margin-left:.5in">&nbsp;</p>

<p><strong>DECLARATION BY INTERMEDIARY/PROPOSER</strong></p>

<p>I, the intermediary/ proposer hereby declare and confirm that I have explained/understood the features, terms and conditions of the policy and questions contained in the proposal form. I have also explained/understood that the answers to the questions contained in the proposal form, forms the basis of the contract of insurance. If any information/statement given in proposal is found to be untrue, the policy shall be treated as void ab intio and the premium paid shall be forfeited to the Company.</p>

<p>&nbsp;</p>

<!--<p><strong>Below can be added as note under T&amp;C</strong></p>-->

<p><strong>PROHIBITION OF REBATES Section 41 of the Insurance Act-1938</strong></p>

<ol>
	<li>No person shall allow or offer to allow, either directly or indirectly as an inducement to any person to take out or renew or continue an insurance in respect of any kind of risk relating to lives or property in India, any rebate of the whole or part of the commission payable or any rebate of the premium shown in the policy, nor shall any person taking out or renewing or continuing a policy accept any rebate, except such rebate as may be allowed in accordance with the published prospectus or tables of the insurer.</li>
</ol>

<p>Any person making default in complying with the provisions of this section shall be punishable with fine which may extend to 10 lac rupees.</p>
