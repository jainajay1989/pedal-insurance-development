@extends('layouts.login')

@section('page-content')
    <!-- START @SIGN WRAPPER -->
    <div id="sign-wrapper">

        <!-- Brand -->
        <div class="brand">
            <h2>{!! strtoupper(config('app.name')) !!}</h2>
        </div>
        <!--/ Brand -->

        <!-- Login form -->
        {!! Form::open(['url' => url('login'), 'class' => 'form floating-label', 'method' => 'post']) !!}
            <div class="sign-header">
                <div class="form-group">
                    <div class="sign-text">
                        <span>Member Area</span>
                    </div>
                </div><!-- /.form-group -->
            </div><!-- /.sign-header -->
            <div class="sign-body">
                <div class="form-group">
                    <div class="input-group input-group-lg rounded no-overflow">
                        {!! Form::text('username', null, ['class' => 'form-control input-sm', 'id' => 'username', 'placeholder' => 'Username']) !!}
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <div class="input-group input-group-lg rounded no-overflow">
                        {!! Form::password('password', ['class' => 'form-control input-sm', 'id' => 'password', 'placeholder' => 'Password']) !!}
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                </div><!-- /.form-group -->
            </div><!-- /.sign-body -->
            <div class="sign-footer">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="ckbox ckbox-theme">
                                {!! Form::checkbox('remember', 1, true, ['id' => 'remember']) !!}
                                <label for="rememberme" class="rounded">Remember me</label>
                            </div>
                        </div>
                        {{--<div class="col-xs-6 text-right">
                            <a href="#" title="lost password">Lost password?</a>
                        </div>--}}
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Sign In</button>
                </div><!-- /.form-group -->
            </div><!-- /.sign-footer -->
        {!! Form::close() !!}
        <!-- /.form-horizontal -->
        <!--/ Login form -->

    </div><!-- /#sign-wrapper -->
    <!--/ END SIGN WRAPPER -->
@endsection