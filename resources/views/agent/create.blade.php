@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-group"></i>Dealer</h2>
            <a href="{!! url('agents') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Add New Dealer</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => '#', 'class' => 'form floating-label', 'method' => 'post', 'id' => 'agent_form']) !!}
                        @if(Auth::user()->A_type == config('constant.user_type.MANAGER.value'))
                            {!! Form::hidden('manager', Auth::user()->A_id) !!}
                        @endif
                        <div class="form-body p-20">
                            <div class="form-group">
                                <div class="row">
<!--                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Code</label>
                                            <input type="text" id="code" name="code" class="form-control required uc" maxlength="50" />
                                        </div>
                                    </div>-->
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">IMD Code</label>
                                            <input type="text" id="imd_code" name="imd_code" class="form-control uc" maxlength="20" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">IMD Name</label>
                                            <input type="text" id="imd_name" name="imd_name" class="form-control required uc" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Company Name</label>
                                            <input type="text" id="company_name" name="company_name" class="form-control required uc" maxlength="100" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->A_type == config('constant.user_type.ADMIN.value'))
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <label class="control-label">Manager</label>
                                                {!! Form::select('manager', $user_list, null, ['id' => 'manager', 'class' => 'form-control required chosen-search-select']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <label class="control-label">RM</label>
                                                {!! Form::select('dealer', $dealer_list, null, ['id' => 'dealer', 'class' => 'form-control required chosen-search-select']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
<!--                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Allowed Policies</label>
                                            {!! Form::select('policy_type[]', $policy_type_list, null, ['class' => 'form-control chosen-select required', 'multiple' => 'multiple']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" id="first_name" name="first_name" class="form-control required uc" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" id="last_name" name="last_name" class="form-control required uc" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" id="email" name="email" class="form-control required uc" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Username</label>
                                            <input type="text" id="username" name="username" class="form-control required" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Password</label>
                                            <input type="password" id="password" name="password" class="form-control required" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Confirm Password</label>
                                            <input type="password" id="confirm_password" name="confirm_password" class="form-control required" maxlength="100" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Phone</label>
                                            <input type="text" id="phone" name="phone" class="form-control uc" maxlength="12" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Mobile</label>
                                            <input type="text" id="mobile" name="mobile" class="form-control required uc" maxlength="10" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" id="address" name="address" class="form-control uc" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">State</label>
                                            {!! Form::select('state', $state_list, null, ['class' => 'form-control chosen-search-select', 'id' => 'state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">City</label>
                                            {!! Form::select('city', $city_list, null, ['class' => 'form-control chosen-search-select', 'id' => 'state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Zip</label>
                                            <input type="text" id="zip" name="zip" class="form-control uc" maxlength="6" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">PAN</label>
                                            <input type="text" id="pan" name="pan" class="form-control uc" maxlength="15" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Payment Mode</label>
                                            <?php $paymentModeType = config('constant.PAYMENT_MODE_TYPE'); ?>
                                            {!! Form::select('payment_mode', $paymentModeType, 1, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Account Type</label>
                                            <?php $accountType = config('constant.BANK_ACCOUNT_TYPE'); ?>
                                            {!! Form::select('account_type', $accountType, 1, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Bank Name</label>
                                            <input type="text" id="bank_name" name="bank_name" class="form-control uc" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Branch Name</label>
                                            <input type="text" id="branch_name" name="branch_name" class="form-control uc" maxlength="100" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Bank Address</label>
                                            <input type="text" id="bank_address" name="bank_address" class="form-control uc" maxlength="255" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Bank Account Number</label>
                                            <input type="text" id="bank_account_number" name="bank_account_number" class="form-control uc" maxlength="20" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Bank IFSC code</label>
                                            <input type="text" id="bank_ifsc_code" name="bank_ifsc_code" class="form-control uc" maxlength="15" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">MICR Code</label>
                                            <input type="text" id="micr_code" name="micr_code" class="form-control uc" maxlength="15" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('active', [1 => 'Active', 0 => 'Inactive'], 1, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button type="button" class="btn btn-theme btn-submit" data-action="{!! url('agent/add') !!}" data-text="Adding...">Add</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/agent.js"></script>
@endsection
