@extends('layouts.app')

@section('head')
    <link href="{!! config('app.assets_url') !!}css/pages/mail.css" rel="stylesheet" />
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-inbox"></i>Inbox</h2>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">

            <!-- Start mail page -->
            <div class="row">
                <div class="col-sm-3">
                    <a href="{!! url('inbox/new-message') !!}" class="btn btn-danger btn-block btn-compose-email">Compose Email</a>
                    <ul class="nav nav-pills nav-stacked nav-email shadow mb-20">
                        <li class="active">
                            <a href="{!! url('inbox') !!}">
                                <i class="fa fa-inbox"></i> Inbox
                                @if(count($unread_message_list) > 0)
                                    <span class="label pull-right">{!! count($unread_message_list) !!}</span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a href="mail-compose.html"><i class="fa fa-envelope-o"></i> Send Mail</a>
                        </li>
                    </ul><!-- /.nav -->
                </div>
                <div class="col-sm-9">
                    <div class="panel rounded shadow panel-teal">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title">Inbox ({!! count($message_list) !!})</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-heading -->
                        {{--<div class="panel-sub-heading inner-all">
                            <div class="pull-left">
                                <ul class="list-inline no-margin">
                                    <li>
                                        <div class="ckbox ckbox-theme">
                                            <input id="checkbox-group" type="checkbox" class="mail-group-checkbox">
                                            <label for="checkbox-group"></label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                All <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">None</a></li>
                                                <li><a href="#">Read</a></li>
                                                <li><a href="#">Unread</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm tooltips" type="button" data-toggle="tooltip" data-container="body" title="Archive"><i class="fa fa-inbox"></i></button>
                                            <button class="btn btn-default btn-sm tooltips" type="button" data-toggle="tooltip" data-container="body" title="Report Spam"><i class="fa fa-warning"></i></button>
                                            <button class="btn btn-default btn-sm tooltips" type="button" data-toggle="tooltip" data-container="body" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </li>
                                    <li class="hidden-xs">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm">More</button>
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#"><i class="fa fa-edit"></i> Mark as read</a></li>
                                                <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <ul class="list-inline no-margin">
                                    <li class="hidden-xs"><span class="text-muted">Showing 1-50 of 2,051 messages</span></li>
                                    <li>
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
                                            <a href="#" class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </li>
                                    <li class="hidden-xs">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li class="dropdown-header">Display density :</li>
                                                <li class="active"><a href="#">Comfortable</a></li>
                                                <li><a href="#">Cozy</a></li>
                                                <li><a href="#">Compact</a></li>
                                                <li class="dropdown-header">Configure inbox</li>
                                                <li><a href="#">Settings</a></li>
                                                <li><a href="#">Themes</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Help</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>--}}
                        <!-- /.panel-sub-heading -->
                        <div class="panel-body no-padding">

                            <div class="table-responsive">
                                <table class="table table-hover table-email">
                                    <tbody>
                                    @foreach($message_list as $index => $message)
                                        <tr class="unread selected">
                                            <td>
                                                <div class="ckbox ckbox-theme">
                                                    <input id="checkbox{{ $index }}" type="checkbox" class="mail-checkbox" />
                                                    <label for="checkbox{{ $index }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <a href="#" class="pull-left">
                                                        <img alt="{{ $message->MG_agent }}" src="{!! config('app.assets_url') !!}images/default-user-image.png" class="media-object">
                                                    </a>
                                                    <div class="media-body">
                                                        <a href="{!! url('inbox/message', ['id' => $message->MG_id]) !!}">
                                                            <h4 class="text-primary">{{ $message->MG_agent }}</h4>
                                                            <p class="email-summary"> {!! substr($message->MG_message, 0, 100) !!}...</p>
                                                            <span class="media-meta">{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message->MG_createdAt)->format('d-M-Y') !!}</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->

                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div>
            </div><!-- /.row -->
            <!--/ End mail page -->

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
