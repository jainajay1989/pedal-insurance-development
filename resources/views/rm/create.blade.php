@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-group"></i>RM</h2>
            <a href="{!! url('dealers') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Add New RM</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => '#', 'class' => 'form floating-label', 'method' => 'post', 'id' => 'agent_form']) !!}
                    @if(Auth::user()->A_type == config('constant.user_type.MANAGER.value'))
                        {!! Form::hidden('type', config('constant.agent_type.AGENT.value')) !!}
                        {!! Form::hidden('manager', Auth::user()->A_id) !!}
                    @endif
                    <div class="form-body p-20">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Code</label>
                                        <input type="text" id="code" name="code" class="form-control required uc" maxlength="50" />
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Allowed Policies</label>
                                        {!! Form::select('policy_type[]', $policy_type_list, null, ['class' => 'form-control chosen-select required', 'multiple' => 'multiple']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">First Name</label>
                                        <input type="text" id="first_name" name="first_name" class="form-control required uc" maxlength="50" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" id="last_name" name="last_name" class="form-control required uc" maxlength="50" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" id="email" name="email" class="form-control required uc" maxlength="50" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Username</label>
                                        <input type="text" id="username" name="username" class="form-control required" maxlength="50" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Password</label>
                                        <input type="password" id="password" name="password" class="form-control required" maxlength="100" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" id="confirm_password" name="confirm_password" class="form-control required" maxlength="100" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Phone</label>
                                        <input type="text" id="phone" name="phone" class="form-control uc" maxlength="12" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" id="mobile" name="mobile" class="form-control required uc" maxlength="10" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Address</label>
                                        <input type="text" id="address" name="address" class="form-control uc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <label class="control-label">State</label>
                                        {!! Form::select('state', $state_list, null, ['class' => 'form-control chosen-search-select', 'id' => 'state']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <label class="control-label">City</label>
                                        {!! Form::select('city', $city_list, null, ['class' => 'form-control chosen-search-select', 'id' => 'state']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <label class="control-label">Zip</label>
                                        <input type="text" id="zip" name="zip" class="form-control uc" maxlength="6" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('active', [1 => 'Active', 0 => 'Inactive'], 1, ['class' => 'form-control chosen-select']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-footer">
                        <button type="button" class="btn btn-theme btn-submit" data-action="{!! url('dealer/add') !!}" data-text="Adding...">Add</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/agent.js"></script>
@endsection
