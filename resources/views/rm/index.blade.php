@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-group"></i>RM</h2>
            @if(check_permission('AGENT_CREATE', Auth::user()->A_type))
                <a href="{!! url('dealer/add') !!}" class="btn btn-xs btn-theme pull-right">Add New RM</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">RM</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive mt-10">
                        <table class="table table-striped table-theme table-middle">
                            <thead>
                            <tr role="row">
<!--                                <th width="50" class="text-center no-sort">
                                    <div class="ckbox ckbox-default">
                                        <input id='trigger_checkboxes_state' class="trigger-checkboxes-state" type="checkbox" value="1" />
                                        <label for='trigger_checkboxes_state'></label>
                                    </div>
                                </th>-->
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Mobile</th>
                                <th width="100">Status</th>
                                @if(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type) || check_permission('COMMISSION_PAYOUT_CREATE', Auth::user()->A_type) || check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type) || check_permission('LEDGER_VIEW', Auth::user()->A_type) || check_permission('LEDGER_CREATE', Auth::user()->A_type) || check_permission('LEDGER_EDIT', Auth::user()->A_type) || check_permission('AGENT_VIEW', Auth::user()->A_type) || check_permission('AGENT_EDIT', Auth::user()->A_type))
                                    <th width="50">Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($agent_list) > 0)
                                @foreach($agent_list as $index => $agent)
                                    <tr role="row">
<!--                                        <td width="50" class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id='checkbox_{{ $index }}' class="trigger-checkbox-state" type="checkbox" value="1" />
                                                <label for='checkbox_{{ $index }}'></label>
                                            </div>
                                        </td>-->
                                        <td>{{ $agent->AG_firstName }} {{ $agent->AG_lastName }}</td>
                                        <td>{{ $agent->AG_email }}</td>
                                        <td>{{ $agent->AG_username }}</td>
                                        <td>{{ $agent->AG_mobile }}</td>
                                        <td class="text-center">
                                            @if($agent->AG_active == 1)
                                                <label class="label label-success">Yes</label>
                                            @else
                                                <label class="label label-warning">No</label>
                                            @endif
                                        </td>
                                        @if(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type) || check_permission('COMMISSION_PAYOUT_CREATE', Auth::user()->A_type) || check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type) || check_permission('LEDGER_VIEW', Auth::user()->A_type) || check_permission('LEDGER_CREATE', Auth::user()->A_type) || check_permission('LEDGER_EDIT', Auth::user()->A_type) || check_permission('AGENT_VIEW', Auth::user()->A_type) || check_permission('AGENT_EDIT', Auth::user()->A_type))
                                            <td class="text-center text-nowrap">
                                                @if(check_permission('AGENT_VIEW', Auth::user()->A_type) || check_permission('AGENT_EDIT', Auth::user()->A_type))
                                                    <a href="{!! url('dealer/edit', ['id' => $agent->AG_id]) !!}" class="btn btn-xs btn-theme">
                                                        @if(check_permission('AGENT_EDIT', Auth::user()->A_type))
                                                            Edit
                                                        @elseif(check_permission('AGENT_VIEW', Auth::user()->A_type))
                                                            View
                                                        @endif
                                                    </a>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr role="row">
                                    <td colspan="7" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
