@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-html5"></i>Page</h2>
            <a href="{!! url('page/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Page</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Add Page</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('page/add'), 'class' => 'form floating-label', 'method' => 'post']) !!}
                    <div class="form-body p-20">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Key</label>
                                        <input type="text" id="key" name="key" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label">Name</label>
                                        <input type="text" id="name" name="name" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="control-group">
                                        <label class="control-label">Content</label>
                                        <textarea id="content" name="content" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('active', [1 => 'Active', 0 => 'Inactive'], 1, ['class' => 'form-control chosen-select']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-theme">Add</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
