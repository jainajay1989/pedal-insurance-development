@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-html5"></i>Pages</h2>
            @if(check_permission('CMS_CREATE', Auth::user()->A_type))
                <a href="{!! url('page/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Page</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Pages</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive mt-10">
                        <table class="table table-striped table-theme table-middle">
                            <thead>
                            <tr role="row">
                                <th width="50" class="text-center no-sort">
                                    <div class="ckbox ckbox-default">
                                        <input id='trigger_checkboxes_state' class="trigger-checkboxes-state" type="checkbox" value="1" />
                                        <label for='trigger_checkboxes_state'></label>
                                    </div>
                                </th>
                                <th>Name</th>
                                <th width="100">Status</th>
                                @if(check_permission('CMS_VIEW', Auth::user()->A_type) || check_permission('CMS_EDIT', Auth::user()->A_type))
                                    <th width="50">Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($page_list) > 0)
                                @foreach($page_list as $index => $page)
                                    <tr role="row">
                                        <td width="50" class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id='checkbox_{{ $index }}' class="trigger-checkbox-state" type="checkbox" value="1" />
                                                <label for='checkbox_{{ $index }}'></label>
                                            </div>
                                        </td>
                                        <td>{{ $page->PG_title }}</td>
                                        <td class="text-center">
                                            @if($page->PG_active == 1)
                                                <label class="label label-success">Yes</label>
                                            @else
                                                <label class="label label-warning">No</label>
                                            @endif
                                        </td>
                                        @if(check_permission('CMS_VIEW', Auth::user()->A_type) || check_permission('CMS_EDIT', Auth::user()->A_type))
                                            <td class="text-center">
                                                <a href="{!! url('page/edit', ['id' => $page->PG_id]) !!}" class="btn btn-xs btn-theme">
                                                    @if(check_permission('CMS_EDIT', Auth::user()->A_type))
                                                        Edit
                                                    @elseif(check_permission('CMS_VIEW', Auth::user()->A_type))
                                                        View
                                                    @endif
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr role="row">
                                    <td colspan="6" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
