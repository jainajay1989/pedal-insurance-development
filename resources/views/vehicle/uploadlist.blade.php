@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-user"></i>Uploaded List</h2>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Uploaded</h3>
                    </div>
                    <div class="pull-right">
                        @if(check_permission('VEHICLE_UPLOAD', Auth::user()->A_type))
                            <a href="{!! url('/vehicle/upload-vehicle') !!}" class="btn btn-theme btn-primary pull-right">Upload New Models</a>
                        @endif
                            <a style='margin-right:10px;'href="{!! url('/Sample.xlsx') !!}" class="btn btn-theme btn-primary pull-right">Sample Excel</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive mt-10">
                        <table class="table table-striped table-theme table-middle">
                            <thead>
                            <tr role="row">
                                <th>File Name</th>
                                <th>Uploaded On</th>
                                <th>Count</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($list))
                                <tr role="row">
                                    <td>{{ $list->file }}</td>
                                    <td>{{ $list->created_at }}</td>
                                    <td>T:{{ $list->total }} | S:{{ $list->success }} | E:{{ $list->error }} | R:{{ $list->repeated }}</td>
                                    <td>
                                        <?php if(!empty($value->file)) { ?>
<!--                                            <a href="{!! url('/vehicle/uploaded-file', ['file' => $value->file]) !!}" target="_blank">
                                        <i class="fa fa-download"></i> Uploaded File
                                        </a>-->
                                        <?php } if(!empty($list->error) || !empty($list->repeated)) { ?>
                                        <a href="{!! url('/vehicle/upload-error-report', ['file' => $list->error_report]) !!}" target="_blank">
                                        <i class="fa fa-download"></i> Error Report
                                        </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            @else
                                <tr role="row">
                                    <td colspan="6" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
