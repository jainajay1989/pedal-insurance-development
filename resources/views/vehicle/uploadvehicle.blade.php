@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
        .new-policy-attachment-container label.error{display: none !important;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-car"></i>Upload Vehicle</h2>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Upload Vehicle</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('vehicle/upload-vehicle'), 'class' => 'form floating-label', 'method' => 'post', 'id' => 'lead_form', 'files' => true]) !!}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <div class="form-body p-20">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6 new-policy-attachment-container">
                                                <div class="control-group">
                                                    <label class="control-label">Upload List</label>
                                                    <div class="fileinput-new input-group" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="vehicle_models" id="new_policy_attachment"></span>
                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6"><br/>
                                                <button type="button" class="btn btn-theme btn-submit">Upload</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection

