<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
    <!-- START @META SECTION -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>{!! config('app.name') !!}</title>
    <!--/ END META SECTION -->

    <!-- START @FONT STYLES -->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet" />--}}
    <!--/ END FONT STYLES -->

    <!-- START @GLOBAL MANDATORY STYLES -->
    <link href="{!! config('app.assets_url') !!}plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!--/ END GLOBAL MANDATORY STYLES -->

    <!-- START @PAGE LEVEL STYLES -->
    <link href="{!! config('app.assets_url') !!}plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/animate.css/animate.min.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/dropzone/downloads/css/dropzone.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/jquery.gritter/css/jquery.gritter.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/bootstrap-tour/build/css/bootstrap-tour.min.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/chosen_v1.2.0/chosen.min.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/bootstrap-datepicker-vitalets/css/datepicker.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}plugins/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet" />
    @yield('head')
    <!--/ END PAGE LEVEL STYLES -->

    <!-- START @THEME STYLES -->
    <link href="{!! config('app.assets_url') !!}css/reset.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}css/layout.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}css/components.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}css/plugins.css" rel="stylesheet" />
    <link href="{!! config('app.assets_url') !!}css/themes/default.theme.css" rel="stylesheet" id="theme" />
    <link href="{!! config('app.assets_url') !!}css/custom.css" rel="stylesheet" />
    <!--/ END THEME STYLES -->

    <!-- START @IE SUPPORT -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{!! config('app.assets_url') !!}plugins/html5shiv/dist/html5shiv.min.js"></script>
    <script src="{!! config('app.assets_url') !!}plugins/respond-minmax/dest/respond.min.js"></script>
    <![endif]-->
    <!--/ END IE SUPPORT -->
</head>
<!--/ END HEAD -->

<body class="page-session page-header-fixed page-sidebar-fixed">

<!-- START @WRAPPER -->
<section id="wrapper">

    <!-- START @HEADER -->
    <header id="header">

        <!-- Start header left -->
        <div class="header-left">
            <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
            <div class="navbar-minimize-mobile left">
                <i class="fa fa-bars"></i>
            </div>
            <!--/ End offcanvas left -->

            <!-- Start navbar header -->
            <div class="navbar-header">

                <!-- Start brand -->
                <a id="tour-1" class="navbar-brand" href="{!! url('/') !!}">
                    {!! config('app.name') !!}
                </a><!-- /.navbar-brand -->
                <!--/ End brand -->

            </div><!-- /.navbar-header -->
            <!--/ End navbar header -->

            <div class="clearfix"></div>
        </div><!-- /.header-left -->
        <!--/ End header left -->

        <!-- Start header right -->
        <div class="header-right">
            <!-- Start navbar toolbar -->
            <div class="navbar navbar-toolbar">

                <!-- Start left navigation -->
                <ul class="nav navbar-nav navbar-left">

                    <!-- Start sidebar shrink -->
                    <li id="tour-2" class="navbar-minimize">
                        <a href="javascript:void(0);" title="Minimize sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <!--/ End sidebar shrink -->

                    <!-- Start form search -->
                    {{--<li class="navbar-search">
                        <!-- Just view on mobile screen-->
                        <a href="#" class="trigger-search"><i class="fa fa-search"></i></a>
                        <form id="tour-3" class="navbar-form">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control typeahead rounded" placeholder="Search for people, places and things">
                                <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
                            </div>
                        </form>
                    </li>--}}
                    <!--/ End form search -->

                </ul><!-- /.nav navbar-nav navbar-left -->
                <!--/ End left navigation -->

                <!-- Start right navigation -->
                <ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->

                    <!-- Start messages -->
                    <!--/ End messages -->

                    <!-- Start notifications -->
                    <!--/ End notifications -->

                    <!-- Start profile -->
                    <li id="tour-6" class="dropdown navbar-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                                    <span class="avatar"><img src="{!! config('app.assets_url') !!}images/default-user-image.png" class="img-circle" alt="admin"></span>
                                    <span class="text hidden-xs hidden-sm text-muted">{!! Auth::user()->A_firstName !!}</span>
                                    <span class="caret"></span>
                                </span>
                        </a>
                        <!-- Start dropdown menu -->
                        <ul class="dropdown-menu animated flipInX">
                            <li class="dropdown-header">Account</li>
                            <li><a href="{!! url('user/edit', ['id' => Auth::user()->A_id]) !!}"><i class="fa fa-user"></i>View profile</a></li>
                            <li class="divider"></li>
                            <li><a href="{!! url('logout') !!}"><i class="fa fa-sign-out"></i>Logout</a></li>
                        </ul>
                        <!--/ End dropdown menu -->
                    </li><!-- /.dropdown navbar-profile -->
                    <!--/ End profile -->

                </ul>
                <!--/ End right navigation -->

            </div><!-- /.navbar-toolbar -->
            <!--/ End navbar toolbar -->
        </div><!-- /.header-right -->
        <!--/ End header left -->

    </header> <!-- /#header -->
    <!--/ END HEADER -->

    <!--
    START @SIDEBAR LEFT
    |=========================================================================================================================|
    |  TABLE OF CONTENTS (Apply to sidebar left class)                                                                        |
    |=========================================================================================================================|
    |  01. sidebar-box               |  Variant style sidebar left with box icon                                              |
    |  02. sidebar-rounded           |  Variant style sidebar left with rounded icon                                          |
    |  03. sidebar-circle            |  Variant style sidebar left with circle icon                                           |
    |=========================================================================================================================|

    -->
    <aside id="sidebar-left" class="sidebar-circle">

        <!-- Start left navigation - profile shortcut -->
        <div id="tour-8" class="sidebar-content">
            <div class="media">
                <a class="pull-left has-notif avatar" href="page-profile.html">
                    <img src="{!! config('app.assets_url') !!}images/default-user-image.png" alt="admin" />
                </a>
                <div class="media-body">
                    <h5 class="media-heading">Hello, <span>{!! Auth::user()->A_firstName !!} {!! Auth::user()->A_lastName !!}</span></h5>
                    <small>{{ Auth::user()->A_type }}</small>
                </div>
            </div>
        </div><!-- /.sidebar-content -->
        <!--/ End left navigation -  profile shortcut -->

        <!-- Start left navigation - menu -->
        <ul id="tour-9" class="sidebar-menu">

<!--            <li {!! $page_name=="dashboard"?"class='active'":"" !!}>
                <a href="{!! url('/') !!}">
                    <span class="icon"><i class="fa fa-dashboard"></i></span>
                    <span class="text">Dashboard</span>
                </a>
            </li>-->
            {{--<li {!! $page_name=="message"?"class=active":"" !!}>
                <a href="{!! url('inbox') !!}">
                    <span class="icon"><i class="fa fa-inbox"></i></span>
                    <span class="text">Messages</span>
                </a>
            </li>--}}
            <li {!! $page_name=="lead"?"class='active'":"" !!}>
                <a href="{!! url('leads') !!}">
                    <span class="icon"><i class="fa fa-car"></i></span>
                    <span class="text">Leads</span>
                </a>
            </li>
            <li {!! $page_name=="Vehicle-Upload"?"class='active'":"" !!}>
                <a href="{!! url('/vehicle/upload-list') !!}">
                    <span class="icon"><i class="fa fa-car"></i></span>
                    <span class="text">Upload Vehicle</span>
                </a>
            </li>
            @if(check_permission('USER_VIEW', Auth::user()->A_type) || check_permission('USER_CREATE', Auth::user()->A_type) || check_permission('USER_EDIT', Auth::user()->A_type))
                <li {!! $page_name=="user"?"class=active":"" !!}>
                    <a href="{!! url('users') !!}">
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <span class="text">Users</span>
                    </a>
                </li>
            @endif
            @if(check_permission('AGENT_VIEW', Auth::user()->A_type) || check_permission('AGENT_CREATE', Auth::user()->A_type) || check_permission('AGENT_EDIT', Auth::user()->A_type))
                <li {!! $page_name=="rm"?"class='active'":"" !!}>
                    <a href="{!! url('dealers') !!}">
                        <span class="icon"><i class="fa fa-group"></i></span>
                        <span class="text">RM</span>
                    </a>
                </li>
            @endif
            @if(check_permission('AGENT_VIEW', Auth::user()->A_type) || check_permission('AGENT_CREATE', Auth::user()->A_type) || check_permission('AGENT_EDIT', Auth::user()->A_type))
                <li {!! $page_name=="agent"?"class='active'":"" !!}>
                    <a href="{!! url('agents') !!}">
                        <span class="icon"><i class="fa fa-group"></i></span>
                        <span class="text">Dealer</span>
                    </a>
                </li>
            @endif
            @if(check_permission('LEDGER_VIEW', Auth::user()->A_type) || check_permission('LEDGER_CREATE', Auth::user()->A_type) || check_permission('LEDGER_EDIT', Auth::user()->A_type))
<!--            <li {!! $page_name=="ledger"?"class='active'":"" !!}>
                <a href="{!! url('ledger') !!}">
                    <span class="icon"><i class="fa fa-money"></i></span>
                    <span class="text">Ledger</span>
                </a>
            </li>-->
            @endif
            @if(check_permission('INSURANCE_COMPANY_VIEW', Auth::user()->A_type) || check_permission('INSURANCE_COMPANY_CREATE', Auth::user()->A_type) || check_permission('INSURANCE_COMPANY_EDIT', Auth::user()->A_type))
<!--                <li {!! $page_name=="insurance-company"?"class='active'":"" !!}>
                    <a href="{!! url('insurance-companies') !!}">
                        <span class="icon"><i class="fa fa-building"></i></span>
                        <span class="text">Insurance Companies</span>
                    </a>
                </li>-->
            @endif
            @if(check_permission('INSURANCE_PRODUCT_VIEW', Auth::user()->A_type) || check_permission('INSURANCE_PRODUCT_CREATE', Auth::user()->A_type) || check_permission('INSURANCE_PRODUCT_EDIT', Auth::user()->A_type))
<!--                <li {!! $page_name=="insurance-product"?"class='active'":"" !!}>
                    <a href="{!! url('insurance-products') !!}">
                        <span class="icon"><i class="fa fa-medkit"></i></span>
                        <span class="text">Insurance Products</span>
                    </a>
                </li>-->
            @endif
            @if(check_permission('CMS_VIEW', Auth::user()->A_type) || check_permission('CMS_CREATE', Auth::user()->A_type) || check_permission('CMS_EDIT', Auth::user()->A_type))
<!--                <li {!! $page_name=="cms"?"class='active'":"" !!}>
                    <a href="{!! url('pages') !!}">
                        <span class="icon"><i class="fa fa-html5"></i></span>
                        <span class="text">CMS</span>
                    </a>
                </li>-->
            @endif

        </ul><!-- /.sidebar-menu -->
        <!--/ End left navigation - menu -->

    </aside><!-- /#sidebar-left -->
    <!--/ END SIDEBAR LEFT -->

    <!-- START @PAGE CONTENT -->
    @yield('page-content')
    <!--/ END PAGE CONTENT -->

    <!-- AJAX Modal -->
    <div id="ajax_modal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body">
                    <h1 class="text-center mb-20">Loading...</h1>
                </div>
            </div>
        </div>
    </div>
    <!----------------->

</section><!-- /#wrapper -->
<!--/ END WRAPPER -->

<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
    <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->

<!-- START @ADDITIONAL ELEMENT -->
@yield('additional-elements')
<!--/ END ADDITIONAL ELEMENT -->

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- START @CORE PLUGINS -->
<script src="{!! config('app.assets_url') !!}plugins/jquery/dist/jquery.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jquery.sparkline.min/index.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jquery-easing-original/jquery.easing.1.3.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/bootbox/bootbox.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/noty/packaged/jquery.noty.packaged.min.js"></script>
<!--/ END CORE PLUGINS -->

<!-- START @PAGE LEVEL PLUGINS -->
<script src="{!! config('app.assets_url') !!}plugins/dropzone/downloads/dropzone.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/chosen_v1.2.0/chosen.jquery.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jquery.gritter/js/jquery.gritter.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/skycons-html5/skycons.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/counter-up/jquery.counterup.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js"></script>
<script src="{!! config('app.assets_url') !!}plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<!--/ END PAGE LEVEL PLUGINS -->

<!-- START @PAGE LEVEL SCRIPTS -->
<script src="{!! config('app.assets_url') !!}js/apps.js"></script>
@yield('bottom')
<!--/ END PAGE LEVEL SCRIPTS -->
<!--/ END JAVASCRIPT SECTION -->

@if(Session::has('msg'))
    <!-- START PAGE MESSAGE -->
    <script>
        App.handleShowMessage('{{ Session::get('msg') }}', '{{ Session::get('type') }}');
    </script>
    <!------------------------>
@endif

</body>
<!--/ END BODY -->

</html>
