@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-car"></i>Lead</h2>
            <a href="{!! url('leads') !!}" class="btn btn-xs btn-theme pull-right mr-10">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Add New Lead</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('lead/add'), 'class' => 'form floating-label', 'method' => 'post']) !!}
                        <div class="form-body p-20">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Agent</label>
                                            {!! Form::select('agent', $agent_list, null, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-30">Customer Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" id="customer_first_name" name="customer_first_name" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" id="customer_last_name" name="customer_last_name" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" id="customer_email" name="customer_email" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Mobile</label>
                                            <input type="text" id="customer_mobile" name="customer_mobile" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Phone</label>
                                            <input type="text" id="customer_phone" name="customer_phone" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" id="customer_address" name="customer_address" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">State</label>
                                            {!! Form::select('customer_state', $state_list, null, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">City</label>
                                            <select id="city_source" class="hide">
                                                @foreach($city_list as $state => $city)
                                                    <option value="{{ $city->CT_name }}" class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                @endforeach
                                            </select>
                                            <select name="customer_city" id="customer_city" class="form-control chosen-search-select" data-city="">
                                                <option value>Select</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Zip</label>
                                            <input type="text" id="customer_zip" name="customer_zip" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-30">Vehicle Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Vehicle Type</label>
                                            {!! Form::select('vehicle_type', ['FOUR_WHEELER' => 'Four Wheeler', 'TWO_WHEELER' => 'Two Wheeler'], null, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Year</label>
                                            <input type="text" id="vehicle_year" name="vehicle_year" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Engine Number</label>
                                            <input type="text" id="vehicle_engine_no" name="vehicle_engine_no" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Chassis Number</label>
                                            <input type="text" id="vehicle_chassis_no" name="vehicle_chassis_no" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Registration Number</label>
                                            <input type="text" id="vehicle_reg_no" name="vehicle_reg_no" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-30">Old Policy Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Company</label>
                                            {!! Form::select('old_policy_company', $company_list, null, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Policy Number</label>
                                            <input type="text" id="old_policy_number" name="old_policy_number" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Start Date</label>
                                            <input type="text" id="old_policy_start_date" name="old_policy_start_date" class="form-control date-picker" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Expiry Date</label>
                                            <input type="text" id="old_policy_end_date" name="old_policy_end_date" class="form-control date-picker" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-30">New Policy Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Company</label>
                                            {!! Form::select('new_policy_company', $company_list, null, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Product</label>
                                            {!! Form::select('new_policy_product', $product_list, null, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Estimated Premium</label>
                                            <input type="text" id="new_policy_estimated_premium" name="new_policy_estimated_premium" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Own Damage Premium</label>
                                            <input type="text" id="new_policy_od_premium" name="new_policy_od_premium" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Third Party Premium</label>
                                            <input type="text" id="new_policy_tp_premium" name="new_policy_tp_premium" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Tax</label>
                                            <input type="text" id="new_policy_tax_premium" name="new_policy_tax_premium" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Premium Amount</label>
                                            <input type="text" id="new_policy_total_premium" name="new_policy_total_premium" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Policy Number</label>
                                            <input type="text" id="new_policy_number" name="new_policy_number" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Start Date</label>
                                            <input type="text" id="new_policy_start_date" name="new_policy_start_date" class="form-control date-picker" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Expiry Date</label>
                                            <input type="text" id="new_policy_end_date" name="new_policy_end_date" class="form-control date-picker" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('status', $lead_status_list, 0, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-theme">Add</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
