@extends('layouts.app')

@section('head')
<style type="text/css">
    .form-control.uc{text-transform: uppercase;}
    .new-policy-attachment-container label.error{display: none !important;}
</style>
@endsection

@section('page-content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2 class="pull-left"><i class="fa fa-bicycle"></i>Lead (Health Insurance)</h2>
        <a href="{!! url('leads') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
        <div class="clearfix"></div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Edit Lead</h3>
                </div>
                <div class="pull-right">
                    <h3 class="panel-title">Dealer Wallet Balance: <span id="wallet-balance">{{ $dealerWalletBalance }}</span></h3>
                </div>
                <div class="clearfix"></div>
            </div> 
            <div class="panel-body no-padding">
                {!! Form::open(['url' => url('healthInsurance/update', ['id' => $lead_detail->L_id]), 'class' => 'form floating-label', 'method' => 'post', 'id' => 'lead_form', 'files' => true]) !!}
                <div class="form-body p-20">

                    <div class="row">
                        <div class="col-lg-6">
                            <input type="hidden" id="lead_id" name="lead_id" class="form-control uc" value="{{ $lead_detail->L_id }}" maxlength="50" />
                            <input type="hidden" id="id" name="id" class="form-control uc" value="{{ $lead_detail->healthInsurance->id }}" maxlength="50" />
                            <input type="hidden" id="id" name="agent_id" class="form-control uc" value="{{ $lead_detail->L_AGid }}" maxlength="50" />
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Paid By</label>
                                            <?php
                                            $paidByWhom = config('constant.PAID_BY_WHOM');
                                            $oldPaidByWhom = (!empty(old('paid_by_whom')) ? old('paid_by_whom') : $lead_detail->healthInsurance->paid_by_whom);
                                            ?>
                                            {!! Form::select('paid_by_whom', $paidByWhom, $lead_detail->healthInsurance->paid_by_whom, ['class' => 'form-control', 'id' => 'paid_by_whom']) !!}
                                            {!! $errors->first('paid_by_whom', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Type</label>
                                            <?php
                                            $policyType = array('' => 'Select Policy Type', 'NEW' => 'New', 'RENEWAL' => 'Renewal');
                                            $selectPolicyType = (!empty(old('policy_renewal_new')) ? old('policy_renewal_new') : $lead_detail->healthInsurance->policy_renewal_new);
                                            ?>
                                            {!! Form::select('policy_renewal_new', $policyType, $selectPolicyType, ['class' => 'form-control required', 'id' => 'policy_renewal_new']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Proposal Form</label>
                                            <div>
                                                <?php $proposalForm = (!empty(old('proposal_form')) ? old('proposal_form') : $lead_detail->healthInsurance->proposal_form); ?>
                                                <a href="{!! url('health-purpose-attachment', ['file' => $proposalForm]) !!}" target="_blank">
                                                    <img src="{!! url('health-purpose-attachment', ['file' => $proposalForm]) !!}" class="img-responsive" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Customer Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" id="customer_first_name" name="customer_first_name" class="form-control uc" value="{{ (!empty(old('customer_first_name'))?old('customer_first_name'):$lead_detail->L_customerFirstName) }}" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" id="customer_last_name" name="customer_last_name" class="form-control uc" value="{{ (!empty(old('customer_last_name'))?old('customer_last_name'):$lead_detail->L_customerLastName) }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" id="customer_email" name="customer_email" class="form-control uc" value="{{ (!empty(old('customer_last_name'))?old('customer_last_name'):$lead_detail->L_customerEmail) }}" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Date of Birth</label>
                                            <?php $customer_dob = (!empty(old('customer_dob')) ? old('customer_dob') : ($lead_detail->L_customerDOB ? Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_customerDOB)->format('d-m-Y') : '')); ?>
                                            <input type="text" id="customer_dob" name="customer_dob" class="form-control date-picker" value="{!! $customer_dob !!}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Mobile</label>
                                            <input type="text" id="customer_mobile" name="customer_mobile" class="form-control uc" value="{{ (!empty(old('customer_mobile'))?old('customer_mobile'):$lead_detail->L_customerMobile) }}" maxlength="10" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" id="customer_address" name="customer_address" class="form-control uc" value="{{ (!empty(old('customer_address'))?old('customer_address'):$lead_detail->L_customerAddress) }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">State</label>
                                            <?php $customerState = (!empty(old('customer_state')) ? old('customer_state') : $lead_detail->L_customerState); ?>
                                            {!! Form::select('customer_state', $state_list, $customerState, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">City</label>
                                            <?php
                                            $customerCity = (!empty(old('customer_city')) ? old('customer_city') : $lead_detail->L_customerCity);
                                            $cityList = array();
                                            foreach ($city_list as $state => $city) {
                                                $cityList[$city->CT_id] = $city->CT_name;
                                            }
                                            ?>
                                            {!! Form::select('customer_city', $cityList, $customerCity, ['class' => 'form-control chosen-search-select', 'id' => 'customer_city']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Zip</label>
                                            <input type="text" id="customer_zip" name="customer_zip" class="form-control uc" value="{{ (!empty(old('customer_zip'))?old('customer_zip'):$lead_detail->L_customerZip) }}" maxlength="7" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Insurance Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">To Whom You want to Cover</label>
                                            <?php
                                            $policyRelationShip = config('constant.policy_relationship');
                                            $policyRelationShip = array_combine($policyRelationShip, $policyRelationShip);
                                            ?>
                                            {!! Form::select('for_whom', $policyRelationShip, $lead_detail->healthInsurance->for_whom, ['class' => 'form-control chosen-search-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Age</th>
                                            <th>Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($lead_detail->healthInsurance->policy_holder_details)) {
                                            $members = json_decode($lead_detail->healthInsurance->policy_holder_details, true);
                                            foreach ($members as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><input type="text" id="customer_address" name='<?php echo "policy_holder_details[$key][name]"; ?>' class="form-control uc" value="{{ $val['name'] }}" /></td>
                                                    <td><input type="text" id="customer_address" name='<?php echo "policy_holder_details[$key][age]"; ?>' class="form-control uc" value="{{ $val['age'] }}" /></td>
                                                    <td>{!! Form::select("policy_holder_details[$key][gender]", array('Male'=>'Male', 'Female'=>'Female'), $val['gender'], ['class' => 'form-control chosen-search-select']) !!}</td>
                                                    <td>{!! Form::select("policy_holder_details[$key][type]", array('Self'=>'Self', 'Spouse'=>'Spouse', 'Child'=>'Child'), $val['type'], ['class' => 'form-control chosen-search-select']) !!}</td>
                                                </tr>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Resident State</label>
<?php $residentState = (!empty(old('resident_state')) ? old('resident_state') : $lead_detail->healthInsurance->resident_state); ?>
                                            {!! Form::select('resident_state', $state_list, $residentState, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Resident City</label>
                                            <?php
                                            $residentCity = (!empty(old('resident_city')) ? old('resident_city') : $lead_detail->healthInsurance->resident_city);
                                            $cityList = array();
                                            foreach ($city_list as $state => $city) {
                                                $cityList[$city->CT_id] = $city->CT_name;
                                            }
                                            ?>
                                            {!! Form::select('resident_city', $cityList, $residentCity, ['class' => 'form-control chosen-search-select', 'id' => 'customer_city']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Last Year Claim</label>
                                            {!! Form::select('last_year_claim', ['Yes' => 'Yes', 'No' => 'No'], (!empty(old('last_year_claim'))?old('last_year_claim'):$lead_detail->healthInsurance->last_year_claim), ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">PYP 1</label>
                                            <div>
<?php $old_policy_attachment1 = (!empty(old('old_policy_attachment1')) ? old('old_policy_attachment1') : $lead_detail->healthInsurance->old_policy_attachment1); ?>
                                                <a href="{!! url('health-attachment', ['file' => $old_policy_attachment1]) !!}" target="_blank">
                                                    <img src="{!! url('health-attachment', ['file' => $old_policy_attachment1]) !!}" class="img-responsive" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">PYP 2</label>
                                            <div>
<?php $old_policy_attachment2 = (!empty(old('old_policy_attachment2')) ? old('old_policy_attachment2') : $lead_detail->healthInsurance->old_policy_attachment2); ?>
                                                <a href="{!! url('health-attachment', ['file' => $old_policy_attachment2]) !!}" target="_blank">
                                                    <img src="{!! url('health-attachment', ['file' => $old_policy_attachment2]) !!}" class="img-responsive" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Quotations</h4>
                            <div class="form-group">
                                <div class="row quote-div">
                                    <div class="col-md-4">
                                        <a href="javascript:void(0)" class="input-group-addon btn btn-success add-quotation-upload">Add Quote Files</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-8 quotes">
                                        <div class="col-md-12 new-policy-attachment-container">
                                            <div class="control-group">
                                                <br/>
                                                <div class="fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="quotations[]" id=""></span>
                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if (!empty($lead_detail->healthInsurance->quotations)) {
                                        $quotations = json_decode($lead_detail->healthInsurance->quotations);
                                        foreach ($quotations as $key => $val) {
                                            ?>
                                            <div class="col-md-8">
                                                <div class="col-md-12 new-policy-attachment-container">
                                                    <div class="control-group">
        <?php echo ++$key; ?>) <a href="{!! url('quotes-attachment', ['file' => $val]) !!}" target="_blank">{{ $val }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
<?php if (!empty($lead_detail->healthInsurance->customer_selected_quote) && !empty($lead_detail->healthInsurance->customer_bank_cheque)) { ?>
                                    <br/><label>Customer Selected Quote & Cheque</label>
                                    <div class="control-group">
                                        1) <a href="{!! url('quotes-attachment', ['file' => $lead_detail->healthInsurance->customer_selected_quote]) !!}" target="_blank">Quote</a><br/>
                                        2) <a href="{!! url('motor-attachment', ['file' => $lead_detail->healthInsurance->customer_bank_cheque]) !!}" target="_blank">Cheque</a>
                                    </div>
<?php } ?>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Insurance Company</label>
                                            {!! Form::select('new_policy_company', $company_list, (!empty(old('new_policy_company'))?old('new_policy_company'):$lead_detail->healthInsurance->new_policy_company), ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                            {!! $errors->first('new_policy_company', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Product</label>
                                            {!! Form::select('new_policy_product', $product_list, (!empty(old('new_policy_product'))?old('new_policy_product'):$lead_detail->healthInsurance->new_policy_product), ['class' => 'form-control chosen-search-select']) !!}
                                            {!! $errors->first('new_policy_product', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Sum Insured</label>
                                            <input type="text" id="vehicle_sgst" name="sum_insured" class="form-control focus-select uc" value="{{ (!empty(old('sum_insured'))?old('sum_insured'):$lead_detail->healthInsurance->sum_insured) }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Estimated Premium</label>
                                            <input type="text" id="vehicle_sgst" name="estimated_premium" class="form-control focus-select uc" value="{{ (!empty(old('estimated_premium'))?old('estimated_premium'):$lead_detail->healthInsurance->estimated_premium) }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Net Premium</label>
                                            <input type="text" id="net_premium" name="net_premium" class="form-control focus-select uc" value="{{ (!empty(old('net_premium'))?old('net_premium'):$lead_detail->healthInsurance->net_premium) }}" />
                                            {!! $errors->first('net_premium', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">GST</label>
                                            <input type="text" id="gst" name="gst" class="form-control focus-select uc" value="{{ (!empty(old('gst'))?old('gst'):$lead_detail->healthInsurance->gst) }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Gross Premium</label>
                                            <input type="text" id="gross_premium" name="gross_premium" class="form-control focus-select uc" value="{{ (!empty(old('gross_premium'))?old('gross_premium'):$lead_detail->healthInsurance->gross_premium) }}" /><span style="font-weight:bold;color:red" id="infoTxt"></span>
                                            {!! $errors->first('gross_premium', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Remark</label>
                                            <textarea id="remark" name="remark" class="form-control uc">{{ (!empty(old('remark'))?old('remark'):$lead_detail->healthInsurance->remark) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Number</label>
                                            <input type="text" id="new_policy_number" name="new_policy_number" class="form-control uc" value="{{ (!empty(old('new_policy_number'))?old('new_policy_number'):$lead_detail->healthInsurance->new_policy_number) }}" maxlength="50" />
                                            {!! $errors->first('new_policy_number', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Start Date</label>
<?php $new_policy_start_date = (!empty(old('new_policy_start_date')) ? old('new_policy_start_date') : ($lead_detail->healthInsurance->new_policy_start_date ? Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->healthInsurance->new_policy_start_date)->format('d-m-Y') : '')); ?>
                                            <input type="text" id="new_policy_start_date" name="new_policy_start_date" class="form-control" value="{!! $new_policy_start_date !!}" autocomplete="off" />
                                            {!! $errors->first('new_policy_start_date', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Expiry Date</label>
<?php $new_policy_end_date = (!empty(old('new_policy_end_date')) ? old('new_policy_end_date') : ($lead_detail->healthInsurance->new_policy_end_date ? Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->healthInsurance->new_policy_end_date)->format('d-m-Y') : '')); ?>
                                            <input type="text" id="new_policy_end_date" name="new_policy_end_date" class="form-control date-picker" value="{!! $new_policy_end_date !!}" autocomplete="off" />
                                            {!! $errors->first('new_policy_end_date', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('status', $lead_status_list, $lead_detail->healthInsurance->status, ['class' => 'form-control chosen-select', 'id' => 'lead_status']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 new-policy-attachment-container">
                                        <div class="control-group">
                                            <label class="control-label">New Policy</label>
                                            <div class="fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="new_policy_attachment" id="new_policy_attachment"></span>
                                                <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                            <a href="{!! url('policies-attachment', ['file' => $lead_detail->healthInsurance->policy_attachment]) !!}" target="_blank">{{ $lead_detail->healthInsurance->policy_attachment }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($lead_detail->L_completedAt)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Complete Date</label>
                                            <div class="text-strong lh-30">{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead_detail->L_completedAt)->format('d-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($lead_detail->L_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($lead_detail->L_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <button type="button" class="btn btn-theme btn-submit">Update</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div><!-- /.body-content -->
    <!--/ End body content -->

</section><!-- /#page-content -->
@endsection

@section('bottom')
<script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection
