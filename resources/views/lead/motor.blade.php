@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
        .new-policy-attachment-container label.error{display: none !important;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-car"></i>Lead (Motor)</h2>
            @if(check_permission('LEAD_CREATE', Auth::user()->A_type))
                <a href="{!! url('lead/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Lead</a>
            @endif
            <a href="{!! url('leads') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Edit Lead</h3>
                    </div>
                    <div class="pull-right">
                        <h3 class="panel-title">Dealer Wallet Balance: <span id="wallet-balance">{{ $dealerWalletBalance }}</span></h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('motorInsurance/update', ['id' => $lead_detail->L_id]), 'class' => 'form floating-label', 'method' => 'post', 'id' => 'lead_form', 'files' => true]) !!}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <input type="hidden" id="lead_id" name="lead_id" class="form-control uc" value="{{ $lead_detail->L_id }}" maxlength="50" />
                        <input type="hidden" id="id" name="id" class="form-control uc" value="{{ $lead_detail->motorInsurance->id }}" maxlength="50" />
                        <input type="hidden" id="id" name="agent_id" class="form-control uc" value="{{ $lead_detail->L_AGid }}" maxlength="50" />
                        <div class="form-body p-20">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Paid By</label>
                                                    <?php $paidByWhom = config('constant.PAID_BY_WHOM');
                                                    $oldPaidByWhom = (!empty(old('paid_by_whom'))?old('paid_by_whom'):$lead_detail->motorInsurance->paid_by_whom);
                                                    ?>
                                                    {!! Form::select('paid_by_whom', $paidByWhom, $oldPaidByWhom, ['class' => 'form-control', 'id' => 'paid_by_whom']) !!}
                                                    {!! $errors->first('paid_by_whom', '<p class="help-block">:message</p>') !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="mt-30">Customer Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">First Name</label>
                                                    
                                                    <input type="text" id="customer_first_name" name="customer_first_name" class="form-control uc" value="{{ (!empty(old('customer_first_name'))?old('customer_first_name'):$lead_detail->L_customerFirstName) }}" maxlength="50" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Last Name</label>
                                                    <input type="text" id="customer_last_name" name="customer_last_name" class="form-control uc" value="{{ (!empty(old('customer_last_name'))?old('customer_last_name'):$lead_detail->L_customerLastName) }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Email</label>
                                                    <input type="text" id="customer_email" name="customer_email" class="form-control uc" value="{{ (!empty(old('customer_email'))?old('customer_email'):$lead_detail->L_customerEmail) }}" maxlength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Date of Birth</label>
                                                    <?php $customer_dob = (!empty(old('customer_dob'))?old('customer_dob'):($lead_detail->L_customerDOB?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_customerDOB)->format('d-m-Y'):'')); ?>
                                                    <input type="text" id="customer_dob" name="customer_dob" class="form-control date-picker" value="{!! $customer_dob !!}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Mobile</label>
                                                    <input type="text" id="customer_mobile" name="customer_mobile" class="form-control uc" value="{{ (!empty(old('customer_mobile'))?old('customer_mobile'):$lead_detail->L_customerMobile) }}" maxlength="10" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Phone</label>
                                                    <input type="text" id="customer_phone" name="customer_phone" class="form-control uc" value="{{ (!empty(old('customer_phone'))?old('customer_phone'):$lead_detail->L_customerPhone) }}" maxlength="12" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="control-group">
                                                    <label class="control-label">Address</label>
                                                    <input type="text" id="customer_address" name="customer_address" class="form-control uc" value="{{ (!empty(old('customer_address'))?old('customer_address'):$lead_detail->L_customerAddress) }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="control-group">
                                                    <label class="control-label">State</label>
                                                    <?php $customerState = (!empty(old('customer_state'))?old('customer_state'):$lead_detail->L_customerState); ?>
                                                    {!! Form::select('customer_state', $state_list, $customerState, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="control-group">                                                   
                                                    <div class="control-group">
                                                        <label class="control-label">City</label>
                                                        <?php $customerCity = (!empty(old('customer_city'))?old('customer_city'):$lead_detail->L_customerCity); 
                                                        $cityList = array();
                                                        foreach($city_list as $state => $city) {
                                                            $cityList[$city->CT_id] = $city->CT_name;
                                                        }
                                                        ?>
                                                        {!! Form::select('customer_city', $cityList, $customerCity, ['class' => 'form-control chosen-search-select', 'id' => 'customer_city']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="control-group">
                                                    <label class="control-label">Zip</label>
                                                    <input type="text" id="customer_zip" name="customer_zip" class="form-control uc" value="{{ (!empty(old('customer_zip'))?old('customer_zip'):$lead_detail->L_customerZip) }}" maxlength="7" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="mt-30">Vehicle Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle Type</label>
                                                    <?php
                                                    $vehicleTypes[] = "Select an option";
                                                    $vehicleMake[] = "Select an option";
                                                    $vehicleModel[] = "Select an option";
                                                    foreach($vehicle_type as $val) {
                                                        $vehicleTypes[$val['id']] = $val['name'];
                                                    }
                                                    foreach($vehicle_make as $val) {
                                                        $vehicleMake[$val['id']] = $val['make'];
                                                    }
                                                    foreach($vehicle_model as $val) {
                                                        $vehicleModel[$val['id']] = $val['model'];
                                                    }
                                                    ?>
                                                    {!! Form::select('vehicle_type', $vehicleTypes, $lead_detail->motorInsurance->vehicle_type, ['class' => 'form-control chosen-select', 'id'=>'vehicle_type']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle Make</label>
                                                    {!! Form::select('vehicle_make', $vehicleMake, $lead_detail->motorInsurance->vehicle_make, ['class' => 'form-control', 'id'=>'vehicle_make']) !!}
<!--                                                    <input type="text" id="vehicle_make" name="vehicle_make" class="form-control uc" value="{{ (!empty(old('vehicle_make'))?old('vehicle_make'):$lead_detail->motorInsurance->vehicle_make) }}" maxlength="50" />-->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle Model</label>
                                                    {!! Form::select('vehicle_model', $vehicleModel, $lead_detail->motorInsurance->vehicle_model, ['class' => 'form-control', 'id'=>'vehicle_model']) !!}
                                                    <!--<input type="text" id="vehicle_model" name="vehicle_model" class="form-control uc" value="{{ (!empty(old('vehicle_model'))?old('vehicle_model'):$lead_detail->motorInsurance->vehicle_model) }}" maxlength="50" />-->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle Year</label>
                                                    <input type="text" id="vehicle_year" name="vehicle_year" class="form-control uc" value="{{ (!empty(old('vehicle_year'))?old('vehicle_year'):$lead_detail->motorInsurance->vehicle_year) }}" maxlength="50" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Engine Number</label>
                                                    <input type="text" id="vehicle_engine_no" name="vehicle_engine_no" class="form-control uc" value="{{ (!empty(old('vehicle_engine_no'))?old('vehicle_engine_no'):$lead_detail->motorInsurance->vehicle_engine_no) }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Chassis Number</label>
                                                    <input type="text" id="vehicle_chassis_no" name="vehicle_chassis_no" class="form-control uc" value="{{ (!empty(old('vehicle_chassis_no'))?old('vehicle_chassis_no'):$lead_detail->motorInsurance->vehicle_chassis_no) }}" maxlength="50" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Registration Number</label>
                                                    <input type="text" id="vehicle_reg_no" name="vehicle_reg_no" class="form-control uc" value="{{ (!empty(old('vehicle_reg_no'))?old('vehicle_reg_no'):$lead_detail->motorInsurance->vehicle_reg_no) }}" maxlength="12" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            {{--<div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">DL</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->dl_photo]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->dl_photo]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>--}}
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">RC</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->rc_photo]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->rc_photo]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">PUC</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->puc_photo]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->puc_photo]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Policy</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' =>  $lead_detail->motorInsurance->policy_photo]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' =>  $lead_detail->motorInsurance->policy_photo]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <?php if(!empty($lead_detail->motorInsurance->vehicle_photo1)) { ?>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 1</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo1]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo1]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } if(!empty($lead_detail->motorInsurance->vehicle_photo2)) { ?>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 2</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo2]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo2]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } if(!empty($lead_detail->motorInsurance->vehicle_photo3)) { ?>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 3</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo3]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo3]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } if(!empty($lead_detail->motorInsurance->vehicle_photo4)) { ?>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 4</label>
                                                    <div>
                                                        <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo4]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->vehicle_photo4]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <h4 class="mt-30">Old Policy Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Company</label>
                                                    {!! Form::select('old_policy_company', $company_list, $lead_detail->motorInsurance->old_policy_company, ['class' => 'form-control chosen-search-select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Policy Number</label>
                                                    <input type="text" id="old_policy_number" name="old_policy_number" class="form-control uc" value="{{ (!empty(old('old_policy_number'))?old('old_policy_number'):$lead_detail->motorInsurance->old_policy_number) }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Start Date</label>
                                                    <?php $old_policy_start_date = (!empty(old('old_policy_start_date'))?old('old_policy_start_date'):($lead_detail->motorInsurance->old_policy_start_date?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->motorInsurance->old_policy_start_date)->format('d-m-Y'):'')); ?>
                                                    <input type="text" id="old_policy_start_date" name="old_policy_start_date" class="form-control date-picker" value="{!! $old_policy_start_date !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Expiry Date</label>
                                                    <?php $old_policy_end_date = (!empty(old('old_policy_end_date'))?old('old_policy_end_date'):($lead_detail->motorInsurance->old_policy_end_date?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->motorInsurance->old_policy_end_date)->format('d-m-Y'):'')); ?>
                                                    <input type="text" id="old_policy_end_date" name="old_policy_end_date" class="form-control date-picker uc" value="{!! $old_policy_end_date !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Last Year Claim</label>
                                                    {!! Form::select('last_year_claim', ['Yes' => 'Yes', 'No' => 'No'], (!empty(old('last_year_claim'))?old('last_year_claim'):$lead_detail->motorInsurance->last_year_claim), ['class' => 'form-control chosen-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">PYP 1</label>
                                                    <div>
                                                        <?php $old_policy_attachment1 = (!empty(old('old_policy_attachment1'))?old('old_policy_attachment1'):$lead_detail->motorInsurance->old_policy_attachment1); ?>
                                                        <a href="{!! url('motor-attachment', ['file' => $old_policy_attachment1]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $old_policy_attachment1]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">PYP 2</label>
                                                    <div>
                                                        <?php $old_policy_attachment2 = (!empty(old('old_policy_attachment2'))?old('old_policy_attachment2'):$lead_detail->motorInsurance->old_policy_attachment2); ?>
                                                        <a href="{!! url('motor-attachment', ['file' => $old_policy_attachment2]) !!}" target="_blank">
                                                            <img src="{!! url('motor-attachment', ['file' => $old_policy_attachment2]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="mt-30">Quotations</h4>
                                    <div class="form-group">
                                        <div class="row quote-div">
                                            <div class="col-md-4">
                                                <a href="javascript:void(0)" class="input-group-addon btn btn-success add-quotation-upload">Add Quote Files</a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-8 quotes">
                                                <div class="col-md-12 new-policy-attachment-container">
                                                    <div class="control-group">
                                                        <br/>
                                                        <div class="fileinput-new input-group" data-provides="fileinput">
                                                            <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                            <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="quotations[]" id=""></span>
                                                            <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php 
                                            if(!empty($lead_detail->motorInsurance->quotations)) {
                                                $quotations = json_decode($lead_detail->motorInsurance->quotations);
                                                foreach($quotations as $key => $val) { ?>
                                                <div class="col-md-8">
                                                    <div class="col-md-12 new-policy-attachment-container">
                                                        <div class="control-group">
                                                            <?php echo ++$key; ?>) <a href="{!! url('quotes-attachment', ['file' => $val]) !!}" target="_blank">{{ $val }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } } ?>
                                        </div>
                                        <?php
                                        if(!empty($lead_detail->motorInsurance->customer_selected_quote) && !empty($lead_detail->motorInsurance->customer_bank_cheque)) { ?>
                                        <br/><label>Customer Selected Quote & Cheque</label>
                                        <div class="control-group">
                                            1) <a href="{!! url('quotes-attachment', ['file' => $lead_detail->motorInsurance->customer_selected_quote]) !!}" target="_blank">Quote</a><br/>
                                            2) <a href="{!! url('motor-attachment', ['file' => $lead_detail->motorInsurance->customer_bank_cheque]) !!}" target="_blank">Cheque</a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <h4 class="mt-30">New Policy Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Company</label>
                                                    
                                                    {!! Form::select('new_policy_company', $company_list, (!empty(old('new_policy_company'))?old('new_policy_company'):$lead_detail->motorInsurance->new_policy_company), ['class' => 'form-control chosen-search-select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Product</label>
                                                    {!! Form::select('new_policy_product', $product_list, (!empty(old('new_policy_product'))?old('new_policy_product'):$lead_detail->motorInsurance->new_policy_product), ['class' => 'form-control chosen-search-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Estimated Premium</label>
                                                    <input type="text" id="new_policy_estimated_premium" name="new_policy_estimated_premium" class="form-control focus-select uc" value="{{ (!empty(old('new_policy_estimated_premium'))?old('new_policy_estimated_premium'):$lead_detail->motorInsurance->new_policy_estimated_premium) }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Own Damage Premium</label>
                                                    <input type="number" id="new_policy_od_premium" name="new_policy_od_premium" class="form-control focus-select uc" value="{{ (!empty(old('new_policy_od_premium'))?old('new_policy_od_premium'):$lead_detail->motorInsurance->new_policy_od_premium) }}" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Third Party Premium</label>
                                                    <input type="number" id="new_policy_tp_premium" name="new_policy_tp_premium" class="form-control focus-select uc" value="{{ (!empty(old('new_policy_tp_premium'))?old('new_policy_tp_premium'):$lead_detail->motorInsurance->new_policy_tp_premium) }}" />
                                                    {!! $errors->first('new_policy_tp_premium', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Tax</label>
                                                    <input type="number" id="new_policy_tax_premium" name="new_policy_tax_premium" class="form-control focus-select uc" value="{{ (!empty(old('new_policy_tax_premium'))?old('new_policy_tax_premium'):$lead_detail->motorInsurance->new_policy_tax_premium) }}" />
                                                    {!! $errors->first('new_policy_tax_premium', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Premium Amount</label>
                                                    <input type="number" id="new_policy_total_premium" name="new_policy_total_premium" class="form-control focus-select uc" value="{{ (!empty(old('new_policy_total_premium'))?old('new_policy_total_premium'):$lead_detail->motorInsurance->new_policy_total_premium) }}" /><span style="font-weight:bold;color:red" id="infoTxt"></span>
                                                    {!! $errors->first('new_policy_total_premium', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Policy Number</label>
                                                    <input type="text" id="new_policy_number" name="new_policy_number" class="form-control uc" value="{{  (!empty(old('new_policy_number'))?old('new_policy_number'):$lead_detail->motorInsurance->new_policy_number) }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php // exit; ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Start Date</label>
                                                    <?php $new_policy_start_date = (!empty(old('new_policy_start_date'))?old('new_policy_start_date'):($lead_detail->motorInsurance->new_policy_start_date?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->motorInsurance->new_policy_start_date)->format('d-m-Y'):'')); ?>
                                                    <input type="text" id="new_policy_start_date" name="new_policy_start_date" class="form-control" value="{!! $new_policy_start_date !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Expiry Date</label>
                                                    <?php $new_policy_end_date = (!empty(old('new_policy_end_date'))?old('new_policy_end_date'):($lead_detail->motorInsurance->new_policy_end_date?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->motorInsurance->new_policy_end_date)->format('d-m-Y'):'')); ?>
                                                    <input type="text" id="new_policy_end_date" name="new_policy_end_date" class="form-control date-picker" value="{!! $new_policy_end_date !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Status</label>
                                                    {!! Form::select('status', $lead_status_list, (!empty(old('status'))?old('status'):$lead_detail->motorInsurance->status), ['class' => 'form-control chosen-select', 'id' => 'lead_status']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 new-policy-attachment-container">
                                                <div class="control-group">
                                                    <label class="control-label">New Policy</label>
                                                    <div class="fileinput-new input-group" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="new_policy_attachment" id="new_policy_attachment"></span>
                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                    <a href="{!! url('policies-attachment', ['file' => $lead_detail->motorInsurance->new_policy_attachment]) !!}" target="_blank">{{ $lead_detail->motorInsurance->new_policy_attachment }}</a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Remark</label>
                                                    <textarea id="remark" name="remark" class="form-control uc">{{ (!empty(old('remark'))?old('remark'):$lead_detail->motorInsurance->remark) }}"</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($lead_detail->L_completedAt)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="control-group">
                                                        <label class="control-label">Complete Date</label>
                                                        <div class="text-strong lh-30">{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead_detail->L_completedAt)->format('d-M-Y h:i A') !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Create Date</label>
                                                    <div class="text-strong lh-30">{!! format_date($lead_detail->L_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Update Date</label>
                                                    <div class="text-strong lh-30">{!! format_date($lead_detail->L_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if((check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead_detail->L_status < config('constant.lead_status.COMPLETED.value')) || check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                            <div class="form-footer">
                                @if(check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead_detail->L_status < config('constant.lead_status.COMPLETED.value'))
                                    <button type="button" class="btn btn-theme btn-submit">Update</button>
                                @endif
                                @if(check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                                    <button type="button" data-target="{!! url('messages', ['lead_id' => $lead_detail->L_id]) !!}" class="btn btn-theme btn-view-messages">View Messages</button>
                                @endif
                            </div>
                        @endif
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection

