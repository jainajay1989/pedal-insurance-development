@extends('layouts.app')

@section('head')
<style type="text/css">
    .form-control.uc{text-transform: uppercase;}
    .new-policy-attachment-container label.error{display: none !important;}
</style>
@endsection

@section('page-content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2 class="pull-left"><i class="fa fa-bicycle"></i>Lead (Pedal Cycle)</h2>
        @if(check_permission('LEAD_CREATE', Auth::user()->A_type))
        <a href="{!! url('lead/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Lead</a>
        @endif
        <a href="{!! url('leads') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
        <div class="clearfix"></div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Edit Lead</h3>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body no-padding">
                {!! Form::open(['url' => url('lead/update', ['id' => $lead_detail->L_id]), 'class' => 'form floating-label', 'method' => 'post', 'id' => 'lead_form', 'files' => true]) !!}
                <div class="form-body p-20">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Agent</label>
                                            <div class="text-strong lh-30">{!! $agent_list[$lead_detail->L_AGid] !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-30">Customer Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" id="customer_first_name" name="customer_first_name" class="form-control uc" value="{{ $lead_detail->L_customerFirstName }}" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" id="customer_last_name" name="customer_last_name" class="form-control uc" value="{{ $lead_detail->L_customerLastName }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" id="customer_email" name="customer_email" class="form-control uc" value="{{ $lead_detail->L_customerEmail }}" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Date of Birth</label>
                                            <?php $dob = date('d/m/Y', strtotime($lead_detail->L_customerDOB)); ?>
                                            <input type="text" id="customer_dob" name="customer_dob" class="form-control" value="{{ $dob }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Mobile</label>
                                            <input type="text" id="customer_mobile" name="customer_mobile" class="form-control uc" value="{{ $lead_detail->L_customerMobile }}" maxlength="10" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Phone</label>
                                            <input type="text" id="customer_phone" name="customer_phone" class="form-control uc" value="{{ $lead_detail->L_customerPhone }}" maxlength="12" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" id="customer_address" name="customer_address" class="form-control uc" value="{{ $lead_detail->L_customerAddress }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">State</label>
                                            {!! Form::select('customer_state', $state_list, $lead_detail->L_customerState, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">City</label>
                                            <select id="city_source" name="customer_city" class="form-control chosen-search-select">
                                                @foreach($city_list as $state => $city)
                                                @if($city->CT_id == $lead_detail->L_customerCity)
                                                <option value="{{ $city->CT_id }}" selected class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                @else
                                                <option value="{{ $city->CT_id }}" class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                @endif;
                                                @endforeach
                                            </select>
<!--                                            <select name="customer_city" id="customer_city" class="form-control chosen-search-select" data-city="{{ $lead_detail->L_customerCity }}">
                                                <option value>Select</option>
                                            </select>-->
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Zip</label>
                                            <input type="text" id="customer_zip" name="customer_zip" class="form-control uc" value="{{ $lead_detail->L_customerZip }}" maxlength="7" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-30">Cycle Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Make</label>
<!--                                                    <input type="text" id="vehicle_make" name="vehicle_make" class="form-control uc" value="{{ $lead_detail->L_vehicleMake }}" maxlength="50" />-->
                                            <select id="vehicle_make" class='form-control chosen-search-select'>
                                                @foreach($make_list as $key => $val)
                                                @if($val->V_code == $lead_detail->L_vehicleMake)
                                                <option value="{{ $val->V_code }}" selected class="{{ $val->V_make }}">{{ $val->V_make }}</option>
                                                @else
                                                <option value="{{ $val->V_code }}" class="{{ $val->V_make }}">{{ $val->V_make }}</option>
                                                @endif;
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Model</label>
<!--                                                    <input type="text" id="vehicle_model" name="vehicle_model" class="form-control uc" value="{{ $lead_detail->L_vehicleModel }}" maxlength="50" />-->
                                            <select id="vehicle_make" class='form-control chosen-search-select'>
                                                @foreach($model_list as $key => $val)
                                                @if($val->V_id == $lead_detail->L_vehicleModel)
                                                <option value="{{ $val->V_id }}" selected class="{{ $val->V_model }}">{{ $val->V_model }}</option>
                                                @else
                                                <option value="{{ $val->V_id }}" class="{{ $val->V_model }}">{{ $val->V_model }}</option>
                                                @endif;
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Chassis Number</label>
                                            <input type="text" id="vehicle_chassis_no" name="vehicle_chassis_no" class="form-control uc" value="{{ $lead_detail->L_vehicleChassisNo }}" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Year of Manufacture</label>
                                            <input type="text" id="vehicle_chassis_no" name="vehicle_year" class="form-control uc" value="{{ $lead_detail->L_vehicleYear }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $accessories = json_decode($lead_detail->L_vehicleAccessoryDetails, true); 
                            if(!empty($accessories)) {
                            ?>
                            <h4 class="mt-30">Accessories</h4>
                            <div class="form-group">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Detail</th>
                                            <th>Price</th>
                                            <th>Invoice</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($accessories as $key => $val) { ?>
                                            <tr>
                                                <td><?= $val['accessory_type']; ?></td>
                                                <td><?= $val['accessory_detail']; ?></td>
                                                <td><?= $val['accessory_price']; ?></td>
                                                <td>
                                                <a href="{!! url('lead-attachment', ['file' => $val['accessory_image']]) !!}" target="_blank"><img src="{!! url('lead-attachment', ['file' => $val['accessory_image']]) !!}" class="img-thumbnail" /></a>
                                                </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Net Price</label>
                                            <input type="text" id="vehicle_net_price" name="vehicle_net_price" class="form-control focus-select uc" value="{{ $lead_detail->L_vehicleNetPrice }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <?php if($lead_detail->L_vehicleSGST != "0.00" && $lead_detail->L_vehicleCGST != "0.00") { ?>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">SGST</label>
                                            <input type="text" id="vehicle_sgst" name="vehicle_sgst" class="form-control focus-select uc" value="{{ $lead_detail->L_vehicleSGST }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">CGST</label>
                                            <input type="text" id="vehicle_cgst" name="vehicle_cgst" class="form-control focus-select uc" value="{{ $lead_detail->L_vehicleCGST }}" />
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($lead_detail->L_vehicleIGST != "0.00") { ?>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">IGST</label>
                                            <input type="text" id="vehicle_cgst" name="vehicle_cgst" class="form-control focus-select uc" value="{{ $lead_detail->L_vehicleIGST }}" />
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Gross Price</label>
                                            <input type="text" id="vehicle_gross_price" name="vehicle_gross_price" class="form-control focus-select uc" value="{{ $lead_detail->L_vehicleGrossPrice }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Invoice</label>
                                            <div>
                                                <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_vehicleInvoicePhoto]) !!}" target="_blank">
                                                    <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_vehicleInvoicePhoto]) !!}" class="img-responsive" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Chassis</label>
                                            <div>
                                                <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_vehicleChassisPhoto]) !!}" target="_blank">
                                                    <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_vehicleChassisPhoto]) !!}" class="img-responsive" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Add On Cover</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Tenure</label>
                                            {!! Form::select('policy_tenure', array(1=>1,3=>3), $lead_detail->L_newPolicyCoverage, ['class' => 'form-control chosen-search-select', 'id' => 'policy_tenure']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Basic Cover</label>
                                            <div class="text-strong lh-30">{{ $lead_detail->L_newPolicyBasicCover }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            $completeDetails = $lead_detail->L_completeLeadDetails; 
                            $completeDetails = json_decode($completeDetails);
                            ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">PA Cover</label>
                                            <?php $option = array(1=>'Yes', 0=>'No'); ?>
                                            <div class="text-strong lh-30">{{$option[$lead_detail->L_newPolicyAD]}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">PA Insured</label>
                                            <div class="text-strong lh-30">{{$lead_detail->L_newPolicyADCover}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">PA Premium</label>
                                            <div class="text-strong lh-30">{{$lead_detail->L_newPolicyADCoverPremium}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Nominee Name</label>
                                            <?php $option = array(1=>'Yes', 0=>'No'); ?>
                                            <div class="text-strong lh-30">{{$completeDetails->NomineeFirstName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Nominee Relationship</label>
                                            <div class="text-strong lh-30">{{$completeDetails->NomineeRelationship}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Guardian Name</label>
                                            <div class="text-strong lh-30">{{(!empty($completeDetails->RepFirstName)?$completeDetails->RepFirstName:'-')}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Public Liability</label>
                                            <?php $option = array(1=>'Yes', 0=>'No'); ?>
                                            <div class="text-strong lh-30">{{$option[$lead_detail->L_newPolicyPL]}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">PA Insured</label>
                                            <div class="text-strong lh-30">{{$lead_detail->L_newPolicyPLCover}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">PA Premium</label>
                                            <div class="text-strong lh-30">{{$lead_detail->L_newPolicyPLPremium}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Insurance Detail</h4>
                            <div class="form-group hide">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Company</label>
                                            {!! Form::select('new_policy_company', $company_list, $lead_detail->L_newPolicyCompany, ['class' => 'form-control chosen-search-select']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Product</label>
                                            {!! Form::select('new_policy_product', $product_list, $lead_detail->L_newPolicyProduct, ['class' => 'form-control chosen-search-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Net Premium</label>
                                            <input type="text" id="new_policy_od_premium" name="new_policy_od_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyNetPremium }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <?php if($lead_detail->L_newPolicyCGSTPremium != "0.00" && $lead_detail->L_newPolicyCGSTPremium != "0.00") { ?>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">CGST</label>
                                            <input type="text" id="new_policy_cgst_premium" name="new_policy_cgst_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyCGSTPremium }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">SGST</label>
                                            <input type="text" id="new_policy_sgst_premium" name="new_policy_sgst_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyCGSTPremium }}" />
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($lead_detail->L_newPolicyIGSTPremium != "0.00") { ?>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">IGST</label>
                                            <input type="text" id="new_policy_igst_premium" name="new_policy_igst_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyIGSTPremium }}" />
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Gross Premium</label>
                                            <input type="text" id="new_policy_total_premium" name="new_policy_total_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyTotalPremium }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Number</label>
                                            <input type="text" id="new_policy_number" name="new_policy_number" class="form-control uc" value="{{ $lead_detail->L_newPolicyNumber }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Start Date</label>
                                            <input type="text" id="new_policy_start_date" name="new_policy_start_date" class="form-control" value="{!! ($lead_detail->L_newPolicyStartDate?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_newPolicyStartDate)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Expiry Date</label>
                                            <input type="text" id="new_policy_end_date" name="new_policy_end_date" class="form-control date-picker" value="{!! ($lead_detail->L_newPolicyEndDate?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_newPolicyEndDate)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('status', $lead_status_list, $lead_detail->L_status, ['class' => 'form-control chosen-select', 'id' => 'lead_status']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($lead_detail->L_completedAt)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Complete Date</label>
                                            <div class="text-strong lh-30">{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead_detail->L_completedAt)->format('d-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($lead_detail->L_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($lead_detail->L_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($lead_detail->L_location)
                        <div class="col-lg-6">
                            <iframe src="https://www.google.com/maps/embed/v1/place?q={{ $lead_detail->L_location }}&key=AIzaSyDlTOadACwWyMDCk2IOubjVwm_x5hN2yLY" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        @endif
                    </div>
                </div>
                @if((check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead_detail->L_status < config('constant.lead_status.COMPLETED.value')) || check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                <div class="form-footer">
                    @if(check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead_detail->L_status < config('constant.lead_status.COMPLETED.value'))
                    <button type="button" class="btn btn-theme btn-submit">Update</button>
                    @endif
                    @if(check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                    <button type="button" data-target="{!! url('messages', ['lead_id' => $lead_detail->L_id]) !!}" class="btn btn-theme btn-view-messages">View Messages</button>
                    @endif
                </div>
                @endif
                {!! Form::close() !!}
            </div>
        </div>

    </div><!-- /.body-content -->
    <!--/ End body content -->

</section><!-- /#page-content -->
@endsection

@section('bottom')
<script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection
