@extends('layouts.app')

@section('head')
<style type="text/css">
    .form-control.uc{text-transform: uppercase;}
    .new-policy-attachment-container label.error{display: none !important;}
</style>
@endsection

@section('page-content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2 class="pull-left"><i class="fa fa-bicycle"></i>Lead (Other Insurance)</h2>
        <a href="{!! url('leads') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
        <div class="clearfix"></div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Edit Lead</h3>
                </div>
                <div class="pull-right">
                    <h3 class="panel-title">Dealer Wallet Balance: <span id="wallet-balance">{{ $dealerWalletBalance }}</span></h3>
                </div>
                <div class="clearfix"></div>
            </div> 
            <div class="panel-body no-padding">
                {!! Form::open(['url' => url('otherInsurance/update', ['id' => $lead_detail->L_id]), 'class' => 'form floating-label', 'method' => 'post', 'id' => 'lead_form', 'files' => true]) !!}
                <div class="form-body p-20">
                   
                    <div class="row">
                        <div class="col-lg-6">
<!--                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Agent</label>
                                            <div class="text-strong lh-30">{!! $agent_list[$lead_detail->L_AGid] !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <input type="hidden" id="lead_id" name="lead_id" class="form-control uc" value="{{ $lead_detail->L_id }}" maxlength="50" />
                            <input type="hidden" id="id" name="id" class="form-control uc" value="{{ $lead_detail->otherInsurance->id }}" maxlength="50" />
                            <input type="hidden" id="id" name="agent_id" class="form-control uc" value="{{ $lead_detail->L_AGid }}" maxlength="50" />
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Paid By</label>
                                            <?php $paidByWhom = array(''=>'Select Payer', 'PAID_BY_DEALER'=>'Paid By Dealer', 'PAID_BY_CUSTOMER'=>'Paid By Customer'); ?>
                                            {!! Form::select('paid_by_whom', $paidByWhom, $lead_detail->otherInsurance->paid_by_whom, ['class' => 'form-control required', 'id' => 'paid_by_whom']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Customer Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" id="customer_first_name" name="customer_first_name" class="form-control uc" value="{{ $lead_detail->L_customerFirstName }}" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" id="customer_last_name" name="customer_last_name" class="form-control uc" value="{{ $lead_detail->L_customerLastName }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" id="customer_email" name="customer_email" class="form-control uc" value="{{ $lead_detail->L_customerEmail }}" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Date of Birth</label>
                                            <input type="text" id="customer_dob" name="customer_dob" class="form-control date-picker" value="{!! ($lead_detail->L_customerDOB?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_customerDOB)->format('d-m-Y'):'') !!}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Mobile</label>
                                            <input type="text" id="customer_mobile" name="customer_mobile" class="form-control uc" value="{{ $lead_detail->L_customerMobile }}" maxlength="10" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" id="customer_address" name="customer_address" class="form-control uc" value="{{ $lead_detail->L_customerAddress }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">State</label>
                                            {!! Form::select('customer_state', $state_list, $lead_detail->L_customerState, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">City</label>
                                            <select name="customer_city" class="form-control chosen-search-select" >
                                                @foreach($city_list as $state => $city)
                                                @if($city->CT_id == $lead_detail->L_customerCity)
                                                <option value="{{ $city->CT_id }}" selected class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                @else
                                                <option value="{{ $city->CT_id }}" class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                @endif;
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="control-group">
                                            <label class="control-label">Zip</label>
                                            <input type="text" id="customer_zip" name="customer_zip" class="form-control uc" value="{{ $lead_detail->L_customerZip }}" maxlength="7" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mt-30">Insurance Detail</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Type</label>
                                            <?php 
                                            $otherPolicyType = config('constant.other_policy_type'); 
                                            $otherPolicyType = array_combine($otherPolicyType, $otherPolicyType);
                                            ?>
                                            {!! Form::select('policy_type', $otherPolicyType, $lead_detail->L_policyType, ['class' => 'form-control chosen-search-select']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Product</label>
                                            {!! Form::select('new_policy_product', $product_list, $lead_detail->otherInsurance->new_policy_product, ['class' => 'form-control chosen-search-select']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Proposer Type</label>
                                            <?php 
                                            $otherPolicyProposer = config('constant.other_policy_proposer'); 
                                            $otherPolicyProposer = array_combine($otherPolicyProposer, $otherPolicyProposer);
                                            ?>
                                            {!! Form::select('proposer_type', $otherPolicyProposer, $lead_detail->otherInsurance->proposer_type, ['class' => 'form-control chosen-search-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group">
                                            <label class="control-label">Insurance Company</label>
                                            {!! Form::select('new_policy_company', $company_list, $lead_detail->otherInsurance->new_policy_company, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Sum Insured</label>
                                            <input type="text" id="vehicle_sgst" name="sum_insured" class="form-control focus-select uc" value="{{ $lead_detail->otherInsurance->sum_insured }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Estimated Premium</label>
                                            <input type="text" id="vehicle_sgst" name="estimated_premium" class="form-control focus-select uc" value="{{ $lead_detail->otherInsurance->estimated_premium }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Net Premium</label>
                                            <input type="text" id="net_premium" name="net_premium" class="form-control focus-select uc" value="{{ $lead_detail->otherInsurance->net_premium }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">GST</label>
                                            <input type="text" id="gst" name="gst" class="form-control focus-select uc" value="{{ $lead_detail->otherInsurance->gst }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Gross Premium</label>
                                            <input type="text" id="gross_premium" name="gross_premium" class="form-control focus-select uc" value="{{ $lead_detail->otherInsurance->gross_premium }}" /><span style="font-weight:bold;color:red" id="infoTxt"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Policy Number</label>
                                            <input type="text" id="new_policy_number" name="new_policy_number" class="form-control uc" value="{{ $lead_detail->otherInsurance->new_policy_number }}" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Start Date</label>
                                            <input type="text" id="new_policy_start_date" name="new_policy_start_date" class="form-control" value="{!! ($lead_detail->otherInsurance->new_policy_start_date?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->otherInsurance->new_policy_start_date)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Expiry Date</label>
                                            <input type="text" id="new_policy_end_date" name="new_policy_end_date" class="form-control date-picker" value="{!! ($lead_detail->otherInsurance->new_policy_end_date?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->otherInsurance->new_policy_end_date)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('status', $lead_status_list, $lead_detail->otherInsurance->status, ['class' => 'form-control chosen-select', 'id' => 'lead_status']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 new-policy-attachment-container">
                                        <div class="control-group">
                                            <label class="control-label">New Policy</label>
                                            <div class="fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="new_policy_attachment" id="new_policy_attachment"></span>
                                                <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                            <a href="{!! url('policies-attachment', ['file' => $lead_detail->otherInsurance->policy_attachment]) !!}" target="_blank">{{ $lead_detail->otherInsurance->policy_attachment }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($lead_detail->L_completedAt)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Complete Date</label>
                                            <div class="text-strong lh-30">{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead_detail->L_completedAt)->format('d-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($lead_detail->L_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($lead_detail->L_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <button type="button" class="btn btn-theme btn-submit">Update</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div><!-- /.body-content -->
    <!--/ End body content -->

</section><!-- /#page-content -->
@endsection

@section('bottom')
<script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection
