@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
        .new-policy-attachment-container label.error{display: none !important;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-car"></i>Lead</h2>
            @if(check_permission('LEAD_CREATE', Auth::user()->A_type))
                <a href="{!! url('lead/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Lead</a>
            @endif
            <a href="{!! url('leads') !!}" class="btn btn-xs btn-theme pull-right">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Edit Lead</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('lead/update', ['id' => $lead_detail->L_id]), 'class' => 'form floating-label', 'method' => 'post', 'id' => 'lead_form', 'files' => true]) !!}
                        <div class="form-body p-20">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Agent</label>
                                                    <div class="text-strong lh-30">{!! $agent_list[$lead_detail->L_AGid] !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="mt-30">Customer Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">First Name</label>
                                                    <input type="text" id="customer_first_name" name="customer_first_name" class="form-control uc" value="{{ $lead_detail->L_customerFirstName }}" maxlength="50" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Last Name</label>
                                                    <input type="text" id="customer_last_name" name="customer_last_name" class="form-control uc" value="{{ $lead_detail->L_customerLastName }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Email</label>
                                                    <input type="text" id="customer_email" name="customer_email" class="form-control uc" value="{{ $lead_detail->L_customerEmail }}" maxlength="100" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Mobile</label>
                                                    <input type="text" id="customer_mobile" name="customer_mobile" class="form-control uc" value="{{ $lead_detail->L_customerMobile }}" maxlength="10" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Phone</label>
                                                    <input type="text" id="customer_phone" name="customer_phone" class="form-control uc" value="{{ $lead_detail->L_customerPhone }}" maxlength="12" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="control-group">
                                                    <label class="control-label">Address</label>
                                                    <input type="text" id="customer_address" name="customer_address" class="form-control uc" value="{{ $lead_detail->L_customerAddress }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="control-group">
                                                    <label class="control-label">State</label>
                                                    {!! Form::select('customer_state', $state_list, $lead_detail->L_customerState, ['class' => 'form-control chosen-search-select', 'id' => 'customer_state']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="control-group">
                                                    <label class="control-label">City</label>
                                                    <select id="city_source" class="hide">
                                                        @foreach($city_list as $state => $city)
                                                            <option value="{{ $city->CT_name }}" class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <select name="customer_city" id="customer_city" class="form-control chosen-search-select" data-city="{{ $lead_detail->L_customerCity }}">
                                                        <option value>Select</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="control-group">
                                                    <label class="control-label">Zip</label>
                                                    <input type="text" id="customer_zip" name="customer_zip" class="form-control uc" value="{{ $lead_detail->L_customerZip }}" maxlength="7" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="mt-30">Vehicle Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle Type</label>
                                                    {!! Form::select('vehicle_type', ['FOUR_WHEELER' => 'Four Wheeler', 'TWO_WHEELER' => 'Two Wheeler'], $lead_detail->L_vehicleType, ['class' => 'form-control chosen-select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Engine Number</label>
                                                    <input type="text" id="vehicle_engine_no" name="vehicle_engine_no" class="form-control uc" value="{{ $lead_detail->L_vehicleEngineNo }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Chassis Number</label>
                                                    <input type="text" id="vehicle_chassis_no" name="vehicle_chassis_no" class="form-control uc" value="{{ $lead_detail->L_vehicleChassisNo }}" maxlength="50" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Registration Number</label>
                                                    <input type="text" id="vehicle_reg_no" name="vehicle_reg_no" class="form-control uc" value="{{ $lead_detail->L_vehicleRegNo }}" maxlength="12" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            {{--<div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">DL</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_DLPhoto]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_DLPhoto]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>--}}
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">RC</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_RCPhoto]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_RCPhoto]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">PUC</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_PUCPhoto]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_PUCPhoto]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Policy</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_policyPhoto]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_policyPhoto]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 1</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto1]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto1]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 2</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto2]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto2]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 3</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto3]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto3]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">Vehicle 4</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto4]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_vehiclePhoto4]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="mt-30">Old Policy Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Company</label>
                                                    {!! Form::select('old_policy_company', $company_list, $lead_detail->L_oldPolicyCompany, ['class' => 'form-control chosen-search-select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Policy Number</label>
                                                    <input type="text" id="old_policy_number" name="old_policy_number" class="form-control uc" value="{{ $lead_detail->L_oldPolicyNumber }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            {{--<div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Start Date</label>
                                                    <input type="text" id="old_policy_start_date" name="old_policy_start_date" class="form-control date-picker" value="{!! ($lead_detail->L_oldPolicyStartDate?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_oldPolicyStartDate)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                                </div>
                                            </div>--}}
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Expiry Date</label>
                                                    <input type="text" id="old_policy_end_date" name="old_policy_end_date" class="form-control date-picker uc" value="{!! ($lead_detail->L_oldPolicyEndDate?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_oldPolicyEndDate)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">PYP 1</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_oldPolicyAttachment1]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_oldPolicyAttachment1]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-group">
                                                    <label class="control-label">PYP 2</label>
                                                    <div>
                                                        <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_oldPolicyAttachment2]) !!}" target="_blank">
                                                            <img src="{!! url('lead-attachment', ['file' => $lead_detail->L_oldPolicyAttachment2]) !!}" class="img-responsive" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="mt-30">New Policy Detail</h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Company</label>
                                                    {!! Form::select('new_policy_company', $company_list, $lead_detail->L_newPolicyCompany, ['class' => 'form-control chosen-search-select']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Product</label>
                                                    {!! Form::select('new_policy_product', $product_list, $lead_detail->L_newPolicyProduct, ['class' => 'form-control chosen-search-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Estimated Premium</label>
                                                    <input type="text" id="new_policy_estimated_premium" name="new_policy_estimated_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyEstimatedPremium }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Own Damage Premium</label>
                                                    <input type="text" id="new_policy_od_premium" name="new_policy_od_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyODPremium }}" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Third Party Premium</label>
                                                    <input type="text" id="new_policy_tp_premium" name="new_policy_tp_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyTPPremium }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Tax</label>
                                                    <input type="text" id="new_policy_tax_premium" name="new_policy_tax_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyTaxPremium }}" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Premium Amount</label>
                                                    <input type="text" id="new_policy_total_premium" name="new_policy_total_premium" class="form-control focus-select uc" value="{{ $lead_detail->L_newPolicyTotalPremium }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Policy Number</label>
                                                    <input type="text" id="new_policy_number" name="new_policy_number" class="form-control uc" value="{{ $lead_detail->L_newPolicyNumber }}" maxlength="50" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Start Date</label>
                                                    <input type="text" id="new_policy_start_date" name="new_policy_start_date" class="form-control" value="{!! ($lead_detail->L_newPolicyStartDate?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_newPolicyStartDate)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Expiry Date</label>
                                                    <input type="text" id="new_policy_end_date" name="new_policy_end_date" class="form-control date-picker" value="{!! ($lead_detail->L_newPolicyEndDate?Carbon\Carbon::createFromFormat('Y-m-d', $lead_detail->L_newPolicyEndDate)->format('d-m-Y'):'') !!}" autocomplete="off" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Status</label>
                                                    {!! Form::select('status', $lead_status_list, $lead_detail->L_status, ['class' => 'form-control chosen-select', 'id' => 'lead_status']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 new-policy-attachment-container" @if($lead_detail->L_status != config('constant.lead_status.COMPLETED.value')) style="display: none;" @endif>
                                                <div class="control-group">
                                                    <label class="control-label">New Policy</label>
                                                    <div class="fileinput-new input-group" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput" style="white-space: nowrap"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="new_policy_attachment" id="new_policy_attachment"></span>
                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                    <a href="{!! url('lead-attachment', ['file' => $lead_detail->L_newPolicyAttachment]) !!}" target="_blank">{{ $lead_detail->L_newPolicyAttachment }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($lead_detail->L_completedAt)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="control-group">
                                                        <label class="control-label">Complete Date</label>
                                                        <div class="text-strong lh-30">{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead_detail->L_completedAt)->format('d-M-Y h:i A') !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Create Date</label>
                                                    <div class="text-strong lh-30">{!! format_date($lead_detail->L_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="control-group">
                                                    <label class="control-label">Update Date</label>
                                                    <div class="text-strong lh-30">{!! format_date($lead_detail->L_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($lead_detail->L_location)
                                    <div class="col-lg-6">
                                        <iframe src="https://www.google.com/maps/embed/v1/place?q={{ $lead_detail->L_location }}&key=AIzaSyDlTOadACwWyMDCk2IOubjVwm_x5hN2yLY" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if((check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead_detail->L_status < config('constant.lead_status.COMPLETED.value')) || check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                            <div class="form-footer">
                                @if(check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead_detail->L_status < config('constant.lead_status.COMPLETED.value'))
                                    <button type="button" class="btn btn-theme btn-submit">Update</button>
                                @endif
                                @if(check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                                    <button type="button" data-target="{!! url('messages', ['lead_id' => $lead_detail->L_id]) !!}" class="btn btn-theme btn-view-messages">View Messages</button>
                                @endif
                            </div>
                        @endif
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection
