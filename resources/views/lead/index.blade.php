@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-car"></i>Leads</h2>
            @if(check_permission('LEAD_CREATE', Auth::user()->A_type))
                <a href="{!! url('lead/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Lead</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Leads</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="filter">
                        <form method="get" action="{{ url('leads') }}">
                            <div class="row">
                                <div class="col-lg-3">
                                    {!! Form::select('agent_id', $agent_list, $agent_id, ['class' => 'form-control chosen-search-select']) !!}
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" id="keyword" name="keyword" class="form-control" value="{{ $search_keyword }}" placeholder="Customer Mobile" />
                                </div>
                                <div class="col-lg-2">
                                    {!! Form::select('status', $lead_status_list, $status, ['class' => 'form-control chosen-select']) !!}
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-theme">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive mt-10">
                        <table id="table_leads" class="table table-striped table-theme table-middle">
                            <thead>
                                <tr role="row">
<!--                                    <th width="50" class="text-center no-sort">
                                        <div class="ckbox ckbox-default">
                                            <input id='trigger_checkboxes_state' class="trigger-checkboxes-state" type="checkbox" value="1" />
                                            <label for='trigger_checkboxes_state'></label>
                                        </div>
                                    </th>-->
                                    <!--<th>Policy Type</th>-->
                                    <th>Agent</th>
                                    <th>Customer</th>
                                    <th>Customer Mobile</th>
                                    <th>Date</th>
                                    <th width="100">Status</th>
                                    <th width="50">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($lead_list) > 0)
                                @foreach($lead_list as $index => $lead)
                                
                                <?php // echo "<pre>";print_r($lead);exit; ?>
                                    <tr role="row">
<!--                                        <td width="50" class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id='checkbox_{{ $index }}' class="trigger-checkbox-state" type="checkbox" value="1" />
                                                <label for='checkbox_{{ $index }}'></label>
                                            </div>
                                        </td>-->
                                        <!--<td><label class="label label-info">{!! config('constant.policy_type.'.$lead->L_policyType.'.caption') !!}</label></td>-->
                                        <td>{{ $lead->AG_companyName }}<br/>({{ $lead->AG_ImdCode }})</td>
                                        <td>{{ $lead->L_customerFirstName }} {{ $lead->L_customerLastName }}</td>
                                        <td>{{ $lead->L_customerMobile }}</td>
                                        <td>{!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead->L_createdAt)->format('d-M-Y h:i A') !!}</td>
                                        <td class="text-center">
                                            <?php if($lead->L_policyType == 'PEDAL_CYCLE') {
                                                $leadStatus = $lead->L_status;
                                            } elseif($lead->L_policyType == 'MOTOR') {
                                                $leadStatus = (!empty($lead->motorInsurance->status)?$lead->motorInsurance->status:1);
                                            } elseif($lead->L_policyType == 'HEALTH') {
                                                $leadStatus = (!empty($lead->healthInsurance->status)?$lead->healthInsurance->status:1);
//                                                $leadStatus = $lead->healthInsurance->status;
                                            } elseif($lead->L_policyType == 'TRAVEL') {
                                                $leadStatus = (!empty($lead->travelInsurance->status)?$lead->travelInsurance->status:1);
//                                                $leadStatus = $lead->healthInsurance->status;
                                            } else {
                                                $leadStatus = (!empty($lead->otherInsurance->status)?$lead->otherInsurance->status:1);
//                                                $leadStatus = $lead->otherInsurance->status;
                                            } ?>
                                            @if($leadStatus == config('constant.lead_status.NEW.value'))
                                                <label class="label label-warning">{!! config('constant.lead_status.NEW.caption') !!}</label>
                                            @elseif($leadStatus == config('constant.lead_status.PROCESSING.value'))
                                                <label class="label label-warning">{!! config('constant.lead_status.PROCESSING.caption') !!}</label>
                                            @elseif($leadStatus == config('constant.lead_status.CANCELLED.value'))
                                                <label class="label label-danger">{!! config('constant.lead_status.CANCELLED.caption') !!}</label>
                                            @elseif($leadStatus == config('constant.lead_status.REJECTED.value'))
                                                <label class="label label-danger">{!! config('constant.lead_status.REJECTED.caption') !!}</label>
                                            @elseif($leadStatus == config('constant.lead_status.COMPLETED.value'))
                                                <label class="label label-success">{!! config('constant.lead_status.COMPLETED.caption') !!}</label>
                                            @endif
                                        </td>
                                        @if(check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type) || (check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead->L_status < config('constant.lead_status.COMPLETED.value')) || check_permission('LEAD_VIEW', Auth::user()->A_type))
                                            <td class="text-center text-nowrap">
                                                <!--<a href="{!! url('ledger', ['lead_id' => $lead->L_id]) !!}" class="btn btn-xs btn-theme">Transactions</a>-->
                                                @if(check_permission('LEAD_MESSAGE_VIEW', Auth::user()->A_type) || check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
                                                    <button type="button" data-target="{!! url('messages', ['lead_id' => $lead->L_id]) !!}" class="btn btn-xs btn-theme btn-view-messages">View Messages</button>
                                                @endif
                                                <a href="{!! url('lead/edit', ['id' => $lead->L_id]) !!}" class="btn btn-xs btn-theme">
                                                    @if(check_permission('LEAD_EDIT', Auth::user()->A_type) && $lead->L_status < config('constant.lead_status.COMPLETED.value'))
                                                        Edit
                                                    @elseif(check_permission('LEAD_VIEW', Auth::user()->A_type))
                                                        View
                                                    @endif
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr role="row">
                                    <td colspan="7" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/lead.js"></script>
@endsection
