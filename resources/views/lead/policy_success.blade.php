<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/61051f2147.js"></script>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 id="message"><?=$message; ?></h1>
        </div>
    </div>
</div>
<style>
    html,
    body {
        height: 100%;
    }
    .container {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
<?php if($status == 'success') { ?>
<script>
    jQuery(document).ready(function() {
        $.ajax({
            url: '/api/send-policy-generation-request',
            type: 'POST',
            dataType: 'JSON',
            success: function(result) {
                var message = result.message;
                $('#message').text(message);
            },
            complete: function(result) {
                var message = result.message;
                $('#message').text(message);
            }
        });
    });
</script>
<?php } ?>