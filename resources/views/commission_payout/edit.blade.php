@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa text-strong">%</i>Commission Payout</h2>
            @if(check_permission('COMMISSION_PAYOUT_CREATE', Auth::user()->A_type))
                <a href="{!! url('commission-payout/add', ['agent_id' => $payout_detail->CP_AGid]) !!}" class="btn btn-xs btn-theme pull-right">Add New Payout</a>
            @endif
            <a href="{!! url('commission-payouts', ['agent_id' => $payout_detail->CP_AGid]) !!}" class="btn btn-xs btn-theme pull-right mr-10">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Edit Payout</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => url('commission-payout/update', ['id' => $payout_detail->CP_id]), 'class' => 'form floating-label', 'method' => 'post']) !!}
                        <div class="form-body p-20">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Agent</label>
                                            <div class="text-strong">{{ $agent_list[$payout_detail->CP_AGid] }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Company</label>
                                            <div class="text-strong">{{ $company_list[$payout_detail->CP_ICid] }}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Product</label>
                                            <div class="text-strong">{{ $product_list[$payout_detail->CP_IPid] }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Commission (%)</label>
                                            <input type="text" id="payout" name="payout" class="form-control" value="{{ $payout_detail->CP_payout }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('active', [1 => 'Active', 0 => 'Inactive'], $payout_detail->CP_active, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($payout_detail->CP_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($payout_detail->CP_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type))
                            <div class="form-footer">
                                <button type="submit" class="btn btn-theme">Update</button>
                            </div>
                        @endif
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
