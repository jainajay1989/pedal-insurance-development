@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa text-strong">%</i>Commission Payouts</h2>
            @if(check_permission('COMMISSION_PAYOUT_CREATE', Auth::user()->A_type))
                <a href="{!! url('commission-payout/add', ['agent_id' => $agent_id]) !!}" class="btn btn-xs btn-theme pull-right">Add New Payout</a><a href="{!! url('agents') !!}" class="btn btn-theme btn-xs mr-10 pull-right">Back</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Commission Payouts</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="filter">
                        <form method="get" action="{{ url('commission-payouts') }}">
                            {!! Form::hidden('agent_id', $agent_id) !!}
                            <div class="row">
                                <div class="col-lg-2">
                                    {!! Form::select('type', $policy_type_list, null, ['class' => 'form-control chosen-search-select']) !!}
                                </div>
                                <div class="col-lg-4">
                                    {!! Form::select('company_id', $company_list, $company_id, ['class' => 'form-control chosen-search-select']) !!}
                                </div>
                                <div class="col-lg-3">
                                    {!! Form::select('product_id', $product_list, $product_id, ['class' => 'form-control chosen-search-select']) !!}
                                </div>
                                <div class="col-lg-2">
                                    {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], $status, ['class' => 'form-control chosen-select']) !!}
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-theme">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive mt-10">
                        <table class="table table-striped table-theme table-middle">
                            <thead>
                            <tr role="row">
                                <th width="50" class="text-center no-sort">
                                    <div class="ckbox ckbox-default">
                                        <input id='trigger_checkboxes_state' class="trigger-checkboxes-state" type="checkbox" value="1" />
                                        <label for='trigger_checkboxes_state'></label>
                                    </div>
                                </th>
                                <th>Policy Type</th>
                                <th>Company</th>
                                <th>Product</th>
                                <th>Payout</th>
                                <th width="100">Status</th>
                                @if(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type) || check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type))
                                    <th width="50">Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($payout_list) > 0)
                                @foreach($payout_list as $index => $payout)
                                    <tr role="row">
                                        <td width="50" class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id='checkbox_{{ $index }}' class="trigger-checkbox-state" type="checkbox" value="1" />
                                                <label for='checkbox_{{ $index }}'></label>
                                            </div>
                                        </td>
                                        <td>{{ $payout->CP_policyType }}</td>
                                        <td>{{ $payout->CP_company }}</td>
                                        <td>{{ $payout->CP_product }}</td>
                                        <td>{{ $payout->CP_payout }}%</td>
                                        <td class="text-center">
                                            @if($payout->CP_active == 1)
                                                <label class="label label-success">Yes</label>
                                            @else
                                                <label class="label label-warning">No</label>
                                            @endif
                                        </td>
                                        @if(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type) || check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type))
                                            <td width="50" class="text-center">
                                                <a href="{!! url('commission-payout/edit', ['id' => $payout->CP_id]) !!}" class="btn btn-xs btn-theme">
                                                    @if(check_permission('COMMISSION_PAYOUT_EDIT', Auth::user()->A_type))
                                                        Edit
                                                    @elseif(check_permission('COMMISSION_PAYOUT_VIEW', Auth::user()->A_type))
                                                        View
                                                    @endif
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr role="row">
                                    <td colspan="4" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
