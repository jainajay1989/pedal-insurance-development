@extends('layouts.app')

@section('head')
    <link href="{!! config('app.assets_url') !!}css/pages/mail.css" rel="stylesheet" />
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-inbox"></i>Message</h2>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">

            <!-- Start mail page -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Star form compose mail -->
                    {!! Form::open(['url' => url('message/send-message'), 'class' => 'form-horizontal', 'method' => 'post']) !!}
                        {!! Form::hidden('lead_id', $lead_id) !!}
                        <div class="panel rounded shadow panel-danger">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h3 class="panel-title">View message</h3>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-sub-heading inner-all">
                                <div class="pull-left">
                                    <ul class="list-inline no-margin">
                                        <li>
                                            <a href="{!! url('inbox') !!}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                                        </li>
                                        <li class="hidden-xs">
                                            <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save Draft</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send Email</button>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-sub-heading -->
                            <div class="panel-body">
                                <div class="compose-mail">
                                    <div class="form-group">
                                        <label for="to" class="col-sm-2 control-label">To:</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" id="to" name="to" />
                                                <div class="input-group-btn">
                                                    <button type="button" data-toggle="collapse" data-target="#ccbcc" class="btn btn-default btn-sm">CC/BCC <span class="caret"></span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div id="ccbcc" class="collapse">
                                        <div class="form-group">
                                            <label for="Cc" class="col-sm-2 control-label">Cc:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control input-sm" id="Cc">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Bcc" class="col-sm-2 control-label">Bcc:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control input-sm" id="Bcc">
                                            </div>
                                        </div>
                                    </div><!-- /.collapse -->
                                    <div class="form-group">
                                        <label for="subject" class="col-sm-2 control-label">Subject:</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="subject" name="subject" class="form-control input-sm">
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <textarea name="message" id="compose-editor" class="form-control" rows="10" placeholder="Write your content here..."></textarea>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.compose-mail -->
                            </div><!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="pull-right">
                                    <button class="btn btn-danger btn-sm">Cancel</button>
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send Email</button>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                    {!! Form::close() !!}
                    <!--/ End form compose mail -->
                </div>
            </div><!-- /.row -->
            <!--/ End mail page -->

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
