@extends('layouts.app')

@section('head')
    <link href="{!! config('app.assets_url') !!}css/pages/mail.css" rel="stylesheet" />
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-inbox"></i>Messages</h2>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">

            <!-- Start mail page -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Star form compose mail -->
                    <form class="form-horizontal">
                        <div class="panel mail-wrapper rounded shadow">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h3 class="panel-title">View Message</h3>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-sub-heading inner-all">
                                <div class="pull-left">
                                    <h3 class="lead no-margin">{{ $message_detail->MG_subject }}</h3>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm tooltips" data-container="body" data-original-title="Print" type="button" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-print"></i> </button>
                                    <button class="btn btn-danger btn-sm tooltips" data-container="body" data-original-title="Trash" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-trash-o"></i></button>
                                    <a href="{!! url('message/compose', ['lead_id' => $message_detail->MG_Lid]) !!}" class="btn btn-success btn-sm"><i class="fa fa-reply"></i> Reply</a>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-sub-heading -->
                            <div class="panel-sub-heading inner-all">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-7">
                                        <img src="{!! config('app.assets_url') !!}images/default-user-image.png" width="35" alt="" class="img-circle" />
                                        <span>maildjavaui@gmail.com</span>
                                        to
                                        <strong>me</strong>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-5">
                                        <p class="pull-right"> {!! Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message_detail->MG_createdAt)->format('d-M-Y') !!}</p>
                                    </div>
                                </div>
                            </div><!-- /.panel-sub-heading -->
                            <div class="panel-body">
                                <div class="view-mail">
                                    <p>
                                        {{ $message_detail->MG_message }}
                                    </p>
                                </div><!-- /.view-mail -->
                            </div><!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="pull-right">
                                    <a href="{!! url('message/compose', ['lead_id' => $message_detail->MG_Lid]) !!}" class="btn btn-success btn-sm"><i class="fa fa-reply"></i> Reply</a>
                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i> Forward</button>
                                    <button class="btn btn-info btn-sm tooltips" data-container="body" data-original-title="Print" type="button" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-print"></i> </button>
                                    <button class="btn btn-danger btn-sm tooltips" data-container="body" data-original-title="Trash" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-trash-o"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                    </form>
                    <!--/ End form compose mail -->
                </div>
            </div><!-- /.row -->
            <!--/ End mail page -->

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
