<!-- Change Process modal -->
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Messages</h4>
        </div>
        <div class="modal-body no-padding" style="max-height: 460px;overflow: auto;">
            <div class="message-list">
                @if(count($message_list) > 0)
                    @foreach($message_list as $index => $message)
                        @if($message->MG_type == 'BTA')
                            <div class="media inner-all no-margin message" data-message_id="{{ $message->MG_id }}">
                                <div class="pull-right">
                                    <img src="{!! config('app.assets_url') !!}images/default-user-image.png" alt="{{ $message->MG_agent }}" width="60" class="img-circle" />
                                </div><!-- /.pull-left -->
                                <div class="media-body text-right">
                                    <span class="h4">Me</span>
                                    <small class="block">{{ $message->MG_message }}</small>
                                    <em class="text-xs text-muted">{!! format_date($message->MG_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</em>
                                </div><!-- /.media-body -->
                            </div>
                        @else
                            <div class="media inner-all no-margin message" data-message_id="{{ $message->MG_id }}">
                                <div class="pull-left">
                                    <img src="{!! config('app.assets_url') !!}images/default-user-image.png" alt="{{ $message->MG_agent }}" width="60" class="img-circle" />
                                </div><!-- /.pull-left -->
                                <div class="media-body">
                                    <span class="h4">{{ $message->MG_agent }}</span>
                                    <small class="block">{{ $message->MG_message }}</small>
                                    <em class="text-xs text-muted">{!! format_date($message->MG_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</em>
                                </div><!-- /.media-body -->
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="p-20 text-center text-light text-default h1">No Messages</div>
                @endif
            </div>
        </div>
        @if(check_permission('LEAD_MESSAGE_SEND', Auth::user()->A_type))
            <div class="modal-footer no-bg">
                {!! Form::open(['url' => url('message/send-message'), 'class' => 'form-horizontal', 'method' => 'post', 'data-refresh_url' => url('message/refresh-messages')]) !!}
                    {!! Form::hidden('lead_id', $lead_id, ['id' => 'lead_id']) !!}
                    <div class="form-group has-feedback no-margin">
                        <input class="form-control" type="text" id="message" name="message" placeholder="Type a message..." autocomplete="off" />
                        <button type="button" class="btn btn-theme btn-send-message fa fa-send form-control-feedback"></button>
                    </div>
                {!! Form::close() !!}
            </div>
        @endif
    </div>
    <!-- /.modal-content -->
</div>
<!-- / Change Process modal -->