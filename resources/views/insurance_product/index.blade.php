@extends('layouts.app')

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-medkit"></i>Insurance Products</h2>
            @if(check_permission('INSURANCE_PRODUCT_CREATE', Auth::user()->A_type))
                <a href="{!! url('insurance-product/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Product</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Insurance Products</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="filter">
                        <form method="get" action="{{ url('insurance-products') }}">
                            <div class="row">
                                <div class="col-lg-5">
                                    {!! Form::select('policy_type', $policy_type_list, $policy_type, ['class' => 'form-control chosen-search-select']) !!}
                                </div>
                                <div class="col-lg-2">
                                    {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], $status, ['class' => 'form-control chosen-select']) !!}
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-theme">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive mt-10">
                        <table class="table table-striped table-theme table-middle">
                            <thead>
                                <tr role="row">
                                    <th width="50" class="text-center no-sort">
                                        <div class="ckbox ckbox-default">
                                            <input id='trigger_checkboxes_state' class="trigger-checkboxes-state" type="checkbox" value="1" />
                                            <label for='trigger_checkboxes_state'></label>
                                        </div>
                                    </th>
                                    <th width="200">Policy Type</th>
                                    <th>Name</th>
                                    <th width="100">Status</th>
                                    @if(check_permission('INSURANCE_PRODUCT_EDIT', Auth::user()->A_type) || check_permission('INSURANCE_PRODUCT_VIEW', Auth::user()->A_type))
                                        <th width="50">Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($product_list) > 0)
                                @foreach($product_list as $index => $product)
                                    <tr role="row">
                                        <td width="50" class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id='checkbox_{{ $index }}' class="trigger-checkbox-state" type="checkbox" value="1" />
                                                <label for='checkbox_{{ $index }}'></label>
                                            </div>
                                        </td>
                                        <td>{!! config('constant.policy_type.'.$product->IP_policyType.'.caption') !!}</td>
                                        <td>{{ $product->IP_name }}</td>
                                        <td class="text-center">
                                            @if($product->IP_active == 1)
                                                <label class="label label-success">Yes</label>
                                            @else
                                                <label class="label label-warning">No</label>
                                            @endif
                                        </td>
                                        @if(check_permission('INSURANCE_PRODUCT_EDIT', Auth::user()->A_type) || check_permission('INSURANCE_PRODUCT_VIEW', Auth::user()->A_type))
                                            <td width="50" class="text-center">
                                                <a href="{!! url('insurance-product/edit', ['id' => $product->IP_id]) !!}" class="btn btn-xs btn-theme">
                                                    @if(check_permission('INSURANCE_PRODUCT_EDIT', Auth::user()->A_type))
                                                        Edit
                                                    @elseif(check_permission('INSURANCE_PRODUCT_VIEW', Auth::user()->A_type))
                                                        View
                                                    @endif
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr role="row">
                                    <td colspan="4" class="text-center">No Data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
@endsection
