@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-building"></i>Insurance Company</h2>
            @if(check_permission('INSURANCE_COMPANY_CREATE', Auth::user()->A_type))
                <a href="{!! url('insurance-company/add') !!}" class="btn btn-xs btn-theme pull-right">Add New Company</a>
            @endif
            <a href="{!! url('insurance-companies') !!}" class="btn btn-xs btn-theme pull-right mr-10">Back</a>
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Edit Insurance Company</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => '#', 'class' => 'form floating-label', 'method' => 'post', 'id' => 'insurance_company_form']) !!}
                        <div class="form-body p-20">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" id="name" name="name" class="form-control required uc" value="{{ $company_detail->IC_name }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('active', [1 => 'Active', 0 => 'Inactive'], $company_detail->IC_active, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($company_detail->IC_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($company_detail->IC_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(check_permission('INSURANCE_COMPANY_EDIT', Auth::user()->A_type))
                            <div class="form-footer">
                                <button type="button" class="btn btn-theme btn-submit" data-action="{!! url('insurance-company/update', ['id' => $company_detail->IC_id]) !!}" data-text="Updating...">Update</button>
                                <button type="button" class="btn btn-danger btn-delete" data-action="{!! url('insurance-company/delete', ['id' => $company_detail->IC_id]) !!}" data-text="Deleting...">Delete</button>
                            </div>
                        @endif
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/insurance-company.js"></script>
@endsection
