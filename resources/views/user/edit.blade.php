@extends('layouts.app')

@section('head')
    <style type="text/css">
        .form-control.uc{text-transform: uppercase;}
    </style>
@endsection

@section('page-content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2 class="pull-left"><i class="fa fa-user"></i>User</h2>
            @if(check_permission('USER_CREATE', Auth::user()->A_type))
                <a href="{!! url('user/add') !!}" class="btn btn-xs btn-theme pull-right">Add New User</a>
            @endif
            <div class="clearfix"></div>
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Edit User</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => '#', 'class' => 'form floating-label', 'method' => 'post', 'id' => 'user_form']) !!}
                        <div class="form-body p-20">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Type</label>
                                            {!! Form::select('type', $user_type_list, $user_detail->A_type, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" id="first_name" name="first_name" class="form-control required uc" value="{{ $user_detail->A_firstName }}" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" id="last_name" name="last_name" class="form-control required uc" value="{{ $user_detail->A_lastName }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Email</label>
                                            <input id="email" name="email" class="form-control required uc" type="text" value="{{ $user_detail->A_email }}" maxlength="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Username</label>
                                            <input id="username" name="username" class="form-control required" type="text" value="{{ $user_detail->A_username }}" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Password</label>
                                            <input id="password" name="password" class="form-control" type="password" maxlength="50" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Confirm Password</label>
                                            <input id="confirm_password" name="confirm_password" class="form-control" type="password" maxlength="50" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Phone</label>
                                            <input type="text" id="phone" name="phone" class="form-control uc" value="{{ $user_detail->A_phone }}" maxlength="12" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Mobile</label>
                                            <input type="text" id="mobile" name="mobile" class="form-control required uc" value="{{ $user_detail->A_mobile }}" maxlength="10" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="control-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" id="address" name="address" class="form-control uc" value="{{ $user_detail->A_address }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">State</label>
                                            {!! Form::select('state', $state_list, $user_detail->A_state, ['class' => 'form-control chosen-search-select', 'id' => 'state']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">City</label>
                                            <select id="city_source" class="hide">
                                                @foreach($city_list as $state => $city)
                                                    <option value="{{ $city->CT_name }}" class="{{ $city->CT_state }}">{{ $city->CT_name }}</option>
                                                @endforeach
                                            </select>
                                            <select name="city" id="city" class="form-control chosen-search-select" data-city="{{ $user_detail->A_city }}">
                                                <option value>Select</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Zip</label>
                                            <input type="text" id="zip" name="zip" class="form-control uc" value="{{ $user_detail->A_zip }}" maxlength="6" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="control-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('active', [1 => 'Active', 0 => 'Inactive'], $user_detail->A_active, ['class' => 'form-control chosen-select']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Create Date</label>
                                            <div class="text-strong lh-30">{!! format_date($user_detail->A_createdAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                            <label class="control-label">Update Date</label>
                                            <div class="text-strong lh-30">{!! format_date($user_detail->A_updatedAt, 'Y-m-d H:i:s', 'd-M-Y h:i A') !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(check_permission('USER_EDIT', Auth::user()->A_type))
                            <div class="form-footer">
                                <button type="button" class="btn btn-theme btn-submit" data-action="{!! url('user/update', ['id' => $user_detail->A_id]) !!}" data-text="Updating...">Update</button>
                                <!--<button type="button" class="btn btn-danger btn-delete" data-action="{!! url('user/delete', ['id' => $user_detail->A_id]) !!}" data-text="Deleting...">Delete</button>-->
                            </div>
                        @endif
                    {!! Form::close() !!}
                </div>
            </div>

        </div><!-- /.body-content -->
        <!--/ End body content -->

    </section><!-- /#page-content -->
@endsection

@section('bottom')
    <script src="{!! config('app.assets_url') !!}js/user.js"></script>
@endsection
