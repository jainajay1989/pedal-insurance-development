-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 10, 2018 at 08:28 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `A_id` int(11) NOT NULL,
  `A_type` int(1) DEFAULT NULL,
  `A_username` varchar(50) DEFAULT NULL,
  `A_password` varchar(100) DEFAULT NULL,
  `A_rememberToken` text,
  `A_firstName` varchar(50) DEFAULT NULL,
  `A_lastName` varchar(50) DEFAULT NULL,
  `A_email` varchar(100) DEFAULT NULL,
  `A_mobile` varchar(20) DEFAULT NULL,
  `A_phone` varchar(20) DEFAULT NULL,
  `A_address` text,
  `A_city` varchar(50) DEFAULT NULL,
  `A_state` varchar(50) DEFAULT NULL,
  `A_zip` varchar(20) DEFAULT NULL,
  `A_active` int(1) DEFAULT NULL,
  `A_createdAt` datetime DEFAULT NULL,
  `A_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`A_id`, `A_type`, `A_username`, `A_password`, `A_rememberToken`, `A_firstName`, `A_lastName`, `A_email`, `A_mobile`, `A_phone`, `A_address`, `A_city`, `A_state`, `A_zip`, `A_active`, `A_createdAt`, `A_updatedAt`) VALUES
(1, 1, 'admin', '$2y$10$ZQOq15pZBmCeo/NQQt5Nb.MgDyPm.OGwuztR9moCHXJmyyYz88wzK', 'XFwRWtDL2drajatTM3XE4sR7R5Ol1bAa6RV0w7Crx1gL3AfLLXqNw4A4WRfF', 'C', 'I', 'codeteam.test@gmail.com', '9999999', '999999', 'Address', 'City', 'State', '10001', 1, '2017-08-13 00:00:00', '2018-05-09 08:00:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent`
--

CREATE TABLE `tbl_agent` (
  `AG_id` int(11) NOT NULL,
  `AG_firstName` varchar(50) DEFAULT NULL,
  `AG_lastName` varchar(50) DEFAULT NULL,
  `AG_email` varchar(50) DEFAULT NULL,
  `AG_username` varchar(50) DEFAULT NULL,
  `AG_password` varchar(100) DEFAULT NULL,
  `AG_mobile` varchar(20) DEFAULT NULL,
  `AG_phone` varchar(20) DEFAULT NULL,
  `AG_address` text,
  `AG_city` varchar(50) DEFAULT NULL,
  `AG_state` varchar(50) DEFAULT NULL,
  `AG_zip` varchar(20) DEFAULT NULL,
  `AG_active` int(1) DEFAULT NULL,
  `AG_createdAt` datetime DEFAULT NULL,
  `AG_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_agent`
--

INSERT INTO `tbl_agent` (`AG_id`, `AG_firstName`, `AG_lastName`, `AG_email`, `AG_username`, `AG_password`, `AG_mobile`, `AG_phone`, `AG_address`, `AG_city`, `AG_state`, `AG_zip`, `AG_active`, `AG_createdAt`, `AG_updatedAt`) VALUES
(1, 'Fname', 'Lname', 'test@test.com', 'agent', '$2y$10$ZQOq15pZBmCeo/NQQt5Nb.MgDyPm.OGwuztR9moCHXJmyyYz88wzK', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 1, '2018-05-08 12:03:16', '2018-05-08 12:03:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commissionpayout`
--

CREATE TABLE `tbl_commissionpayout` (
  `CP_id` int(11) NOT NULL,
  `CP_ICid` int(11) DEFAULT NULL,
  `CP_IPid` int(11) DEFAULT NULL,
  `CP_payout` decimal(5,2) DEFAULT NULL,
  `CP_active` int(1) DEFAULT NULL,
  `CP_createdAt` datetime DEFAULT NULL,
  `CP_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_commissionpayout`
--

INSERT INTO `tbl_commissionpayout` (`CP_id`, `CP_ICid`, `CP_IPid`, `CP_payout`, `CP_active`, `CP_createdAt`, `CP_updatedAt`) VALUES
(1, 1, 1, '19.00', 1, NULL, '2018-05-09 09:33:20'),
(2, 2, 2, '20.00', 1, '2018-05-09 09:33:52', '2018-05-09 09:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `CN_name` varchar(50) NOT NULL,
  `CN_value` text,
  `CN_createdAt` datetime DEFAULT NULL,
  `CN_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_config`
--

INSERT INTO `tbl_config` (`CN_name`, `CN_value`, `CN_createdAt`, `CN_updatedAt`) VALUES
('GENERAL_SETTING', '{\"CN_companyName\":\"Car Care\",\"CN_timezone\":\"Asia\\/Calcutta\",\"CN_dateFormat\":\"d-M-Y\",\"CN_inputDateFormat\":\"Input Date Format\"}', '2017-07-01 00:00:00', '2017-07-29 11:31:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_insurancecompany`
--

CREATE TABLE `tbl_insurancecompany` (
  `IC_id` int(11) NOT NULL,
  `IC_name` varchar(100) DEFAULT NULL,
  `IC_active` int(1) DEFAULT NULL,
  `IC_createdAt` datetime DEFAULT NULL,
  `IC_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_insurancecompany`
--

INSERT INTO `tbl_insurancecompany` (`IC_id`, `IC_name`, `IC_active`, `IC_createdAt`, `IC_updatedAt`) VALUES
(1, 'Bajaj Allianz General Insurance Co. Ltd.', 1, NULL, NULL),
(2, 'ICICI Lombard General Insurance Co. Ltd. ', 1, NULL, NULL),
(3, 'IFFCO Tokio General Insurance Co. Ltd.', 1, NULL, NULL),
(4, 'National Insurance Co. Ltd.', 1, NULL, NULL),
(5, 'The New India Assurance Co. Ltd.', 1, NULL, NULL),
(6, 'The Oriental Insurance Co. Ltd.', 1, NULL, NULL),
(7, 'United India Insurance Co. Ltd.', 1, NULL, NULL),
(8, 'Reliance General Insurance Co. Ltd.', 1, NULL, NULL),
(9, 'Royal Sundaram General Insurance Co. Limited', 1, NULL, NULL),
(10, ' Tata AIG General Insurance Co. Ltd.', 1, NULL, NULL),
(11, 'Cholamandalam MS General Insurance Co. Ltd.', 1, NULL, NULL),
(12, 'HDFC ERGO General Insurance Co. Ltd.', 1, NULL, NULL),
(13, 'Export Credit Guarantee Corporation of India Ltd.', 1, NULL, NULL),
(14, 'Agriculture Insurance Co. of India Ltd.', 1, NULL, NULL),
(15, 'Star Health and Allied Insurance Company Limited', 1, NULL, NULL),
(16, 'Apollo Munich Health Insurance Company Limited', 1, NULL, NULL),
(17, 'Future Generali India Insurance Company Limited', 1, NULL, NULL),
(18, 'Universal Sompo General Insurance Co. Ltd. ', 1, NULL, NULL),
(19, 'Shriram General Insurance Company Limited,', 1, NULL, NULL),
(20, 'Bharti AXA General Insurance Company Limited', 1, NULL, NULL),
(21, 'Raheja QBE General Insurance Company Limited,', 1, NULL, NULL),
(22, 'SBI General Insurance Company Limited', 1, NULL, NULL),
(23, 'Max Bupa Health Insurance Company Ltd.', 1, NULL, NULL),
(24, 'Religare Health Insurance Company Limited', 1, NULL, NULL),
(25, 'Magma HDI General Insurance Company Limited', 1, NULL, NULL),
(26, 'Liberty Videocon General Insurance Company Limited, ', 1, NULL, NULL),
(27, 'Cigna TTK Health Insurance Company Ltd. ', 1, NULL, NULL),
(28, 'Kotak Mahindra General Insurance Company Limited', 1, NULL, NULL),
(29, 'Aditya Birla Health Insurance Co. Limited', 1, NULL, NULL),
(30, 'DHFL General Insurance Limited', 1, NULL, NULL),
(31, 'Acko General Insurance Limited', 1, NULL, NULL),
(32, 'Go Digit General Insurance Limited', 1, NULL, NULL),
(33, 'Edelweiss General Insurance Company Limited ', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_insuranceproduct`
--

CREATE TABLE `tbl_insuranceproduct` (
  `IP_id` int(11) NOT NULL,
  `IP_name` varchar(100) DEFAULT NULL,
  `IP_active` int(1) DEFAULT NULL,
  `IP_createdAt` datetime DEFAULT NULL,
  `IP_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_insuranceproduct`
--

INSERT INTO `tbl_insuranceproduct` (`IP_id`, `IP_name`, `IP_active`, `IP_createdAt`, `IP_updatedAt`) VALUES
(1, 'Car', 1, NULL, NULL),
(2, '2wheeler', 1, NULL, NULL),
(3, 'Car TP', 1, NULL, NULL),
(4, '2W TP', 1, NULL, NULL),
(5, 'Fire', 1, NULL, NULL),
(6, 'Marine', 1, NULL, NULL),
(7, 'Retail Health', 1, NULL, NULL),
(8, 'GMC', 1, NULL, NULL),
(9, 'GPA', 1, NULL, NULL),
(10, 'Liability', 1, NULL, NULL),
(11, 'WC', 1, NULL, NULL),
(12, 'PA', 1, NULL, NULL),
(13, 'DNO', 1, NULL, NULL),
(14, 'P Indemnity', 1, NULL, NULL),
(15, 'CGL', 1, NULL, NULL),
(16, 'CV', 1, NULL, NULL),
(17, 'CV TP', 1, NULL, NULL),
(18, 'Credit', 1, NULL, NULL),
(19, 'Money', 1, NULL, NULL),
(20, 'Home Package', 1, NULL, NULL),
(21, 'Industrial Package', 1, NULL, NULL),
(22, 'Commercial Package', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lead`
--

CREATE TABLE `tbl_lead` (
  `L_id` int(11) NOT NULL,
  `L_AGid` int(11) DEFAULT NULL,
  `L_ICid` int(11) DEFAULT NULL,
  `L_IPid` int(11) DEFAULT NULL,
  `L_customerFirstName` varchar(50) DEFAULT NULL,
  `L_customerLastName` varchar(50) DEFAULT NULL,
  `L_customerEmail` varchar(100) DEFAULT NULL,
  `L_customerMobile` varchar(20) DEFAULT NULL,
  `L_customerPhone` varchar(20) DEFAULT NULL,
  `L_customerAddress` text,
  `L_customerCity` varchar(50) DEFAULT NULL,
  `L_customerState` varchar(50) DEFAULT NULL,
  `L_customerZip` varchar(20) DEFAULT NULL,
  `L_vehicleType` varchar(20) DEFAULT NULL,
  `L_vehicleYear` int(4) DEFAULT NULL,
  `L_vehicleEngineNo` varchar(100) DEFAULT NULL,
  `L_vehicleChassisNo` varchar(100) DEFAULT NULL,
  `L_vehicleRegNo` varchar(50) DEFAULT NULL,
  `L_vehicleOldPolicy` varchar(50) DEFAULT NULL,
  `L_estimatedPremium` decimal(10,2) DEFAULT NULL,
  `L_premium` decimal(10,2) DEFAULT NULL,
  `L_status` int(1) DEFAULT NULL,
  `L_createdAt` datetime DEFAULT NULL,
  `L_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_lead`
--

INSERT INTO `tbl_lead` (`L_id`, `L_AGid`, `L_ICid`, `L_IPid`, `L_customerFirstName`, `L_customerLastName`, `L_customerEmail`, `L_customerMobile`, `L_customerPhone`, `L_customerAddress`, `L_customerCity`, `L_customerState`, `L_customerZip`, `L_vehicleType`, `L_vehicleYear`, `L_vehicleEngineNo`, `L_vehicleChassisNo`, `L_vehicleRegNo`, `L_vehicleOldPolicy`, `L_estimatedPremium`, `L_premium`, `L_status`, `L_createdAt`, `L_updatedAt`) VALUES
(1, 1, 2, 4, 'Fname', 'Lname', 'test@test.com', '294783', '293478', 'Address', 'City', 'State', 'Zip', 'Type', NULL, 'Eno', 'Cno', 'Rno', '928347923', '2198393.00', '29783492.00', 1, '2018-05-09 11:43:06', '2018-05-09 11:43:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ledger`
--

CREATE TABLE `tbl_ledger` (
  `LG_id` int(11) NOT NULL,
  `LG_Lid` int(11) DEFAULT NULL,
  `LG_AGid` int(11) DEFAULT NULL,
  `LG_type` varchar(20) DEFAULT NULL,
  `LG_amount` decimal(10,2) DEFAULT NULL,
  `LG_attachemnt` varchar(20) DEFAULT NULL,
  `LG_status` int(1) DEFAULT NULL,
  `LG_createdAt` datetime DEFAULT NULL,
  `LG_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ledger`
--

INSERT INTO `tbl_ledger` (`LG_id`, `LG_Lid`, `LG_AGid`, `LG_type`, `LG_amount`, `LG_attachemnt`, `LG_status`, `LG_createdAt`, `LG_updatedAt`) VALUES
(1, NULL, 1, 'PAYMENT', '1000.00', NULL, NULL, '2018-05-10 12:13:20', '2018-05-10 12:13:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `MG_id` int(11) NOT NULL,
  `MG_Lid` int(11) DEFAULT NULL,
  `MG_type` varchar(20) DEFAULT NULL,
  `MG_from` int(11) DEFAULT NULL,
  `MG_to` int(11) DEFAULT NULL,
  `MG_subject` varchar(200) DEFAULT NULL,
  `MG_message` text,
  `MG_read` int(1) DEFAULT NULL,
  `MG_createdAt` datetime DEFAULT NULL,
  `MG_updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`MG_id`, `MG_Lid`, `MG_type`, `MG_from`, `MG_to`, `MG_subject`, `MG_message`, `MG_read`, `MG_createdAt`, `MG_updatedAt`) VALUES
(1, NULL, 'ATB', 1, 1, 'Test subject', 'I was impressed how fast the content is loaded. Congratulations. nice design.', 0, '2018-05-10 00:00:00', '2018-05-10 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`A_id`);

--
-- Indexes for table `tbl_agent`
--
ALTER TABLE `tbl_agent`
  ADD PRIMARY KEY (`AG_id`);

--
-- Indexes for table `tbl_commissionpayout`
--
ALTER TABLE `tbl_commissionpayout`
  ADD PRIMARY KEY (`CP_id`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`CN_name`),
  ADD UNIQUE KEY `CN_name` (`CN_name`);

--
-- Indexes for table `tbl_insurancecompany`
--
ALTER TABLE `tbl_insurancecompany`
  ADD PRIMARY KEY (`IC_id`);

--
-- Indexes for table `tbl_insuranceproduct`
--
ALTER TABLE `tbl_insuranceproduct`
  ADD PRIMARY KEY (`IP_id`);

--
-- Indexes for table `tbl_lead`
--
ALTER TABLE `tbl_lead`
  ADD PRIMARY KEY (`L_id`);

--
-- Indexes for table `tbl_ledger`
--
ALTER TABLE `tbl_ledger`
  ADD PRIMARY KEY (`LG_id`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
  ADD PRIMARY KEY (`MG_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `A_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_agent`
--
ALTER TABLE `tbl_agent`
  MODIFY `AG_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_commissionpayout`
--
ALTER TABLE `tbl_commissionpayout`
  MODIFY `CP_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_insurancecompany`
--
ALTER TABLE `tbl_insurancecompany`
  MODIFY `IC_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_insuranceproduct`
--
ALTER TABLE `tbl_insuranceproduct`
  MODIFY `IP_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_lead`
--
ALTER TABLE `tbl_lead`
  MODIFY `L_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_ledger`
--
ALTER TABLE `tbl_ledger`
  MODIFY `LG_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
  MODIFY `MG_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
