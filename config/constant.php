<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Admin User Type Constant
    |--------------------------------------------------------------------------
    |
    */
    'user_type'  => ['ADMIN'   	    =>  ['value' => 'ADMIN',    'caption' => 'ADMIN'],
                     'MANAGER'      =>  ['value' => 'MANAGER',  'caption' => 'MANAGER'],
                     'EXECUTIVE'    =>  ['value' => 'EXECUTIVE','caption' => 'EXECUTIVE']],

    /*
    |--------------------------------------------------------------------------
    | Agent Type Constant
    |--------------------------------------------------------------------------
    |
    */
    'agent_type'  => ['AGENT'   	=>  ['value' => 'AGENT',    'caption' => 'AGENT'],
                      'DEALER'      =>  ['value' => 'DEALER',   'caption' => 'DEALER']],


    /*
    |--------------------------------------------------------------------------
    | Policy Type Constant
    |--------------------------------------------------------------------------
    |
    */
    'policy_type'  => ['FIRE'   	    =>  ['value' => 'FIRE',             'caption' => 'FIRE'],
                       'MOTOR'          =>  ['value' => 'MOTOR',            'caption' => 'MOTOR'],
                       'TRAVEL'         =>  ['value' => 'TRAVEL',           'caption' => 'TRAVEL'],
                       'HEALTH'         =>  ['value' => 'HEALTH',           'caption' => 'HEALTH'],
                       'MARINE'         =>  ['value' => 'MARINE',           'caption' => 'MARINE'],
                       'PEDAL_CYCLE'    =>  ['value' => 'PEDAL_CYCLE',      'caption' => 'PEDAL CYCLE'],
                       'MISCELLANEOUS'  =>  ['value' => 'MISCELLANEOUS',    'caption' => 'MISCELLANEOUS']],
    
    /*
    |--------------------------------------------------------------------------
    | Other Policy Type Constant
    |--------------------------------------------------------------------------
    |
    */
    'other_policy_type'  => [
        'FIRE',
        'MARINE',
        'MISCELLANEOUS'
    ],

    
    /*
    |--------------------------------------------------------------------------
    | Other Policy Proposer Constant
    |--------------------------------------------------------------------------
    |
    */
    'other_policy_proposer'  => [
        'Individual',
        'Corporate'
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Policy Relationship Constant
    |--------------------------------------------------------------------------
    |
    */
    'policy_relationship'  => [
        'Self',
        'Spouse',
        'Children',
        'Self-Spouse',
        'Self-Spouse-Children',
        'Spouse-Children',
        'Self-Children'
    ],
    
        /*
    |--------------------------------------------------------------------------
    | Policy Relationship Constant
    |--------------------------------------------------------------------------
    |
    */
    'travel_policy_type'  => [
        'Individual',
        'Family Policy',
        'Student Policy'
    ],

    /*
    |--------------------------------------------------------------------------
    | Message Type Constant
    |--------------------------------------------------------------------------
    |
    */
    'message_type'  => ['BTA'   =>  ['value' => 'BTA',  'caption' => 'BROKER TO AGENT'],
                        'ATB'   =>  ['value' => 'ATB',  'caption' => 'AGENT TO BROKER']],

    /*
    |--------------------------------------------------------------------------
    | Transaction Type Constant
    |--------------------------------------------------------------------------
    |
    */
    'transaction_type'  => ['RECHARGE'      =>  ['value' => 'RECHARGE',     'caption' => 'RECHARGE',     'message' => 'PAYMENT DEPOSITED'],
                            'WITHDRAW'      =>  ['value' => 'WITHDRAW',     'caption' => 'WITHDRAWAL',   'message' => 'BALANCE WITHDRAWN'],
                            'PAIDFORPOLICY' =>  ['value' => 'PAIDFORPOLICY','caption' => 'PAID',         'message' => 'PAID FOR POLICY'],
                            'COMMISSION'    =>  ['value' => 'COMMISSION',   'caption' => 'EARNINGS',     'message' => 'COMMISSION EARNED']],

    /*
    |--------------------------------------------------------------------------
    | Transaction Status Constant
    |--------------------------------------------------------------------------
    |
    */
    'transaction_status'  => ['PENDING'         =>  ['value' => 1,    'caption' => 'PENDING'],
                              'PAYMENTRELEASED' =>  ['value' => 2,    'caption' => 'PAYMENT RELEASED'],

                              'REJECTED'        =>  ['value' => 11,   'caption' => 'REJECTED'],

                              'APPROVED'        =>  ['value' => 21,   'caption' => 'APPROVED']],

    /*
    |--------------------------------------------------------------------------
    | Lead Status Constant
    |--------------------------------------------------------------------------
    |
    */
    'lead_status'  => ['NEW'        =>  ['value' => 1,     'caption' => 'PENDING'],
                       'REJECTED'   =>  ['value' => 11,    'caption' => 'REJECTED'],
                       'COMPLETED'  =>  ['value' => 21,    'caption' => 'COMPLETED']],
    
    'lead_status_api'  => [['id' => 1,     'caption' => 'PENDING'],
                       ['id' => 11,    'caption' => 'REJECTED'],
                       ['id' => 21,    'caption' => 'COMPLETED']],
    
    'lead_policy_status'  => [  1   => 'PENDING',
                                11  => 'REJECTED',
                                21  => 'COMPLETED'
                            ],

    /*
    |--------------------------------------------------------------------------
    | Default Commission Percentage
    |--------------------------------------------------------------------------
    |
    */
    'commission_payout'  => ['DEFAULT'  =>  ['value' => 15,      'caption' => 'DEFAULT COMMISSION']],
    
    /*
    |--------------------------------------------------------------------------
    | Payu details
    |--------------------------------------------------------------------------
    |
    */
    'payu_money' => [
        'merchant_key' => 'D98T9I',
        'salt' => '1Zm4bX5k',
    ],
    
    'BASE_URL' => 'http://pedal.dev.com:9010/api/',
    
    /*
    |--------------------------------------------------------------------------
    | Nominee relationship in case of PA cover
    |--------------------------------------------------------------------------
    |
    */
    
    'nominee_relations' => [
        'Self', 'Spouse', 'Dependent Son', 'Dependent Daughter', 'Mother', 'Father', 'Mother-in-law', 'Father-in-law', 'Brother', 'Sister', 'Others'
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Calculation Config
    |--------------------------------------------------------------------------
    |
    */
    
    'calculation_config' => [
        'VEHICLE_CGST'  => (float)6,
        'VEHICLE_SGST'  => (float)6,
        'VEHICLE_IGST' => (float)12,
        'SUM_INSURED_PER'   => (float)95,
        'CGST_PREMIUM' => (float)9,
        'SGST_PREMIUM' => (float)9,
        'IGST_PREMIUM' => (float)18,
        'MIN_PURCHASE_PRICE' => (float)4000,
        'MAX_PURCHASE_PRICE' => (float)250000,
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Policy Tenure
    |--------------------------------------------------------------------------
    |
    */
    
    'policy_tenure' => [
        1,
//        2,
        3
        ],
    
    /*
    |--------------------------------------------------------------------------
    | Liberty Policy Fixed Parameter
    |--------------------------------------------------------------------------
    |
    */
    'TPEmailID'     => 'care@milbinsure.com',
    'TPSourceName'  =>  'MILB',
    'PaymentSource' =>  'LGI-PAYU',
    'BiCycleType'   =>  'ROAD',
    'Purchased'     =>  'New',
    'BicycleBodyType'  =>  'Others',
    'BusinessType'  =>  'New',
    'TPSource'  =>  'MILBINSURE',
    'FrameType'     =>  'Others',
   
    /*
    |--------------------------------------------------------------------------
    | Liberty Policy Policy Issue Location
    |--------------------------------------------------------------------------
    |
    */
    'INUSRANCE_COMPANY_STATE' => 'MH',  //Used to define SGST OR IGST
    'API_ENDPOINT' => "https://api.libertyinsurance.in", //Live SERVER
//    'API_ENDPOINT' => "https://uatservices.libertyvideocon.com", //UAT SERVER
    
    'BANK_ACCOUNT_TYPE' => [
        'CURRENT' => 'CURRENT',
        'SAVING' => 'SAVING'
    ],
    
    'PAYMENT_MODE_TYPE' => [
        'NEFT' => 'NEFT'
    ],
    
    'APP_VERSION' => 1,
    'APP_URL' => "http://ec2-13-232-103-15.ap-south-1.compute.amazonaws.com/android/pedal.apk",
    'DEALER_PAYMENT_SUCCESS_URL' => 'http://ec2-13-232-103-15.ap-south-1.compute.amazonaws.com/api/thank-you?status=success',
    'DEALER_PAYMENT_FAILURE_URL' => 'http://ec2-13-232-103-15.ap-south-1.compute.amazonaws.com/api/thank-you?status=error',
    'COUNTRY_ID_INDIA' => 113,
    'MIS_REPORT_EMAILS' => [
        'James.George@libertyinsurance.in',
        'Achint.saini@libertyinsurance.in',
        'Sunil.Bamm@libertyinsurance.in',
        'Nishita.lad@libertyinsurance.in',
        'Karthikeyan.iyer@libertyinsurance.in',
        'Vikas.Kelaskar@libertyinsurance.in',
        'dipanka.saikia@milbinsure.com',
        'suraj.grover@milbinsure.com',
        'anmol.verma@milbinsure.com'
    ],
    'MIS_REPORT_EMAILS_BCC' => [
        'shailendra.kulria@milbinsure.com',
        'jitendra.girdhar@milbinsure.com',
        'engineering@fondostech.in'
    ],
    'RSA_MIN_AMOUNT'=> 20001,
    
    'SPECIAL_MIN_PURCHASE_PRICE' => (float)7500, //Used for some special IMD dealers
    'SPECIAL_MAX_PURCHASE_PRICE' => (float)700000, //Used for some special IMD dealers
    'SPECIAL_IMDCODE'=>[
        'IMD1102071'
    ],
    'VEHICLE_TYPE'=>[
        ['id'=>1, 'name'=>'2 Wheeler'],
        ['id'=>2, 'name'=>'Pvt Car'],
        ['id'=>3, 'name'=>'MiscD'],
        ['id'=>4, 'name'=>'COMMERCIAL']
    ],
    'PAID_BY_WHOM'=>[
        ''=>'Select Payer', 
        'PAID_BY_DEALER'=>'Paid By Dealer', 
        'PAID_BY_CUSTOMER'=>'Paid By Customer',
        'PAID_BY_CHEQUE'=>'Paid By Cheque'
    ]
];