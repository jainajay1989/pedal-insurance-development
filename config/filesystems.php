<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'lead' => [
            'driver' => 'local',
            'root' => storage_path('app/public/lead'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'motorinsurance' => [
            'driver' => 'local',
            'root' => storage_path('app/public/lead/motor_insurance'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'healthinsurance' => [
            'driver' => 'local',
            'root' => storage_path('app/public/lead/health_insurance'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
               
        'health_insurance_purpose' => [
            'driver' => 'local',
            'root' => storage_path('app/public/lead/health_insurance/purposal_forms'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'quotes' => [
            'driver' => 'local',
            'root' => storage_path('app/public/lead/quotes'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'models_list' => [
            'driver' => 'local',
            'root' => storage_path('app/public/vehicle/uploaded'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'models_upload_error' => [
            'driver' => 'local',
            'root' => storage_path('app/public/vehicle/error'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'policies' => [
            'driver' => 'local',
            'root' => storage_path('app/public/lead/policies'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'ledger' => [
            'driver' => 'local',
            'root' => storage_path('app/public/ledger'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
    ],

];
